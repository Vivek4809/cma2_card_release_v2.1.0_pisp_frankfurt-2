package com.capgemini.psd2.pisp.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.capgemini.psd2"})
public class PaymentSubmissionRetrieveITApplication {

	static ConfigurableApplicationContext context = null;
	public static void main(String[] args) {
		context = SpringApplication.run(PaymentSubmissionRetrieveITApplication.class, args);
	}

}
