package com.capgemini.psd2.model;

import java.util.ArrayList;

public class Authorisation {
	private String member_state;

	public String getMember_state() {
		return this.member_state;
	}

	public void setMember_state(String member_state) {
		this.member_state = member_state;
	}

	private ArrayList<String> roles;

	public ArrayList<String> getRoles() {
		return this.roles;
	}

	public void setRoles(ArrayList<String> roles) {
		this.roles = roles;
	}
}