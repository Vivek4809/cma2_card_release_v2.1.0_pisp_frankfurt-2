package com.capgemini.psd2.service;

import java.util.ArrayList;

import com.capgemini.psd2.model.Authorisation;
import com.capgemini.psd2.model.OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations;

public class PassportingMockData {

	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA1()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(true);
		a1.setMemberState("GB");
		a1.setPsd2Role("PISP");
		return a1;
	}
	
	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA2()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(true);
		a1.setMemberState("GB");
		a1.setPsd2Role("AISP");
		return a1;
	}
	
	
	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA3()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(true);
		a1.setMemberState("IR");
		a1.setPsd2Role("AISP");
		return a1;
	}
	
	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA4()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(false);
		a1.setMemberState("IR");
		a1.setPsd2Role("PISP");
		return a1;
	}
	
	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA5()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(true);
		a1.setMemberState("NR");
		a1.setPsd2Role("AISP");
		return a1;
	}
	public static Authorisation getAuthorisation1() {
		
		Authorisation C1=new Authorisation();
		C1.setMember_state("GB");
		ArrayList<String> roles=new ArrayList<>();
		roles.add("AISP");
		roles.add("PISP");
		//roles.add("CISP");
		C1.setRoles(roles);
		return C1;
	}
	
	public static Authorisation getAuthorisation2() {
		
		Authorisation C1=new Authorisation();
		C1.setMember_state("IR");
		ArrayList<String> roles=new ArrayList<>();
		roles.add("AISP");
		roles.add("PISP");
		C1.setRoles(roles);
		
		return C1;
		
	}
	
public static Authorisation getAuthorisation3() {
		
		Authorisation C1=new Authorisation();
		C1.setMember_state("NR");
		ArrayList<String> roles=new ArrayList<>();
		roles.add("AISP");
		roles.add("PISP");
		C1.setRoles(roles);
		
		return C1;
		
	}
	
}
