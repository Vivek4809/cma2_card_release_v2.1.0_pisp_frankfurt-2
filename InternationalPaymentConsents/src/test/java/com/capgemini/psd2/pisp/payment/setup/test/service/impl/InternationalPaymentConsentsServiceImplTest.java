package com.capgemini.psd2.pisp.payment.setup.test.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.payment.setup.comparator.InternationalPaymentSetupPayloadComparator;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.payment.setup.service.impl.InternationalPaymentConsentsServiceImpl;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentConsentProcessingAdapter;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalPaymentStagingAdapter;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentConsentsServiceImplTest {

	@Mock
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private InternationalPaymentSetupPayloadComparator iPaymentConsentsComparator;

	@Mock
	private InternationalPaymentStagingAdapter iPaymentStagingAdapter;

	@Mock
	private PaymentConsentProcessingAdapter paymentRequestAdapter;

	@InjectMocks
	private InternationalPaymentConsentsServiceImpl IPaymentsserviceimpl;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void creattest_withNULLpaymentId() {

		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setPaymentConsentId(null);
		paymentConsentsPlatformResource.setCreatedAt("22-11-2018");

		Mockito.when(paymentRequestAdapter.preConsentProcessFlows(anyObject(), anyObject()))
				.thenReturn(paymentConsentsPlatformResource);

		CustomIPaymentConsentsPOSTRequest internationalPaymentConsentsRequest = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data1 = new OBWriteDataInternationalConsent1();
		OBInternational1 initiation1 = new OBInternational1();
		OBCashAccountDebtor3 debtorAccount1 = new OBCashAccountDebtor3();
		debtorAccount1.setName("Debtor");
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		initiation1.setRemittanceInformation(remittanceInformation);
		initiation1.setDebtorAccount(debtorAccount1);
		data1.setInitiation(initiation1);
		internationalPaymentConsentsRequest.setData(data1);
		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<>();
		String e = "Ground Floor";
		addressLine.add(e);
		deliveryAddress.setAddressLine(addressLine);
		internationalPaymentConsentsRequest.setRisk(risk);

		CustomIPaymentConsentsPOSTResponse customIPaymentConsentsPOSTResponse = new CustomIPaymentConsentsPOSTResponse();
		OBWriteDataInternationalConsentResponse1 data = new OBWriteDataInternationalConsentResponse1();
		customIPaymentConsentsPOSTResponse.setData(data);
		data.setStatus(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION);
		data.setConsentId("1234");
		customIPaymentConsentsPOSTResponse.setData(data);
		OBInternational1 initiation = new OBInternational1();
		initiation.setRemittanceInformation(remittanceInformation);
		initiation.setCurrencyOfTransfer("currency of  tranfer");
		customIPaymentConsentsPOSTResponse.setRisk(risk);
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);
		debtorAccount.setName("Debtor");

		Mockito.when(iPaymentStagingAdapter.processInternationalPaymentConsents(anyObject(), anyObject(), anyMap(),
				Matchers.any(), Matchers.any())).thenReturn(customIPaymentConsentsPOSTResponse);

		IPaymentsserviceimpl.createInternationalPaymentConsentsResource(internationalPaymentConsentsRequest);
		assertEquals("1234", data.getConsentId());
	}

	@Test
	public void creattest_withpaymentId() {

		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setPaymentConsentId("12345");
		paymentConsentsPlatformResource.setCreatedAt("22-11-2018");

		Mockito.when(paymentRequestAdapter.preConsentProcessFlows(anyObject(), anyObject()))
				.thenReturn(paymentConsentsPlatformResource);

		CustomIPaymentConsentsPOSTResponse customIPaymentConsentsPOSTResponse = new CustomIPaymentConsentsPOSTResponse();
		OBWriteDataInternationalConsentResponse1 data = new OBWriteDataInternationalConsentResponse1();
		customIPaymentConsentsPOSTResponse.setData(data);
		data.setStatus(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION);
		data.setConsentId("12345");

		Mockito.when(iPaymentStagingAdapter.processInternationalPaymentConsents(anyObject(), anyObject(), anyMap(),
				Matchers.any(), Matchers.any())).thenReturn(customIPaymentConsentsPOSTResponse);

		CustomIPaymentConsentsPOSTRequest internationalPaymentConsentsRequest = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 requestData = new OBWriteDataInternationalConsent1();
		OBInternational1 initiation1 = new OBInternational1();
		OBCashAccountDebtor3 debtorAccount1 = new OBCashAccountDebtor3();
		debtorAccount1.setName("Debtor");
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		initiation1.setRemittanceInformation(remittanceInformation);
		initiation1.setDebtorAccount(debtorAccount1);
		requestData.setInitiation(initiation1);
		internationalPaymentConsentsRequest.setData(requestData);
		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<>();
		String e = "Ground Floor";
		addressLine.add(e);
		deliveryAddress.setAddressLine(addressLine);
		internationalPaymentConsentsRequest.setRisk(risk);

		CustomIPaymentConsentsPOSTResponse value = new CustomIPaymentConsentsPOSTResponse();
		OBWriteDataInternationalConsentResponse1 data1 = new OBWriteDataInternationalConsentResponse1();
		data1.setConsentId("12345");
		value.setData(data1);
		OBInternational1 initiation = new OBInternational1();
		data1.setInitiation(initiation);
		initiation1.setRemittanceInformation(remittanceInformation);
		value.setRisk(risk);
		initiation.setCurrencyOfTransfer("currency of  tranfer");
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		initiation.setDebtorAccount(debtorAccount);
		debtorAccount.setName("Debitor");
		Mockito.when(iPaymentStagingAdapter.retrieveStagedInternationalPaymentConsents(anyObject(), Matchers.any()))
				.thenReturn(value);

		Mockito.when(iPaymentConsentsComparator.comparePaymentDetails(anyObject(), anyObject(), anyObject()))
				.thenReturn(0);

		IPaymentsserviceimpl.createInternationalPaymentConsentsResource(internationalPaymentConsentsRequest);
		assertEquals("12345", data.getConsentId());
	}

	@Test
	public void retrieve_test() {

		PaymentConsentsPlatformResource value1 = new PaymentConsentsPlatformResource();
		value1.setSetupCmaVersion("3.0");
		Mockito.when(paymentRequestAdapter.preConsentProcessGETFlow(anyObject())).thenReturn(value1);

		CustomIPaymentConsentsPOSTResponse value = new CustomIPaymentConsentsPOSTResponse();
		OBWriteDataInternationalConsentResponse1 data1 = new OBWriteDataInternationalConsentResponse1();
		data1.setConsentId("12345");
		value.setData(data1);
		Mockito.when(iPaymentStagingAdapter.retrieveStagedInternationalPaymentConsents(anyObject(), Matchers.any()))
				.thenReturn(value);

		CustomIPaymentConsentsPOSTResponse response = new CustomIPaymentConsentsPOSTResponse();
		Mockito.when(
				paymentRequestAdapter.postConsentProcessFlows(Matchers.any(), anyObject(), anyObject(), Matchers.any()))
				.thenReturn((CustomIPaymentConsentsPOSTResponse) response);

		PaymentRetrieveGetRequest request = new PaymentRetrieveGetRequest();
		request.setConsentId("12345");
		IPaymentsserviceimpl.retrieveInternationalPaymentConsentsResource(request);

	}

}
