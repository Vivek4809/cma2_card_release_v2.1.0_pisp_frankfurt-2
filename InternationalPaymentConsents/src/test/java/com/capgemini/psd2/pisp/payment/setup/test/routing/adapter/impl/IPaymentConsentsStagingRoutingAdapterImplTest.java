package com.capgemini.psd2.pisp.payment.setup.test.routing.adapter.impl;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.impl.IPaymentConsentsStagingRoutingAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.routing.IPaymentConsentsAdapterFactory;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalPaymentStagingAdapter;
import com.capgemini.psd2.token.Token;

@RunWith(SpringJUnit4ClassRunner.class)
public class IPaymentConsentsStagingRoutingAdapterImplTest {

	@InjectMocks
	private IPaymentConsentsStagingRoutingAdapterImpl adapter;

	@Mock
	private IPaymentConsentsAdapterFactory factory;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void createStaging_Test() {
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("consentid");
		customStageIdentifiers.setPaymentSetupVersion("3.0");
		PaymentTypeEnum paymentTypeEnum = null;
		customStageIdentifiers.setPaymentTypeEnum(paymentTypeEnum.INTERNATIONAL_PAY);

		Map<String, String> params = new HashMap<>();
		params.put("header attribute", "123");

		CustomIPaymentConsentsPOSTRequest customIPaymentConsentsPOSTResponse = new CustomIPaymentConsentsPOSTRequest();

		InternationalPaymentStagingAdapter value = new InternationalPaymentStagingAdapter() {

			@Override
			public CustomIPaymentConsentsPOSTResponse retrieveStagedInternationalPaymentConsents(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public CustomIPaymentConsentsPOSTResponse processInternationalPaymentConsents(
					CustomIPaymentConsentsPOSTRequest internationalPaymentConsentsRequest,
					CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
					OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		Mockito.when(factory.getInternationalPaymentSetupStagingInstance(anyString())).thenReturn(value);
		adapter.processInternationalPaymentConsents(customIPaymentConsentsPOSTResponse, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AUTHORISED, OBExternalConsentStatus1Code.REJECTED);
	}
	
	@Test
	public void createStaging_Test_ParamNull() {
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("consentid");
		customStageIdentifiers.setPaymentSetupVersion("3.0");
		PaymentTypeEnum paymentTypeEnum = null;
		customStageIdentifiers.setPaymentTypeEnum(paymentTypeEnum.INTERNATIONAL_PAY);

		Map<String, String> params = null;
		
		CustomIPaymentConsentsPOSTRequest customIPaymentConsentsPOSTResponse = new CustomIPaymentConsentsPOSTRequest();

		InternationalPaymentStagingAdapter value = new InternationalPaymentStagingAdapter() {

			@Override
			public CustomIPaymentConsentsPOSTResponse retrieveStagedInternationalPaymentConsents(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public CustomIPaymentConsentsPOSTResponse processInternationalPaymentConsents(
					CustomIPaymentConsentsPOSTRequest internationalPaymentConsentsRequest,
					CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
					OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		Mockito.when(factory.getInternationalPaymentSetupStagingInstance(anyString())).thenReturn(value);
		adapter.processInternationalPaymentConsents(customIPaymentConsentsPOSTResponse, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AUTHORISED, OBExternalConsentStatus1Code.REJECTED);
	}
	
	@Test
	public void createStaging_Test_TokenNotNull() {
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("consentid");
		customStageIdentifiers.setPaymentSetupVersion("3.0");
		PaymentTypeEnum paymentTypeEnum = null;
		customStageIdentifiers.setPaymentTypeEnum(paymentTypeEnum.INTERNATIONAL_PAY);

		Map<String, String> params = null;
		
		when(reqHeaderAtrributes.getToken()).thenReturn(new Token());

		CustomIPaymentConsentsPOSTRequest customIPaymentConsentsPOSTResponse = new CustomIPaymentConsentsPOSTRequest();

		InternationalPaymentStagingAdapter value = new InternationalPaymentStagingAdapter() {

			@Override
			public CustomIPaymentConsentsPOSTResponse retrieveStagedInternationalPaymentConsents(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public CustomIPaymentConsentsPOSTResponse processInternationalPaymentConsents(
					CustomIPaymentConsentsPOSTRequest internationalPaymentConsentsRequest,
					CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
					OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		Mockito.when(factory.getInternationalPaymentSetupStagingInstance(anyString())).thenReturn(value);
		adapter.processInternationalPaymentConsents(customIPaymentConsentsPOSTResponse, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AUTHORISED, OBExternalConsentStatus1Code.REJECTED);
	}

	@Test
	public void retrieveTest() {

		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("consentid");
		customStageIdentifiers.setPaymentSetupVersion("3.0");
		PaymentTypeEnum paymentTypeEnum = null;
		customStageIdentifiers.setPaymentTypeEnum(paymentTypeEnum.INTERNATIONAL_PAY);

		Map<String, String> params = new HashMap<>();
		params.put("header attribute", "123");

		InternationalPaymentStagingAdapter value = new InternationalPaymentStagingAdapter() {

			@Override
			public CustomIPaymentConsentsPOSTResponse retrieveStagedInternationalPaymentConsents(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public CustomIPaymentConsentsPOSTResponse processInternationalPaymentConsents(
					CustomIPaymentConsentsPOSTRequest internationalPaymentConsentsRequest,
					CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
					OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		Mockito.when(factory.getInternationalPaymentSetupStagingInstance(anyString())).thenReturn(value);
		adapter.retrieveStagedInternationalPaymentConsents(customStageIdentifiers, params);

	}

}
