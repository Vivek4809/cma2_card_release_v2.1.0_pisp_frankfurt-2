package com.capgemini.psd2.pisp.payment.setup.test.transformer.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.payment.setup.transformer.impl.InternationalPaymentConsentsResponseTransformerImpl;
import com.capgemini.psd2.pisp.status.PaymentStatusCompatibilityMapEnum;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;
import com.capgemini.psd2.pisp.validation.adapter.constants.PaymentSetupConstants;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentConsentsResponseTransformerImplTest {

	@InjectMocks
	InternationalPaymentConsentsResponseTransformerImpl transformer;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private PispDateUtility pispDateUtility;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testInternationalPaymentConsentsResponseTransformerImpl() {
		CustomIPaymentConsentsPOSTResponse response = new CustomIPaymentConsentsPOSTResponse();
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();

		OBWriteDataInternationalConsentResponse1 data = new OBWriteDataInternationalConsentResponse1();
		response.setData(data);
		data.setConsentId("12345");
		response.getData().setCreationDateTime("2017-06-05T15:15:13+00:00");
		resource.setStatus("ACCEPTEDTECHNICALVALIDATION");
		resource.setPaymentConsentId("1234");
		resource.setStatusUpdateDateTime("2017-06-05T15:18:13+00:00");
		resource.setCreatedAt("2:27");
		resource.setUpdatedAt("3:53");
		resource.getUpdatedAt();

		OBRisk1 risk = new OBRisk1();
		response.setRisk(risk);
		PaymentStatusCompatibilityMapEnum status = null;

		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		initiation.setInstructionIdentification("BOI");

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("2018");
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setSchemeName("scheme");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("2019");
		initiation.setDebtorAccount(debtorAccount);
		debtorAccount.setSchemeName("schemeName");

		PaymentSetupPOSTResponseLinks links = new PaymentSetupPOSTResponseLinks();
		response.setLinks(links);

		PaymentSetupPOSTResponseMeta meta = new PaymentSetupPOSTResponseMeta();
		meta.setTotalPages(100);
		meta.getTotalPages();
		Mockito.when(reqHeaderAtrributes.getSelfUrl()).thenReturn("/testSelfUrl");

		CustomIPaymentConsentsPOSTResponse iResponse = transformer.paymentConsentsResponseTransformer(response,
				resource, "GET");
		assertEquals(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, iResponse.getData().getStatus());
		assertEquals("2017-06-05T15:18:13+00:00", iResponse.getData().getStatusUpdateDateTime());
	}

	@Test
	public void testInternationalPaymentConsentsResponseTransformerImplNull() {
		CustomIPaymentConsentsPOSTResponse response = new CustomIPaymentConsentsPOSTResponse();
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();

		OBWriteDataInternationalConsentResponse1 data = new OBWriteDataInternationalConsentResponse1();
		response.setData(data);
		data.setConsentId("12345");
		resource.setStatus("ACCEPTEDTECHNICALVALIDATION");
		resource.setPaymentConsentId("1234");
		resource.setTppDebtorDetails("false");
		resource.setTppDebtorNameDetails("false");

		OBRisk1 risk = new OBRisk1();
		response.setRisk(risk);
		PaymentStatusCompatibilityMapEnum status = null;

		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		initiation.setInstructionIdentification("BOI");

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setSchemeName(null);

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName(null);

		transformer.paymentConsentsResponseTransformer(response, resource, "GET");
	}

	@Test
	public void testInternationalPaymentConsentsResponseTransformerImplConstants() {
		CustomIPaymentConsentsPOSTResponse response = new CustomIPaymentConsentsPOSTResponse();
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();

		OBWriteDataInternationalConsentResponse1 data = new OBWriteDataInternationalConsentResponse1();
		response.setData(data);
		data.setConsentId("2311");
		response.getData().setCreationDateTime("2017-06-05T15:15:13+00:00");
		resource.setStatus("ACCEPTEDTECHNICALVALIDATION");
		resource.setPaymentConsentId("1234");
		resource.setStatusUpdateDateTime("2017-06-05T15:18:13+00:00");
		resource.setCreatedAt("2:27");

		OBRisk1 risk = new OBRisk1();
		response.setRisk(risk);
		PaymentStatusCompatibilityMapEnum status = null;

		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		initiation.setInstructionIdentification("BOI");

		PaymentSetupConstants.schemeNameList.add("schemeName");
		PaymentSetupConstants.schemeNameList.add("scheme");

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("2018");
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setSchemeName("scheme");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("2019");
		initiation.setDebtorAccount(debtorAccount);
		debtorAccount.setSchemeName("schemeName");

		PaymentSetupPOSTResponseLinks links = new PaymentSetupPOSTResponseLinks();
		links.setFirst("FirstLink");
		response.setLinks(links);

		PaymentSetupPOSTResponseMeta meta = new PaymentSetupPOSTResponseMeta();
		meta.setTotalPages(100);
		meta.getTotalPages();
		Mockito.when(reqHeaderAtrributes.getSelfUrl()).thenReturn("/testSelfUrl");

		CustomIPaymentConsentsPOSTResponse iResponse = transformer.paymentConsentsResponseTransformer(response,
				resource, "GET");
		assertEquals(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, iResponse.getData().getStatus());
		assertEquals("2017-06-05T15:18:13+00:00", iResponse.getData().getStatusUpdateDateTime());
	}

	@Test
	public void testInternationalPaymentConsentsResponseTransformerImplWithTppDebtorDetails() {
		CustomIPaymentConsentsPOSTResponse response = new CustomIPaymentConsentsPOSTResponse();
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();

		OBWriteDataInternationalConsentResponse1 data = new OBWriteDataInternationalConsentResponse1();
		response.setData(data);
		data.setConsentId("2311");
		response.getData().setCreationDateTime("2017-06-05T15:15:13+00:00");
		resource.setStatus("ACCEPTEDTECHNICALVALIDATION");
		resource.setPaymentConsentId("1234");
		resource.setStatusUpdateDateTime("2017-06-05T15:18:13+00:00");
		resource.setCreatedAt("2:27");
		resource.setTppDebtorDetails(null);
		resource.setTppDebtorNameDetails(null);

		OBRisk1 risk = new OBRisk1();
		response.setRisk(risk);
		PaymentStatusCompatibilityMapEnum status = null;

		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.28");
		initiation.setInstructionIdentification("BOI");

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setIdentification("NWBK60161331926819");
		creditorAccount.setName("Test user");
		creditorAccount.setSecondaryIdentification("2018");
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setSchemeName("scheme");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setIdentification("GB29NWBK60161331926819");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSecondaryIdentification("2019");
		initiation.setDebtorAccount(debtorAccount);
		debtorAccount.setSchemeName("schemeName");

		PaymentSetupPOSTResponseLinks links = null;
		response.setLinks(new PaymentSetupPOSTResponseLinks());

		PaymentSetupPOSTResponseMeta meta = null;
		response.setMeta(new PaymentSetupPOSTResponseMeta());

		CustomIPaymentConsentsPOSTResponse iResponse = transformer.paymentConsentsResponseTransformer(response,
				resource, "GET");
		assertEquals(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, iResponse.getData().getStatus());
		assertEquals("2017-06-05T15:18:13+00:00", iResponse.getData().getStatusUpdateDateTime());
	}

	@Test
	public void testpaymentConsentRequestTransformer() {
		CustomIPaymentConsentsPOSTRequest request = new CustomIPaymentConsentsPOSTRequest();
		request.setData(new OBWriteDataInternationalConsent1());
		request.getData().setAuthorisation(new OBAuthorisation1());
		request.getData().getAuthorisation().setCompletionDateTime("2019-12-31T00:00:00+00:00");

		when(pispDateUtility.transformDateTimeInRequest(anyString())).thenReturn("2019-12-31T00:00:00+00:00");
		transformer.paymentConsentRequestTransformer(request);
	}

	@After
	public void tearDown() throws Exception {
		transformer = null;
		reqHeaderAtrributes = null;
	}

}