package com.capgemini.psd2.pisp.payments.consent.test.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticStandingOrderConsent1;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.payments.consent.test.mockdata.DStandingOrderConsentMockData;
import com.capgemini.psd2.pisp.payments.consents.controller.DomesticStandingOrderController;
import com.capgemini.psd2.pisp.payments.consents.service.DomesticStandingOrderConsentsService;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticStandingOrderControllerTest {
	
	private MockMvc mockMvc;

	@InjectMocks
	private DomesticStandingOrderController dStandingOrderConsentController;
	
	@Mock
	private DomesticStandingOrderConsentsService dStandingOrderConsentService;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(dStandingOrderConsentController).dispatchOptions(true).build();
		Map<String, String> map = new HashMap<>();
		map.put("HEADER", "header error message");
		map.put("FREQUENCY", "freq ma");
		map.put("AFTER_CUTOFF_DATE", "freq ma");
		map.put("FIELD", "freq ma");
		map.put("SIGNATURE", "freq ma");
		map.put("INCOMPATIBLE", "freq ma");
		map.put("RES_NOTFOUND", "freq ma");
		map.put("INTERNAL", "freq ma");

		Map<String, String> map1 = new HashMap<>();
		map1.put("signature_missing", "drtyrty");
		
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(map1);
	}
	
	@Test
	public void createDSOConsentResource() throws Exception {
		OBWriteDomesticStandingOrderConsent1 domesticStandingOrderRequest = new OBWriteDomesticStandingOrderConsent1();
		CustomDStandingOrderConsentsPOSTResponse customDomesticStandingOrderPOSTRespone = new CustomDStandingOrderConsentsPOSTResponse();
		
		Mockito.when(dStandingOrderConsentService.createDomesticStandingOrderConsentResource(any())).thenReturn(customDomesticStandingOrderPOSTRespone);
		
		this.mockMvc.perform(post("/domestic-standing-order-consents").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(DStandingOrderConsentMockData.asJsonString(domesticStandingOrderRequest)))
		.andExpect(status().isCreated());
		
		dStandingOrderConsentController.createDomesticStandingSetupResource(domesticStandingOrderRequest);
	}
	
	@Test(expected=PSD2Exception.class)
	public void createDSOConsentResource_Exception() throws Exception {
		OBWriteDomesticStandingOrderConsent1 domesticStandingOrderRequest = new OBWriteDomesticStandingOrderConsent1();
		
		ReflectionTestUtils.setField(dStandingOrderConsentController, "responseErrorMessageEnabled", "true");
		Mockito.when(dStandingOrderConsentService.createDomesticStandingOrderConsentResource(any())).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_DEBITOR_AGENT_IDENTIFICATION_INVALID));	
		
		try {
			this.mockMvc.perform(post("/domestic-standing-order-consents").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
					.content(DStandingOrderConsentMockData.asJsonString(domesticStandingOrderRequest)))
			.andExpect(status().isBadRequest());
		} catch (Exception e) {
			assertEquals(false, e.getMessage().contains("In Valid HTTP Request"));
		}
		
		dStandingOrderConsentController.createDomesticStandingSetupResource(domesticStandingOrderRequest);
	}
	
	@Test
	public void retrieveDSOConsentResource() throws Exception {
		PaymentRetrieveGetRequest paymentRetrieveRequest = new PaymentRetrieveGetRequest();
		paymentRetrieveRequest.setConsentId("12345");
		CustomDStandingOrderConsentsPOSTResponse customDomesticStandingOrderPOSTRespone = new CustomDStandingOrderConsentsPOSTResponse();
		
		Mockito.when(dStandingOrderConsentService.retrieveDomesticStandingOrderConsentsResource(any())).thenReturn(customDomesticStandingOrderPOSTRespone);
		
		this.mockMvc.perform(get("/domestic-standing-order-consents/{ConsentId}",12345).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(DStandingOrderConsentMockData.asJsonString(paymentRetrieveRequest)))
		.andExpect(status().isOk());
		
		dStandingOrderConsentController.retrieveDomesticStandingOrderConsentResource(paymentRetrieveRequest);
	}
	
	@Test(expected=PSD2Exception.class)
	public void retrieveDSOConsentResource_BadRequest() {
		PaymentRetrieveGetRequest paymentRetrieveRequest = new PaymentRetrieveGetRequest();
		paymentRetrieveRequest.setConsentId("12345");
		ReflectionTestUtils.setField(dStandingOrderConsentController, "responseErrorMessageEnabled", "true");
		
		Mockito.when(dStandingOrderConsentService.retrieveDomesticStandingOrderConsentsResource(any())).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_DEBITOR_AGENT_IDENTIFICATION_INVALID));
		
		try {
			this.mockMvc.perform(get("/domestic-standing-order-consents/{ConsentId}",12345).contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
					.content(DStandingOrderConsentMockData.asJsonString(paymentRetrieveRequest)))
			.andExpect(status().isBadRequest());
		} catch (Exception e) {
			assertEquals(false, e.getMessage().contains("In Valid HTTP Request/DomesticConsentId"));
		}
		
		dStandingOrderConsentController.retrieveDomesticStandingOrderConsentResource(paymentRetrieveRequest);
	}
}
		