package com.capgemini.psd2.config.server.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.eclipse.jgit.api.FetchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.TransportConfigCallback;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig.Host;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.transport.Transport;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.utilities.DateUtilites;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;

@RestController
@ConfigurationProperties
@EnableConfigurationProperties
@RequestMapping("/config")
public class ConfigSyncUtilityController {

	@Autowired
	private DiscoveryClient discoveryClient;

	@Autowired
	private RestTemplate restTemplate;

	@Value("${app.appRepoPath}")
	private String appRepoPath;

	@Value("${app.uiRepoPath}")
	private String uiRepoPath;

	@Value("${app.passPhrase}")
	private String passPhrase;

	@Value("${app.apiConnectionTimeOut:#{30000}}")
	private String connectionTimeOut;

	@Value("${app.apiReadTimeOut:#{90000}}")
	private String readTimeOut;

	@Value("${springAppName}")
	private String[] applicationName;

	private Map<String, String> uiMappings;

	/**
	 * @return the uiMappings
	 */
	public Map<String, String> getUiMappings() {
		return uiMappings;
	}

	/**
	 * @param uiMappings
	 *            the uiMappings to set
	 */
	public void setUiMappings(Map<String, String> uiMappings) {
		this.uiMappings = uiMappings;
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigSyncUtilityApplication.class);

	@RequestMapping(value = "/ui/update" , method= RequestMethod.POST)
	public void runUiUpdate() {
		LOGGER.info("{\"Enter\":\"{}\",\"timestamp\":\"{}\"}",
				"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.runUiUpdate()",
				DateUtilites.generateCurrentTimeStamp());
		List<DiffEntry> list = gitDiffAndSync(uiRepoPath);
		if (list != null) {
			Iterator<DiffEntry> itr = list.iterator();
			while (itr.hasNext()) {
				DiffEntry temp = itr.next();
				String changeType = temp.getChangeType().name();
				if (changeType.equalsIgnoreCase("MODIFY") || changeType.equalsIgnoreCase("ADD")) {
					String configName = temp.getNewPath().split("-")[0];
					String envName = temp.getNewPath().split("-")[1];
					ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
					try {

						String servId = uiMappings.get(configName);
						discoveryClient.getInstances(servId).forEach((ServiceInstance s) -> {
							try {
								String url = null;
								if (!Arrays.asList(applicationName).contains(servId)) {
									url = s.getUri() + "/ui/staticContentUpdate";
								} else {
									Map appName = mapper.readValue(new File(appRepoPath + "/" + servId + "-" + envName), Map.class);
									String contextPath = (String) ((Map) appName.get("server")).get("context-path");
									url = s.getUri() + contextPath + "/ui/staticContentUpdate";
								}
								buildSSLRequest();
								ResponseEntity<String> restVal = restTemplate.exchange(url, HttpMethod.GET, null,
										String.class);
								LOGGER.info("{\"Exit\":\"{}\",\"timestamp\":\"{}\"}",
										"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.runUiUpdate()",
										DateUtilites.generateCurrentTimeStamp());
							} catch (Exception e) {
								LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"ErrorDetails\":{}}",
										"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.runUiUpdate()",
										DateUtilites.generateCurrentTimeStamp(), e.getMessage());
								if (LOGGER.isDebugEnabled())
									LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"ErrorDetails\":{}}",
											"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.runUiUpdate()",
											DateUtilites.generateCurrentTimeStamp(), e.getStackTrace().toString());
							}
						});
					} catch (Exception e) {
						LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"ErrorDetails\":{}}",
								"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.runUiUpdate()",
								DateUtilites.generateCurrentTimeStamp(), e.getMessage());
						if (LOGGER.isDebugEnabled())
							LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"ErrorDetails\":{}}",
									"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.runUiUpdate()",
									DateUtilites.generateCurrentTimeStamp(), e.getStackTrace().toString());
					}
				}
			}
		}
	}

	/*
	 * @PostConstruct public void init() throws Exception{
	 * 
	 * }
	 */
	@RequestMapping(value = "/app/restart", method= RequestMethod.POST)
	public void runAppRestart() {
		LOGGER.info("{\"Enter\":\"{}\",\"timestamp\":\"{}\"}",
				"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.runAppRestart()",
				DateUtilites.generateCurrentTimeStamp());
		List<DiffEntry> list = gitDiffAndSync(appRepoPath);
		if (list != null) {
			Iterator<DiffEntry> itr = list.iterator();
			while (itr.hasNext()) {
				DiffEntry temp = itr.next();
				String changeType = temp.getChangeType().name();
				if (changeType.equalsIgnoreCase("MODIFY") || changeType.equalsIgnoreCase("ADD")) {
					String servId = temp.getNewPath().split("-")[0];
					discoveryClient.getInstances(servId).forEach((ServiceInstance s) -> {
						try {
							String url;
							if (!Arrays.asList(applicationName).contains(servId)) {
								url = s.getUri() + "/restart";
							} else {
								ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
								String contextPath = null;
								try {
									Map appName = mapper.readValue(new File(appRepoPath + "/" + temp.getNewPath()),
											Map.class);
									contextPath = (String) ((Map) appName.get("server")).get("context-path");

								} catch (Exception e) {
									LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"ErrorDetails\":{}}",
											"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.runAppRestart()",
											DateUtilites.generateCurrentTimeStamp(), e.getMessage());
									if (LOGGER.isDebugEnabled())
										LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"ErrorDetails\":{}}",
												"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.runAppRestart()",
												DateUtilites.generateCurrentTimeStamp(), e.getStackTrace().toString());
								}
								url = s.getUri() + contextPath + "/restart";
							}
							buildSSLRequest();
							ResponseEntity<String> restVal = restTemplate.exchange(url, HttpMethod.POST, null,
									String.class);
							LOGGER.info("{\"Exit\":\"{}\",\"timestamp\":\"{}\"}",
									"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.runAppRestart()",
									DateUtilites.generateCurrentTimeStamp());
						} catch (Exception e) {
							LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"ErrorDetails\":{}}",
									"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.runAppRestart()",
									DateUtilites.generateCurrentTimeStamp(), e.getMessage());
							if (LOGGER.isDebugEnabled())
								LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"ErrorDetails\":{}}",
										"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.runAppRestart()",
										DateUtilites.generateCurrentTimeStamp(), e.getStackTrace().toString());
						}
					});
				}
			}
		}
	}

	private KeyStore populateTrustStore() throws Exception {
		LOGGER.info("{\"Enter\":\"{}\",\"timestamp\":\"{}\"}",
				"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.populateTrustStore()",
				DateUtilites.generateCurrentTimeStamp());
		KeyStore ks = null;
		String trustStoreFile = System.getProperty("javax.net.ssl.trustStore");
		String trustStorePwd = System.getProperty("javax.net.ssl.trustStorePassword");
		try {
			if (trustStoreFile != null && !trustStoreFile.isEmpty() && trustStorePwd != null
					&& !trustStorePwd.isEmpty()) {
				ks = KeyStore.getInstance("jks");
				ks.load(new FileInputStream(new File(trustStoreFile)), trustStorePwd.toCharArray());
			}
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
			LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"ErrorDetails\":{}}",
					"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.populateTrustStore()",
					DateUtilites.generateCurrentTimeStamp(), e.getMessage());
			if (LOGGER.isDebugEnabled())
				LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"ErrorDetails\":{}}",
						"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.populateTrustStore()",
						DateUtilites.generateCurrentTimeStamp(), e.getStackTrace().toString());
			throw e;
		}
		LOGGER.info("{\"Exit\":\"{}\",\"timestamp\":\"{}\"}",
				"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.populateTrustStore()",
				DateUtilites.generateCurrentTimeStamp());
		return ks;
	}

	private void buildSSLRequest() throws Exception {
		LOGGER.info("{\"Enter\":\"{}\",\"timestamp\":\"{}\"}",
				"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.buildSSLRequest()",
				DateUtilites.generateCurrentTimeStamp());
		SSLContext sslcontext = null;
		KeyStore trustStore = populateTrustStore();
		HttpComponentsClientHttpRequestFactory httpComponentsClientHttpRequestFactory = null;
		try {
			if (trustStore == null) {
				return;
			}
			sslcontext = SSLContexts.custom().loadTrustMaterial(trustStore, new TrustSelfSignedStrategy()).build();
			HostnameVerifier hostNameVerifier = new CustomHostNameVerifier();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, hostNameVerifier);
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
			httpComponentsClientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
			if (connectionTimeOut != null) {
				httpComponentsClientHttpRequestFactory.setConnectionRequestTimeout(Integer.parseInt(connectionTimeOut));
			}
			if (readTimeOut != null) {
				httpComponentsClientHttpRequestFactory.setReadTimeout(Integer.parseInt(readTimeOut));
			}
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"ErrorDetails\":{}}",
					"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.buildSSLRequest()",
					DateUtilites.generateCurrentTimeStamp(), e.getMessage());
			if (LOGGER.isDebugEnabled())
				LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"ErrorDetails\":{}}",
						"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.buildSSLRequest()",
						DateUtilites.generateCurrentTimeStamp(), e.getStackTrace().toString());
			throw e;
		}
		LOGGER.info("{\"Exit\":\"{}\",\"timestamp\":\"{}\"}",
				"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.buildSSLRequest()",
				DateUtilites.generateCurrentTimeStamp());
		this.restTemplate.setRequestFactory(httpComponentsClientHttpRequestFactory);
	}

	private AbstractTreeIterator prepareTreeParser(Repository repository, String objectId)
			throws IOException, IncorrectObjectTypeException {
		LOGGER.info("{\"Enter\":\"{}\",\"timestamp\":\"{}\"}",
				"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.prepareTreeParser()",
				DateUtilites.generateCurrentTimeStamp());
		RevWalk walk = new RevWalk(repository);
		ObjectId resolve = repository.resolve(objectId);
		RevCommit commit = walk.parseCommit(resolve);
		RevTree tree = walk.parseTree(commit.getTree().getId());

		CanonicalTreeParser oldTreeParser = new CanonicalTreeParser();
		ObjectReader oldReader = repository.newObjectReader();
		try {
			oldTreeParser.reset(oldReader, tree.getId());
		} finally {
			oldReader.release();
		}

		walk.dispose();
		LOGGER.info("{\"Exit\":\"{}\",\"timestamp\":\"{}\"}",
				"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.prepareTreeParser()",
				DateUtilites.generateCurrentTimeStamp());
		return oldTreeParser;
	}

	private List<DiffEntry> gitDiffAndSync(String repoPath) {
		LOGGER.info("{\"Enter\":\"{}\",\"timestamp\":\"{}\"}",
				"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.gitDiffAndSync()",
				DateUtilites.generateCurrentTimeStamp());
		List<DiffEntry> list = null;
		Git git = null;
		Repository repo = null;
		try {
			git = Git.init().setDirectory(new File(repoPath)).call();
			SshSessionFactory sshSessionFactory = new JschConfigSessionFactory() {

				@Override
				protected void configure(Host host, Session session) {
					session.setUserInfo(new UserInfo() {
						@Override
						public String getPassphrase() {
							return passPhrase;
						}

						@Override
						public String getPassword() {
							return null;
						}

						@Override
						public boolean promptPassword(String message) {
							return false;
						}

						@Override
						public boolean promptPassphrase(String message) {
							return true;
						}

						@Override
						public boolean promptYesNo(String message) {
							return false;
						}

						@Override
						public void showMessage(String message) {
						}
					});

				}

				/*
				 * @Override protected JSch createDefaultJSch(FS fs) throws
				 * JSchException { JSch defaultJSch =
				 * super.createDefaultJSch(fs);
				 * defaultJSch.addIdentity("C:\\Users\\rupasani\\.ssh\\id_rsa");
				 * return defaultJSch; }
				 */
			};

			repo = git.getRepository();
			FetchCommand fetchCommand = git.fetch();
			fetchCommand.setTransportConfigCallback(new TransportConfigCallback() {
				@Override
				public void configure(Transport transport) {
					SshTransport sshTransport = (SshTransport) transport;
					sshTransport.setSshSessionFactory(sshSessionFactory);
				}

			});
			fetchCommand.call();
			AbstractTreeIterator oldTreeParser = prepareTreeParser(repo, "refs/heads/master");
			AbstractTreeIterator newTreeParser = prepareTreeParser(repo, "origin/master");

			list = git.diff().setOldTree(oldTreeParser).setNewTree(newTreeParser).call();
			Iterator<DiffEntry> itr = list.iterator();
			while (itr.hasNext()) {
				LOGGER.info(itr.next().toString());
			}
			git.stashCreate().call();
			PullCommand pullCmd = git.pull();
			pullCmd.setTransportConfigCallback(new TransportConfigCallback() {
				@Override
				public void configure(Transport transport) {
					SshTransport sshTransport = (SshTransport) transport;
					sshTransport.setSshSessionFactory(sshSessionFactory);
				}
			});

			PullResult pullResult = pullCmd.call();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"ErrorDetails\":{}}",
					"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.gitDiffAndSync()",
					DateUtilites.generateCurrentTimeStamp(), e.getMessage());
			if (LOGGER.isDebugEnabled())
				LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"ErrorDetails\":{}}",
						"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.gitDiffAndSync()",
						DateUtilites.generateCurrentTimeStamp(), e.getStackTrace().toString());
		} finally {
			if(repo!=null){
				repo.close();
			}
			if(git!=null){
				git.close();
			}
		}
		LOGGER.info("{\"Exit\":\"{}\",\"timestamp\":\"{}\"}",
				"com.capgemini.psd2.config.server.utility.ConfigSyncUtilityController.gitDiffAndSync()",
				DateUtilites.generateCurrentTimeStamp());
		return list;
	}
}
