package com.capgemini.psd2.customer.account.list.routing.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBAccount2;
import com.capgemini.psd2.aisp.domain.OBCashAccount3;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification4;
import com.capgemini.psd2.aisp.domain.OBReadAccount2;
import com.capgemini.psd2.aisp.domain.OBReadAccount2Data;
import com.capgemini.psd2.consent.domain.CustomerAccountInfo;

public class CustomerAccountListRoutingAdapterMockData {


	public static List<CustomerAccountInfo> customerAccountInfoList;
	
	public static List<CustomerAccountInfo> getCustomerAccountInfoList(){
		customerAccountInfoList = new ArrayList<>();
		CustomerAccountInfo customerAccountInfo1 = new CustomerAccountInfo();
		customerAccountInfo1.setAccountName("John Doe");
		customerAccountInfo1.setUserId("1234");
		customerAccountInfo1.setAccountNumber("10203345");
		customerAccountInfo1.setAccountNSC("SC802001");
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setAccountType("checking");
		
		CustomerAccountInfo customerAccountInfo2 = new CustomerAccountInfo();
		customerAccountInfo2.setAccountName("Tiffany Doe");
		customerAccountInfo2.setUserId("1234");
		customerAccountInfo2.setAccountNumber("10203346");
		customerAccountInfo2.setAccountNSC("SC802002");
		customerAccountInfo2.setCurrency("GRP");
		customerAccountInfo2.setNickname("Tiffany");
		customerAccountInfo2.setAccountType("savings");
		
		customerAccountInfoList.add(customerAccountInfo1);
		customerAccountInfoList.add(customerAccountInfo2);
		return customerAccountInfoList;
	}
	
	public static OBReadAccount2 getCustomerAccountInfo(){
		OBReadAccount2 OBReadAccount2 = new OBReadAccount2();
		
		List<OBAccount2> accountList2 =  new ArrayList<>();
		
		OBAccount2 account2 = new OBAccount2();
		account2.setAccountId("12435455");
		account2.setCurrency("EUR");
	    
		OBBranchAndFinancialInstitutionIdentification4 servicer = new OBBranchAndFinancialInstitutionIdentification4();
	    servicer.setIdentification("123445");
		account2.setServicer(servicer);
		List<OBCashAccount3> accountList= new ArrayList<>();
		OBCashAccount3 account = new OBCashAccount3();
		account.setIdentification("243543");
		accountList.add(account);
		account2.setAccount(accountList);
		accountList2.add(account2);
		OBReadAccount2Data data2 = new OBReadAccount2Data();
		data2.setAccount(accountList2);
		OBReadAccount2.setData(data2);
		return OBReadAccount2;
	}


}
