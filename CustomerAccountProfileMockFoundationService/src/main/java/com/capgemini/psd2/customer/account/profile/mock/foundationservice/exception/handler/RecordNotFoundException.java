package com.capgemini.psd2.customer.account.profile.mock.foundationservice.exception.handler;

public class RecordNotFoundException extends Exception {

	public RecordNotFoundException(String s){
		super(s);
		
	}
	
}
