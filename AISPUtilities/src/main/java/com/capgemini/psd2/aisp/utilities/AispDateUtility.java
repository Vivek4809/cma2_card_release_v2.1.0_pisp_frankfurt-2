package com.capgemini.psd2.aisp.utilities;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.utilities.DateUtilitiesCMA2;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class AispDateUtility extends DateUtilitiesCMA2 {

	@Override
	public String transformDateTimeInRequest(String dateTime) {
		if (!NullCheckUtils.isNullOrEmpty(dateTime)) {
			return super.transformDateTimeInRequest(dateTime);
		}

		return null;
	}

	@Override
	public void validateDateTimeInRequest(String dateTime) {
		if (!NullCheckUtils.isNullOrEmpty(dateTime))
			super.validateDateTimeInRequest(dateTime);
	}

	@Override
	public void validateDateTimeInResponse(String dateTime) {
		if (!NullCheckUtils.isNullOrEmpty(dateTime))
			super.validateDateTimeInResponse(dateTime);
	}
}
