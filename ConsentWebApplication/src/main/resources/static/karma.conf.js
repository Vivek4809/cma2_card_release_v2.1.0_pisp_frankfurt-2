// Karma configuration
// Generated on Tue Aug 15 2017 15:23:51 GMT+0530 (IST)

module.exports = function(config) {
    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '',


        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['jasmine-jquery','jasmine'],


        // list of files / patterns to load in the browser
        files: [
            "node_modules/angular/angular.js",
            "node_modules/angular-mocks/angular-mocks.js",
            "node_modules/angular-ui-router/src/*.js",
            "node_modules/angular-bootstrap/*.js",
            "node_modules/angular-ui-bootstrap/dist/*.js",
            "node_modules/angular-block-ui/dist/*.js",
            "node_modules/angular-translate/dist/*.js",
            "node_modules/angular-translate/dist/**/*.js",
            "node_modules/jquery/dist/jquery.js",
            "node_modules/underscore/*.js",
            "js/app.js",
            "js/templates.js",
            "tests/controllers/*.js",
            "tests/fixtures/*.html"
        ],

        plugins: [
        	"karma-jasmine",
        	"karma-jasmine-jquery",
            'karma-junit-reporter',
            "karma-coverage",
            "karma-phantomjs-launcher"
        ],
        // list of files to exclude
        exclude: ["js/**/Cisp*.js"],


        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            'js/**/*.js': ['coverage']
        },
        
        coverageReporter: {
            type: 'lcov',
            dir: 'reports/',
            subdir: '.'
        },

        // optionally, configure the reporter
       /* coverageReporter: {
            includeAllSources: true,
            dir: 'coverage/',
            reporters: [
                { type: "html", subdir: "html" },
                { type: 'text-summary' }
            ]
        },*/

        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['progress', 'dots', 'junit', 'coverage'],

        junitReporter: {
            outputDir: 'test-results.xml', // results will be saved as $outputDir/$browserName.xml 
           // outputFile: 'test-results.xml', // if included, results will be saved as $outputDir/$browserName/$outputFile 
            suite: 'models' // suite will become the package name attribute in xml testsuite element 
//            useBrowserName: true, // add browser name to report and classes names 
//            nameFormatter: undefined, // function (browser, result) to customize the name attribute in xml testcase element 
//            classNameFormatter: undefined, // function (browser, result) to customize the classname attribute in xml testcase element 
//            properties: {} // key value pair of properties to add to the <properties> section of the report 
          },
        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['PhantomJS'],


        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: true,

        // Concurrency level
        // how many browser should be started simultaneous
        concurrency: Infinity


    })
}