"use strict";
describe("ConsentApp.maskNumberFilter_test", function() {
    var $filter;
    beforeEach(function() {
        module("consentApp");

        inject(function(_$filter_) {
            $filter = _$filter_;
        });
    });

    var service;

    it("test filter", function() {
        var testValue = "10203355",
            result;

        result = $filter("maskNumber")(testValue, 4);

        expect(result).toEqual("~3355");
    });
});