
package com.capgemini.psd2.foundationservice.sca.authentication.service.boi.adapter.client;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.adapter.security.domain.Login;
import org.springframework.beans.factory.annotation.Value;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.adapter.security.domain.LoginResponse;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class SCAAuthenticationServiceFoundationServiceClient {

	@Autowired
	@Qualifier("restClientSecurityFoundation")
	private RestClientSync restClient;

	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private DataMask dataMask;
	
	@Value("${foundationService.maskPayloadLog:#{false}}")
	private boolean maskPayloadLog; 
	
	@PostConstruct
	private void init() {
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        converter.setObjectMapper(mapper);
        restTemplate.getMessageConverters().add(0,converter);
	}

	
	public LoginResponse restTransportForAuthenticationServicePOST(RequestInfo reqInfo,
			Login digitalUserRequest, Class<LoginResponse> response, HttpHeaders headers) {
			
		
		LoginResponse loginResponse =  restClient.callForPost(reqInfo, digitalUserRequest, response, headers);
		
		if(maskPayloadLog)
		{
			digitalUserRequest = dataMask.maskRequestLog(digitalUserRequest,"transformAuthenticationServiceRequest");
		} 
		
		return loginResponse;

	}
}
