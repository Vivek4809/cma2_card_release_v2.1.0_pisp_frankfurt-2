package com.capgemini.psd2.service.qtsp;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.model.qtsp.QTSPResource;
import com.capgemini.psd2.service.qtsp.config.PFConfig;
import com.capgemini.psd2.service.qtsp.integration.MongoDbAdapter;
import com.capgemini.psd2.service.qtsp.stub.QTSPStub;
import com.capgemini.psd2.service.qtsp.utility.QTSPServiceUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class QTSPServiceTest {

	@InjectMocks
	private QTSPService qtspService;

	@Mock
	private MongoDbAdapter qtspServiceMongoDbAdapter;

	@Mock
	private PFConfig pfConfig;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void processQTSPResponse_NoOBResponse() {
		when(qtspServiceMongoDbAdapter.retrieveQTSPs()).thenReturn(QTSPStub.getCerts());
		Assert.assertTrue(qtspService.processQTSPResponse(new ArrayList<>()).isEmpty());
	}

	@Test
	public void processQTSPResponse_newCert() {
		List<QTSPResource> existingQTSPs = new ArrayList<>();
		existingQTSPs.add(QTSPStub.getCerts().get(0));
		when(qtspServiceMongoDbAdapter.retrieveQTSPs()).thenReturn(existingQTSPs);
		Map<String, String> prefix = new HashMap<>();
		prefix.put("BOI", "url");
		when(pfConfig.getPrefix()).thenReturn(prefix);
		List<QTSPResource> newQTSPs = qtspService.processQTSPResponse(QTSPStub.getCerts())
				.get(QTSPServiceUtility.NEW_QTSPS);
		List<QTSPResource> modifiedQTSPs = qtspService.processQTSPResponse(QTSPStub.getCerts())
				.get(QTSPServiceUtility.MODIFIED_QTSPS);
		Assert.assertTrue(!newQTSPs.isEmpty());
		Assert.assertTrue(modifiedQTSPs.isEmpty());
	}

	@Test
	public void processQTSPResponse_modifiedCert() {
		List<QTSPResource> existingQTSPs = QTSPStub.getCerts();
		QTSPResource cert = existingQTSPs.get(0);
		cert.setServiceStatus("http://uri.etsi.org/TrstSvc/TrustedList/Svcstatus/withdrawn");
		existingQTSPs.add(cert);
		when(qtspServiceMongoDbAdapter.retrieveQTSPs()).thenReturn(existingQTSPs);
		Map<String, List<QTSPResource>> result = qtspService.processQTSPResponse(QTSPStub.getCerts());
		List<QTSPResource> newQTSPs = result.get(QTSPServiceUtility.NEW_QTSPS);
		List<QTSPResource> modifiedQTSPs = result.get(QTSPServiceUtility.MODIFIED_QTSPS);
		Assert.assertTrue(newQTSPs.isEmpty());
		Assert.assertTrue(!modifiedQTSPs.isEmpty());
	}

	@Test
	public void getNewQTSPs() {
		qtspService.getNewQTSPs();
	}

	@Test
	public void getModifiedQTSPs() {
		qtspService.getModifiedQTSPs();
	}
}