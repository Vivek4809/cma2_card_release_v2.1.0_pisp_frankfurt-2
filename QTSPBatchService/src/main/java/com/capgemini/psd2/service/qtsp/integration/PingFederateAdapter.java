package com.capgemini.psd2.service.qtsp.integration;

import java.util.List;

import org.springframework.http.HttpHeaders;

import com.capgemini.psd2.model.qtsp.QTSPResource;

public interface PingFederateAdapter {

	void importCAInPF(List<QTSPResource> updatedQTSPs);

	void removeCAFromPF(List<QTSPResource> updatedQTSPs);

	void replicatePFConfiguration(HttpHeaders httpHeaders, String prefix);

}