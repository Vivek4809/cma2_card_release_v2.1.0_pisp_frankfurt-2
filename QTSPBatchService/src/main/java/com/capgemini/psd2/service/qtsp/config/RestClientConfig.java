package com.capgemini.psd2.service.qtsp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.service.qtsp.exceptionhandler.QTSPExceptionHandler;

@Lazy
@Configuration
public class RestClientConfig {

	@Bean(name = "qtspServiceRestClient")
	public RestClientSync restClientSyncImpl() {
		return new RestClientSyncImpl(exceptionHandlerImpl());
	}

	@Bean
	public ExceptionHandler exceptionHandlerImpl() {
		return new QTSPExceptionHandler();
	}

}