/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.exceptions;


import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.domain.OBErrorResponse1;	

/**
 * The Class PSD2ExceptionTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class PSD2ExceptionTest {
	
	@InjectMocks
	private PSD2Exception psd2Exception;

	@Before
	public void setUp() throws Exception {
		
		ErrorInfo errorInfo = new ErrorInfo("500");
		errorInfo.setErrorCode("110");
		errorInfo.setErrorMessage("Consent exists");
		
		OBErrorResponse1 oBErrorResponse1 = new OBErrorResponse1();
		oBErrorResponse1.setCode("400");
		psd2Exception =  new PSD2Exception("Consent already exists", errorInfo );
	}
	@Test
	public void testPopulatePSD2Exception() {
		assertNotNull(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONSENT_EXISTS));
		assertNotNull(PSD2Exception.populatePSD2Exception( "Consent already exists",ErrorCodeEnum.CONSENT_EXISTS));
	}
	
	@Test
	public void testPopulateOAuth2Exception() {
		OAuthErrorInfo errorInfo = new OAuthErrorInfo();
		errorInfo.setError("error");
		errorInfo.setDetailErrorMessage("detailErrorMessage");
		errorInfo.setErrorMessage("errorMessage");
		psd2Exception.populateOAuth2Exception(errorInfo);
		
	}
	@Test
	public void generateResponseForInternalErrorsTest(){
		Throwable e = new Throwable();
		psd2Exception.generateResponseForInternalErrors(e );
	}
}
