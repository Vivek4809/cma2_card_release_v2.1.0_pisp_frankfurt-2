package com.capgemini.psd2.utilities;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.PSD2Exception;

public class ValidationUtilityTest {

	
	@InjectMocks
	private ValidationUtility validationUtility;
	
	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test(expected = PSD2Exception.class)
	public void isValidField(){
		validationUtility.isValidField("field", "regEx" , 0, 0);
	}
	
	@Test
	public void isValidField1(){
		validationUtility.isValidField("field", "regEx" , 4, 6);
	}
	
	@Test(expected = PSD2Exception.class)
	public void isValidUUIDTest(){
		ReflectionTestUtils.setField(validationUtility, "uuIdRegexPattern", "uuIdRegexPattern");
		validationUtility.isValidUUID("uuidString");
	}
	
	@Test
	public void isValidUUIDTest1(){
		ReflectionTestUtils.setField(validationUtility, "uuIdRegexPattern", "uuIdRegexPattern");
		validationUtility.isValidUUID("uuIdRegexPattern");
	}
	
	@Test
	public void setUuIdRegexPatternTest(){
	
		validationUtility.setUuIdRegexPattern("uuIdRegexPtrn");
	}
}
