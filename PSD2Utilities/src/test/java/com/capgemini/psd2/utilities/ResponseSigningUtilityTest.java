package com.capgemini.psd2.utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;

public class ResponseSigningUtilityTest {
	
	@Before
	public void setup() {
		Map<String, String> map=new HashMap<>();
		map.put("HEADER", "header error message");
		map.put("FREQUENCY", "freq ma");
		map.put("AFTER_CUTOFF_DATE", "freq ma");
		map.put("FIELD", "freq ma");
		map.put("SIGNATURE", "freq ma");
		map.put("INCOMPATIBLE", "freq ma");
		map.put("RES_NOTFOUND", "freq ma");
		map.put("INTERNAL", "freq ma"); 
 
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		specificErrorMessageMap.put("unexpected_error", "unexpected error occured");
		
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	@Test
	public void testSigningMethod() {	
	ResponseSigningUtility responseSigningUtility=new ResponseSigningUtility();
	String responseInput="{\r\n" + 
			"    \"errorCode\": \"128\",\r\n" + 
			"    \"errorMessage\": \"Validation error found with the provided ouput\",\r\n" + 
			"    \"detailErrorMessage\": \"instructedAmount.amount : must match \\\"^\\\\d{1,13}\\\\.\\\\d{1,5}$\\\";\"\r\n" + 
			"}";
	List<String> critHeaders = new ArrayList<>();
	critHeaders.add("b64");	
	ReflectionTestUtils.setField(responseSigningUtility, "signingAlgo", "RS256");
	ReflectionTestUtils.setField(responseSigningUtility, "critData", critHeaders);
	ReflectionTestUtils.setField(responseSigningUtility, "keystorePath", "keystore.jks");
	ReflectionTestUtils.setField(responseSigningUtility, "keystorePassword", "password");
	ReflectionTestUtils.setField(responseSigningUtility, "kid", "yTXZEcT2QV3jHeJtKaOfY0v3ytQ");
	ReflectionTestUtils.setField(responseSigningUtility, "signingKeyAlias", "6di2de88s8d2zvxgyxm0b2 (open banking test issuing ca)");
	responseSigningUtility.populateKeyStore();
	responseSigningUtility.signResponseBody(responseInput);
	responseSigningUtility.setKid(null);
	responseSigningUtility.setSigningAlgo(null);
	responseSigningUtility.setSigningKeyAlias(null);
	responseSigningUtility.getCritData();
	responseSigningUtility.signResponseBody(null);
	responseSigningUtility.signResponseBody(null);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testSigningMethodForException() {	
	ResponseSigningUtility responseSigningUtility=new ResponseSigningUtility();
	ReflectionTestUtils.setField(responseSigningUtility, "keystorePath", "keystore.jks");
	ReflectionTestUtils.setField(responseSigningUtility, "keystorePassword", "pasword");
	responseSigningUtility.populateKeyStore();
	}
	
	@Test(expected=PSD2Exception.class)
	public void testSigningMethodForException2() {	
	ResponseSigningUtility responseSigningUtility=new ResponseSigningUtility();
	String responseInput="{\r\n" + 
			"    \"errorCode\": \"128\",\r\n" + 
			"    \"errorMessage\": \"Validation error found with the provided ouput\",\r\n" + 
			"    \"detailErrorMessage\": \"instructedAmount.amount : must match \\\"^\\\\d{1,13}\\\\.\\\\d{1,5}$\\\";\"\r\n" + 
			"}";
	List<String> critHeaders = new ArrayList<>();
	critHeaders.add("b64");	
	ReflectionTestUtils.setField(responseSigningUtility, "signingAlgo", "RS256");
	ReflectionTestUtils.setField(responseSigningUtility, "critData", critHeaders);
	ReflectionTestUtils.setField(responseSigningUtility, "keystorePath", "keystore.jks");
	ReflectionTestUtils.setField(responseSigningUtility, "keystorePassword", "pasword");
	ReflectionTestUtils.setField(responseSigningUtility, "kid", "yTXZEcT2QV3jHeJtKaOfY0v3ytQ");
	ReflectionTestUtils.setField(responseSigningUtility, "signingKeyAlias", "6di2de88s8d2zvxgyxm0b2 (open banking test issuing ca)");
	responseSigningUtility.populateKeyStore();
	responseSigningUtility.signResponseBody(responseInput);
	}
	
	@Test
	public void testSigningMethod1() {	
	ResponseSigningUtility responseSigningUtility=new ResponseSigningUtility();
	Sample sampleObj=new Sample();
	sampleObj.setFname("fname");
	ResponseEntity<Sample> responseEntity=new ResponseEntity<>(HttpStatus.ACCEPTED).ok(sampleObj);
	
	
	List<String> critHeaders = new ArrayList<>();
	critHeaders.add("b64");	
	ReflectionTestUtils.setField(responseSigningUtility, "signingAlgo", "RS256");
	ReflectionTestUtils.setField(responseSigningUtility, "critData", critHeaders);
	ReflectionTestUtils.setField(responseSigningUtility, "keystorePath", "keystore.jks");
	ReflectionTestUtils.setField(responseSigningUtility, "keystorePassword", "password");
	ReflectionTestUtils.setField(responseSigningUtility, "kid", "yTXZEcT2QV3jHeJtKaOfY0v3ytQ");
	ReflectionTestUtils.setField(responseSigningUtility, "signingKeyAlias", "6di2de88s8d2zvxgyxm0b2 (open banking test issuing ca)");
	responseSigningUtility.populateKeyStore();
	responseSigningUtility.signResponseBody(responseEntity);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testSigningMethod2() {	
	ResponseSigningUtility responseSigningUtility=new ResponseSigningUtility();
	Sample sampleObj=new Sample();
	sampleObj.setFname("fname");	
	List<String> critHeaders = new ArrayList<>();
	critHeaders.add("b64");	
	ReflectionTestUtils.setField(responseSigningUtility, "signingAlgo", "RS256");
	ReflectionTestUtils.setField(responseSigningUtility, "critData", critHeaders);
	ReflectionTestUtils.setField(responseSigningUtility, "keystorePath", "keystore.jks");
	ReflectionTestUtils.setField(responseSigningUtility, "keystorePassword", "password");
	ReflectionTestUtils.setField(responseSigningUtility, "kid",null);
	responseSigningUtility.populateKeyStore();
	responseSigningUtility.signResponseBody(sampleObj);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testSigningMethod3() {	
	ResponseSigningUtility responseSigningUtility=new ResponseSigningUtility();
	Sample sampleObj=new Sample();
	sampleObj.setFname("fname");	
	List<String> critHeaders = new ArrayList<>();
	critHeaders.add("b64");	
	ReflectionTestUtils.setField(responseSigningUtility, "signingAlgo", "RS256");
	ReflectionTestUtils.setField(responseSigningUtility, "critData", critHeaders);
	ReflectionTestUtils.setField(responseSigningUtility, "keystorePath", "keystore.jks");
	ReflectionTestUtils.setField(responseSigningUtility, "keystorePassword", "password");
	ReflectionTestUtils.setField(responseSigningUtility, "kid",null);
	ReflectionTestUtils.setField(responseSigningUtility, "signingKeyAlias", "6di2de88s8d2zvxgyxm0b2 (open banking test issuing ca)");
	responseSigningUtility.populateKeyStore();
	responseSigningUtility.signResponseBody(sampleObj);
	}
	
	class Sample{
		String fname;
		String lname;
		int age;
		public String getFname() {
			return fname;
		}
		public void setFname(String fname) {
			this.fname = fname;
		}
		public String getLname() {
			return lname;
		}
		public void setLname(String lname) {
			this.lname = lname;
		}
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
		
	}
}
