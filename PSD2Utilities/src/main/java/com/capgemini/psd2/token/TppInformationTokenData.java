package com.capgemini.psd2.token;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class TppInformationTokenData {

	private String tppLegalEntityName;
	private String tppRegisteredId;
	private Set<String> tppRoles;
	
	public String getTppLegalEntityName() {
		return tppLegalEntityName;
	}
	public void setTppLegalEntityName(String tppLegalEntityName) {
		this.tppLegalEntityName = tppLegalEntityName;
	}
	public String getTppRegisteredId() {
		return tppRegisteredId;
	}
	public void setTppRegisteredId(String tppRegisteredId) {
		this.tppRegisteredId = tppRegisteredId;
	}
	public Set<String> getTppRoles() {
		return tppRoles;
	}
	public void setTppRoles(Set<String> tppRoles) {
		this.tppRoles = tppRoles;
	}

	

}
