/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.cisp.consent.adapter.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;

/**
 * The Interface PispConsentMongoRepository.
 */
public interface CispConsentMongoRepository extends MongoRepository<CispConsent, String> {
	
	/**
	 * Find by consent id.
	 *
	 * @param fundsCheckConsentId the consent id
	 * @return the pispConsent
	 */
	public CispConsent findByConsentIdAndCmaVersionIn(String consentId, List<String> cmaVersion);
	
	public CispConsent findByFundsIntentIdAndStatusAndCmaVersionIn(String fundsIntentId, ConsentStatusEnum status, List<String> cmaVersion);
	
	public CispConsent findByFundsIntentIdAndCmaVersionIn(String fundsIntentId , List<String> cmaVersion);
	
	public List<CispConsent> findByPsuIdAndStatusAndCmaVersionIn(String psuId, ConsentStatusEnum status, List<String> cmaVersion);

	public List<CispConsent> findByPsuIdAndCmaVersionIn(String psuId, List<String> cmaVersion);
	
	public List<CispConsent> findByPsuIdAndTenantIdAndCmaVersionIn(String psuId, String tenantId, List<String> cmaVersion);
	
	public List<CispConsent> findByPsuIdAndStatusAndTenantIdAndCmaVersionIn(String psuId, ConsentStatusEnum status, String tenantid, List<String> cmaVersion);
}
