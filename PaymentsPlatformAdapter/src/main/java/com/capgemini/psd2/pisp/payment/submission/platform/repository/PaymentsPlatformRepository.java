package com.capgemini.psd2.pisp.payment.submission.platform.repository;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;

public interface PaymentsPlatformRepository extends MongoRepository<PaymentsPlatformResource, String> {
	public PaymentsPlatformResource findOneByTppCIDAndPaymentTypeAndIdempotencyKeyAndIdempotencyRequestAndCreatedAtGreaterThanAndSubmissionCmaVersionIn(String tppCID,String paymentType, String idempotencyKey,String idempotencyRequest,String date, List<String> cmaVersion);
	public PaymentsPlatformResource findOneByPaymentSubmissionIdOrSubmissionIdAndSubmissionCmaVersionIn(String paymentSubmissionId, String submissionId, List<String> cmaVersion);
	public PaymentsPlatformResource findOneByPaymentConsentIdAndSubmissionCmaVersionIn(String paymentConsentId, List<String> cmaVersion);
	public PaymentsPlatformResource findOneByPaymentIdOrPaymentConsentIdAndSubmissionCmaVersionIn(String paymentId, String paymentConsentId, List<String> cmaVersion);
	public PaymentsPlatformResource findOneBySubmissionIdAndPaymentTypeAndSubmissionCmaVersionIn(String paymentId, String paymentType, List<String> cmaVersion);
	public PaymentsPlatformResource findOneByPaymentSubmissionIdAndSubmissionCmaVersionIn(String submissionId, List<String> cmaVersion);
}

