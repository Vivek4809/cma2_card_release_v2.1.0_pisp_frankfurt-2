package com.capgemini.psd2.pisp.dsp.test.models;

import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentsResponse;
import com.capgemini.psd2.pisp.domain.OBDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.status.PaymentConstants;

public class DSPTestData {
	
	public static PaymentConsentsPlatformResource getConsentPlatformResource () {
		PaymentConsentsPlatformResource paymentConsentPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentPlatformResource.setPaymentConsentId("12345");
		return paymentConsentPlatformResource;
	}
	
	public static PaymentsPlatformResource getPlatformResource() {
		PaymentsPlatformResource platformResource = new PaymentsPlatformResource();
		platformResource.setProccessState(PaymentConstants.INCOMPLETE);
		platformResource.setPaymentConsentId("12345");
		platformResource.setPaymentId("12345");
		return platformResource;
	}
	
	public static CustomDSPaymentsPOSTRequest getRequest() {
		CustomDSPaymentsPOSTRequest request = new CustomDSPaymentsPOSTRequest();
		OBWriteDataDomesticScheduled1 data = new OBWriteDataDomesticScheduled1();
		data.setConsentId("12345");
		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		data.setInitiation(initiation);
		request.setData(data);
		return request;
	}
	
	public static CustomDSPaymentsPOSTRequest getRequestwithDebtor() {
		CustomDSPaymentsPOSTRequest request = new CustomDSPaymentsPOSTRequest();
		OBWriteDataDomesticScheduled1 data = new OBWriteDataDomesticScheduled1();
		data.setConsentId("12345");
		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		OBCashAccountDebtor3 debtor = new OBCashAccountDebtor3();
	//	debtor.setName("asd");
		debtor.setIdentification("aaq1q11");
		debtor.setSchemeName("scheme");
		initiation.setDebtorAccount(debtor);
		data.setInitiation(initiation);
		request.setData(data);
		return request;
	}
	
	public static GenericPaymentsResponse getGenericResponse() {
		GenericPaymentsResponse genericResponse = new GenericPaymentsResponse();
		return genericResponse;
	}
	
	public static CustomDSPConsentsPOSTResponse getConsentResponse() {
		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();
		OBWriteDataDomesticScheduledConsentResponse1 data = new OBWriteDataDomesticScheduledConsentResponse1();
		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		OBCashAccountDebtor3 debtor = new OBCashAccountDebtor3();
		debtor.setName("asd");
		debtor.setIdentification("aaq1q11");
		debtor.setSchemeName("scheme");
		initiation.setDebtorAccount(debtor);
		data.setInitiation(initiation);
		response.setData(data);
		return response;
	}
	
	public static CustomDSPaymentsPOSTResponse getPostResponse() {
		CustomDSPaymentsPOSTResponse response = new CustomDSPaymentsPOSTResponse();
		OBWriteDataDomesticScheduledResponse1 data = new OBWriteDataDomesticScheduledResponse1();
		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		data.setInitiation(initiation);
		response.setData(data);
		return response;
	}

}
