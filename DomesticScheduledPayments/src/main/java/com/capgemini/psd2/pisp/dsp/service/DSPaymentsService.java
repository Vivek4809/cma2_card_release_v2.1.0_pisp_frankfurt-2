package com.capgemini.psd2.pisp.dsp.service;

import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTResponse;

public interface DSPaymentsService {

	public CustomDSPaymentsPOSTResponse createDomesticScheduledPaymentsResource(
			CustomDSPaymentsPOSTRequest customRequest);

	public CustomDSPaymentsPOSTResponse retrieveDomesticScheduledPaymentsResource(String domesticScheduledPaymentId);

}
