package com.capgemini.psd2.pisp.dsp.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticScheduledResponse1;
import com.capgemini.psd2.pisp.dsp.service.DSPaymentsService;
import com.fasterxml.jackson.databind.ObjectMapper;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-12-03T16:57:36.342+05:30")

@Controller
public class DSPaymentsApiController implements DSPaymentsApi {

	@Autowired
	private DSPaymentsService service;

	private final ObjectMapper objectMapper;

	private final HttpServletRequest request;

	@org.springframework.beans.factory.annotation.Autowired
	public DSPaymentsApiController(ObjectMapper objectMapper, HttpServletRequest request) {
		this.objectMapper = objectMapper;
		this.request = request;
	}

	@Override
	public Optional<ObjectMapper> getObjectMapper() {
		return Optional.ofNullable(objectMapper);
	}

	@Override
	public Optional<HttpServletRequest> getRequest() {
		return Optional.ofNullable(request);
	}

	@Override
	public ResponseEntity<OBWriteDomesticScheduledResponse1> createDomesticScheduledPayments(
			@RequestBody OBWriteDomesticScheduled1 obWriteDomesticScheduled1Param, String xFapiFinancialId,
			String authorization, String xIdempotencyKey, String xJwsSignature, String xFapiCustomerLastLoggedTime,
			String xFapiCustomerIpAddress, String xFapiInteractionId, String xCustomerUserAgent) {
		if (getObjectMapper().isPresent()) {
			CustomDSPaymentsPOSTRequest customRequest = new CustomDSPaymentsPOSTRequest();
			customRequest.setData(obWriteDomesticScheduled1Param.getData());
			customRequest.setRisk(obWriteDomesticScheduled1Param.getRisk());
			CustomDSPaymentsPOSTResponse customResponse = service
					.createDomesticScheduledPaymentsResource(customRequest);
			OBWriteDomesticScheduledResponse1 tppResponse = new OBWriteDomesticScheduledResponse1();
			tppResponse.setData(customResponse.getData());
			tppResponse.setLinks(customResponse.getLinks());
			tppResponse.setMeta(customResponse.getMeta());
			return new ResponseEntity<>(tppResponse, HttpStatus.CREATED);

		}
		throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT,
				ErrorMapKeys.RESOURCE_INVALIDFORMAT));
	}

	@Override
	public ResponseEntity<OBWriteDomesticScheduledResponse1> getDomesticScheduledPaymentsDomesticScheduledPaymentId(
			@PathVariable("DomesticScheduledPaymentId") String domesticScheduledPaymentId, String xFapiFinancialId,
			String authorization, String xFapiCustomerLastLoggedTime, String xFapiCustomerIpAddress,
			String xFapiInteractionId, String xCustomerUserAgent) {
		if (getObjectMapper().isPresent()) {

			CustomDSPaymentsPOSTResponse customResponse = service
					.retrieveDomesticScheduledPaymentsResource(domesticScheduledPaymentId);
			OBWriteDomesticScheduledResponse1 tppResponse = new OBWriteDomesticScheduledResponse1();
			tppResponse.setData(customResponse.getData());
			tppResponse.setLinks(customResponse.getLinks());
			tppResponse.setMeta(customResponse.getMeta());
			return new ResponseEntity<>(tppResponse, HttpStatus.OK);
		}
		throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT,
				ErrorMapKeys.RESOURCE_INVALIDFORMAT));
	}

}
