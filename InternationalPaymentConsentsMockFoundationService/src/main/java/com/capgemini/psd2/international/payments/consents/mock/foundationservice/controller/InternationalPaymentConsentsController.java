package com.capgemini.psd2.international.payments.consents.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;
import com.capgemini.psd2.international.payments.consents.mock.foundationservice.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.international.payments.consents.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.international.payments.consents.mock.foundationservice.service.InternationalPaymentConsentsService;

@RestController
@RequestMapping("/fs-internationalpaymentconsent-service/services/internationalpaymentconsent/")
public class InternationalPaymentConsentsController {
	/** The account information service. */
	@Autowired
	private InternationalPaymentConsentsService internationalpaymentConsentsService;

	/** The validation utility. */
	@Autowired
	private ValidationUtility validationUtility;

	/**
	 * Channel A InternationalPaymentConsents information.
	 *
	 * @param paymentInstructionProposalId
	 *            the payment instruction proposal id
	 * @param channelcode
	 *            the channelcode
	 * @param sourceuse
	 *            the sourceuse
	 * @param transactionid
	 *            the transactionid
	 * @param sourcesystem
	 *            the sourcesystem
	 * @param correlationID
	 *            the correlation ID
	 * @return the accounts
	 * @throws Exception
	 *             the exception
	 */
	
	@RequestMapping(value = "/{version}/international/payment-instruction-proposals/{paymentInstructionProposalId}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	public PaymentInstructionProposalInternational internationalpaymentconsentsretrieve(
			@PathVariable("paymentInstructionProposalId") String paymentInstructionProposalId,
			@RequestHeader(required = false, value = "X-API-CHANNEL-CODE") String channelcode,
			@RequestHeader(required = false, value = "X-API-TRANSACTION-ID") String transactionId,
			@RequestHeader(required = false, value = "X-API-SOURCE-SYSTEM") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "X-API-SOURCE-USER") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "X-API-PARTY-SOURCE-ID-NUMBER") String partysourceReqHeader,
			@RequestHeader(required = false, value = "X-API-CORRELATION-ID") String correlationId,
			@RequestHeader(required = false, value = "X-API-CHANNEL-BRAND") String channelBrand) throws Exception {

		if (null == sourceSystemReqHeader || null == channelBrand) 
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPD_PIP);

		if (paymentInstructionProposalId == null)
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.RESOURCE_NOT_FOUND_PPD_PIP);
		
		validationUtility.validateErrorCode(transactionId);
		
		return internationalpaymentConsentsService.retrieveInternationalPaymentConsentsResource(paymentInstructionProposalId);
	}
	
	/**
	 * International payment consent post.
	 *
	 * @param paymentInstProposalReq the payment inst proposal req
	 * @param channelcode the channelcode
	 * @param correlationId the correlation id
	 * @param sourceSystemReqHeader the source system req header
	 * @param sourceUserReqHeader the source user req header
	 * @param channelInReqHeader the channel in req header
	 * @return the response entity
	 * @throws Exception 
	 */
	@RequestMapping(value = "/{version}/international/payment-instruction-proposals", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<PaymentInstructionProposalInternational> internationalPaymentConsentValidatePost(
			@RequestBody PaymentInstructionProposalInternational paymentInstProposalReq,
			@RequestHeader(required = false, value = "X-API-CHANNEL-CODE") String channelcode,
			@RequestHeader(required = false, value = "X-API-TRANSACTION-ID") String transactionId,
			@RequestHeader(required = false, value = "X-API-SOURCE-SYSTEM") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "X-API-SOURCE-USER") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "X-API-PARTY-SOURCE-ID-NUMBER") String partysourceReqHeader,
			@RequestHeader(required = false, value = "X-API-CORRELATION-ID") String correlationId,
			@RequestHeader(required = false, value = "X-API-CHANNEL-BRAND") String channelBrand) throws Exception {
		
		if (null == sourceSystemReqHeader || null == channelBrand)
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPD_PIP);
		
		System.out.println(paymentInstProposalReq);
		
		if (null == paymentInstProposalReq)	
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.RESOURCE_NOT_FOUND_PPD_PIP);
	
		PaymentInstructionProposalInternational paymentInstructionProposalResponse = null;

		try {
 			paymentInstructionProposalResponse = internationalpaymentConsentsService.createInternationalPaymentConsentsResource(paymentInstProposalReq);

		}catch (RecordNotFoundException e) {
			e.printStackTrace();
		}
 		return new ResponseEntity<>(paymentInstructionProposalResponse, HttpStatus.CREATED);
	}

}
