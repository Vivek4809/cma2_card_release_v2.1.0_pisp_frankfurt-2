package com.capgemini.psd2.pisp.processing.adapter;

public interface PaymentSubmissionCustomValidator<T, U> {

	// validatePaymentsPOSTRequest
	public void validatePaymentsPOSTRequest(T submissionRequestBody);

	// ValidateSetupResponse
	public void validatePaymentsResponse(U submissionResponse);

}
