package com.capgemini.psd2.pisp.processing.adapter.impl;

import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.processing.adapter.PaymentSubmissionTransformer;

@Lazy
@Component("paymentsResponseTransformRoutingBean")
public class PaymentSubmissionReponseTransformerRoutingImpl<T, U>
		implements PaymentSubmissionTransformer<T, U>, ApplicationContextAware {

	private PaymentSubmissionTransformer<T, U> beanInstance;

	@Value("${app.reponseTransformerBean:null}")
	private String beanName;

	private ApplicationContext applicationContext;

	@SuppressWarnings("unchecked")
	public T paymentSubmissionResponseTransformer(T paymentConsentsResponse,
			Map<String, Object> paymentsPlatformResourceMap, String methodType) {
		return (T) getPaymentsTransformerInstance(beanName)
				.paymentSubmissionResponseTransformer(paymentConsentsResponse, paymentsPlatformResourceMap, methodType);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public PaymentSubmissionTransformer getPaymentsTransformerInstance(String beanName) {
		if (beanInstance == null)
			beanInstance = (PaymentSubmissionTransformer<T, U>) applicationContext.getBean(beanName);
		return beanInstance;
	}

	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	@SuppressWarnings("unchecked")
	@Override
	public U paymentSubmissionRequestTransformer(U paymentSubmissionRequest) {
		return (U) getPaymentsTransformerInstance(beanName)
				.paymentSubmissionRequestTransformer(paymentSubmissionRequest);
	}

}
