package com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Credit Card Account object
 */
@ApiModel(description = "Credit Card Account object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-02-07T14:38:09.445+05:30")

public class CreditCardAccount   {
  @JsonProperty("card")
  private CreditCardAccountCard card = null;

  @JsonProperty("creditLimit")
  private Balance creditLimit = null;

  @JsonProperty("statementClosingBalance")
  private Balance statementClosingBalance = null;

  public CreditCardAccount card(CreditCardAccountCard card) {
    this.card = card;
    return this;
  }

  /**
   * Get card
   * @return card
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public CreditCardAccountCard getCard() {
    return card;
  }

  public void setCard(CreditCardAccountCard card) {
    this.card = card;
  }

  public CreditCardAccount creditLimit(Balance creditLimit) {
    this.creditLimit = creditLimit;
    return this;
  }

  /**
   * Get creditLimit
   * @return creditLimit
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public Balance getCreditLimit() {
    return creditLimit;
  }

  public void setCreditLimit(Balance creditLimit) {
    this.creditLimit = creditLimit;
  }

  public CreditCardAccount statementClosingBalance(Balance statementClosingBalance) {
    this.statementClosingBalance = statementClosingBalance;
    return this;
  }

  /**
   * Get statementClosingBalance
   * @return statementClosingBalance
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public Balance getStatementClosingBalance() {
    return statementClosingBalance;
  }

  public void setStatementClosingBalance(Balance statementClosingBalance) {
    this.statementClosingBalance = statementClosingBalance;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreditCardAccount creditCardAccount = (CreditCardAccount) o;
    return Objects.equals(this.card, creditCardAccount.card) &&
        Objects.equals(this.creditLimit, creditCardAccount.creditLimit) &&
        Objects.equals(this.statementClosingBalance, creditCardAccount.statementClosingBalance);
  }

  @Override
  public int hashCode() {
    return Objects.hash(card, creditLimit, statementClosingBalance);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CreditCardAccount {\n");
    
    sb.append("    card: ").append(toIndentedString(card)).append("\n");
    sb.append("    creditLimit: ").append(toIndentedString(creditLimit)).append("\n");
    sb.append("    statementClosingBalance: ").append(toIndentedString(statementClosingBalance)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

