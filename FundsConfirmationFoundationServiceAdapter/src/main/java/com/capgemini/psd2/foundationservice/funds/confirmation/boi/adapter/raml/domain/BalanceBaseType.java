/*
 * Account Process API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain;

import java.util.Objects;

import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Balance object
 */
@ApiModel(description = "Balance object")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-10-23T10:49:07.015+05:30")
public class BalanceBaseType {
  @SerializedName("balanceType")
  private BalanceTypeEnum balanceType = null;

  @SerializedName("balanceAmount")
  private BalanceAmountBaseType balanceAmount = null;

  @SerializedName("balanceCurrency")
  private BalanceCurrencyBaseType balanceCurrency = null;

  @SerializedName("balanceDate")
  private String balanceDate = null;

  public BalanceBaseType balanceType(BalanceTypeEnum balanceType) {
    this.balanceType = balanceType;
    return this;
  }

   /**
   * Get balanceType
   * @return balanceType
  **/
  @ApiModelProperty(required = true, value = "")
  public BalanceTypeEnum getBalanceType() {
    return balanceType;
  }

  public void setBalanceType(BalanceTypeEnum balanceType) {
    this.balanceType = balanceType;
  }

  public BalanceBaseType balanceAmount(BalanceAmountBaseType balanceAmount) {
    this.balanceAmount = balanceAmount;
    return this;
  }

   /**
   * Get balanceAmount
   * @return balanceAmount
  **/
  @ApiModelProperty(required = true, value = "")
  public BalanceAmountBaseType getBalanceAmount() {
    return balanceAmount;
  }

  public void setBalanceAmount(BalanceAmountBaseType balanceAmount) {
    this.balanceAmount = balanceAmount;
  }

  public BalanceBaseType balanceCurrency(BalanceCurrencyBaseType balanceCurrency) {
    this.balanceCurrency = balanceCurrency;
    return this;
  }

   /**
   * Get balanceCurrency
   * @return balanceCurrency
  **/
  @ApiModelProperty(required = true, value = "")
  public BalanceCurrencyBaseType getBalanceCurrency() {
    return balanceCurrency;
  }

  public void setBalanceCurrency(BalanceCurrencyBaseType balanceCurrency) {
    this.balanceCurrency = balanceCurrency;
  }

  public BalanceBaseType balanceDate(String balanceDate) {
    this.balanceDate = balanceDate;
    return this;
  }

   /**
   * 
   * @return balanceDate
  **/
  @ApiModelProperty(value = "")
  public String getBalanceDate() {
    return balanceDate;
  }

  public void setBalanceDate(String balanceDate) {
    this.balanceDate = balanceDate;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BalanceBaseType balanceBaseType = (BalanceBaseType) o;
    return Objects.equals(this.balanceType, balanceBaseType.balanceType) &&
        Objects.equals(this.balanceAmount, balanceBaseType.balanceAmount) &&
        Objects.equals(this.balanceCurrency, balanceBaseType.balanceCurrency) &&
        Objects.equals(this.balanceDate, balanceBaseType.balanceDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(balanceType, balanceAmount, balanceCurrency, balanceDate);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BalanceBaseType {\n");
    
    sb.append("    balanceType: ").append(toIndentedString(balanceType)).append("\n");
    sb.append("    balanceAmount: ").append(toIndentedString(balanceAmount)).append("\n");
    sb.append("    balanceCurrency: ").append(toIndentedString(balanceCurrency)).append("\n");
    sb.append("    balanceDate: ").append(toIndentedString(balanceDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

