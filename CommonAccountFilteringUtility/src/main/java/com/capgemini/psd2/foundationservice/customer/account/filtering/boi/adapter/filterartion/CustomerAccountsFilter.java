package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.AccountEntitlements2;
import com.capgemini.psd2.logger.PSD2Constants;

/**
 * The Class CustomerAccountProfileFoundationServiceTransformer
 *
 */

@Component
@ConfigurationProperties(prefix = "foundationService")
@Configuration
@EnableAutoConfiguration
public class CustomerAccountsFilter  {
	
	/** The Account filters */
	private Map<String , Map<String,List<String>>> accountFiltering = new HashMap<>();

	public List<AccountEntitlements> getFilteredAccounts(List<AccountEntitlements> inputResObject, PartyEntitlements partyEntitlements,  Map<String, String> params) {
		
		/** Configuring the chain of responsibility filters */
		//FilterationChain jurisdictionFilter = new JurisdictionFilter();
		FilterationChain accountTypeFilter = new AccountTypeFilter();
		FilterationChain permissionFilter = new PermissionFilter();
		//jurisdictionFilter.setNext(permissionFilter);
		accountTypeFilter.setNext(permissionFilter);
		String consentFlowType = params.get(PSD2Constants.CONSENT_FLOW_TYPE);
		return accountTypeFilter.process(inputResObject, partyEntitlements, consentFlowType, accountFiltering);
	}
	
public List<AccountEntitlements2> getFilteredAccountsPISP(List<AccountEntitlements2> inputResObject, com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyEntitlements partyEntitlements,  Map<String, String> params) {
		
		/** Configuring the chain of responsibility filters */
		//FilterationChain jurisdictionFilter = new JurisdictionFilter();
		FilterationChain accountTypeFilter = new AccountTypeFilter();
		FilterationChain permissionFilter = new PermissionFilter();
		//jurisdictionFilter.setNext(permissionFilter);
		accountTypeFilter.setNext(permissionFilter);
		String consentFlowType = params.get(PSD2Constants.CONSENT_FLOW_TYPE);
		return accountTypeFilter.processPISP(inputResObject, partyEntitlements, consentFlowType, accountFiltering);
	}
	
	public Map<String, Map<String, List<String>>> getAccountFiltering() {
		return accountFiltering;
	}
	public void setAccountFiltering(Map<String, Map<String, List<String>>> accountFiltering) {
		this.accountFiltering = accountFiltering;
	}

}