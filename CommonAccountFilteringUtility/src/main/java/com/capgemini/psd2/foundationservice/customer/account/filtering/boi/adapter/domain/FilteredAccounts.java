package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain;

import java.util.List;

public class FilteredAccounts extends Accnts{
	
		public void setAccount(List<Accnt> account) {
			this.account = account;
		}

		public void setCreditCardAccount(List<CreditCardAccnt> creditCardAccount) {
			this.creditCardAccount = creditCardAccount;
		}

}
