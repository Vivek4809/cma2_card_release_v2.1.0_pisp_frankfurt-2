package com.capgemini.psd2.authentication.application.mule.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.CustomLoginResponse;




public interface NewBOLAuthenticationRepository extends MongoRepository<CustomLoginResponse, String>{
	
	public CustomLoginResponse findByDigitalUserIdentifier(String Id);
	
}
