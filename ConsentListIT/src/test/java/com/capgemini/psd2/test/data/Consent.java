package com.capgemini.psd2.test.data;

import java.util.List;

import org.springframework.data.annotation.Id;

import com.capgemini.psd2.aisp.domain.Data1.PermissionsEnum;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class Consent {

	/** The consent id. */
	@Id
	private String consentId;

	/** The psu id. */
	private String psuId;

	/** The tpp CID. */
	@JsonIgnore
	private String tppCId;

	private String tppLegalEntityName;

	/** The account details. */
	private List<AccountDetails> accountDetails;

	private String startDate;

	private String endDate;

	private String consentCreationDate;

	private String status;

	private String accountRequestId;

	private String transactionFromDateTime;

	private String transactionToDateTime;

	private List<PermissionsEnum> permissions;

	public List<PermissionsEnum> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<PermissionsEnum> permissions) {
		this.permissions = permissions;
	}

	/**
	 * Gets the consent id.
	 *
	 * @return the consent id
	 */
	public String getConsentId() {
		return consentId;
	}

	/**
	 * Sets the consent id.
	 *
	 * @param consentId
	 *            the new consent id
	 */
	public void setConsentId(String consentId) {
		this.consentId = consentId;
	}

	/**
	 * Gets the psu id.
	 *
	 * @return the psu id
	 */
	public String getPsuId() {
		return psuId;
	}

	/**
	 * Sets the psu id.
	 *
	 * @param psuId
	 *            the new psu id
	 */
	public void setPsuId(String psuId) {
		this.psuId = psuId;
	}

	/**
	 * Gets the account details.
	 *
	 * @return the account details
	 */
	public List<AccountDetails> getAccountDetails() {
		return accountDetails;
	}

	/**
	 * Sets the account details.
	 *
	 * @param accountDetails
	 *            the new account details
	 */
	public void setAccountDetails(List<AccountDetails> accountDetails) {
		this.accountDetails = accountDetails;
	}

	public String getTppCId() {
		return tppCId;
	}

	public void setTppCId(String tppCId) {
		this.tppCId = tppCId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransactionFromDateTime() {
		return transactionFromDateTime;
	}

	public void setTransactionFromDateTime(String transactionFromDateTime) {
		this.transactionFromDateTime = transactionFromDateTime;
	}

	public String getTransactionToDateTime() {
		return transactionToDateTime;
	}

	public void setTransactionToDateTime(String transactionToDateTime) {
		this.transactionToDateTime = transactionToDateTime;
	}

	public String getAccountRequestId() {
		return accountRequestId;
	}

	public void setAccountRequestId(String accountRequestId) {
		this.accountRequestId = accountRequestId;
	}

	public String getConsentCreationDate() {
		return consentCreationDate;
	}

	public void setConsentCreationDate(String consentCreationDate) {
		this.consentCreationDate = consentCreationDate;
	}

	public String getTppLegalEntityName() {
		return tppLegalEntityName;
	}

	public void setTppLegalEntityName(String tppLegalEntityName) {
		this.tppLegalEntityName = tppLegalEntityName;
	}

}
