package com.capgemini.psd2.account.request.routing.adapter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.account.request.routing.adapter.impl.AccountRequestRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;

@Configuration
public class AccountRequestRoutingAdapterConfig {

	@Bean
	public AccountRequestAdapter accountRequestRoutingAdapter(){
		return new AccountRequestRoutingAdapter();
	}
}
