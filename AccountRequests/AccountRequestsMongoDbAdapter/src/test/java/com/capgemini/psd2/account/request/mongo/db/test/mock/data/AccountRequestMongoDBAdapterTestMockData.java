package com.capgemini.psd2.account.request.mongo.db.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBExternalPermissions1Code;
import com.capgemini.psd2.aisp.domain.OBExternalRequestStatus1Code;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.aisp.domain.OBReadDataResponse1;
import com.capgemini.psd2.aisp.domain.OBReadDataResponse1.PermissionsEnum;
import com.capgemini.psd2.aisp.domain.OBReadDataResponse1.StatusEnum;

public class AccountRequestMongoDBAdapterTestMockData {

	public static OBReadConsentResponse1Data getData() {
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		List<OBExternalPermissions1Code> permissions = new ArrayList<>();
		permissions.add(OBExternalPermissions1Code.READACCOUNTSDETAIL);
		permissions.add(OBExternalPermissions1Code.READBALANCES);
		data.setCmaVersion("2.0");
		data.setPermissions(permissions);
		data.setExpirationDateTime("2017-05-05T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00-00:00");
		data.setConsentId("12345");
		data.setCreationDateTime("2017-05-04T00:00:00-00:00");
		data.setTppCID("12345");
		data.setStatus(OBExternalRequestStatus1Code.AWAITINGAUTHORISATION);
		return data;
	}
	
	public static OBReadDataResponse1 getDataResponse() {
		OBReadDataResponse1 data = new OBReadDataResponse1();
		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(PermissionsEnum.READBALANCES);
		data.setCmaVersion("2.0");
		data.setPermissions(permissions);
		data.setExpirationDateTime("2017-05-05T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00-00:00");
		data.setAccountRequestId("12345");
		data.setCreationDateTime("2017-05-04T00:00:00-00:00");
		data.setTppCID("12345");
		data.setStatus(StatusEnum.AWAITINGAUTHORISATION);
		return data;
	}
	
	public static OBReadConsentResponse1Data getDataInvalidTppAccess() {
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		List<OBExternalPermissions1Code> permissions = new ArrayList<>();
		permissions.add(OBExternalPermissions1Code.READACCOUNTSDETAIL);
		permissions.add(OBExternalPermissions1Code.READBALANCES);
		data.setPermissions(permissions);
		data.setExpirationDateTime("2017-05-05T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00-00:00");
		data.setConsentId("12345");
		data.setCreationDateTime("2017-05-04T00:00:00-00:00");
		data.setTppCID("12345");
		data.setStatus(OBExternalRequestStatus1Code.AWAITINGAUTHORISATION);
		return data;
	}

}
