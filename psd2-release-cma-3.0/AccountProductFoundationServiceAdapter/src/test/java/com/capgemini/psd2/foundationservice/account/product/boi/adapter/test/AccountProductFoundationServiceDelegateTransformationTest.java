package com.capgemini.psd2.foundationservice.account.product.boi.adapter.test;
/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountProductsResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.client.AccountProductFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.delegate.AccountProductFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.raml.domain.Product;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.transformer.AccountProductFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

/**
 * The Class AccountInformationFoundationServiceDelegateTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountProductFoundationServiceDelegateTransformationTest {

	/** The delegate. */
	@InjectMocks
	private AccountProductFoundationServiceDelegate delegate;

	/** The account information foundation service client. */
	@InjectMocks
	private AccountProductFoundationServiceClient accountProductFoundationServiceClient;

	/** The account information FS transformer. */
	@Mock
	private AccountProductFoundationServiceTransformer accountProductFSTransformer;

	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;

	/** The adapterUtility. */
	@Mock
	private AdapterUtility adapterUtility;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	/**
	 * Test rest transport for single account information.
	 */
	@Test
	public void testRestTransportForSingleAccountInformation() {
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");

		Account acc = new Account();

		Product product = new Product();

		product.setProductCode("123");
		product.setProductName("test");
		product.setProductDescription("BCA");

		acc.setProduct(product);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");

		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(acc);

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("http://localhost:9087/fs-abt-service/services/account");

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-WORKSTATION", "header workstation");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");

		accountProductFoundationServiceClient.restTransportForAccountProduct(requestInfo, Account.class, httpHeaders);
	}

	/**
	 * Test get foundation service URL.
	 */
	@Test
	public void testGetFoundationServiceURL() {

		String baseURL = "https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.30/core-banking/p/abt";
		String version = "v1.0";
		String nsc = "123";
		String accountNumber = "123";

		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(baseURL);
		delegate.getFoundationServiceURL(baseURL, version, nsc, accountNumber);
	}

	/**
	 * Test get foundation service with account NSC as null.
	 */
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithAccountNSCAsNull() {

		String baseURL = "https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.30/core-banking/p/abt";
		String version = "v1.0";
		String nsc = null;
		String accountNumber = "123";

		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(baseURL);
		delegate.getFoundationServiceURL(baseURL, version, nsc, accountNumber);
	}

	/**
	 * Test get foundation service with account number as null.
	 */
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithAccountNumberAsNull() {

		String baseURL = null;
		String version = "v1.0";
		String nsc = "123";
		String accountNumber = "123";

		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(baseURL);
		delegate.getFoundationServiceURL(baseURL, version, nsc, accountNumber);
	}

	/**
	 * Test get foundation service with account number as null.
	 */

	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithChannelIdAsNull() {

		String baseURL = "https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.30/core-banking/p/abt";
		String version = "v1.0";
		String nsc = "123";
		String accountNumber = null;

		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(baseURL);
		delegate.getFoundationServiceURL(baseURL, version, nsc, accountNumber);
	}

	/**
	 * Test create request headers actual.
	 */
	@Test
	public void testCreateRequestHeadersActual() {

		AccountMapping accountMapping = new AccountMapping();

		accountMapping.setPsuId("userId");
		accountMapping.setCorrelationId("correlationId");

		ReflectionTestUtils.setField(delegate, "userInReqHeadeRaml", "x-api-source-user");
		ReflectionTestUtils.setField(delegate, "channelInReqHeaderRaml", "x-api-channel-Code");
		ReflectionTestUtils.setField(delegate, "platformInReqHeaderRaml", "x-api-source-system");
		ReflectionTestUtils.setField(delegate, "transactionReqHeaderRaml", "x-api-transaction-id");
		ReflectionTestUtils.setField(delegate, "correlationReqHeaderRaml", "x-api-correlation-id");

		Map<String, String> params = new HashMap<>();
		params.put("channelId", "channel123");

		delegate.createRequestHeaders(new RequestInfo(), accountMapping, params);
	}

	/**
	 * Test transform response from FD to API.
	 */
	@Test
	public void testTransformResponseFromFDToAPI() {

		Account acc = new Account();

		Product product = new Product();

		product.setProductName("test");
		product.setProductDescription("BCA");

		acc.setProduct(product);

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		Mockito.when(delegate.transformResponseFromFDToAPI(anyObject(), anyObject()))
				.thenReturn(new PlatformAccountProductsResponse());
		PlatformAccountProductsResponse res = delegate.transformResponseFromFDToAPI(acc, new HashMap<String, String>());
		assertNotNull(res);

	}
}