package com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.transformer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.constants.PispScaConsentFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.FraudSystemResponse;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.PaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.ProposalStatus;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.ScheduledPaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsValidationResponse;
import com.capgemini.psd2.pisp.stage.domain.AmountDetails;
import com.capgemini.psd2.pisp.stage.domain.ChargeDetails;
import com.capgemini.psd2.pisp.stage.domain.CreditorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomDSOData;
import com.capgemini.psd2.pisp.stage.domain.CustomDSPData;
import com.capgemini.psd2.pisp.stage.domain.CustomDebtorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.stage.domain.CustomPreAuthorizeAdditionalInfo;
import com.capgemini.psd2.pisp.stage.domain.RemittanceDetails;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component
public class PispScaConsentFoundationServiceTransformer {

	@Autowired
	private PSD2Validator psd2Validator;

	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	@Value("${foundationService.endToEndIdentificationMockValue}")
	private String endToEndIdentificationMockValue;

	public <T> CustomConsentAppViewData transformPispScaConsentResponse(T inputBalanceObj, Map<String, String> params) {

		/* Root Elements */
		CustomConsentAppViewData customConsentAppViewData = new CustomConsentAppViewData();
		PaymentInstructionProposal paymentIns = (PaymentInstructionProposal) inputBalanceObj;

		customConsentAppViewData.setPaymentType(String.valueOf(paymentIns.getPaymentType()));

		CustomDebtorDetails debtorDetails = new CustomDebtorDetails();
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisingPartyAccount())) {

			debtorDetails.setIdentification(paymentIns.getAuthorisingPartyAccount().getAccountIdentification());
			debtorDetails.setName(paymentIns.getAuthorisingPartyAccount().getAccountName());
			debtorDetails.setSchemeName(paymentIns.getAuthorisingPartyAccount().getSchemeName());
			// debtorDetails.setSchemeNameEnum(paymentIns.getAuthorisingPartyAccount().getSchemeName());

			if (paymentIns.getAuthorisingPartyAccount().getSecondaryIdentification() != null) {
				debtorDetails.setSecondaryIdentification(
						paymentIns.getAuthorisingPartyAccount().getSecondaryIdentification());
			}

			customConsentAppViewData.setDebtorDetails(debtorDetails);
		}

		CreditorDetails creditorDetails = new CreditorDetails();
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyAccount())) {
			creditorDetails.setIdentification(paymentIns.getProposingPartyAccount().getAccountIdentification());
			creditorDetails.setName(paymentIns.getProposingPartyAccount().getAccountName());
			creditorDetails.setSchemeName(paymentIns.getProposingPartyAccount().getSchemeName());
			if (paymentIns.getProposingPartyAccount().getSecondaryIdentification() != null) {
				creditorDetails
						.setSecondaryIdentification(paymentIns.getProposingPartyAccount().getSecondaryIdentification());
			}
			customConsentAppViewData.setCreditorDetails(creditorDetails);
		}

		ChargeDetails chargeDetails = null;
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getCharges())) {
			chargeDetails = new ChargeDetails();
			List<OBCharge1> obcharge1list = new ArrayList<>();
			chargeDetails.setChargesList(obcharge1list);
			customConsentAppViewData.setChargeDetails(chargeDetails);

			List<PaymentInstructionCharge> paymChargeList = paymentIns.getCharges();
			for (PaymentInstructionCharge pymtCharge : paymChargeList) {
				OBCharge1 obcharge1 = new OBCharge1();
				OBCharge1Amount ob = new OBCharge1Amount();
				ob.setAmount(String.valueOf(pymtCharge.getAmount().getTransactionCurrency()));
				ob.setCurrency(pymtCharge.getCurrency().getIsoAlphaCode());
				obcharge1.setAmount(ob);
				obcharge1.setChargeBearer(
						OBChargeBearerType1Code.fromValue(String.valueOf(pymtCharge.getChargeBearer())));
				obcharge1.setType(pymtCharge.getType());
				obcharge1list.add(obcharge1);
			}

		}

		RemittanceDetails remittanceDetails = new RemittanceDetails();
		if (paymentIns.getAuthorisingPartyReference() != null) {
			remittanceDetails.setReference(paymentIns.getAuthorisingPartyReference());
		}
		if (paymentIns.getAuthorisingPartyUnstructuredReference() != null) {
			remittanceDetails.setUnstructured(paymentIns.getAuthorisingPartyUnstructuredReference());
		}
		customConsentAppViewData.setRemittanceDetails(remittanceDetails);

		AmountDetails amountDetails = new AmountDetails();
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getFinancialEventAmount())
				&& !NullCheckUtils.isNullOrEmpty(paymentIns.getTransactionCurrency())) {
			amountDetails.setAmount(String.valueOf(paymentIns.getFinancialEventAmount().getTransactionCurrency()));
			amountDetails.setCurrency(paymentIns.getTransactionCurrency().getIsoAlphaCode());
			customConsentAppViewData.setAmountDetails(amountDetails);
		}

		psd2Validator.validate(customConsentAppViewData);
		return customConsentAppViewData;

	}

	public <T> CustomConsentAppViewData transformPispScaConsentResponseScheduled(T inputBalanceObj,
			Map<String, String> params) {

		/* Root Elements */
		CustomConsentAppViewData customConsentAppViewData = new CustomConsentAppViewData();

		ScheduledPaymentInstructionProposal paymentIns = (ScheduledPaymentInstructionProposal) inputBalanceObj;

		CustomDSPData customDSPData = new CustomDSPData();
		customConsentAppViewData.setCustomDSPData(customDSPData);

		if (paymentIns.getRequestedExecutionDateTime() != null)
		{
			customDSPData.setRequestedExecutionDateTime(paymentIns.getRequestedExecutionDateTime());
		} 
		CustomDebtorDetails debtorDetails = new CustomDebtorDetails();
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisingPartyAccount())) {

			debtorDetails.setIdentification(paymentIns.getAuthorisingPartyAccount().getAccountIdentification());
			debtorDetails.setName(paymentIns.getAuthorisingPartyAccount().getAccountName());
			debtorDetails.setSchemeName(paymentIns.getAuthorisingPartyAccount().getSchemeName());

			if (paymentIns.getAuthorisingPartyAccount().getSecondaryIdentification() != null) {
				debtorDetails.setSecondaryIdentification(
						paymentIns.getAuthorisingPartyAccount().getSecondaryIdentification());
			}

			customConsentAppViewData.setDebtorDetails(debtorDetails);
		}

		CreditorDetails creditorDetails = new CreditorDetails();
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyAccount())) {
			creditorDetails.setIdentification(paymentIns.getProposingPartyAccount().getAccountIdentification());
			creditorDetails.setName(paymentIns.getProposingPartyAccount().getAccountName());
			creditorDetails.setSchemeName(paymentIns.getProposingPartyAccount().getSchemeName());
			if (paymentIns.getProposingPartyAccount().getSecondaryIdentification() != null) {
				creditorDetails
						.setSecondaryIdentification(paymentIns.getProposingPartyAccount().getSecondaryIdentification());
			}
			customConsentAppViewData.setCreditorDetails(creditorDetails);
		}

		ChargeDetails chargeDetails = null;
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getCharges())) {

			chargeDetails = new ChargeDetails();
			List<OBCharge1> obcharge1list = new ArrayList<>();
			chargeDetails.setChargesList(obcharge1list);
			customConsentAppViewData.setChargeDetails(chargeDetails);

			List<PaymentInstructionCharge> paymChargeList = paymentIns.getCharges();
			for (PaymentInstructionCharge pymtCharge : paymChargeList) {
				OBCharge1 obcharge1 = new OBCharge1();
				OBCharge1Amount ob = new OBCharge1Amount();
				ob.setAmount(String.valueOf(pymtCharge.getAmount().getTransactionCurrency()));
				ob.setCurrency(pymtCharge.getCurrency().getIsoAlphaCode());
				obcharge1.setAmount(ob);
				obcharge1.setChargeBearer(
						OBChargeBearerType1Code.fromValue(String.valueOf(pymtCharge.getChargeBearer())));
				obcharge1.setType(pymtCharge.getType());
				obcharge1list.add(obcharge1);
			}

		}

		RemittanceDetails remittanceDetails = new RemittanceDetails();
		if (paymentIns.getAuthorisingPartyReference() != null) {
			remittanceDetails.setReference(paymentIns.getAuthorisingPartyReference());
		}
		if (paymentIns.getAuthorisingPartyUnstructuredReference() != null) {
			remittanceDetails.setUnstructured(paymentIns.getAuthorisingPartyUnstructuredReference());
		}
		customConsentAppViewData.setRemittanceDetails(remittanceDetails);

		AmountDetails amountDetails = new AmountDetails();
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getFinancialEventAmount())
				&& !NullCheckUtils.isNullOrEmpty(paymentIns.getTransactionCurrency())) {
			amountDetails.setAmount(String.valueOf(paymentIns.getFinancialEventAmount().getTransactionCurrency()));
			amountDetails.setCurrency(paymentIns.getTransactionCurrency().getIsoAlphaCode());
			customConsentAppViewData.setAmountDetails(amountDetails);
		}

		psd2Validator.validate(customConsentAppViewData);
		return customConsentAppViewData;

	}

	public PaymentInstructionProposal transformDomesticConsentResponseFromAPIToFDForInsert(
			OBCashAccountDebtor3 selectedDebtorDetails, CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo,
			CustomPaymentStageIdentifiers stageIdentifiers, PaymentInstructionProposal paymentInstructionProposal,
			Map<String, String> params) {

		AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();

		if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails)) {

			if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getSchemeName())) {
				authorisingPartyAccount.setSchemeName(selectedDebtorDetails.getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getIdentification())) {
				authorisingPartyAccount.setAccountIdentification(selectedDebtorDetails.getIdentification());

			}

			if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getName())) {
				authorisingPartyAccount.setAccountName(selectedDebtorDetails.getName());
			}

			if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getSecondaryIdentification())) {
				authorisingPartyAccount.setSecondaryIdentification(selectedDebtorDetails.getSecondaryIdentification());
			}
		}
		paymentInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);

		return paymentInstructionProposal;

	}

	public <T> PaymentConsentsValidationResponse transformDomesticConsentResponseFromFDToAPIForInsert(
			T paymentInstructionProposalResponse, CustomPaymentStageIdentifiers stageIdentifiers,
			Map<String, String> params) {

		PaymentInstructionProposal paymentIns = (PaymentInstructionProposal) paymentInstructionProposalResponse;
		PaymentConsentsValidationResponse paymentConsentsValidationResponse = new PaymentConsentsValidationResponse();

		if (!NullCheckUtils.isNullOrEmpty(stageIdentifiers)) {

			if (!NullCheckUtils.isNullOrEmpty(stageIdentifiers.getPaymentSubmissionId())) {
				paymentConsentsValidationResponse.setPaymentSubmissionId(stageIdentifiers.getPaymentSubmissionId());
			}
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentIns)) {

			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposalStatus())) {
				paymentConsentsValidationResponse.setPaymentSetupValidationStatus(
						OBTransactionIndividualStatus1Code.fromValue(paymentIns.getProposalStatus().toString()));
			}
		}
		// psd2Validator.validate(paymentConsentsValidationResponse);
		return paymentConsentsValidationResponse;
	}

	public PaymentInstructionProposal transformPispScaConsentResponseForUpdate(CustomPaymentStageUpdateData updateData,
			PaymentInstructionProposal paymentInstructionProposal, Map<String, String> params) {
		if (!NullCheckUtils.isNullOrEmpty(updateData)) {

			if (updateData.getSetupStatusUpdated() == true) {
				paymentInstructionProposal.setProposalStatusUpdateDatetime(updateData.getSetupStatusUpdateDateTime());
				
				if(updateData.getSetupStatus().equalsIgnoreCase("AcceptedCustomerProfile")){
					paymentInstructionProposal.setProposalStatus(ProposalStatus.AcceptedCustomerProfile);
				}
				else{
					paymentInstructionProposal.setProposalStatus(ProposalStatus.fromValue(updateData.getSetupStatus()));
				}
			}

			if (!NullCheckUtils.isNullOrEmpty(updateData.getFraudScore())) {
				FraudServiceResponse fraud = (FraudServiceResponse) updateData.getFraudScore();
				if (updateData.getFraudScoreUpdated() == true) {
					paymentInstructionProposal
							.setFraudSystemResponse(FraudSystemResponse.valueOf(fraud.getDecisionType()));
					System.out.println("FraudSystemResponse" + FraudSystemResponse.valueOf(fraud.getDecisionType()));
				}

			}
			if (updateData.getDebtorDetailsUpdated() == true) {
				
				
				if  (!NullCheckUtils.isNullOrEmpty(updateData.getDebtorDetails())) {
				AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
				paymentInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);
					System.out.println("Debtor details" + updateData.getDebtorDetails());
				paymentInstructionProposal.getAuthorisingPartyAccount()
						.setSchemeName(updateData.getDebtorDetails().getSchemeName());
				paymentInstructionProposal.getAuthorisingPartyAccount()
						.setAccountIdentification(updateData.getDebtorDetails().getIdentification());
				paymentInstructionProposal.getAuthorisingPartyAccount()
						.setAccountName(updateData.getDebtorDetails().getName());
				paymentInstructionProposal.getAuthorisingPartyAccount()
						.setSecondaryIdentification(updateData.getDebtorDetails().getSecondaryIdentification());
			}
			}
		}
		return paymentInstructionProposal;
	}

	public ScheduledPaymentInstructionProposal transformDomesticScheduledConsentResponseFromAPIToFDForInsert(
			OBCashAccountDebtor3 selectedDebtorDetails, CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo,
			CustomPaymentStageIdentifiers stageIdentifiers,
			ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal, Map<String, String> params) {

		AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
		scheduledPaymentInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);
		
		if (!NullCheckUtils.isNullOrEmpty(scheduledPaymentInstructionProposal.getInstructionEndToEndReference()))
		{
			scheduledPaymentInstructionProposal.setInstructionEndToEndReference(scheduledPaymentInstructionProposal.getInstructionEndToEndReference());
		}else
		{
			scheduledPaymentInstructionProposal.setInstructionEndToEndReference(endToEndIdentificationMockValue);
		}
		
		if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails)) {

			if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getSchemeName())) {
				authorisingPartyAccount.setSchemeName(selectedDebtorDetails.getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getIdentification())) {
				authorisingPartyAccount.setAccountIdentification(selectedDebtorDetails.getIdentification());
			}

			if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getName())) {
				authorisingPartyAccount.setAccountName(selectedDebtorDetails.getName());
			}

			if (!NullCheckUtils.isNullOrEmpty(selectedDebtorDetails.getSecondaryIdentification())) {
				authorisingPartyAccount.setSecondaryIdentification(selectedDebtorDetails.getSecondaryIdentification());
			}
		}
		

		return scheduledPaymentInstructionProposal;

	}

	public <T> PaymentConsentsValidationResponse transformDomesticScheduledConsentResponseFromFDToAPIForInsert(
			T paymentInstructionProposalResponse, CustomPaymentStageIdentifiers stageIdentifiers,
			Map<String, String> params) {

		ScheduledPaymentInstructionProposal paymentIns = (ScheduledPaymentInstructionProposal) paymentInstructionProposalResponse;
		PaymentConsentsValidationResponse paymentConsentsValidationResponse = new PaymentConsentsValidationResponse();

		if (!NullCheckUtils.isNullOrEmpty(stageIdentifiers)) {

			if (!NullCheckUtils.isNullOrEmpty(stageIdentifiers.getPaymentSubmissionId())) {
				paymentConsentsValidationResponse.setPaymentSubmissionId(stageIdentifiers.getPaymentSubmissionId());
			}
		}
	
		return paymentConsentsValidationResponse;
	}

	public ScheduledPaymentInstructionProposal transformPispScaConsentScheduledResponseForUpdate(
			CustomPaymentStageUpdateData updateData,
			ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal, Map<String, String> params) {

		if (!NullCheckUtils.isNullOrEmpty(scheduledPaymentInstructionProposal.getInstructionEndToEndReference()))
		{
			scheduledPaymentInstructionProposal.setInstructionEndToEndReference(scheduledPaymentInstructionProposal.getInstructionEndToEndReference());
		}else
		{
			scheduledPaymentInstructionProposal.setInstructionEndToEndReference(endToEndIdentificationMockValue);
		}

		if (!NullCheckUtils.isNullOrEmpty(updateData)) {
			if (updateData.getSetupStatusUpdated() == true) {
				scheduledPaymentInstructionProposal.setProposalStatusUpdateDatetime(
						updateData.getSetupStatusUpdateDateTime());
				scheduledPaymentInstructionProposal
				.setProposalStatus(ProposalStatus.fromValue(updateData.getSetupStatus()));
			}

			if (!NullCheckUtils.isNullOrEmpty(updateData.getFraudScore())) {

				FraudServiceResponse fraud = (FraudServiceResponse) updateData.getFraudScore();
				if (updateData.getFraudScoreUpdated() == true) {
					System.out.println("scheduledPaymentInstructionProposal"+FraudSystemResponse.fromValue(String.valueOf(fraud.getDecisionType())));
					scheduledPaymentInstructionProposal
					.setFraudSystemResponse(FraudSystemResponse.fromValue(String.valueOf(fraud.getDecisionType())));

				}
			}
			if (updateData.getDebtorDetailsUpdated() == true) {

				if (!NullCheckUtils.isNullOrEmpty(updateData.getDebtorDetails())) {
					System.out.println("updateData.getDebtorDetails()"+updateData.getDebtorDetails());
					AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
					scheduledPaymentInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);
					scheduledPaymentInstructionProposal.getAuthorisingPartyAccount()
					.setSchemeName(updateData.getDebtorDetails().getSchemeName());

					scheduledPaymentInstructionProposal.getAuthorisingPartyAccount()
					.setAccountIdentification(updateData.getDebtorDetails().getIdentification());
					scheduledPaymentInstructionProposal.getAuthorisingPartyAccount()
					.setAccountName(updateData.getDebtorDetails().getName());
					scheduledPaymentInstructionProposal.getAuthorisingPartyAccount()
					.setSecondaryIdentification(updateData.getDebtorDetails().getSecondaryIdentification());

				}
			}

		}
		System.out.println("scheduledPaymentInstructionProposal"+scheduledPaymentInstructionProposal);
		return scheduledPaymentInstructionProposal;
	}

	public <T> CustomConsentAppViewData transformPispScaConsentResponseForStandingOrders(T inputBalanceObj,
			Map<String, String> params) {

		CustomConsentAppViewData customConsentAppViewData = new CustomConsentAppViewData();
		StandingOrderInstructionProposal proposal = (StandingOrderInstructionProposal) inputBalanceObj;
		params.put("PaymentType", PispScaConsentFoundationServiceConstants.DOMESTIC_ST_ORD);
		customConsentAppViewData.setPaymentType(params.get("PaymentType"));

		CustomDebtorDetails debtorDetails = new CustomDebtorDetails();
		if (!NullCheckUtils.isNullOrEmpty(proposal.getAuthorisingPartyAccount())) {

			debtorDetails.setIdentification(proposal.getAuthorisingPartyAccount().getAccountIdentification());
			debtorDetails.setName(proposal.getAuthorisingPartyAccount().getAccountName());
			debtorDetails.setSchemeName(proposal.getAuthorisingPartyAccount().getSchemeName());

			if (proposal.getAuthorisingPartyAccount().getSecondaryIdentification() != null) {
				debtorDetails
						.setSecondaryIdentification(proposal.getAuthorisingPartyAccount().getSecondaryIdentification());
			}

			customConsentAppViewData.setDebtorDetails(debtorDetails);
		}

		CreditorDetails creditorDetails = new CreditorDetails();
		if (!NullCheckUtils.isNullOrEmpty(proposal.getProposingPartyAccount())) {
			creditorDetails.setIdentification(proposal.getProposingPartyAccount().getAccountIdentification());
			creditorDetails.setName(proposal.getProposingPartyAccount().getAccountName());
			creditorDetails.setSchemeName(proposal.getProposingPartyAccount().getSchemeName());
			if (proposal.getProposingPartyAccount().getSecondaryIdentification() != null) {
				creditorDetails
						.setSecondaryIdentification(proposal.getProposingPartyAccount().getSecondaryIdentification());
			}
			customConsentAppViewData.setCreditorDetails(creditorDetails);
		}

		ChargeDetails chargeDetails = new ChargeDetails();
		if (!NullCheckUtils.isNullOrEmpty(proposal.getCharges())) {

			List<OBCharge1> obcharge1list = new ArrayList<>();
			chargeDetails.setChargesList(obcharge1list);
			customConsentAppViewData.setChargeDetails(chargeDetails);

			List<PaymentInstructionCharge> paymChargeList = proposal.getCharges();
			for (PaymentInstructionCharge pymtCharge : paymChargeList) {
				OBCharge1 obcharge1 = new OBCharge1();
				OBCharge1Amount ob = new OBCharge1Amount();
				ob.setAmount(String.valueOf(pymtCharge.getAmount().getTransactionCurrency()));
				ob.setCurrency(pymtCharge.getCurrency().getIsoAlphaCode());
				obcharge1.setAmount(ob);
				obcharge1.setChargeBearer(
						OBChargeBearerType1Code.fromValue(String.valueOf(pymtCharge.getChargeBearer())));
				obcharge1.setType(pymtCharge.getType());
				obcharge1list.add(obcharge1);
			}

		}

		AmountDetails amountDetails = new AmountDetails();
		CustomDSOData customDSOData = new CustomDSOData();
		customDSOData.setFinalPaymentDateTime(
				timeZoneDateTimeAdapter.parseOffsetDateTimeToCMA(proposal.getFinalPaymentDateTime()));
		customDSOData.setFrequency(proposal.getFrequency());
		customDSOData.setFirstPaymentDateTime(
				timeZoneDateTimeAdapter.parseOffsetDateTimeToCMA(proposal.getFirstPaymentDateTime()));
		customDSOData.setNumberOfPayment(String.valueOf(proposal.getNumberOfPayments()));
		if (!NullCheckUtils.isNullOrEmpty(proposal.getRecurringPaymentAmount())) {
			amountDetails.setAmount(String
					.valueOf(proposal.getRecurringPaymentAmount().getFinancialEventAmount().getTransactionCurrency()));
			amountDetails.setCurrency(
					String.valueOf(proposal.getRecurringPaymentAmount().getTransactionCurrency().getIsoAlphaCode()));
			customDSOData.setRecurringChargeDetails(amountDetails);
		}
		if (!NullCheckUtils.isNullOrEmpty(proposal.getFinalPaymentAmount())) {
			amountDetails.setAmount(String
					.valueOf(proposal.getFinalPaymentAmount().getFinancialEventAmount().getTransactionCurrency()));
			amountDetails.setCurrency(
					String.valueOf(proposal.getFinalPaymentAmount().getTransactionCurrency().getIsoAlphaCode()));
			customDSOData.setFinalChargeDetails(amountDetails);
		}
		if (!NullCheckUtils.isNullOrEmpty(proposal.getFirstPaymentAmount())
				&& !NullCheckUtils.isNullOrEmpty(proposal.getFirstPaymentAmount().getFinancialEventAmount())
				&& !NullCheckUtils.isNullOrEmpty(proposal.getFirstPaymentAmount().getTransactionCurrency())) {
			amountDetails.setAmount(String
					.valueOf(proposal.getFirstPaymentAmount().getFinancialEventAmount().getTransactionCurrency()));
			amountDetails.setCurrency(proposal.getFirstPaymentAmount().getTransactionCurrency().getIsoAlphaCode());
			customConsentAppViewData.setAmountDetails(amountDetails);
		}

		customConsentAppViewData.setCustomDSOData(customDSOData);

		return customConsentAppViewData;
	}

	public CustomFraudSystemPaymentData transformFraudSystemDomesticPaymentStagedData(
			PaymentInstructionProposal paymentInstructionProposal, Map<String, String> params) {

		CustomFraudSystemPaymentData customFraudSystemPaymentData = new CustomFraudSystemPaymentData();

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getFinancialEventAmount())) {
			customFraudSystemPaymentData.setTransferAmount(
					String.valueOf(paymentInstructionProposal.getFinancialEventAmount().getTransactionCurrency()));
		}
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getTransactionCurrency())) {
			customFraudSystemPaymentData
					.setTransferCurrency(paymentInstructionProposal.getTransactionCurrency().getIsoAlphaCode());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getAuthorisingPartyReference())) {
			customFraudSystemPaymentData.setTransferMemo(paymentInstructionProposal.getAuthorisingPartyReference());
		}
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getProposingPartyAccount())) {
			CreditorDetails creditor = new CreditorDetails();
			creditor.setSchemeName(paymentInstructionProposal.getProposingPartyAccount().getSchemeName());
			creditor.setIdentification(
					paymentInstructionProposal.getProposingPartyAccount().getAccountIdentification());
			creditor.setName(paymentInstructionProposal.getProposingPartyAccount().getAccountName());
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getProposingPartyAccount().getSecondaryIdentification())) {
				creditor.setSecondaryIdentification(
						paymentInstructionProposal.getProposingPartyAccount().getSecondaryIdentification());
			}
			customFraudSystemPaymentData.setCreditorDetails(creditor);
		}
		Date transferTime = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		String transferDate = dateFormat.format(transferTime);
		customFraudSystemPaymentData.setTransferTime(transferDate);

		return customFraudSystemPaymentData;
	}

	public CustomFraudSystemPaymentData transformFraudSystemDomesticScheduledPaymentStagedData(
			ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal, Map<String, String> params) {

		CustomFraudSystemPaymentData customFraudSystemPaymentData = new CustomFraudSystemPaymentData();

		if (!NullCheckUtils.isNullOrEmpty(scheduledPaymentInstructionProposal.getFinancialEventAmount())) {
			customFraudSystemPaymentData.setTransferAmount(String
					.valueOf(scheduledPaymentInstructionProposal.getFinancialEventAmount().getTransactionCurrency()));
		}

		if (!NullCheckUtils.isNullOrEmpty(scheduledPaymentInstructionProposal.getTransactionCurrency())) {
			customFraudSystemPaymentData.setTransferCurrency(
					scheduledPaymentInstructionProposal.getTransactionCurrency().getIsoAlphaCode());
		}

		if (!NullCheckUtils.isNullOrEmpty(scheduledPaymentInstructionProposal.getAuthorisingPartyReference())) {
			customFraudSystemPaymentData
					.setTransferMemo(scheduledPaymentInstructionProposal.getAuthorisingPartyReference());
		}
		if (!NullCheckUtils.isNullOrEmpty(scheduledPaymentInstructionProposal.getProposingPartyAccount())) {
			CreditorDetails creditor = new CreditorDetails();
			creditor.setSchemeName(scheduledPaymentInstructionProposal.getProposingPartyAccount().getSchemeName());
			creditor.setIdentification(
					scheduledPaymentInstructionProposal.getProposingPartyAccount().getAccountIdentification());
			creditor.setName(scheduledPaymentInstructionProposal.getProposingPartyAccount().getAccountName());
			if (!NullCheckUtils.isNullOrEmpty(
					scheduledPaymentInstructionProposal.getProposingPartyAccount().getSecondaryIdentification())) {
				creditor.setSecondaryIdentification(
						scheduledPaymentInstructionProposal.getProposingPartyAccount().getSecondaryIdentification());
			}
			customFraudSystemPaymentData.setCreditorDetails(creditor);
		}
		Date transferTime = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		String transferDate = dateFormat.format(transferTime);
		customFraudSystemPaymentData.setTransferTime(transferDate);

		return customFraudSystemPaymentData;
	}

	public CustomFraudSystemPaymentData transformFraudSystemDomesticStandingOrderPaymentStagedData(
			StandingOrderInstructionProposal standingOrderInstructionProposal, Map<String, String> params) {

		CustomFraudSystemPaymentData customFraudSystemPaymentData = new CustomFraudSystemPaymentData();

		if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFirstPaymentAmount()
				.getFinancialEventAmount().getTransactionCurrency())) {
			customFraudSystemPaymentData.setTransferAmount(String.valueOf(standingOrderInstructionProposal
					.getFirstPaymentAmount().getFinancialEventAmount().getTransactionCurrency()));
		}
		if (!NullCheckUtils.isNullOrEmpty(
				standingOrderInstructionProposal.getFirstPaymentAmount().getTransactionCurrency().getIsoAlphaCode())) {
			customFraudSystemPaymentData.setTransferCurrency(standingOrderInstructionProposal.getFirstPaymentAmount()
					.getTransactionCurrency().getIsoAlphaCode());
		}

		if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getReference())) {
			customFraudSystemPaymentData.setTransferMemo(standingOrderInstructionProposal.getReference());
		}
		if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposingPartyAccount())) {
			CreditorDetails creditor = new CreditorDetails();
			creditor.setSchemeName(standingOrderInstructionProposal.getProposingPartyAccount().getSchemeName());
			creditor.setIdentification(
					standingOrderInstructionProposal.getProposingPartyAccount().getAccountIdentification());
			creditor.setName(standingOrderInstructionProposal.getProposingPartyAccount().getAccountName());
			if (!NullCheckUtils.isNullOrEmpty(
					standingOrderInstructionProposal.getProposingPartyAccount().getSecondaryIdentification())) {
				creditor.setSecondaryIdentification(
						standingOrderInstructionProposal.getProposingPartyAccount().getSecondaryIdentification());
			}
			customFraudSystemPaymentData.setCreditorDetails(creditor);
		}
		Date transferTime = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		String transferDate = dateFormat.format(transferTime);
		customFraudSystemPaymentData.setTransferTime(transferDate);

		return customFraudSystemPaymentData;
	}
}
