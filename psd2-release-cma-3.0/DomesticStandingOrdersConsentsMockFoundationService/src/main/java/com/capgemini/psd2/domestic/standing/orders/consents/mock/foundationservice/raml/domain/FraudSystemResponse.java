package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets fraudSystemResponse
 */
public enum FraudSystemResponse {
  
  APPROVE("Approve"),
  
  INVESTIGATE("Investigate"),
  
  REJECT("Reject");

  private String value;

  FraudSystemResponse(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static FraudSystemResponse fromValue(String text) {
    for (FraudSystemResponse b : FraudSystemResponse.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

