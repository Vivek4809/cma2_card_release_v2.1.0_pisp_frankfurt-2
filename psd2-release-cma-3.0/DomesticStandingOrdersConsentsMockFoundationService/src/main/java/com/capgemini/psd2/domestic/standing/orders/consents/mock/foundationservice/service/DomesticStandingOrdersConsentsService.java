package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.service;

import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain.StandingOrderInstructionProposal;

public interface DomesticStandingOrdersConsentsService {
 
	public StandingOrderInstructionProposal retrieveAccountInformation(String paymentInstructionProposalId) throws Exception;

	public StandingOrderInstructionProposal createDomesticStandingOrdersConsentsResource(StandingOrderInstructionProposal standingOrderInstructionProposalReq) throws Exception;
	
}
