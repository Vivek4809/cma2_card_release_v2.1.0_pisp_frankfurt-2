package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * ElectronicDeviceData
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class ElectronicDeviceData   {
  @JsonProperty("deviceFieldName")
  private String deviceFieldName = null;

  @JsonProperty("deviceFieldType")
  private DeviceFieldType deviceFieldType = null;

  @JsonProperty("deviceFieldValue")
  private String deviceFieldValue = null;

  public ElectronicDeviceData deviceFieldName(String deviceFieldName) {
    this.deviceFieldName = deviceFieldName;
    return this;
  }

  /**
   * Name of the metadata field
   * @return deviceFieldName
  **/
  @ApiModelProperty(required = true, value = "Name of the metadata field")
  @NotNull


  public String getDeviceFieldName() {
    return deviceFieldName;
  }

  public void setDeviceFieldName(String deviceFieldName) {
    this.deviceFieldName = deviceFieldName;
  }

  public ElectronicDeviceData deviceFieldType(DeviceFieldType deviceFieldType) {
    this.deviceFieldType = deviceFieldType;
    return this;
  }

  /**
   * Get deviceFieldType
   * @return deviceFieldType
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public DeviceFieldType getDeviceFieldType() {
    return deviceFieldType;
  }

  public void setDeviceFieldType(DeviceFieldType deviceFieldType) {
    this.deviceFieldType = deviceFieldType;
  }

  public ElectronicDeviceData deviceFieldValue(String deviceFieldValue) {
    this.deviceFieldValue = deviceFieldValue;
    return this;
  }

  /**
   * The value of the metadata field
   * @return deviceFieldValue
  **/
  @ApiModelProperty(required = true, value = "The value of the metadata field")
  @NotNull


  public String getDeviceFieldValue() {
    return deviceFieldValue;
  }

  public void setDeviceFieldValue(String deviceFieldValue) {
    this.deviceFieldValue = deviceFieldValue;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ElectronicDeviceData electronicDeviceData = (ElectronicDeviceData) o;
    return Objects.equals(this.deviceFieldName, electronicDeviceData.deviceFieldName) &&
        Objects.equals(this.deviceFieldType, electronicDeviceData.deviceFieldType) &&
        Objects.equals(this.deviceFieldValue, electronicDeviceData.deviceFieldValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(deviceFieldName, deviceFieldType, deviceFieldValue);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ElectronicDeviceData {\n");
    
    sb.append("    deviceFieldName: ").append(toIndentedString(deviceFieldName)).append("\n");
    sb.append("    deviceFieldType: ").append(toIndentedString(deviceFieldType)).append("\n");
    sb.append("    deviceFieldValue: ").append(toIndentedString(deviceFieldValue)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

