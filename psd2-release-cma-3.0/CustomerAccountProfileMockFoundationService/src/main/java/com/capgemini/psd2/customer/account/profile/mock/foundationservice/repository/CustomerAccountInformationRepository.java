package com.capgemini.psd2.customer.account.profile.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.domain.ChannelProfile;


public interface CustomerAccountInformationRepository extends MongoRepository<ChannelProfile,String> {

	public ChannelProfile findById(String id);

}
