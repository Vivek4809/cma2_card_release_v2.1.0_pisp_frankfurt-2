/*
 * Authentication Service API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.domain;

import java.time.OffsetDateTime;
import java.util.Objects;

import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Digital User Session object
 */
@ApiModel(description = "Digital User Session object")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-10-24T18:15:40.212+05:30")
public class DigitalUserSessionBaseType {
  @SerializedName("sessionInitiationFailureIndicator")
  private Boolean sessionInitiationFailureIndicator = null;

  @SerializedName("sessionInitiationFailureReasonCode")
  private String sessionInitiationFailureReasonCode = null;

  @SerializedName("sessionStartDateTime")
  private OffsetDateTime sessionStartDateTime = null;

  public DigitalUserSessionBaseType sessionInitiationFailureIndicator(Boolean sessionInitiationFailureIndicator) {
    this.sessionInitiationFailureIndicator = sessionInitiationFailureIndicator;
    return this;
  }

   /**
   * 
   * @return sessionInitiationFailureIndicator
  **/
  @ApiModelProperty(required = true, value = "")
  public Boolean isSessionInitiationFailureIndicator() {
    return sessionInitiationFailureIndicator;
  }

  public void setSessionInitiationFailureIndicator(Boolean sessionInitiationFailureIndicator) {
    this.sessionInitiationFailureIndicator = sessionInitiationFailureIndicator;
  }

  public DigitalUserSessionBaseType sessionInitiationFailureReasonCode(String sessionInitiationFailureReasonCode) {
    this.sessionInitiationFailureReasonCode = sessionInitiationFailureReasonCode;
    return this;
  }

   /**
   * 
   * @return sessionInitiationFailureReasonCode
  **/
  @ApiModelProperty(value = "")
  public String getSessionInitiationFailureReasonCode() {
    return sessionInitiationFailureReasonCode;
  }

  public void setSessionInitiationFailureReasonCode(String sessionInitiationFailureReasonCode) {
    this.sessionInitiationFailureReasonCode = sessionInitiationFailureReasonCode;
  }

  public DigitalUserSessionBaseType sessionStartDateTime(OffsetDateTime sessionStartDateTime) {
    this.sessionStartDateTime = sessionStartDateTime;
    return this;
  }

   /**
   * 
   * @return sessionStartDateTime
  **/
  @ApiModelProperty(value = "")
  public OffsetDateTime getSessionStartDateTime() {
    return sessionStartDateTime;
  }

  public void setSessionStartDateTime(OffsetDateTime sessionStartDateTime) {
    this.sessionStartDateTime = sessionStartDateTime;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DigitalUserSessionBaseType digitalUserSessionBaseType = (DigitalUserSessionBaseType) o;
    return Objects.equals(this.sessionInitiationFailureIndicator, digitalUserSessionBaseType.sessionInitiationFailureIndicator) &&
        Objects.equals(this.sessionInitiationFailureReasonCode, digitalUserSessionBaseType.sessionInitiationFailureReasonCode) &&
        Objects.equals(this.sessionStartDateTime, digitalUserSessionBaseType.sessionStartDateTime);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sessionInitiationFailureIndicator, sessionInitiationFailureReasonCode, sessionStartDateTime);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DigitalUserSessionBaseType {\n");
    
    sb.append("    sessionInitiationFailureIndicator: ").append(toIndentedString(sessionInitiationFailureIndicator)).append("\n");
    sb.append("    sessionInitiationFailureReasonCode: ").append(toIndentedString(sessionInitiationFailureReasonCode)).append("\n");
    sb.append("    sessionStartDateTime: ").append(toIndentedString(sessionStartDateTime)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

