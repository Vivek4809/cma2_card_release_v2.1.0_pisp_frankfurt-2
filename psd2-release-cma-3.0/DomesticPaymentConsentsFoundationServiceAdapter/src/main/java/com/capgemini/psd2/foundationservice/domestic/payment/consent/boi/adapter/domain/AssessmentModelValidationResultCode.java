package com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets assessmentModelValidationResultCode
 */
public enum AssessmentModelValidationResultCode {
  
  GREEN("Green"),
  
  AMBER("Amber"),
  
  RED("Red");

  private String value;

  AssessmentModelValidationResultCode(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static AssessmentModelValidationResultCode fromValue(String text) {
    for (AssessmentModelValidationResultCode b : AssessmentModelValidationResultCode.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

