package com.capgemini.psd2.foundationservice.domestic.payment.submission.boi.adapter.test;
/*package com.capgemini.psd2.foundationservice.domestic.payment.submission.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.OBAddressTypeCode;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.AddressCountry;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.Amount;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.AuthorisationType;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.ChargeBearer;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.CounterPartyAddress;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.Currency;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.DomesticpaymentinstructionproposalsAmount;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.PaymentInstruction5;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.PaymentInstructionProposal2;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.PaymentInstructionRiskFactorReference;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.PaymentInstructionStatusCode2;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.ProposalStatus;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.ProposingPartyPostalAddress;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.ScheduledPaymentInstructionComposite;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.TransactionCurrency;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.transformer.DomesticScheduledPaymentSubmissionFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.GenericPaymentConsentResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBExternalPermissions2Code;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticScheduled1;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticScheduledPaymentSubmissionFoundationServiceTransformerTest {
	
	@InjectMocks
	DomesticScheduledPaymentSubmissionFoundationServiceTransformer transformer;
	
	@Mock
	private PSD2Validator psd2Validator;
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	*//**
	 * Context loads.
	 *//*
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void  testtransformDomesticConsentResponseFromFDToAPIForInsert(){
		Map<String, String> params= new HashMap<>();
		
		
		ScheduledPaymentInstructionComposite composite = new ScheduledPaymentInstructionComposite();
		PaymentInstructionProposal2 paymentInstructionProposal=new PaymentInstructionProposal2();
		PaymentInstruction5 instruction5=new PaymentInstruction5();
		List<PaymentInstructionCharge> charges = new ArrayList<PaymentInstructionCharge>();
		PaymentInstructionCharge charge = new PaymentInstructionCharge();
		DomesticpaymentinstructionproposalsAmount amount = new DomesticpaymentinstructionproposalsAmount();
		Currency currency = new Currency(); 
		
		currency.setIsoAlphaCode("pata nai");
		amount.setTransactionCurrency(new BigDecimal(22.00));
		
		charge.setAmount(amount);
		charge.setCurrency(currency);
		charges.add(charge);
		
		paymentInstructionProposal.setCharges(charges);
		paymentInstructionProposal.setPaymentInstructionProposalId("paymentInstructionProposalId");
		instruction5.paymentInstructionNumber("242423");
		instruction5.setPaymentInstructionStatusCode(PaymentInstructionStatusCode2.INITIATIONCOMPLETED);
		composite.setPaymentInstruction(instruction5);
		composite.setPaymentInstructionProposal(paymentInstructionProposal);
		transformer.transformDomesticScheduledPaymentsFromFDToAPIForInsert(composite, params);
		
	}
	
	@Test
	public void  testtransformDomesticConsentResponseFromFDToAPIForInsert1(){
		Map<String, String> params= new HashMap<>();
		
		
		ScheduledPaymentInstructionComposite composite = new ScheduledPaymentInstructionComposite();
		PaymentInstructionProposal2 paymentInstructionProposal=new PaymentInstructionProposal2();
		PaymentInstruction5 instruction5=new PaymentInstruction5();
		List<PaymentInstructionCharge> charges = new ArrayList<PaymentInstructionCharge>();
		PaymentInstructionCharge charge = new PaymentInstructionCharge();
		DomesticpaymentinstructionproposalsAmount amount = new DomesticpaymentinstructionproposalsAmount();
		Currency currency = new Currency(); 
		
		currency.setIsoAlphaCode("pata nai");
		amount.setTransactionCurrency(new BigDecimal(22.00));
		
		charge.setAmount(amount);
		charge.setCurrency(currency);
		charges.add(charge);
		
		paymentInstructionProposal.setCharges(charges);
		paymentInstructionProposal.setPaymentInstructionProposalId("paymentInstructionProposalId");
		instruction5.paymentInstructionNumber("242423");
		instruction5.setPaymentInstructionStatusCode(PaymentInstructionStatusCode2.INITIATIONFAILED);
		composite.setPaymentInstruction(instruction5);
		composite.setPaymentInstructionProposal(paymentInstructionProposal);
		transformer.transformDomesticScheduledPaymentsFromFDToAPIForInsert(composite, params);
		
	}
	@Test
	public void testtransformDomesticPaymentResponse1(){
		//PaymentInstructionProposal2 inputBalanceObj=new PaymentInstructionProposal2();
		CustomDSPaymentsPOSTRequest  paymentConsentsRequest=new CustomDSPaymentsPOSTRequest();
		ScheduledPaymentInstructionComposite scheduledPaymentInstructionComposite=new ScheduledPaymentInstructionComposite();
		PaymentInstructionProposal2 scheduledinstructionProposal= new PaymentInstructionProposal2();
		
		Map<String, String> params= new HashMap<>();
		OBWriteDataDomesticScheduled1 data = new OBWriteDataDomesticScheduled1();
		OBWriteDomesticScheduled1 initiation = new OBWriteDomesticScheduled1();
		initiation.setRequestedExecutionDateTime("2018-12-30T15:25:30-05:00");;
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("ftgfy");
		remittanceInformation.setUnstructured("unstructured");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setIdentification("hgh");
		creditorAccount.setName("vg0");
		creditorAccount.setSchemeName("cfg");
		creditorAccount.setSecondaryIdentification("fgg");
		
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setIdentification("12323");
		debtorAccount.setName("erer");
		debtorAccount.setSchemeName("wedfdf");
		debtorAccount.setSecondaryIdentification("dfdfd");
		
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("1234");
		instructedAmount.setCurrency("EUR");
		initiation.setDebtorAccount(debtorAccount);
		initiation.setInstructedAmount(instructedAmount);
		initiation.setCreditorAccount(creditorAccount);
		initiation.setRemittanceInformation(remittanceInformation);
		initiation.setInstructionIdentification("tuyht");
		initiation.setLocalInstrument("tfh");
		initiation.setEndToEndIdentification("cg");
		OBAuthorisation1 authorisation=new OBAuthorisation1();
		authorisation.setAuthorisationType(OBExternalAuthorisation1Code.SINGLE);
		OBRisk1 obRisk1=new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress=new OBRisk1DeliveryAddress();
		deliveryAddress.setCountry("India");
		deliveryAddress.setTownName("Pune");
		obRisk1.setDeliveryAddress(deliveryAddress);
		data.setAuthorisation(authorisation);
		data.setPermission(OBExternalPermissions2Code.CREATE);
		data.setInitiation(initiation);
		paymentConsentsRequest.setData(data);
		paymentConsentsRequest.setRisk(obRisk1);
		transformer.transformDomesticScheduledPaymentsFromAPIToFDForInsert(paymentConsentsRequest, params);
	}
@Test
	public void testtransformDomesticPaymentResponse(){
	PaymentInstructionProposal2 inputBalanceObj = new PaymentInstructionProposal2();
	inputBalanceObj.setPaymentInstructionProposalId("1234");
	inputBalanceObj.setInstructionEndToEndReference("user");
	inputBalanceObj.setInstructionReference("boi");
	inputBalanceObj.setInstructionLocalInstrument("card");
	FinancialEventAmount amt = new FinancialEventAmount();
	amt.setTransactionCurrency(23.00);
	inputBalanceObj.setFinancialEventAmount(amt);
	TransactionCurrency currency = new TransactionCurrency();
	currency.isoAlphaCode("EU");
	inputBalanceObj.setTransactionCurrency(currency);
	AuthorisingPartyAccount act = new AuthorisingPartyAccount();
	act.setAccountName("nam");
	act.setAccountNumber("345");
	act.setSchemeName("debit");
	act.setSecondaryIdentification("678");
	inputBalanceObj.setAuthorisingPartyAccount(act);
	ProposingPartyAccount ppa = new ProposingPartyAccount();
	ppa.setAccountName("nam");
	ppa.setAccountNumber("987");
	ppa.setSchemeName("credit");
	ppa.setSecondaryIdentification("456");
	inputBalanceObj.setProposingPartyAccount(ppa);
	ProposingPartyPostalAddress add = new ProposingPartyPostalAddress();
	add.setTownName("pune");
	add.setDepartment("A");
	add.setCountrySubDivision("MH");
	add.setSubDepartment("ABC");
	add.setPostCodeNumber("600789");
	add.setGeoCodeBuildingName("capg");
	add.setGeoCodeBuildingNumber("1521");
	AddressCountry country = new AddressCountry();
	country.setIsoCountryAlphaTwoCode("IND");
	add.setAddressCountry(country);
	List<String> line = new ArrayList<>();
	line.add("Talwade");
	add.setAddressLine(line);
	add.setAddressType("POSTAL");
	List<PaymentInstructionCharge> charges = new ArrayList<>();
	PaymentInstructionCharge charge = new PaymentInstructionCharge();
	PaymentInstructionCharge charge1 = new PaymentInstructionCharge();
	charge.setType("credit");
	charge1.setType("debit");
	Currency cur = new Currency();
	cur.setIsoAlphaCode("EUR");
	charge.setCurrency(cur);
	charge1.setCurrency(cur);
	Amount amount = new Amount();
	amount.setTransactionCurrency(4567.00);
	charge.setAmount(amount);
	charge1.setAmount(amount);
	charge.setChargeBearer(ChargeBearer.BORNEBYCREDITOR);
	charge1.setChargeBearer(ChargeBearer.BORNEBYDEBTOR);
	charges.add(charge);
	charges.add(charge1);
	inputBalanceObj.setCharges(charges);
	inputBalanceObj.setProposingPartyPostalAddress(add);
	inputBalanceObj.setAuthorisationType(AuthorisationType.ANY);
	OffsetDateTime date = OffsetDateTime.now();
	inputBalanceObj.setAuthorisationDatetime(OffsetDateTime.now());
	inputBalanceObj.setProposalCreationDatetime(OffsetDateTime.now());
	inputBalanceObj.setProposalStatusUpdateDatetime(OffsetDateTime.now());
	inputBalanceObj.setRequestedExecutionDateTime(OffsetDateTime.now());
	PaymentInstructionRiskFactorReference ref = new PaymentInstructionRiskFactorReference();
	ref.setMerchantCategoryCode("BUI");
	ref.setPaymentContextCode("BILLPAYMENT");
	ref.setMerchantCustomerIdentification("SBU");
	CounterPartyAddress cpa=new CounterPartyAddress();
	cpa.setFirstAddressLine("134");
	cpa.setSecondAddressLine("pradhikaran");
	cpa.setThirdAddressLine("sector27");
	cpa.setFourthAddressLine("nigdi");
	cpa.setFifthAddressLine("pune");
	cpa.setPostCodeNumber("600024");
	cpa.setGeoCodeBuildingNumber("1654");
	cpa.setGeoCodeBuildingName("Vinalay");
	cpa.setAddressCountry(country);
	ref.setCounterPartyAddress(cpa);
	inputBalanceObj.setPaymentInstructionRiskFactorReference(ref);
	Map<String, String> params = new HashMap<>();
	CustomDSPConsentsPOSTResponse response = transformer.transformDomesticScheduledPaymentResponse(inputBalanceObj, params);
	assertNotNull(response);  
	}

	@Test
	public void testtransformDomesticPaymentResponse2(){
	PaymentInstructionProposal2 inputBalanceObj = new PaymentInstructionProposal2();
	inputBalanceObj.setPaymentInstructionProposalId("1234");
	inputBalanceObj.setInstructionEndToEndReference("user");
	inputBalanceObj.setInstructionReference("boi");
	FinancialEventAmount amt = new FinancialEventAmount();
	amt.setTransactionCurrency(23.00);
	inputBalanceObj.setFinancialEventAmount(amt);
	TransactionCurrency currency = new TransactionCurrency();
	currency.isoAlphaCode("EU");
	inputBalanceObj.setTransactionCurrency(currency);
	AuthorisingPartyAccount act = new AuthorisingPartyAccount();
	act.setAccountName("nam");
	act.setAccountNumber("345");
	act.setSchemeName("debit");
	act.setSecondaryIdentification("678");
	inputBalanceObj.setAuthorisingPartyAccount(act);
	ProposingPartyAccount ppa = new ProposingPartyAccount();
	ppa.setAccountName("nam");
	ppa.setAccountNumber("987");
	ppa.setSchemeName("credit");
	ppa.setSecondaryIdentification("456");
	inputBalanceObj.setProposingPartyAccount(ppa);
	ProposingPartyPostalAddress add = new ProposingPartyPostalAddress();
	add.setTownName("pune");
	add.setDepartment("A");
	add.setCountrySubDivision("MH");
	add.setSubDepartment("ABC");
	add.setPostCodeNumber("600789");
	add.setGeoCodeBuildingName("capg");
	add.setGeoCodeBuildingNumber("1521");
	AddressCountry country = new AddressCountry();
	country.setIsoCountryAlphaTwoCode("IND");
	add.setAddressCountry(country);
	List<String> line = new ArrayList<>();
	line.add("Talwade");
	add.setAddressLine(line);
	add.setAddressType("POSTAL");
	inputBalanceObj.setProposingPartyPostalAddress(add);
	OffsetDateTime date = OffsetDateTime.now();
	//inputBalanceObj.setProposalCreationDatetime(null);
	//inputBalanceObj.setProposalStatusUpdateDatetime(null);
	//inputBalanceObj.setRequestedExecutionDateTime(null);
	Map<String, String> params = new HashMap<>();
	CustomDSPConsentsPOSTResponse response = transformer.transformDomesticScheduledPaymentResponse(inputBalanceObj, params);
	assertNotNull(response);;  
	}


}
*/