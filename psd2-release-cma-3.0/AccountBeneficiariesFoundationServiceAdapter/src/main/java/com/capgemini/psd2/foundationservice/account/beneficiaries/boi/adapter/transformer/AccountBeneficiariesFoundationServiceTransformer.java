package com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.transformer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBBeneficiary2;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification3;
import com.capgemini.psd2.aisp.domain.OBCashAccount3;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary2;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary2Data;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBeneficiariesResponse;
import com.capgemini.psd2.aisp.transformer.AccountBeneficiariesTransformer;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.constants.AccountBeneficiariesFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.PartiesPaymentBeneficiariesresponse;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.PaymentBeneficiary;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component
public class AccountBeneficiariesFoundationServiceTransformer implements AccountBeneficiariesTransformer {

	@Autowired
	private PSD2Validator validator;

	@Value("${foundationService.filteredBeneficiariesCountryCode1}")
	private String filteredBeneficiariesCountryCode1;

	@Value("${foundationService.filteredBeneficiariesCountryCode2}")
	private String filteredBeneficiariesCountryCode2;

	@Value("${foundationService.filteredBeneficiariesCountryCode3}")
	private String filteredBeneficiariesCountryCode3;

	@Value("${foundationService.filteredBeneficiariesCountryCode4}")
	private String filteredBeneficiariesCountryCode4;

	@Value("${foundationService.filteredBeneficiariesCountryCode5}")
	private String filteredBeneficiariesCountryCode5;

	@Value("${foundationService.filterBeneficiariesResponse}")
	private boolean filterBeneficiariesResponse;

	public <T> PlatformAccountBeneficiariesResponse transformAccountBeneficiaries(T source,
			Map<String, String> params) {

		PlatformAccountBeneficiariesResponse platformAccountBeneficiariesResponse = new PlatformAccountBeneficiariesResponse();
		OBReadBeneficiary2 obReadBeneficiary2 = new OBReadBeneficiary2();
		OBReadBeneficiary2Data obReadBeneficiary2Data = new OBReadBeneficiary2Data();

		PartiesPaymentBeneficiariesresponse beneficiaries = (PartiesPaymentBeneficiariesresponse) source;

		List<PaymentBeneficiary> beneficiaryList = beneficiaries.getPaymentBeneficiaryList();
		for (PaymentBeneficiary beneficiary : beneficiaryList) {
			String code = beneficiary.getBeneficiaryAccount().getOperationalOrganisationUnit().getAddressReference()
					.getAddressCountry().getIsoCountryAlphaTwoCode();
			if (!NullCheckUtils.isNullOrEmpty(code) && code.length() == 2) {
				if (filteredBeneficiariesCountryCode1
						.contains(beneficiary.getBeneficiaryAccount().getOperationalOrganisationUnit()
								.getAddressReference().getAddressCountry().getIsoCountryAlphaTwoCode())) {

					OBBranchAndFinancialInstitutionIdentification3 obBeneficiary2CreditorAgent = new OBBranchAndFinancialInstitutionIdentification3();

					obBeneficiary2CreditorAgent
							.setSchemeName(AccountBeneficiariesFoundationServiceConstants.UK_OBIE_BICFI);
					obBeneficiary2CreditorAgent
							.setIdentification(beneficiary.getBeneficiaryAccount().getSwiftBankIdentifierCodeBIC());

					OBBeneficiary2 obBeneficiary2 = new OBBeneficiary2();

					// AccountId, beneficiaryId and Reference
					obBeneficiary2.setAccountId(params.get(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID));
					if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryIdentifier())) {
						obBeneficiary2.setBeneficiaryId(beneficiary.getPaymentBeneficiaryIdentifier());
					}
					if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryNarrativeText())) {
						obBeneficiary2.setReference(beneficiary.getPaymentBeneficiaryNarrativeText());
					}

					OBCashAccount3 obBeneficiary2CreditorAccount = new OBCashAccount3();

					obBeneficiary2CreditorAccount
							.setSchemeName(AccountBeneficiariesFoundationServiceConstants.UK_OBIE_IBAN);
					obBeneficiary2CreditorAccount.setIdentification(
							beneficiary.getBeneficiaryAccount().getInternationalBankAccountNumberIBAN());
					obBeneficiary2CreditorAccount.setName(beneficiary.getPaymentBeneficiaryLabel());

					obBeneficiary2.setCreditorAgent(obBeneficiary2CreditorAgent);
					obBeneficiary2.setCreditorAccount(obBeneficiary2CreditorAccount);
					validator.validate(obBeneficiary2);
					obReadBeneficiary2Data.addBeneficiaryItem(obBeneficiary2);
				} else if (filteredBeneficiariesCountryCode2
						.contains(beneficiary.getBeneficiaryAccount().getOperationalOrganisationUnit()
								.getAddressReference().getAddressCountry().getIsoCountryAlphaTwoCode())) {

					OBBeneficiary2 obBeneficiary2 = new OBBeneficiary2();
					// AccountId, beneficiaryId and Reference
					obBeneficiary2.setAccountId(params.get(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID));
					if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryIdentifier())) {
						obBeneficiary2.setBeneficiaryId(beneficiary.getPaymentBeneficiaryIdentifier());
					}
					if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryNarrativeText())) {
						obBeneficiary2.setReference(beneficiary.getPaymentBeneficiaryNarrativeText());
					}

					OBCashAccount3 obBeneficiary2CreditorAccount = new OBCashAccount3();

					obBeneficiary2CreditorAccount.setSchemeName(
							AccountBeneficiariesFoundationServiceConstants.UK_OBIE_SortCodeAccountNumber);
					obBeneficiary2CreditorAccount.setIdentification(
							beneficiary.getBeneficiaryAccount().getParentNationalSortCodeNSCNumber().trim()
									+ beneficiary.getBeneficiaryAccount().getAccountNumber().trim());
					obBeneficiary2CreditorAccount.setName(beneficiary.getPaymentBeneficiaryLabel());

					obBeneficiary2.setCreditorAccount(obBeneficiary2CreditorAccount);
					validator.validate(obBeneficiary2);
					obReadBeneficiary2Data.addBeneficiaryItem(obBeneficiary2);
				} else if (filteredBeneficiariesCountryCode3
						.contains(beneficiary.getBeneficiaryAccount().getOperationalOrganisationUnit()
								.getAddressReference().getAddressCountry().getIsoCountryAlphaTwoCode())) {

					if (!NullCheckUtils
							.isNullOrEmpty(beneficiary.getBeneficiaryAccount().getABARTNRoutingTransitNumber())
							&& (beneficiary.getBeneficiaryAccount().getABARTNRoutingTransitNumber().length() == 9)) {

						OBBeneficiary2 obBeneficiary2 = new OBBeneficiary2();

						// AccountId, beneficiaryId and Reference
						obBeneficiary2
								.setAccountId(params.get(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID));
						if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryIdentifier())) {
							obBeneficiary2.setBeneficiaryId(beneficiary.getPaymentBeneficiaryIdentifier());
						}
						if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryNarrativeText())) {
							obBeneficiary2.setReference(beneficiary.getPaymentBeneficiaryNarrativeText());
						}

						OBCashAccount3 obBeneficiary2CreditorAccount = new OBCashAccount3();

						obBeneficiary2CreditorAccount.setSchemeName(
								AccountBeneficiariesFoundationServiceConstants.UK_BOI_ABACodeAccountNumber);
						obBeneficiary2CreditorAccount.setIdentification(
								beneficiary.getBeneficiaryAccount().getABARTNRoutingTransitNumber().trim()
										+ beneficiary.getBeneficiaryAccount().getAccountNumber().trim());
						obBeneficiary2CreditorAccount.setName(beneficiary.getPaymentBeneficiaryLabel());

						obBeneficiary2.setCreditorAccount(obBeneficiary2CreditorAccount);
						validator.validate(obBeneficiary2);
						obReadBeneficiary2Data.addBeneficiaryItem(obBeneficiary2);
					}
				} else if ((filteredBeneficiariesCountryCode4
						.contains(beneficiary.getBeneficiaryAccount().getOperationalOrganisationUnit()
								.getAddressReference().getAddressCountry().getIsoCountryAlphaTwoCode()))
						|| (filteredBeneficiariesCountryCode5
								.contains(beneficiary.getBeneficiaryAccount().getOperationalOrganisationUnit()
										.getAddressReference().getAddressCountry().getIsoCountryAlphaTwoCode()))) {

					OBBranchAndFinancialInstitutionIdentification3 obBeneficiary2CreditorAgent = new OBBranchAndFinancialInstitutionIdentification3();

					obBeneficiary2CreditorAgent
							.setSchemeName(AccountBeneficiariesFoundationServiceConstants.UK_OBIE_BICFI);
					obBeneficiary2CreditorAgent
							.setIdentification(beneficiary.getBeneficiaryAccount().getSwiftBankIdentifierCodeBIC());

					OBBeneficiary2 obBeneficiary2 = new OBBeneficiary2();

					// AccountId, beneficiaryId and Reference
					obBeneficiary2.setAccountId(params.get(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID));
					if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryIdentifier())) {
						obBeneficiary2.setBeneficiaryId(beneficiary.getPaymentBeneficiaryIdentifier());
					}
					if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryNarrativeText())) {
						obBeneficiary2.setReference(beneficiary.getPaymentBeneficiaryNarrativeText());
					}

					OBCashAccount3 obBeneficiary2CreditorAccount = new OBCashAccount3();

					obBeneficiary2CreditorAccount
							.setSchemeName(AccountBeneficiariesFoundationServiceConstants.UK_BOI_AccountNumber);
					obBeneficiary2CreditorAccount
							.setIdentification(beneficiary.getBeneficiaryAccount().getAccountNumber());
					obBeneficiary2CreditorAccount.setName(beneficiary.getPaymentBeneficiaryLabel());

					obBeneficiary2.setCreditorAgent(obBeneficiary2CreditorAgent);
					obBeneficiary2.setCreditorAccount(obBeneficiary2CreditorAccount);
					validator.validate(obBeneficiary2);
					obReadBeneficiary2Data.addBeneficiaryItem(obBeneficiary2);
				}
			}
			if (NullCheckUtils.isNullOrEmpty(code)) {
				OBBeneficiary2 obBeneficiary2 = new OBBeneficiary2();
				// AccountId, beneficiaryId and Reference
				obBeneficiary2.setAccountId(params.get(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID));
				if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryIdentifier())) {
					obBeneficiary2.setBeneficiaryId(beneficiary.getPaymentBeneficiaryIdentifier());
				}
				if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryNarrativeText())) {
					obBeneficiary2.setReference(beneficiary.getPaymentBeneficiaryNarrativeText());
				}

				OBCashAccount3 obBeneficiary2CreditorAccount = null;

				if (!NullCheckUtils
						.isNullOrEmpty(beneficiary.getBeneficiaryAccount().getInternationalBankAccountNumberIBAN())) {
					if (!NullCheckUtils
							.isNullOrEmpty(beneficiary.getBeneficiaryAccount().getSwiftBankIdentifierCodeBIC())) {
						OBBranchAndFinancialInstitutionIdentification3 obBeneficiary2CreditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
						obBeneficiary2CreditorAgent
								.setSchemeName(AccountBeneficiariesFoundationServiceConstants.UK_OBIE_BICFI);
						obBeneficiary2CreditorAgent
								.setIdentification(beneficiary.getBeneficiaryAccount().getSwiftBankIdentifierCodeBIC());
						obBeneficiary2.setCreditorAgent(obBeneficiary2CreditorAgent);
					}
					obBeneficiary2CreditorAccount = new OBCashAccount3();
					obBeneficiary2CreditorAccount
							.setSchemeName(AccountBeneficiariesFoundationServiceConstants.UK_OBIE_IBAN);
					obBeneficiary2CreditorAccount.setIdentification(
							beneficiary.getBeneficiaryAccount().getInternationalBankAccountNumberIBAN());
					obBeneficiary2CreditorAccount.setName(beneficiary.getPaymentBeneficiaryLabel());

				} else {
					obBeneficiary2CreditorAccount = new OBCashAccount3();
					obBeneficiary2CreditorAccount.setSchemeName(
							AccountBeneficiariesFoundationServiceConstants.UK_OBIE_SortCodeAccountNumber);
					obBeneficiary2CreditorAccount.setIdentification(
							beneficiary.getBeneficiaryAccount().getParentNationalSortCodeNSCNumber().trim()
									+ beneficiary.getBeneficiaryAccount().getAccountNumber().trim());
					obBeneficiary2CreditorAccount.setName(beneficiary.getPaymentBeneficiaryLabel());

				}

				obBeneficiary2.setCreditorAccount(obBeneficiary2CreditorAccount);
				validator.validate(obBeneficiary2);
				obReadBeneficiary2Data.addBeneficiaryItem(obBeneficiary2);
			}
		}
		if (null == obReadBeneficiary2Data.getBeneficiary() || obReadBeneficiary2Data.getBeneficiary().isEmpty()) {
			obReadBeneficiary2Data.setBeneficiary(new ArrayList<>());
		}
		validator.validate(obReadBeneficiary2Data);
		obReadBeneficiary2.setData(obReadBeneficiary2Data);
		platformAccountBeneficiariesResponse.setoBReadBeneficiary2(obReadBeneficiary2);
		return platformAccountBeneficiariesResponse;
	}
}
