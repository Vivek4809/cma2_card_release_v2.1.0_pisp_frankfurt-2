package com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.aisp.domain.OBReadTransaction3;
import com.capgemini.psd2.aisp.domain.OBReadTransaction3Data;
import com.capgemini.psd2.aisp.domain.OBTransaction3;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.constants.AccountTransactionsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.BalanceAmountBaseType;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.BalanceBaseType;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.BalanceCurrencyBaseType;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.BalanceTypeEnum;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.CreditDebitEventCodeEnum;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.CurrencyBaseType;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.FinancialEvent;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.FinancialEventAmountBaseType;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Merchant;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Party;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.Transaction;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.domain.TransactionList;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.transformer.AccountTransactionsFoundationServiceTransformer;
import com.capgemini.psd2.validator.impl.PSD2ValidatorImpl;

public class AccountTransactionsFoundationServiceTransformerTest {

	/** The account information FS transformer. */
	@InjectMocks
	private AccountTransactionsFoundationServiceTransformer accountTransactionsFSTransformer = new AccountTransactionsFoundationServiceTransformer();

	@Mock
	private PSD2ValidatorImpl psd2Validator;

	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	/**
	 * Test transform response from FD to API.
	 */

	@Test
	public void testtransformAccountTransaction1a() {
		OBReadTransaction3Data obReadTransaction2Data = new OBReadTransaction3Data();
		OBReadTransaction3 obReadTransaction2 = new OBReadTransaction3();
		//OBTransaction2 obTransaction2 = new OBTransaction2();
		
		//obReadTransaction2Data.setTransaction(null);

		List<OBTransaction3> transactionList = new ArrayList<>();
		//transactionList.add(obTransaction2);

		TransactionList transactions = new TransactionList();
		transactions.setTransactionsList(null);
		transactions.setHasMoreRecords(true);
		transactions.setTotal(30.0);
		transactions.setPageNumber(1);
		transactions.setPageSize(25);
		Transaction tbt = new Transaction();
		List<Transaction> transList = new ArrayList<Transaction>();
		transList.add(tbt);

		transactions.setTransactionsList(transList);

		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID, "1234");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_NUMBER, "12");

		tbt.setTransactionIdentificationNumber("1234");

		FinancialEventAmountBaseType feabt = new FinancialEventAmountBaseType();
		Double transcurr = 1000d;
		feabt.setTransactionCurrency(transcurr);
		FinancialEvent fe = new FinancialEvent();
		fe.setFinancialEventAmount(feabt);
		tbt.setFinancialEvent(fe);

		CurrencyBaseType cbt = new CurrencyBaseType();
		cbt.setIsoAlphaCode("GBU");
		fe.setCurrency(cbt);
		tbt.setFinancialEvent(fe);

		tbt.setCreditDebitEventCode(CreditDebitEventCodeEnum.CR);

		fe.setFinancialEventStatusCode("BOOKED");
		tbt.setFinancialEvent(fe);

		tbt.setTransactionDecription("ABC");
		tbt.setTransactionLocation("XYZ");
		tbt.setRunDate(new Date());
		tbt.setValueDate(new Date());

		fe.setFinancialEventSubtype("mno");
		tbt.setFinancialEvent(fe);

		BalanceBaseType balance = new BalanceBaseType();
		tbt.setBalance(balance);

		BalanceAmountBaseType babt = new BalanceAmountBaseType();
		babt.setTransactionCurrency(transcurr);
		balance.setBalanceAmount(babt);
		tbt.setBalance(balance);
		BalanceCurrencyBaseType bcbt = new BalanceCurrencyBaseType();
		bcbt.setIsoAlphaCode("GBU");
		balance.setBalanceCurrency(bcbt);
		tbt.setBalance(balance);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");
		balance.setBalanceType(BalanceTypeEnum.LEDGER_BALANCE);
		tbt.setBalance(balance);

		obReadTransaction2Data.setTransaction(transactionList);
		obReadTransaction2.setData(obReadTransaction2Data);

		accountTransactionsFSTransformer.transformAccountTransaction(transactions, params);
	}

	@Test
	public void testtransformAccountTransaction1b() {
		OBReadTransaction3Data obReadTransaction2Data = new OBReadTransaction3Data();
		OBReadTransaction3 obReadTransaction2 = new OBReadTransaction3();
		OBTransaction3 obTransaction2 = new OBTransaction3();

		List<OBTransaction3> transactionList = new ArrayList<>();
		obReadTransaction2Data.setTransaction(null);

		transactionList.add(obTransaction2);

		TransactionList transactions = new TransactionList();
		transactions.setTransactionsList(null);
		transactions.setHasMoreRecords(true);
		transactions.setTotal(30.0);
		transactions.setPageNumber(1);
		transactions.setPageSize(25);
		Transaction tbt = new Transaction();
		List<Transaction> transList = new ArrayList<Transaction>();
		transList.add(tbt);

		transactions.setTransactionsList(transList);

		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "Savings");

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID, "1234");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_NUMBER, "12");

		tbt.setTransactionIdentificationNumber("1234");

		FinancialEventAmountBaseType feabt = new FinancialEventAmountBaseType();
		Double transcurr = 1000d;
		feabt.setTransactionCurrency(transcurr);
		FinancialEvent fe = new FinancialEvent();
		fe.setFinancialEventAmount(feabt);
		tbt.setFinancialEvent(fe);

		CurrencyBaseType cbt = new CurrencyBaseType();
		cbt.setIsoAlphaCode("GBU");
		fe.setCurrency(cbt);
		tbt.setFinancialEvent(fe);

		tbt.setCreditDebitEventCode(CreditDebitEventCodeEnum.DR);

		fe.setFinancialEventStatusCode("BOOKED");
		tbt.setFinancialEvent(fe);

		tbt.setTransactionDecription("ABC");
		tbt.setTransactionLocation("XYZ");
		tbt.setRunDate(new Date());
		tbt.setValueDate(new Date());

		fe.setFinancialEventSubtype("mno");
		tbt.setFinancialEvent(fe);

		BalanceBaseType balance = new BalanceBaseType();
		tbt.setBalance(balance);

		BalanceAmountBaseType babt = new BalanceAmountBaseType();
		babt.setTransactionCurrency(transcurr);
		balance.setBalanceAmount(babt);
		tbt.setBalance(balance);

		BalanceCurrencyBaseType bcbt = new BalanceCurrencyBaseType();
		bcbt.setIsoAlphaCode("GBU");
		balance.setBalanceCurrency(bcbt);
		tbt.setBalance(balance);

		balance.setBalanceType(BalanceTypeEnum.SHADOW_BALANCE);
		tbt.setBalance(balance);

		obReadTransaction2Data.setTransaction(transactionList);
		obReadTransaction2.setData(obReadTransaction2Data);

		accountTransactionsFSTransformer.transformAccountTransaction(transactions, params);
	}

	@Test
	public void testtransformAccountTransaction1c() {
		OBReadTransaction3Data obReadTransaction2Data = new OBReadTransaction3Data();
		OBReadTransaction3 obReadTransaction2 = new OBReadTransaction3();
		OBTransaction3 obTransaction2 = new OBTransaction3();

		List<OBTransaction3> transactionList = new ArrayList<>();
		obReadTransaction2Data.setTransaction(null);

		transactionList.add(obTransaction2);

		TransactionList transactions = new TransactionList();
		transactions.setTransactionsList(null);

		Transaction tbt = new Transaction();
		List<Transaction> transList = new ArrayList<Transaction>();
		transList.add(tbt);

		transactions.setTransactionsList(transList);
		transactions.setHasMoreRecords(true);
		transactions.setTotal(30.0);
		transactions.setPageNumber(1);
		transactions.setPageSize(25);
		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "");

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID, "1234");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_NUMBER, "12");

		tbt.setTransactionIdentificationNumber("1234");

		FinancialEventAmountBaseType feabt = new FinancialEventAmountBaseType();
		Double transcurr = 1000.0;
		feabt.setTransactionCurrency(transcurr);
		FinancialEvent fe = new FinancialEvent();
		fe.setFinancialEventAmount(feabt);
		tbt.setFinancialEvent(fe);

		CurrencyBaseType cbt = new CurrencyBaseType();
		cbt.setIsoAlphaCode("GBU");
		fe.setCurrency(cbt);
		tbt.setFinancialEvent(fe);

		tbt.setCreditDebitEventCode(CreditDebitEventCodeEnum.DR);

		fe.setFinancialEventStatusCode("PENDING");
		tbt.setFinancialEvent(fe);

		tbt.setTransactionDecription("ABC");
		tbt.setTransactionLocation("XYZ");
		tbt.setRunDate(new Date());
		tbt.setValueDate(new Date());

		fe.setFinancialEventSubtype("mno");
		tbt.setFinancialEvent(fe);

		BalanceBaseType balance = new BalanceBaseType();
		tbt.setBalance(balance);

		BalanceAmountBaseType babt = new BalanceAmountBaseType();
		babt.setTransactionCurrency(transcurr);
		balance.setBalanceAmount(babt);
		tbt.setBalance(balance);

		BalanceCurrencyBaseType bcbt = new BalanceCurrencyBaseType();
		bcbt.setIsoAlphaCode("GBU");
		balance.setBalanceCurrency(bcbt);
		tbt.setBalance(balance);

		balance.setBalanceType(BalanceTypeEnum.LEDGER_BALANCE);
		tbt.setBalance(balance);

		obReadTransaction2Data.setTransaction(transactionList);
		obReadTransaction2.setData(obReadTransaction2Data);

		accountTransactionsFSTransformer.transformAccountTransaction(transactions, params);
	}

	@Test
	public void testtransformAccountTransaction1d() {
		OBReadTransaction3Data obReadTransaction2Data = new OBReadTransaction3Data();
		OBReadTransaction3 obReadTransaction2 = new OBReadTransaction3();
		OBTransaction3 obTransaction2 = new OBTransaction3();

		List<OBTransaction3> transactionList = new ArrayList<>();
		obReadTransaction2Data.setTransaction(null);

		transactionList.add(obTransaction2);

		TransactionList transactions = new TransactionList();
		transactions.setTransactionsList(null);
		transactions.setHasMoreRecords(true);
		transactions.setTotal(30.0);
		transactions.setPageNumber(1);
		transactions.setPageSize(25);
		Transaction tbt = new Transaction();
		List<Transaction> transList = new ArrayList<Transaction>();
		transList.add(tbt);

		transactions.setTransactionsList(transList);

		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_NUMBER, "12");

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID, "1234");

		tbt.setTransactionIdentificationNumber("1234");

		FinancialEventAmountBaseType feabt = new FinancialEventAmountBaseType();
		Double transcurr = -1000d;
		feabt.setTransactionCurrency(transcurr);
		FinancialEvent fe = new FinancialEvent();
		fe.setFinancialEventAmount(feabt);
		tbt.setFinancialEvent(fe);

		CurrencyBaseType cbt = new CurrencyBaseType();
		cbt.setIsoAlphaCode("GBU");
		fe.setCurrency(cbt);
		tbt.setFinancialEvent(fe);

		tbt.setCreditDebitEventCode(CreditDebitEventCodeEnum.CR);

		fe.setFinancialEventStatusCode("BOOKED");
		tbt.setFinancialEvent(fe);

		tbt.setTransactionDecription("ABC");
		tbt.setTransactionLocation("XYZ");
		tbt.setRunDate(new Date());
		tbt.setValueDate(new Date());

		fe.setFinancialEventSubtype("mno");
		tbt.setFinancialEvent(fe);

		BalanceBaseType balance = new BalanceBaseType();
		tbt.setBalance(balance);

		BalanceAmountBaseType babt = new BalanceAmountBaseType();
		babt.setTransactionCurrency(transcurr);
		balance.setBalanceAmount(babt);
		tbt.setBalance(balance);

		BalanceCurrencyBaseType bcbt = new BalanceCurrencyBaseType();
		bcbt.setIsoAlphaCode("GBU");
		balance.setBalanceCurrency(bcbt);
		tbt.setBalance(balance);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");
		balance.setBalanceType(BalanceTypeEnum.LEDGER_BALANCE);
		tbt.setBalance(balance);

		obReadTransaction2Data.setTransaction(transactionList);
		obReadTransaction2.setData(obReadTransaction2Data);

		accountTransactionsFSTransformer.transformAccountTransaction(transactions, params);
	}
	
	@Test
	public void testtransformAccountTransaction2a() {
		OBReadTransaction3Data obReadTransaction2Data = new OBReadTransaction3Data();
		OBReadTransaction3 obReadTransaction2 = new OBReadTransaction3();
		OBTransaction3 obTransaction2 = new OBTransaction3();

		List<OBTransaction3> transactionList = new ArrayList<>();
		obReadTransaction2Data.setTransaction(null);

		transactionList.add(obTransaction2);

		TransactionList transactions = new TransactionList();
		transactions.setTransactionsList(null);

		Transaction tbt = new Transaction();
		List<Transaction> transList = new ArrayList<Transaction>();
		transList.add(tbt);

		transactions.setTransactionsList(transList);
		transactions.setHasMoreRecords(true);
		transactions.setTotal(30.0);
		transactions.setPageNumber(1);
		transactions.setPageSize(25);
		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_NUMBER, "12");

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID, "1234");

		tbt.setTransactionIdentificationNumber("1234");

		FinancialEventAmountBaseType feabt = new FinancialEventAmountBaseType();
		Double transcurr = 1000d;
		feabt.setTransactionCurrency(transcurr);
		FinancialEvent fe = new FinancialEvent();
		fe.setFinancialEventAmount(feabt);
		tbt.setFinancialEvent(fe);

		CurrencyBaseType cbt = new CurrencyBaseType();
		cbt.setIsoAlphaCode("GBU");
		fe.setCurrency(cbt);
		tbt.setFinancialEvent(fe);

		tbt.setCreditDebitEventCode(CreditDebitEventCodeEnum.CR);

		fe.setFinancialEventStatusCode("BOOKED");
		tbt.setFinancialEvent(fe);

		tbt.setTransactionDecription("ABC");
		tbt.setTransactionLocation("XYZ");
		tbt.setRunDate(new Date());
		tbt.setValueDate(new Date());

		fe.setFinancialEventSubtype("mno");
		tbt.setFinancialEvent(fe);

		BalanceBaseType balance = new BalanceBaseType();
		tbt.setBalance(balance);

		BalanceAmountBaseType babt = new BalanceAmountBaseType();
		babt.setTransactionCurrency(transcurr);
		balance.setBalanceAmount(babt);
		tbt.setBalance(balance);

		BalanceCurrencyBaseType bcbt = new BalanceCurrencyBaseType();
		bcbt.setIsoAlphaCode("GBU");
		balance.setBalanceCurrency(bcbt);
		tbt.setBalance(balance);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		balance.setBalanceType(BalanceTypeEnum.LEDGER_BALANCE);
		tbt.setBalance(balance);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		Party party = new Party();
		Merchant mer = new Merchant();
		mer.setMerchantName("Akhil");
		mer.setMerchantCategoryCode("Open");
		party.setMerchant(mer);
		tbt.setParty(party);
		
		
		obReadTransaction2Data.setTransaction(transactionList);
		obReadTransaction2.setData(obReadTransaction2Data);

		accountTransactionsFSTransformer.transformAccountTransaction(transactions, params);
	}

	@Test
	public void testtransformAccountTransaction2b() {
		OBReadTransaction3Data obReadTransaction2Data = new OBReadTransaction3Data();
		OBReadTransaction3 obReadTransaction2 = new OBReadTransaction3();
		OBTransaction3 obTransaction2 = new OBTransaction3();

		List<OBTransaction3> transactionList = new ArrayList<>();
		obReadTransaction2Data.setTransaction(null);

		transactionList.add(obTransaction2);

		TransactionList transactions = new TransactionList();
		transactions.setTransactionsList(null);
		transactions.setHasMoreRecords(true);
		transactions.setTotal(30.0);
		transactions.setPageNumber(1);
		transactions.setPageSize(25);
		Transaction tbt = new Transaction();
		List<Transaction> transList = new ArrayList<Transaction>();
		transList.add(tbt);

		transactions.setTransactionsList(transList);

		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_NUMBER, "12");

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID, "1234");

		tbt.setTransactionIdentificationNumber("1234");

		FinancialEventAmountBaseType feabt = new FinancialEventAmountBaseType();
		Double transcurr = 1000d;
		feabt.setTransactionCurrency(transcurr);
		FinancialEvent fe = new FinancialEvent();
		fe.setFinancialEventAmount(feabt);
		tbt.setFinancialEvent(fe);

		CurrencyBaseType cbt = new CurrencyBaseType();
		cbt.setIsoAlphaCode("GBU");
		fe.setCurrency(cbt);
		tbt.setFinancialEvent(fe);

		tbt.setCreditDebitEventCode(CreditDebitEventCodeEnum.DR);

		fe.setFinancialEventStatusCode("BOOKED");
		tbt.setFinancialEvent(fe);

		tbt.setTransactionDecription("ABC");
		tbt.setTransactionLocation("XYZ");
		tbt.setRunDate(new Date());
		tbt.setValueDate(new Date());

		fe.setFinancialEventSubtype("mno");
		tbt.setFinancialEvent(fe);

		BalanceBaseType balance = new BalanceBaseType();
		tbt.setBalance(balance);

		BalanceAmountBaseType babt = new BalanceAmountBaseType();
		babt.setTransactionCurrency(transcurr);
		balance.setBalanceAmount(babt);
		tbt.setBalance(balance);

		BalanceCurrencyBaseType bcbt = new BalanceCurrencyBaseType();
		bcbt.setIsoAlphaCode("GBU");
		balance.setBalanceCurrency(bcbt);
		tbt.setBalance(balance);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		balance.setBalanceType(BalanceTypeEnum.LEDGER_BALANCE);
		tbt.setBalance(balance);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		Party party = new Party();
		Merchant mer = new Merchant();
		mer.setMerchantName("Akhil");
		mer.setMerchantCategoryCode("Open");
		party.setMerchant(mer);
		tbt.setParty(party);
		
		obReadTransaction2Data.setTransaction(transactionList);
		obReadTransaction2.setData(obReadTransaction2Data);

		accountTransactionsFSTransformer.transformAccountTransaction(transactions, params);
	}

	@Test
	public void testtransformAccountTransaction2c() {
		OBReadTransaction3Data obReadTransaction2Data = new OBReadTransaction3Data();
		OBReadTransaction3 obReadTransaction2 = new OBReadTransaction3();
		OBTransaction3 obTransaction2 = new OBTransaction3();

		List<OBTransaction3> transactionList = new ArrayList<>();
		obReadTransaction2Data.setTransaction(null);

		transactionList.add(obTransaction2);

		TransactionList transactions = new TransactionList();
		transactions.setTransactionsList(null);

		Transaction tbt = new Transaction();
		List<Transaction> transList = new ArrayList<Transaction>();
		transList.add(tbt);

		transactions.setTransactionsList(transList);
		transactions.setHasMoreRecords(true);
		transactions.setTotal(30.0);
		transactions.setPageNumber(1);
		transactions.setPageSize(25);
		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID, "1234");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_NUMBER, "12");

		tbt.setTransactionIdentificationNumber("1234");

		FinancialEventAmountBaseType feabt = new FinancialEventAmountBaseType();
		Double transcurr = 1000.0;
		feabt.setTransactionCurrency(transcurr);
		FinancialEvent fe = new FinancialEvent();
		fe.setFinancialEventAmount(feabt);
		tbt.setFinancialEvent(fe);

		CurrencyBaseType cbt = new CurrencyBaseType();
		cbt.setIsoAlphaCode("GBU");
		fe.setCurrency(cbt);
		tbt.setFinancialEvent(fe);

		tbt.setCreditDebitEventCode(CreditDebitEventCodeEnum.DR);

		fe.setFinancialEventStatusCode("PENDING");
		tbt.setFinancialEvent(fe);

		tbt.setTransactionDecription("ABC");
		tbt.setTransactionLocation("XYZ");
		tbt.setRunDate(new Date());
		tbt.setValueDate(new Date());

		fe.setFinancialEventSubtype("mno");
		tbt.setFinancialEvent(fe);

		BalanceBaseType balance = new BalanceBaseType();
		tbt.setBalance(balance);

		BalanceAmountBaseType babt = new BalanceAmountBaseType();
		babt.setTransactionCurrency(transcurr);
		balance.setBalanceAmount(babt);
		tbt.setBalance(balance);

		BalanceCurrencyBaseType bcbt = new BalanceCurrencyBaseType();
		bcbt.setIsoAlphaCode("GBU");
		balance.setBalanceCurrency(bcbt);
		tbt.setBalance(balance);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		balance.setBalanceType(BalanceTypeEnum.AVAILABLE_LIMIT);
		tbt.setBalance(balance);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		Party party = new Party();
		Merchant mer = new Merchant();
		mer.setMerchantName("Akhil");
		mer.setMerchantCategoryCode("Open");
		party.setMerchant(mer);
		tbt.setParty(party);

		obReadTransaction2Data.setTransaction(transactionList);
		obReadTransaction2.setData(obReadTransaction2Data);

		accountTransactionsFSTransformer.transformAccountTransaction(transactions, params);
	}

	@Test
	public void testtransformAccountTransaction2d() {
		OBReadTransaction3Data obReadTransaction2Data = new OBReadTransaction3Data();
		OBReadTransaction3 obReadTransaction2 = new OBReadTransaction3();
		OBTransaction3 obTransaction2 = new OBTransaction3();

		List<OBTransaction3> transactionList = new ArrayList<>();
		obReadTransaction2Data.setTransaction(null);

		transactionList.add(obTransaction2);

		TransactionList transactions = new TransactionList();
		transactions.setTransactionsList(null);

		Transaction tbt = new Transaction();
		List<Transaction> transList = new ArrayList<Transaction>();
		transList.add(tbt);

		transactions.setTransactionsList(transList);
		transactions.setHasMoreRecords(true);
		transactions.setTotal(30.0);
		transactions.setPageNumber(1);
		transactions.setPageSize(25);
		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID, "1234");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_NUMBER, "12");

		tbt.setTransactionIdentificationNumber("1234");

		FinancialEventAmountBaseType feabt = new FinancialEventAmountBaseType();
		Double transcurr = -1000d;
		feabt.setTransactionCurrency(transcurr);
		FinancialEvent fe = new FinancialEvent();
		fe.setFinancialEventAmount(feabt);
		tbt.setFinancialEvent(fe);

		CurrencyBaseType cbt = new CurrencyBaseType();
		cbt.setIsoAlphaCode("GBU");
		fe.setCurrency(cbt);
		tbt.setFinancialEvent(fe);

		tbt.setCreditDebitEventCode(CreditDebitEventCodeEnum.CR);

		fe.setFinancialEventStatusCode("BOOKED");
		tbt.setFinancialEvent(fe);

		tbt.setTransactionDecription("ABC");
		tbt.setTransactionLocation("XYZ");
		tbt.setRunDate(new Date());
		tbt.setValueDate(new Date());

		fe.setFinancialEventSubtype("mno");
		tbt.setFinancialEvent(fe);

		BalanceBaseType balance = new BalanceBaseType();
		tbt.setBalance(balance);

		BalanceAmountBaseType babt = new BalanceAmountBaseType();
		babt.setTransactionCurrency(transcurr);
		balance.setBalanceAmount(babt);
		tbt.setBalance(balance);

		BalanceCurrencyBaseType bcbt = new BalanceCurrencyBaseType();
		bcbt.setIsoAlphaCode("GBU");
		balance.setBalanceCurrency(bcbt);
		tbt.setBalance(balance);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		balance.setBalanceType(BalanceTypeEnum.STATEMENT_CLOSING_BALANCE);
		tbt.setBalance(balance);

		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		Party party = new Party();
		Merchant mer = new Merchant();
		mer.setMerchantName("Akhil");
		mer.setMerchantCategoryCode("Open");
		party.setMerchant(mer);
		tbt.setParty(party);

		obReadTransaction2Data.setTransaction(transactionList);
		obReadTransaction2.setData(obReadTransaction2Data);

		accountTransactionsFSTransformer.transformAccountTransaction(transactions, params);
	}

	
	@Test
	public void testtransformAccountTransaction3() {
		TransactionList transactions = null;
		Map<String, String> params = null;
		accountTransactionsFSTransformer.transformAccountTransactions(transactions, params);
	}
	
}
