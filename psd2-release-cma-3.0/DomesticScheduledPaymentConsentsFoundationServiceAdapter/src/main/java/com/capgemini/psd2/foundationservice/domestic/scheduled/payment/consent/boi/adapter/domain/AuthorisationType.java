package com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Type of authorisation flow requested.
 */
public enum AuthorisationType {
  
	Single("Single"),
  
	Multiple("Multiple"),
  
	Any("Any");

  private String value;

  AuthorisationType(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static AuthorisationType fromValue(String text) {
    for (AuthorisationType b : AuthorisationType.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

