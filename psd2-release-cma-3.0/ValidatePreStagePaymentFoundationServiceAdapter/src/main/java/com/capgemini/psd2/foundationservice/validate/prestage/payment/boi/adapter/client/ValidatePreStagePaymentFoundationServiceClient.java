
package com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.client;

import org.springframework.http.HttpHeaders;

import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@FunctionalInterface
public interface ValidatePreStagePaymentFoundationServiceClient {

	public ValidationPassed validatePreStagePayment(RequestInfo requestInfo, PaymentInstruction paymentInstruction, Class<ValidationPassed> response, HttpHeaders httpHeaders);

}
