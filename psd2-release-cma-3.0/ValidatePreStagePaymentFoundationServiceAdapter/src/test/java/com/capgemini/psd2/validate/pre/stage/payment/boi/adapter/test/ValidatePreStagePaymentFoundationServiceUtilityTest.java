package com.capgemini.psd2.validate.pre.stage.payment.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.transformer.ValidatePreStagePaymentFoundationServiceTransformer;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.utility.ValidatePreStagePaymentFoundationServiceUtility;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@RunWith(SpringJUnit4ClassRunner.class)
public class ValidatePreStagePaymentFoundationServiceUtilityTest {

	@InjectMocks
	ValidatePreStagePaymentFoundationServiceUtility validatePreStagePaymentFoundationServiceUtility;
	
	@Mock
	private ValidatePreStagePaymentFoundationServiceTransformer validatePreStagePaymentFSTransformer;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void transformRequestFromAPIToFSTest(){
		
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = new CustomPaymentSetupPOSTRequest();
		
		Mockito.when(validatePreStagePaymentFSTransformer.transformPaymentSetupPOSTRequest(anyObject())).thenReturn(paymentInstruction);
		
		paymentInstruction = validatePreStagePaymentFoundationServiceUtility.transformRequestFromAPIToFS(paymentSetupPOSTRequest);
		assertNotNull(paymentInstruction);
	}
	
	@Test
	public void transformResponseFromFSToAPITest(){
		
		ValidationPassed validationPassed = new ValidationPassed();
		PaymentSetupValidationResponse paymentSetupValidationResponse = new PaymentSetupValidationResponse();
		
		Mockito.when(validatePreStagePaymentFSTransformer.transformValidatePreStagePaymentResponse(anyObject())).thenReturn(paymentSetupValidationResponse);
		
		paymentSetupValidationResponse = validatePreStagePaymentFoundationServiceUtility.transformResponseFromFSToAPI(validationPassed);
		assertNotNull(paymentSetupValidationResponse);
	}
	
	@Test
	public void createRequestHeadersTest(){
		
		ReflectionTestUtils.setField(validatePreStagePaymentFoundationServiceUtility, "userInReqHeader", "X-BOI-USER");
		ReflectionTestUtils.setField(validatePreStagePaymentFoundationServiceUtility, "channelInReqHeader", "X-BOI-CHANNEL");
		ReflectionTestUtils.setField(validatePreStagePaymentFoundationServiceUtility, "platformInReqHeader", "X-BOI-PLATFORM");
		ReflectionTestUtils.setField(validatePreStagePaymentFoundationServiceUtility, "correlationReqHeader", "X-CORRELATION-ID");
		ReflectionTestUtils.setField(validatePreStagePaymentFoundationServiceUtility, "platform", "PSD2API");
		
		HashMap<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_IN_REQ_HEADER, "testUser");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "testChannel");
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, "testCorrelation");
		params.put(PSD2Constants.PLATFORM_IN_REQ_HEADER, "testPlatform");
		
		RequestInfo requestInfo = new RequestInfo();
		CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = new CustomPaymentSetupPOSTRequest();
		HttpHeaders headers = new HttpHeaders();
		
		headers = validatePreStagePaymentFoundationServiceUtility.createRequestHeaders(requestInfo, paymentSetupPOSTRequest, params);
		assertNotNull(headers);
	}
	
}
