package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Schedule object
 */
@ApiModel(description = "Schedule object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class Schedule   {
  @JsonProperty("scheduleDayOfMonthNumber")
  private Double scheduleDayOfMonthNumber = null;

  @JsonProperty("scheduleNextDate")
  private LocalDate scheduleNextDate = null;

  @JsonProperty("scheduleStartDate")
  private LocalDate scheduleStartDate = null;

  @JsonProperty("scheduleEndDate")
  private LocalDate scheduleEndDate = null;

  @JsonProperty("scheduleRegularityTypeCode")
  private ScheduleRegularityTypeCode scheduleRegularityTypeCode = null;

  @JsonProperty("scheduleItemsList")
  @Valid
  private List<ScheduleItem> scheduleItemsList = null;

  public Schedule scheduleDayOfMonthNumber(Double scheduleDayOfMonthNumber) {
    this.scheduleDayOfMonthNumber = scheduleDayOfMonthNumber;
    return this;
  }

  /**
   * The day of month number for schedule
   * @return scheduleDayOfMonthNumber
  **/
  @ApiModelProperty(value = "The day of month number for schedule")


  public Double getScheduleDayOfMonthNumber() {
    return scheduleDayOfMonthNumber;
  }

  public void setScheduleDayOfMonthNumber(Double scheduleDayOfMonthNumber) {
    this.scheduleDayOfMonthNumber = scheduleDayOfMonthNumber;
  }

  public Schedule scheduleNextDate(LocalDate scheduleNextDate) {
    this.scheduleNextDate = scheduleNextDate;
    return this;
  }

  /**
   * Next date for schedule
   * @return scheduleNextDate
  **/
  @ApiModelProperty(value = "Next date for schedule")

  @Valid

  public LocalDate getScheduleNextDate() {
    return scheduleNextDate;
  }

  public void setScheduleNextDate(LocalDate scheduleNextDate) {
    this.scheduleNextDate = scheduleNextDate;
  }

  public Schedule scheduleStartDate(LocalDate scheduleStartDate) {
    this.scheduleStartDate = scheduleStartDate;
    return this;
  }

  /**
   * Start date for schedule
   * @return scheduleStartDate
  **/
  @ApiModelProperty(value = "Start date for schedule")

  @Valid

  public LocalDate getScheduleStartDate() {
    return scheduleStartDate;
  }

  public void setScheduleStartDate(LocalDate scheduleStartDate) {
    this.scheduleStartDate = scheduleStartDate;
  }

  public Schedule scheduleEndDate(LocalDate scheduleEndDate) {
    this.scheduleEndDate = scheduleEndDate;
    return this;
  }

  /**
   * End date for schedule
   * @return scheduleEndDate
  **/
  @ApiModelProperty(value = "End date for schedule")

  @Valid

  public LocalDate getScheduleEndDate() {
    return scheduleEndDate;
  }

  public void setScheduleEndDate(LocalDate scheduleEndDate) {
    this.scheduleEndDate = scheduleEndDate;
  }

  public Schedule scheduleRegularityTypeCode(ScheduleRegularityTypeCode scheduleRegularityTypeCode) {
    this.scheduleRegularityTypeCode = scheduleRegularityTypeCode;
    return this;
  }

  /**
   * Get scheduleRegularityTypeCode
   * @return scheduleRegularityTypeCode
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ScheduleRegularityTypeCode getScheduleRegularityTypeCode() {
    return scheduleRegularityTypeCode;
  }

  public void setScheduleRegularityTypeCode(ScheduleRegularityTypeCode scheduleRegularityTypeCode) {
    this.scheduleRegularityTypeCode = scheduleRegularityTypeCode;
  }

  public Schedule scheduleItemsList(List<ScheduleItem> scheduleItemsList) {
    this.scheduleItemsList = scheduleItemsList;
    return this;
  }

  public Schedule addScheduleItemsListItem(ScheduleItem scheduleItemsListItem) {
    if (this.scheduleItemsList == null) {
      this.scheduleItemsList = new ArrayList<ScheduleItem>();
    }
    this.scheduleItemsList.add(scheduleItemsListItem);
    return this;
  }

  /**
   * Get scheduleItemsList
   * @return scheduleItemsList
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<ScheduleItem> getScheduleItemsList() {
    return scheduleItemsList;
  }

  public void setScheduleItemsList(List<ScheduleItem> scheduleItemsList) {
    this.scheduleItemsList = scheduleItemsList;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Schedule schedule = (Schedule) o;
    return Objects.equals(this.scheduleDayOfMonthNumber, schedule.scheduleDayOfMonthNumber) &&
        Objects.equals(this.scheduleNextDate, schedule.scheduleNextDate) &&
        Objects.equals(this.scheduleStartDate, schedule.scheduleStartDate) &&
        Objects.equals(this.scheduleEndDate, schedule.scheduleEndDate) &&
        Objects.equals(this.scheduleRegularityTypeCode, schedule.scheduleRegularityTypeCode) &&
        Objects.equals(this.scheduleItemsList, schedule.scheduleItemsList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(scheduleDayOfMonthNumber, scheduleNextDate, scheduleStartDate, scheduleEndDate, scheduleRegularityTypeCode, scheduleItemsList);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Schedule {\n");
    
    sb.append("    scheduleDayOfMonthNumber: ").append(toIndentedString(scheduleDayOfMonthNumber)).append("\n");
    sb.append("    scheduleNextDate: ").append(toIndentedString(scheduleNextDate)).append("\n");
    sb.append("    scheduleStartDate: ").append(toIndentedString(scheduleStartDate)).append("\n");
    sb.append("    scheduleEndDate: ").append(toIndentedString(scheduleEndDate)).append("\n");
    sb.append("    scheduleRegularityTypeCode: ").append(toIndentedString(scheduleRegularityTypeCode)).append("\n");
    sb.append("    scheduleItemsList: ").append(toIndentedString(scheduleItemsList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

