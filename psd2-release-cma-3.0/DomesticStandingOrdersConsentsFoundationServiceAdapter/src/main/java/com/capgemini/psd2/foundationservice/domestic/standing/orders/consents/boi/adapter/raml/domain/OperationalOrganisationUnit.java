package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Account object
 */
@ApiModel(description = "Account object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class OperationalOrganisationUnit   {
  @JsonProperty("addressReference")
  private Address addressReference = null;

  public OperationalOrganisationUnit addressReference(Address addressReference) {
    this.addressReference = addressReference;
    return this;
  }

  /**
   * Get addressReference
   * @return addressReference
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public Address getAddressReference() {
    return addressReference;
  }

  public void setAddressReference(Address addressReference) {
    this.addressReference = addressReference;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OperationalOrganisationUnit operationalOrganisationUnit = (OperationalOrganisationUnit) o;
    return Objects.equals(this.addressReference, operationalOrganisationUnit.addressReference);
  }

  @Override
  public int hashCode() {
    return Objects.hash(addressReference);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OperationalOrganisationUnit {\n");
    
    sb.append("    addressReference: ").append(toIndentedString(addressReference)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

