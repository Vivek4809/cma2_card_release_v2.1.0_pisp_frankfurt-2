package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * The device session that originated the event
 */
@ApiModel(description = "The device session that originated the event")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class ElectronicDevice   {
  @JsonProperty("deviceUDIDUniqueDeviceIdentifier")
  private String deviceUDIDUniqueDeviceIdentifier = null;

  @JsonProperty("deviceAddress")
  private String deviceAddress = null;

  @JsonProperty("deviceData")
  @Valid
  private List<ElectronicDeviceData> deviceData = new ArrayList<ElectronicDeviceData>();

  @JsonProperty("deviceLocation")
  private DeviceLocation deviceLocation = null;

  public ElectronicDevice deviceUDIDUniqueDeviceIdentifier(String deviceUDIDUniqueDeviceIdentifier) {
    this.deviceUDIDUniqueDeviceIdentifier = deviceUDIDUniqueDeviceIdentifier;
    return this;
  }

  /**
   * Get deviceUDIDUniqueDeviceIdentifier
   * @return deviceUDIDUniqueDeviceIdentifier
  **/
  @ApiModelProperty(value = "")


  public String getDeviceUDIDUniqueDeviceIdentifier() {
    return deviceUDIDUniqueDeviceIdentifier;
  }

  public void setDeviceUDIDUniqueDeviceIdentifier(String deviceUDIDUniqueDeviceIdentifier) {
    this.deviceUDIDUniqueDeviceIdentifier = deviceUDIDUniqueDeviceIdentifier;
  }

  public ElectronicDevice deviceAddress(String deviceAddress) {
    this.deviceAddress = deviceAddress;
    return this;
  }

  /**
   * Four octets of the IP address.
   * @return deviceAddress
  **/
  @ApiModelProperty(required = true, value = "Four octets of the IP address.")
  @NotNull


  public String getDeviceAddress() {
    return deviceAddress;
  }

  public void setDeviceAddress(String deviceAddress) {
    this.deviceAddress = deviceAddress;
  }

  public ElectronicDevice deviceData(List<ElectronicDeviceData> deviceData) {
    this.deviceData = deviceData;
    return this;
  }

  public ElectronicDevice addDeviceDataItem(ElectronicDeviceData deviceDataItem) {
    this.deviceData.add(deviceDataItem);
    return this;
  }

  /**
   * Get deviceData
   * @return deviceData
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public List<ElectronicDeviceData> getDeviceData() {
    return deviceData;
  }

  public void setDeviceData(List<ElectronicDeviceData> deviceData) {
    this.deviceData = deviceData;
  }

  public ElectronicDevice deviceLocation(DeviceLocation deviceLocation) {
    this.deviceLocation = deviceLocation;
    return this;
  }

  /**
   * Get deviceLocation
   * @return deviceLocation
  **/
  @ApiModelProperty(value = "")

  @Valid

  public DeviceLocation getDeviceLocation() {
    return deviceLocation;
  }

  public void setDeviceLocation(DeviceLocation deviceLocation) {
    this.deviceLocation = deviceLocation;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ElectronicDevice electronicDevice = (ElectronicDevice) o;
    return Objects.equals(this.deviceUDIDUniqueDeviceIdentifier, electronicDevice.deviceUDIDUniqueDeviceIdentifier) &&
        Objects.equals(this.deviceAddress, electronicDevice.deviceAddress) &&
        Objects.equals(this.deviceData, electronicDevice.deviceData) &&
        Objects.equals(this.deviceLocation, electronicDevice.deviceLocation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(deviceUDIDUniqueDeviceIdentifier, deviceAddress, deviceData, deviceLocation);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ElectronicDevice {\n");
    
    sb.append("    deviceUDIDUniqueDeviceIdentifier: ").append(toIndentedString(deviceUDIDUniqueDeviceIdentifier)).append("\n");
    sb.append("    deviceAddress: ").append(toIndentedString(deviceAddress)).append("\n");
    sb.append("    deviceData: ").append(toIndentedString(deviceData)).append("\n");
    sb.append("    deviceLocation: ").append(toIndentedString(deviceLocation)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

