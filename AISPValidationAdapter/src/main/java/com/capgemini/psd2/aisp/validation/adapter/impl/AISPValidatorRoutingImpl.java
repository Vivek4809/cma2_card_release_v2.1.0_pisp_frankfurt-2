package com.capgemini.psd2.aisp.validation.adapter.impl;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;

@Component("accountsRoutingValidator")
public class AISPValidatorRoutingImpl<T, V> implements AISPCustomValidator<T>, ApplicationContextAware {
	
	@Value("${app.customValidationBean:null}")
	private String beanName;
	
	private ApplicationContext applicationContext;

	@SuppressWarnings("unchecked")
	public AISPCustomValidator<T> getAccountsValidatorInstance(String beanName) {
		return (AISPCustomValidator<T>) applicationContext.getBean(beanName);
	}

	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	@Override
	public boolean validateAccountsResponse(T t) {
		return getAccountsValidatorInstance(beanName).validateAccountsResponse(t);
	}

}
