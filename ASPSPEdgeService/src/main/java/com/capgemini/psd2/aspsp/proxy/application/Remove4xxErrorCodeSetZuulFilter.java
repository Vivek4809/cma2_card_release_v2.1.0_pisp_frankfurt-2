/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.aspsp.proxy.application;
import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

/**
 * The Class Remove4xxErrorCodeSetZuulFilter.
 */
@Component
public class Remove4xxErrorCodeSetZuulFilter extends ZuulFilter
{
    
    /** The Constant ERROR_STATUS_CODE_KEY. */
    private static final String ERROR_STATUS_CODE_KEY = "error.status_code";

    /* (non-Javadoc)
     * @see com.netflix.zuul.IZuulFilter#shouldFilter()
     */
    @Override
    public boolean shouldFilter()
    {
        Integer statusCode = (Integer) RequestContext.getCurrentContext().get(ERROR_STATUS_CODE_KEY);
        return statusCode != null && statusCode >= 400 && statusCode < 500;
    }

    /* (non-Javadoc)
     * @see com.netflix.zuul.IZuulFilter#run()
     */
    @Override
    public Object run()
    {
        RequestContext.getCurrentContext().set(ERROR_STATUS_CODE_KEY, null);
        return null;
    }

    /* (non-Javadoc)
     * @see com.netflix.zuul.ZuulFilter#filterType()
     */
    @Override
    public String filterType()
    {
        return "post";
    }

    /* (non-Javadoc)
     * @see com.netflix.zuul.ZuulFilter#filterOrder()
     */
    @Override
    public int filterOrder()
    {
        return Integer.MIN_VALUE;
    }

}
