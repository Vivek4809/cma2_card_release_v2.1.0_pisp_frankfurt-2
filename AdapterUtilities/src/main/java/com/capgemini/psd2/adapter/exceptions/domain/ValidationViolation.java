
package com.capgemini.psd2.adapter.exceptions.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "errorCode",
    "errorText",
    "errorField",
    "errorValue"
})
public class ValidationViolation {

    @JsonProperty("errorCode")
    private String errorCode;
    @JsonProperty("errorText")
    private String errorText;
    @JsonProperty("errorField")
    private String errorField;
    @JsonProperty("errorValue")
    private String errorValue;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ValidationViolation() {
    }

    /**
     * 
     * @param errorText
     * @param errorCode
     * @param errorValue
     * @param errorField
     */
    public ValidationViolation(String errorCode, String errorText, String errorField, String errorValue) {
        super();
        this.errorCode = errorCode;
        this.errorText = errorText;
        this.errorField = errorField;
        this.errorValue = errorValue;
    }

    @JsonProperty("errorCode")
    public String getErrorCode() {
        return errorCode;
    }

    @JsonProperty("errorCode")
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @JsonProperty("errorText")
    public String getErrorText() {
        return errorText;
    }

    @JsonProperty("errorText")
    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }

    @JsonProperty("errorField")
    public String getErrorField() {
        return errorField;
    }

    @JsonProperty("errorField")
    public void setErrorField(String errorField) {
        this.errorField = errorField;
    }

    @JsonProperty("errorValue")
    public String getErrorValue() {
        return errorValue;
    }

    @JsonProperty("errorValue")
    public void setErrorValue(String errorValue) {
        this.errorValue = errorValue;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("errorCode", errorCode).append("errorText", errorText).append("errorField", errorField).append("errorValue", errorValue).toString();
    }

}
