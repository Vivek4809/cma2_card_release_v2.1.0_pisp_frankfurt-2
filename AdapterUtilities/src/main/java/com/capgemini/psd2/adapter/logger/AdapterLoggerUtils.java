/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.adapter.logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.utilities.DateUtilites;

/**
 * The Class LoggerUtils.
 */
@Component
public class AdapterLoggerUtils {

	@Autowired
	private LoggerUtils loggerUtils;
	
	/** The logger attribute. */
	@Autowired
	private LoggerAttribute loggerAttribute;
	
	/** The foundation Service api id. */
	@Value("${foundationService.apiId}")
	private String foundationServiceApiId;
	
	public LoggerAttribute populateLoggerStartData(String methodName){
		loggerUtils.populateLoggerData(methodName);
		loggerAttribute.setUpstream_api_id(foundationServiceApiId);
		loggerAttribute.setUpstream_start_time(DateUtilites.generateCurrentTimeStamp());
		loggerAttribute.setUpstream_end_time(null);
		loggerAttribute.setUpstream_request_payload(null);
		return loggerAttribute;
	}
	
	public LoggerAttribute populateLoggerEndData(String methodName){
		loggerUtils.populateLoggerData(methodName);
		loggerAttribute.setUpstream_api_id(foundationServiceApiId);
		loggerAttribute.setUpstream_end_time(DateUtilites.generateCurrentTimeStamp());
		loggerAttribute.setUpstream_response_payload(null);
		return loggerAttribute;
	}

}
