package com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.stereotype.Component;

@Component
@Configuration
@EnableRetry
public class InsertPreStagePaymentFoundationServiceConfig {

}
