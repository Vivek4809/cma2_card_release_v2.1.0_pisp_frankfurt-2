package com.capgemini.psd2.foundationservice.insertprestagepayment.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.InsertPreStagePaymentFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.client.InsertPreStagePaymentFoundationClient;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.constants.InsertPreStagePaymentFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.insert.prestage.payment.boi.adapter.utility.InsertPreStagePaymentFoundationServiceUtility;
import com.capgemini.psd2.pisp.domain.CreditorAccount;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.PaymentSetup;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupStagingResponse;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.pisp.domain.Risk;
import com.capgemini.psd2.pisp.domain.Risk.PaymentContextCodeEnum;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;

@RunWith(SpringJUnit4ClassRunner.class)
public class InsertPreStagePaymentFoundationServiceAdapterTest {
	
	@InjectMocks
	private InsertPreStagePaymentFoundationServiceAdapter adapter;
	
	@Mock
	private InsertPreStagePaymentFoundationServiceUtility insertPreStagePaymentFoundationServiceUtility;
	
	@Mock
	private InsertPreStagePaymentFoundationClient insertPreStagePaymentFoundationClient;
	
	@Mock
	private AdapterUtility utility;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void createStagingPaymentSetupTest(){
		
		HttpHeaders httpHeaders = new HttpHeaders();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		ValidationPassed validationPassed = new ValidationPassed();
		PaymentSetupStagingResponse paymentSetupStagingResponse = new PaymentSetupStagingResponse();
		
		ReflectionTestUtils.setField(adapter, "insertPreStagePaymentBaseURL", "test");
		
		Map<String, String> params = new HashMap<String, String>();

		params.put(InsertPreStagePaymentFoundationServiceConstants.USER_ID, "testUser");
		params.put(InsertPreStagePaymentFoundationServiceConstants.CHANNEL_ID, "BOL");
		params.put(InsertPreStagePaymentFoundationServiceConstants.CORRELATION_ID, "testCorrelation");
		params.put(InsertPreStagePaymentFoundationServiceConstants.PLATFORM_ID, "testPlatform");
		params.put(InsertPreStagePaymentFoundationServiceConstants.VALIDATION_STATUS, "Pending");
		
		CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest = new CustomPaymentSetupPOSTRequest();
		PaymentSetup data = new PaymentSetup();
		PaymentSetupInitiation initiation = new PaymentSetupInitiation();
		
		initiation.setInstructionIdentification("ANSM023");
		initiation.setEndToEndIdentification("FRESCO.21302.GFX.37");
		
		DebtorAccount debtorAccount = new DebtorAccount();
		debtorAccount.setIdentification("01234567");
		debtorAccount.setName("Andrea Smith");
		debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
		
		CreditorAccount creditorAccount = new CreditorAccount();
		creditorAccount.setIdentification("21325698");
		creditorAccount.setName("Bob Clements");
		creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER);

		PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
		instructedAmount.setAmount("30.00");
		instructedAmount.setCurrency("GBP");
		
		RemittanceInformation remittanceInformation = new RemittanceInformation();
		remittanceInformation.setReference("FRESCO-037");
		remittanceInformation.setUnstructured("Internal ops code 5120103");
		
		RiskDeliveryAddress riskDeliveryAddress = new RiskDeliveryAddress();
		
		initiation.setCreditorAccount(creditorAccount);
		initiation.setDebtorAccount(debtorAccount);
		initiation.setInstructedAmount(instructedAmount);
		initiation.setRemittanceInformation(remittanceInformation);
		
		
		Risk risk = new Risk();
		risk.setPaymentContextCode(PaymentContextCodeEnum.PERSONTOPERSON);
		risk.setDeliveryAddress(riskDeliveryAddress);
		
		data.setInitiation(initiation);
		
			
		paymentSetupPOSTRequest.setData(data);
		paymentSetupPOSTRequest.setRisk(risk);
		
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject())).thenReturn(httpHeaders);
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.transformResponseFromAPIToFDForInsert(anyObject(), anyObject())).thenReturn(paymentInstruction);
		Mockito.when(insertPreStagePaymentFoundationClient.restTransportForInsertPreStagePayment(anyObject(), anyObject(), anyObject(), anyObject())).thenReturn(validationPassed);
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.transformResponseFromFDToAPIForInsert(anyObject())).thenReturn(paymentSetupStagingResponse);
		paymentSetupStagingResponse = adapter.createStagingPaymentSetup(paymentSetupPOSTRequest, params);
		assertNotNull(paymentSetupStagingResponse);
	}
	
	@Test(expected = AdapterException.class)
	public void createStagingPaymentSetupAdapterExceptionTest(){
		
		HttpHeaders httpHeaders = new HttpHeaders();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		CustomPaymentSetupPOSTRequest pr = new CustomPaymentSetupPOSTRequest();
        ReflectionTestUtils.setField(adapter, "insertPreStagePaymentBaseURL", null);
        Map<String, String> params = new HashMap<String, String>();
        ErrorInfo errorInfo = new ErrorInfo();
        errorInfo.setErrorCode("test");
        errorInfo.setErrorMessage("test");
      
        Mockito.when(insertPreStagePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject())).thenReturn(httpHeaders);
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.transformResponseFromAPIToFDForInsert(anyObject(), anyObject())).thenReturn(paymentInstruction);
		Mockito.when(insertPreStagePaymentFoundationClient.restTransportForInsertPreStagePayment(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("test", new ErrorInfo()));
		adapter.createStagingPaymentSetup(pr, params);
		
	}
	
	@Test(expected = Exception.class)
	public void createStagingPaymentSetupExceptionTest(){
		
		HttpHeaders httpHeaders = new HttpHeaders();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		CustomPaymentSetupPOSTRequest pr = new CustomPaymentSetupPOSTRequest();
        ReflectionTestUtils.setField(adapter, "insertPreStagePaymentBaseURL", null);
        Map<String, String> params = new HashMap<String, String>();
        ErrorInfo errorInfo = new ErrorInfo();
        errorInfo.setErrorCode("test");
        errorInfo.setErrorMessage("test");
       
      
        Mockito.when(insertPreStagePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject())).thenReturn(httpHeaders);
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.transformResponseFromAPIToFDForInsert(anyObject(), anyObject())).thenReturn(paymentInstruction);
		Mockito.when(insertPreStagePaymentFoundationClient.restTransportForInsertPreStagePayment(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_SUBMISSION_PAYLOAD_COMPARISON_FAILED));
		adapter.createStagingPaymentSetup(pr, params);
		
	}
	
	
	@Test
	public void retrieveStagedPaymentSetupTest(){
		
		HttpHeaders httpHeaders = new HttpHeaders();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		Map<String, String> params = new HashMap<String, String>();
		String paymentId = "123test";
		

		
		ReflectionTestUtils.setField(adapter, "paymentRetrievalBaseURL", "test");
		ReflectionTestUtils.setField(adapter, "paymentRetrievalEndURL", "test");
		
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject())).thenReturn(httpHeaders);
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.getFoundationServiceURL(anyObject(), anyObject(), anyObject())).thenReturn("TestURL");
		Mockito.when(insertPreStagePaymentFoundationClient.restTransportForPaymentRetrieval(anyObject(), anyObject(), anyObject())).thenReturn(paymentInstruction);
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.transformResponseFromFDToAPI(anyObject())).thenReturn(paymentSetupPOSTResponse);
		
		paymentSetupPOSTResponse = adapter.retrieveStagedPaymentSetup(paymentId, params);
		assertNotNull(paymentSetupPOSTResponse);
	}
	
	@Test(expected = AdapterException.class)
	public void retrieveStagedPaymentSetupAdapterExceptionTest(){
		
		HttpHeaders httpHeaders = new HttpHeaders();
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		Map<String, String> params = new HashMap<String, String>();
		String paymentId = "123test";
		ErrorInfo errorInfo = new ErrorInfo();
        errorInfo.setErrorCode("test");
        errorInfo.setErrorMessage("test");

		
		ReflectionTestUtils.setField(adapter, "paymentRetrievalBaseURL", "test");
		ReflectionTestUtils.setField(adapter, "paymentRetrievalEndURL", "test");
		
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject())).thenReturn(httpHeaders);
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.getFoundationServiceURL(anyObject(), anyObject(), anyObject())).thenReturn("TestURL");
		Mockito.when(insertPreStagePaymentFoundationClient.restTransportForPaymentRetrieval(anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("test", errorInfo));
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.transformResponseFromFDToAPI(anyObject())).thenReturn(paymentSetupPOSTResponse);
		
		adapter.retrieveStagedPaymentSetup(paymentId, params);
		
	}
	
	@Test(expected = Exception.class)
	public void retrieveStagedPaymentSetupExceptionTest(){
		
		HttpHeaders httpHeaders = new HttpHeaders();
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse = new CustomPaymentSetupPOSTResponse();
		Map<String, String> params = new HashMap<String, String>();
		String paymentId = "123test";
		ErrorInfo errorInfo = new ErrorInfo();
        errorInfo.setErrorCode("test");
        errorInfo.setErrorMessage("test");

		
		ReflectionTestUtils.setField(adapter, "paymentRetrievalBaseURL", "test");
		ReflectionTestUtils.setField(adapter, "paymentRetrievalEndURL", "test");
		
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject())).thenReturn(httpHeaders);
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.getFoundationServiceURL(anyObject(), anyObject(), anyObject())).thenReturn("TestURL");
		Mockito.when(insertPreStagePaymentFoundationClient.restTransportForPaymentRetrieval(anyObject(), anyObject(), anyObject())).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONNECTION_ERROR));
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.transformResponseFromFDToAPI(anyObject())).thenReturn(paymentSetupPOSTResponse);
		
		adapter.retrieveStagedPaymentSetup(paymentId, params);
	}
	
	@Test
	public void updateStagedPaymentSetupTest(){
		
		HttpHeaders httpHeaders = new HttpHeaders();
		Map<String, String> params = new HashMap<String, String>();
		ValidationPassed validationPassed = new ValidationPassed();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		CustomPaymentSetupPOSTResponse paymentSetupPostResp = new CustomPaymentSetupPOSTResponse();
		PaymentSetupStagingResponse paymentSetupStagingResponse = new PaymentSetupStagingResponse();
		
		ReflectionTestUtils.setField(adapter, "updatePreStagePaymentBaseURL", "test");
		
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject())).thenReturn(httpHeaders);
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.transformResponseFromAPIToFDForUpdate(anyObject(), anyObject())).thenReturn(paymentInstruction);
		Mockito.when(insertPreStagePaymentFoundationClient.restTransportForUpdatePreStagePayment(anyObject(), anyObject(), anyObject(), anyObject())).thenReturn(validationPassed);
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.transformResponseFromFDToAPIForUpdate(anyObject())).thenReturn(paymentSetupStagingResponse);
		
		paymentSetupStagingResponse = adapter.updateStagedPaymentSetup(paymentSetupPostResp, params);
		assertNotNull(paymentSetupStagingResponse);
	}
	
	@Test(expected = AdapterException.class)
	public void updateStagedPaymentSetupAdapterExceptionTest(){
		
		HttpHeaders httpHeaders = new HttpHeaders();
		Map<String, String> params = new HashMap<String, String>();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		CustomPaymentSetupPOSTResponse paymentSetupPostResp = new CustomPaymentSetupPOSTResponse();
		PaymentSetupStagingResponse paymentSetupStagingResponse = new PaymentSetupStagingResponse();
		
		ReflectionTestUtils.setField(adapter, "updatePreStagePaymentBaseURL", "test");
		ErrorInfo errorInfo = new ErrorInfo();
        errorInfo.setErrorCode("test");
        errorInfo.setErrorMessage("test");
		
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject())).thenReturn(httpHeaders);
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.transformResponseFromAPIToFDForUpdate(anyObject(), anyObject())).thenReturn(paymentInstruction);
		Mockito.when(insertPreStagePaymentFoundationClient.restTransportForUpdatePreStagePayment(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("test", errorInfo));
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.transformResponseFromFDToAPIForUpdate(anyObject())).thenReturn(paymentSetupStagingResponse);
		
		adapter.updateStagedPaymentSetup(paymentSetupPostResp, params);
	}
	
	
	@Test(expected = AdapterException.class)
	public void updateStagedPaymentSetupExceptionTest(){
		
		HttpHeaders httpHeaders = new HttpHeaders();
		Map<String, String> params = new HashMap<String, String>();
		PaymentInstruction paymentInstruction = new PaymentInstruction();
		CustomPaymentSetupPOSTResponse paymentSetupPostResp = new CustomPaymentSetupPOSTResponse();
		PaymentSetupStagingResponse paymentSetupStagingResponse = new PaymentSetupStagingResponse();
		
		ReflectionTestUtils.setField(adapter, "updatePreStagePaymentBaseURL", "test");
		ErrorInfo errorInfo = new ErrorInfo();
        errorInfo.setErrorCode("test");
        errorInfo.setErrorMessage("test");
		
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject())).thenReturn(httpHeaders);
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.transformResponseFromAPIToFDForUpdate(anyObject(), anyObject())).thenReturn(paymentInstruction);
		Mockito.when(insertPreStagePaymentFoundationClient.restTransportForUpdatePreStagePayment(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONNECTION_ERROR));
		Mockito.when(insertPreStagePaymentFoundationServiceUtility.transformResponseFromFDToAPIForUpdate(anyObject())).thenReturn(paymentSetupStagingResponse);
		
		adapter.updateStagedPaymentSetup(paymentSetupPostResp, params);
	}
	
	
	
}
