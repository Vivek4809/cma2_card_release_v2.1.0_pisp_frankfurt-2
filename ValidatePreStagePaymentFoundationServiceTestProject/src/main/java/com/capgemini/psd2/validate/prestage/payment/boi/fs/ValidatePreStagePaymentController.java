package com.capgemini.psd2.validate.prestage.payment.boi.fs;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.ValidatePreStagePaymentFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.validate.prestage.payment.boi.adapter.constants.ValidatePreStagePaymentFoundationServiceConstants;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;

@RestController
@RequestMapping("/testValidatePreStagePayment")
public class ValidatePreStagePaymentController {
	
	@Autowired
	private ValidatePreStagePaymentFoundationServiceAdapter adapter;

	@RequestMapping(method = RequestMethod.POST, consumes= {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public PaymentSetupValidationResponse getResponse(
			@RequestBody CustomPaymentSetupPOSTRequest paymentSetupPOSTRequest,
			@RequestHeader(required = false, value = "X-BOI-USER") String boiUser,
			@RequestHeader(required = false, value = "X-BOI-CHANNEL") String boiChannel,
			@RequestHeader(required = false, value = "X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false, value = "X-CORRELATION-ID") String correlationID) {
		
		 Map<String, String> params = new HashMap<String, String>();

		    params.put(ValidatePreStagePaymentFoundationServiceConstants.USER_ID, boiUser);
		    params.put(ValidatePreStagePaymentFoundationServiceConstants.CHANNEL_ID, boiChannel);
		    params.put(ValidatePreStagePaymentFoundationServiceConstants.CORRELATION_ID, correlationID);
		    params.put(ValidatePreStagePaymentFoundationServiceConstants.PLATFORM_ID, boiPlatform);

		   return adapter.validatePreStagePaymentSetup(paymentSetupPOSTRequest, params);
		   

		   // return paymentSetupPreValidationResponse;


	}
	
}
