package com.capgemini.psd2.foundationservice.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceAuthException;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.utilities.MockServiceUtility;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;

@Component
public class ValidationUtility {

	@Autowired
	private MockServiceUtility mockServiceUtility;

	public ValidationPassed validateMockBusinessValidations(String endToEndIdentification, String successCode, String successMessage) {

		ValidationPassed validationPassed = new ValidationPassed();

		if (!NullCheckUtils.isNullOrEmpty(endToEndIdentification)) {
			ErrorCodeEnum errorCodeEnum = null;
			String validationError = mockServiceUtility.getValidationError().get(endToEndIdentification);
			if (!NullCheckUtils.isNullOrEmpty(validationError)) {
				errorCodeEnum = ErrorCodeEnum.valueOf(validationError);
				throw MockFoundationServiceException.populateMockFoundationServiceException(errorCodeEnum);
			}
			else {
				validationPassed.setSuccessCode(successCode);
				validationPassed.setSuccessMessage(successMessage);
			}
		}
		return validationPassed;
	}

	public void validateUserLogin(String loginId) {

		if (!NullCheckUtils.isNullOrEmpty(loginId)) {

			ErrorCodeEnum errorCodeEnum = null;
			String validationError = mockServiceUtility.getValidationError().get(loginId);
			
			if (!NullCheckUtils.isNullOrEmpty(validationError) && validationError.contains("AUTHENTICATION_FAILURE_LOGIN_AUTH")) {
				errorCodeEnum = ErrorCodeEnum.valueOf(validationError);
				throw MockFoundationServiceAuthException.populateMockFoundationServiceException(errorCodeEnum, 0);
			}
			else if (!NullCheckUtils.isNullOrEmpty(validationError)) {
				errorCodeEnum = ErrorCodeEnum.valueOf(validationError);
				throw MockFoundationServiceAuthException.populateMockFoundationServiceException(errorCodeEnum);
			}
		}

	}
}
