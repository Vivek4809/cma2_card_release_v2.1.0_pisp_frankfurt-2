package com.capgemini.psd2.scaconsenthelper.models;

public class PFInstanceData {
	
	private String pfInstanceId;
	private String pfInstanceUserName;
	private String pfInstanceUserPwd;
	
	public String getPfInstanceId() {
		return pfInstanceId;
	}
	public void setPfInstanceId(String pfInstanceId) {
		this.pfInstanceId = pfInstanceId;
	}
	public String getPfInstanceUserName() {
		return pfInstanceUserName;
	}
	public void setPfInstanceUserName(String pfInstanceUserName) {
		this.pfInstanceUserName = pfInstanceUserName;
	}
	public String getPfInstanceUserPwd() {
		return pfInstanceUserPwd;
	}
	public void setPfInstanceUserPwd(String pfInstanceUserPwd) {
		this.pfInstanceUserPwd = pfInstanceUserPwd;
	}
	
	

}
