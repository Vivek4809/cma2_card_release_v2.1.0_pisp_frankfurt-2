package com.capgemini.psd2.scaconsenthelper.config.helpers;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;

public final class SCAConsentHelper {
	
	private SCAConsentHelper(){
		
	}
	
	public static PickupDataModel populatePickupDataModel(HttpServletRequest request){
		PickupDataModel pickUpDataModel = (PickupDataModel)request.getAttribute(SCAConsentHelperConstants.INTENT_DATA);
		return pickUpDataModel;
	}	
	
	public static void invalidateCookie(HttpServletResponse response){
		Cookie cookie = new Cookie(SCAConsentHelperConstants.CUSTOM_SESSION_CONTROLL_COOKIE, null);
		cookie.setMaxAge(0);
		cookie.setHttpOnly(true);
		cookie.setSecure(true);
		response.addCookie(cookie);
	}
	
	public static void populateSecurityHeaders(HttpServletResponse response){
		 response.setHeader(PSD2SecurityConstants.X_XSS_PROTECTION_HEADER,PSD2SecurityConstants.X_XSS_PROTECTION_VALUE);
	    response.setHeader(PSD2SecurityConstants.CACHE_CONTROL_HEADER,PSD2SecurityConstants.CACHE_CONTROL_VALUE);
	    response.setHeader(PSD2SecurityConstants.PRAGMA_HEADER,PSD2SecurityConstants.PRAGMA_VALUE);
	    response.setHeader(PSD2SecurityConstants.X_FRAME_OPTIONS_HEADER,PSD2SecurityConstants.X_FRAME_OPTIONS_VALUE);
	    response.setHeader(PSD2SecurityConstants.X_CONTENT_TYPE_OPTIONS_HEADER,PSD2SecurityConstants.X_CONTENT_TYPE_OPTIONS_VALUE);
	}
}
