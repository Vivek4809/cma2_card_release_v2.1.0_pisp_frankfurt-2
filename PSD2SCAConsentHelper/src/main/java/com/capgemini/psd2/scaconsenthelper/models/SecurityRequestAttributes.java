package com.capgemini.psd2.scaconsenthelper.models;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SecurityRequestAttributes {

/*	private String flowType = null;
	private String callBackEncUrl = null;
	private String requestIdParamName = null;
*/	private Map<String,String[]> paramMap = null;

	
	

	
	/*private AuthorizedTokenDTO authorizedTokenDTO = null;
	private String code;*/

	
	/*	
	public String getFlowType() {
		return flowType;
	}
	public void setFlowType(String flowType) {
		this.flowType = flowType;
	}
	public String getCallBackEncUrl() {
		return callBackEncUrl;
	}
	public void setCallBackEncUrl(String callBackEncUrl) {
		this.callBackEncUrl = callBackEncUrl;
	}*/
	public Map<String, String[]> getParamMap() {
		return paramMap;
	}
	public void setParamMap(Map<String, String[]> paramMap) {
		this.paramMap = paramMap;
	}
/*	public String getRequestIdParamName() {
		return requestIdParamName;
	}
	public void setRequestIdParamName(String requestIdParamName) {
		this.requestIdParamName = requestIdParamName;
	}
	public AuthorizedTokenDTO getAuthorizedTokenDTO() {
		return authorizedTokenDTO;
	}
	public void setAuthorizedTokenDTO(AuthorizedTokenDTO authorizedTokenDTO) {
		this.authorizedTokenDTO = authorizedTokenDTO;
	}*/
	
/*	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}*/
}