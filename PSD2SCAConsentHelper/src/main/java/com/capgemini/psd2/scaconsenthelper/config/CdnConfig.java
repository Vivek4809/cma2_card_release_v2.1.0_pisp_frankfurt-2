package com.capgemini.psd2.scaconsenthelper.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.security.constants.PSD2SecurityConstants;

@Configuration
public class CdnConfig {

     private static String cdnBaseURL;
	
	private static String libsHashCode;
	
	private static String templateHashCode;
	
	private static String appHashCode;
	
	private static String buildVersion;
	
	private static String b365ScaHashCode;
	
	private static String b365ScaBuildVersion;
	
	
	@Value("${cdn.baseURL}")
	public  void setCdnBaseURL(String cdnBaseURL) {
		CdnConfig.cdnBaseURL = cdnBaseURL;
	}
	@Value("${cdn.libsHashCode}")
	public  void setLibsHashCode(String libsHashCode) {
		CdnConfig.libsHashCode = libsHashCode;
	}
	@Value("${cdn.templateHashCode}")
	public  void setTemplateHashCode(String templateHashCode) {
		CdnConfig.templateHashCode = templateHashCode;
	}
	@Value("${cdn.appHashCode}")
	public  void setAppHashCode(String appHashCode) {
		CdnConfig.appHashCode = appHashCode;
	}
	@Value("${cdn.buildVersion}")
	public  void setBuildVersion(String buildVersion) {
	System.out.println("buildVersion"+ buildVersion);
		CdnConfig.buildVersion = buildVersion;
	}
	@Value("${cdn.b365ScaHashCode}")
	public void setB365ScaHashCode(String b365ScaHashCode) {
		CdnConfig.b365ScaHashCode = b365ScaHashCode;
	}
	@Value("${cdn.b365ScaBuildVersion}")
	public void setB365ScaBuildVersion(String b365ScaBuildVersion) {
		CdnConfig.b365ScaBuildVersion = b365ScaBuildVersion;
	}
	
	public static Map<String,String> populateCdnAttributes(){
		Map<String,String> map = new HashMap<>();
		map.put("cdnBaseURL",cdnBaseURL);
		map.put("libsHashCode",PSD2SecurityConstants.CDN_SHA_256+libsHashCode);
		map.put("templateHashCode",PSD2SecurityConstants.CDN_SHA_256+templateHashCode);
		map.put("appHashCode",PSD2SecurityConstants.CDN_SHA_256+appHashCode);
		map.put("buildVersion",buildVersion);
		map.put("b365ScaHashCode", PSD2SecurityConstants.CDN_SHA_256+b365ScaHashCode);
		map.put("b365ScaBuildVersion", b365ScaBuildVersion);
		return map;
	}
}