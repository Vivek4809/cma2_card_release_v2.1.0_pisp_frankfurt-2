package com.capgemini.psd2.consent.list.test.utilities;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.consent.list.utilities.ConsentListUtilities;
import com.capgemini.psd2.exceptions.PSD2Exception;

@RunWith(SpringJUnit4ClassRunner.class)
public class ConsentListUtilitiesTest {

	@Test(expected = PSD2Exception.class)
	public void testGetConsentListNullOrEmptyUserIdException() {
		String userId = "";
		ConsentListUtilities.validateInputs(userId, "test");
	}

	@Test(expected = PSD2Exception.class)
	public void testGetConsentListInvalidStatusException() {
		String userId = "boi123";
		String status = "abc";
		ConsentListUtilities.validateInputs(userId, status);
	}
}
