package com.capgemini.psd2.pisp.sca.consent.operations.routing;

import com.capgemini.psd2.pisp.sca.consent.operations.adapter.PispScaConsentOperationsAdapter;

public interface PispScaConsentOperationsAdapterFactory {
	
	public PispScaConsentOperationsAdapter getAdapterInstance(String coreSystemName);
}
