package com.capgemini.psd2.mock.foundationservice.account.transactions.service;

import com.capgemini.psd2.mock.foundationservice.account.transactions.raml.domain.TransactionList;

public interface AccountTransactionsService {

		public TransactionList retrieveMuleJsonTransactions(String accountclassifier, String accountId, String startDate, String endDate, String txnType, String pageNumber, String pageSize) throws Exception;
		public TransactionList reteriveCreditCardTransaction(String customerReference, String accountId, String startDate, String endDate,String txnType) throws Exception;
}
