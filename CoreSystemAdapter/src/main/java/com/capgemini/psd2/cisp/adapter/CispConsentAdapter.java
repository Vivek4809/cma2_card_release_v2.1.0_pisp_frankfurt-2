package com.capgemini.psd2.cisp.adapter;

import java.util.List;

import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;

public interface CispConsentAdapter {
	
	public void createConsent(CispConsent cispConsent);
	   
	public CispConsent retrieveConsentByFundsIntentIdAndStatus(String fundsIntentId,ConsentStatusEnum statusEnum);
	
	public CispConsent retrieveConsentByFundsIntentId(String fundsIntentId);
	
	public CispConsent retrieveConsent(String consentId);
	
	public void updateConsentStatus(String consentId,ConsentStatusEnum statusEnum);
	
	public void updateConsentStatusWithResponse(String consentId,ConsentStatusEnum statusEnum);
	
	public List<CispConsent> retrieveConsentByPsuIdAndConsentStatus(String psuId, ConsentStatusEnum statusEnum);

	List<CispConsent> retrieveConsentByPsuIdConsentStatusAndTenantId(String psuId, ConsentStatusEnum statusEnum,
			String tenantId);	
}
