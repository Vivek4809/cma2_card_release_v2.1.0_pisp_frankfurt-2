package com.capgemini.psd2.pisp.domain;

import java.util.Objects;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Specifies the Open Banking service request types. 
 */
public enum OBExternalPermissions2Code {
  
  CREATE("Create");

  private String value;

  OBExternalPermissions2Code(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static OBExternalPermissions2Code fromValue(String text) {
    for (OBExternalPermissions2Code b : OBExternalPermissions2Code.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

