package com.capgemini.psd2.pisp.stage.domain;

import java.math.BigDecimal;

import com.capgemini.psd2.pisp.domain.OBExchangeRateType2Code;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ExchangeRateDetails {
	
	@JsonProperty("UnitCurrency")
	  private String unitCurrency = null;

	  @JsonProperty("ExchangeRate")
	  private BigDecimal exchangeRate = null;

	  @JsonProperty("RateType")
	  private OBExchangeRateType2Code rateType = null;

	  @JsonProperty("ContractIdentification")
	  private String contractIdentification = null;

	  @JsonProperty("ExpirationDateTime")
	  private String expirationDateTime = null;

	public String getUnitCurrency() {
		return unitCurrency;
	}

	public void setUnitCurrency(String unitCurrency) {
		this.unitCurrency = unitCurrency;
	}

	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public OBExchangeRateType2Code getRateType() {
		return rateType;
	}

	public void setRateType(OBExchangeRateType2Code rateType) {
		this.rateType = rateType;
	}

	public String getContractIdentification() {
		return contractIdentification;
	}

	public void setContractIdentification(String contractIdentification) {
		this.contractIdentification = contractIdentification;
	}

	public String getExpirationDateTime() {
		return expirationDateTime;
	}

	public void setExpirationDateTime(String expirationDateTime) {
		this.expirationDateTime = expirationDateTime;
	}

}
