package com.capgemini.psd2.pisp.adapter;

import java.util.Map;

import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentInternationalSubmitPOST201Response;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public interface InternationalPaymentsAdapter {
	// Called from Domestic Payments API to process payment
	public PaymentInternationalSubmitPOST201Response processPayments(CustomIPaymentsPOSTRequest requestBody,
			Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap, Map<String, String> params);

	// Called from Domestic Payments API to retrieve payment
	public PaymentInternationalSubmitPOST201Response retrieveStagedPaymentsResponse(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params);



}
