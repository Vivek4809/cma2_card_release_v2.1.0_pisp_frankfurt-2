package com.capgemini.psd2.pisp.stage.domain;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class CustomDebtorDetails {
	@JsonProperty("SchemeName")
	private String schemeNameEnum = null;

	@JsonProperty("Identification")
	private String identification = null;

	@JsonProperty("Name")
	private String name = null;

	@JsonProperty("SecondaryIdentification")
	private String secondaryIdentification = null;

	public CustomDebtorDetails schemeNameEnum(String schemeNameEnum) {
		this.schemeNameEnum = schemeNameEnum;
		return this;
	}

	/**
	 * Name of the identification scheme, in a coded form as published in an
	 * external list.
	 * 
	 * @return schemeName
	 **/
	@ApiModelProperty(required = true, value = "Name of the identification scheme, in a coded form as published in an external list.")
	@NotNull

	public String getSchemeName() {
		return schemeNameEnum;
	}

	public void setSchemeName(String schemeName) {
		this.schemeNameEnum = schemeName;
	}

	public CustomDebtorDetails identification(String identification) {
		this.identification = identification;
		return this;
	}

	/**
	 * Identification assigned by an institution to identify an account. This
	 * identification is known by the account owner.
	 * 
	 * @return identification
	 **/
	@ApiModelProperty(required = true, value = "Identification assigned by an institution to identify an account. This identification is known by the account owner.")
	@NotNull

	@Size(min = 1, max = 256)
	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public CustomDebtorDetails name(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Name of the account, as assigned by the account servicing institution.
	 * Usage: The account name is the name or names of the account owner(s)
	 * represented at an account level. The account name is not the product name
	 * or the nickname of the account.
	 * 
	 * @return name
	 **/
	@ApiModelProperty(value = "Name of the account, as assigned by the account servicing institution. Usage: The account name is the name or names of the account owner(s) represented at an account level. The account name is not the product name or the nickname of the account.")

	@Size(min = 1, max = 70)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CustomDebtorDetails secondaryIdentification(String secondaryIdentification) {
		this.secondaryIdentification = secondaryIdentification;
		return this;
	}

	/**
	 * This is secondary identification of the account, as assigned by the
	 * account servicing institution. This can be used by building societies to
	 * additionally identify accounts with a roll number (in addition to a sort
	 * code and account number combination).
	 * 
	 * @return secondaryIdentification
	 **/
	@ApiModelProperty(value = "This is secondary identification of the account, as assigned by the account servicing institution.  This can be used by building societies to additionally identify accounts with a roll number (in addition to a sort code and account number combination).")

	@Size(min = 1, max = 34)
	public String getSecondaryIdentification() {
		return secondaryIdentification;
	}

	public void setSecondaryIdentification(String secondaryIdentification) {
		this.secondaryIdentification = secondaryIdentification;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		CustomDebtorDetails DebtorDetails = (CustomDebtorDetails) o;
		return Objects.equals(this.schemeNameEnum, DebtorDetails.schemeNameEnum)
				&& Objects.equals(this.identification, DebtorDetails.identification)
				&& Objects.equals(this.name, DebtorDetails.name)
				&& Objects.equals(this.secondaryIdentification, DebtorDetails.secondaryIdentification);
	}

	@Override
	public int hashCode() {
		return Objects.hash(schemeNameEnum, identification, name, secondaryIdentification);
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CustomDebtorDetails [schemeNameEnum=" + schemeNameEnum + ", identification=" + identification
				+ ", name=" + name + ", secondaryIdentification=" + secondaryIdentification + "]";
	}
}
