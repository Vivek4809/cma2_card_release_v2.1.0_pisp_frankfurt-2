package com.capgemini.psd2.pisp.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;

@Document(collection = "DSPaymentsFoundationResources")
public class CustomDSPaymentsPOSTResponse extends OBWriteDomesticScheduledResponse1 {
	private ProcessExecutionStatusEnum processExecutionStatus = null;

	public ProcessExecutionStatusEnum getProcessExecutionStatus() {
		return processExecutionStatus;
	}

	public void setProcessExecutionStatus(ProcessExecutionStatusEnum processExecutionStatus) {
		this.processExecutionStatus = processExecutionStatus;
	}

	@Override
	public String toString() {
		return "CustomDSPaymentsPOSTResponse [processExecutionStatus=" + processExecutionStatus + "]";
	}
}
