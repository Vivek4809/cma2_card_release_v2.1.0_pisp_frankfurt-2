package com.capgemini.psd2.pisp.domain;

public class PaymentSubmissionExecutionResponse {

	private String paymentSubmissionId;
	private String paymentSubmissionStatus;

	public String getPaymentSubmissionStatus() {
		return paymentSubmissionStatus;
	}

	public void setPaymentSubmissionStatus(String paymentSubmissionStatus) {
		this.paymentSubmissionStatus = paymentSubmissionStatus;
	}

	public String getPaymentSubmissionId() {
		return paymentSubmissionId;
	}

	public void setPaymentSubmissionId(String paymentSubmissionId) {
		this.paymentSubmissionId = paymentSubmissionId;
	}

}
