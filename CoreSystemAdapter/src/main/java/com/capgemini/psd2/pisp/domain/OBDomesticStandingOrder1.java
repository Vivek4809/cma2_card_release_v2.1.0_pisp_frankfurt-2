package com.capgemini.psd2.pisp.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Initiation payload is sent by the initiating party to the ASPSP. It is
 * used to request movement of funds from the debtor account to a creditor for a
 * domestic standing order.
 */
@ApiModel(description = "The Initiation payload is sent by the initiating party to the ASPSP. It is used to request movement of funds from the debtor account to a creditor for a domestic standing order.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-12-05T13:30:08.162+05:30")

public class OBDomesticStandingOrder1 {
	@JsonProperty("Frequency")
	private String frequency = null;

	@JsonProperty("Reference")
	private String reference = null;

	@JsonProperty("NumberOfPayments")
	private String numberOfPayments = null;

	@JsonProperty("FirstPaymentDateTime")
	private String firstPaymentDateTime = null;

	@JsonProperty("RecurringPaymentDateTime")
	private String recurringPaymentDateTime = null;

	@JsonProperty("FinalPaymentDateTime")
	private String finalPaymentDateTime = null;

	@JsonProperty("FirstPaymentAmount")
	private OBDomesticStandingOrder1FirstPaymentAmount firstPaymentAmount = null;

	@JsonProperty("RecurringPaymentAmount")
	private OBDomesticStandingOrder1RecurringPaymentAmount recurringPaymentAmount = null;

	@JsonProperty("FinalPaymentAmount")
	private OBDomesticStandingOrder1FinalPaymentAmount finalPaymentAmount = null;

	@JsonProperty("DebtorAccount")
	private OBCashAccountDebtor3 debtorAccount = null;

	@JsonProperty("CreditorAccount")
	private OBCashAccountCreditor2 creditorAccount = null;

	public OBDomesticStandingOrder1 frequency(String frequency) {
		this.frequency = frequency;
		return this;
	}

	/**
	 * Individual Definitions: EvryDay - Every day EvryWorkgDay - Every working
	 * day IntrvlWkDay - An interval specified in weeks (01 to 09), and the day
	 * within the week (01 to 07) WkInMnthDay - A monthly interval, specifying
	 * the week of the month (01 to 05) and day within the week (01 to 07)
	 * IntrvlMnthDay - An interval specified in months (between 01 to 06, 12,
	 * 24), specifying the day within the month (-5 to -1, 1 to 31) QtrDay -
	 * Quarterly (either ENGLISH, SCOTTISH, or RECEIVED). ENGLISH = Paid on the
	 * 25th March, 24th June, 29th September and 25th December. SCOTTISH = Paid
	 * on the 2nd February, 15th May, 1st August and 11th November. RECEIVED =
	 * Paid on the 20th March, 19th June, 24th September and 20th December.
	 * Individual Patterns: EvryDay (ScheduleCode) EvryWorkgDay (ScheduleCode)
	 * IntrvlWkDay:IntervalInWeeks:DayInWeek (ScheduleCode + IntervalInWeeks +
	 * DayInWeek) WkInMnthDay:WeekInMonth:DayInWeek (ScheduleCode + WeekInMonth
	 * + DayInWeek) IntrvlMnthDay:IntervalInMonths:DayInMonth (ScheduleCode +
	 * IntervalInMonths + DayInMonth) QtrDay: + either (ENGLISH, SCOTTISH or
	 * RECEIVED) ScheduleCode + QuarterDay The regular expression for this
	 * element combines five smaller versions for each permitted pattern. To aid
	 * legibility - the components are presented individually here: EvryDay
	 * EvryWorkgDay IntrvlWkDay:0[1-9]:0[1-7] WkInMnthDay:0[1-5]:0[1-7]
	 * IntrvlMnthDay:(0[1-6]|12|24):(-0[1-5]|0[1-9]|[12][0-9]|3[01])
	 * QtrDay:(ENGLISH|SCOTTISH|RECEIVED) Full Regular Expression:
	 * ^(EvryDay)$|^(EvryWorkgDay)$|^(IntrvlWkDay:0[1-9]:0[1-7])$|^(WkInMnthDay:0[1-5]:0[1-7])$|^(IntrvlMnthDay:(0[1-6]|12|24):(-0[1-5]|0[1-9]|[12][0-9]|3[01]))$|^(QtrDay:(ENGLISH|SCOTTISH|RECEIVED))$
	 * 
	 * @return frequency
	 **/
	@ApiModelProperty(required = true, value = "Individual Definitions: EvryDay - Every day EvryWorkgDay - Every working day IntrvlWkDay - An interval specified in weeks (01 to 09), and the day within the week (01 to 07) WkInMnthDay - A monthly interval, specifying the week of the month (01 to 05) and day within the week (01 to 07) IntrvlMnthDay - An interval specified in months (between 01 to 06, 12, 24), specifying the day within the month (-5 to -1, 1 to 31) QtrDay - Quarterly (either ENGLISH, SCOTTISH, or RECEIVED).  ENGLISH = Paid on the 25th March, 24th June, 29th September and 25th December.  SCOTTISH = Paid on the 2nd February, 15th May, 1st August and 11th November. RECEIVED = Paid on the 20th March, 19th June, 24th September and 20th December.  Individual Patterns: EvryDay (ScheduleCode) EvryWorkgDay (ScheduleCode) IntrvlWkDay:IntervalInWeeks:DayInWeek (ScheduleCode + IntervalInWeeks + DayInWeek) WkInMnthDay:WeekInMonth:DayInWeek (ScheduleCode + WeekInMonth + DayInWeek) IntrvlMnthDay:IntervalInMonths:DayInMonth (ScheduleCode + IntervalInMonths + DayInMonth) QtrDay: + either (ENGLISH, SCOTTISH or RECEIVED) ScheduleCode + QuarterDay The regular expression for this element combines five smaller versions for each permitted pattern. To aid legibility - the components are presented individually here: EvryDay EvryWorkgDay IntrvlWkDay:0[1-9]:0[1-7] WkInMnthDay:0[1-5]:0[1-7] IntrvlMnthDay:(0[1-6]|12|24):(-0[1-5]|0[1-9]|[12][0-9]|3[01]) QtrDay:(ENGLISH|SCOTTISH|RECEIVED) Full Regular Expression: ^(EvryDay)$|^(EvryWorkgDay)$|^(IntrvlWkDay:0[1-9]:0[1-7])$|^(WkInMnthDay:0[1-5]:0[1-7])$|^(IntrvlMnthDay:(0[1-6]|12|24):(-0[1-5]|0[1-9]|[12][0-9]|3[01]))$|^(QtrDay:(ENGLISH|SCOTTISH|RECEIVED))$")
	@NotNull

	@Pattern(regexp = "^(EvryDay)$|^(EvryWorkgDay)$|^(IntrvlWkDay:0[1-9]:0[1-7])$|^(WkInMnthDay:0[1-5]:0[1-7])$|^(IntrvlMnthDay:(0[1-6]|12|24):(-0[1-5]|0[1-9]|[12][0-9]|3[01]))$|^(QtrDay:(ENGLISH|SCOTTISH|RECEIVED))$")
	@Size(min = 1, max = 35)
	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public OBDomesticStandingOrder1 reference(String reference) {
		this.reference = reference;
		return this;
	}

	/**
	 * Unique reference, as assigned by the creditor, to unambiguously refer to
	 * the payment transaction. Usage: If available, the initiating party should
	 * provide this reference in the structured remittance information, to
	 * enable reconciliation by the creditor upon receipt of the amount of
	 * money. If the business context requires the use of a creditor reference
	 * or a payment remit identification, and only one identifier can be passed
	 * through the end-to-end chain, the creditor's reference or payment
	 * remittance identification should be quoted in the end-to-end transaction
	 * identification.
	 * 
	 * @return reference
	 **/
	@ApiModelProperty(value = "Unique reference, as assigned by the creditor, to unambiguously refer to the payment transaction. Usage: If available, the initiating party should provide this reference in the structured remittance information, to enable reconciliation by the creditor upon receipt of the amount of money. If the business context requires the use of a creditor reference or a payment remit identification, and only one identifier can be passed through the end-to-end chain, the creditor's reference or payment remittance identification should be quoted in the end-to-end transaction identification.")

	@Size(min = 1, max = 35)
	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public OBDomesticStandingOrder1 numberOfPayments(String numberOfPayments) {
		this.numberOfPayments = numberOfPayments;
		return this;
	}

	/**
	 * Number of the payments that will be made in completing this frequency
	 * sequence including any executed since the sequence start date.
	 * 
	 * @return numberOfPayments
	 **/
	@ApiModelProperty(value = "Number of the payments that will be made in completing this frequency sequence including any executed since the sequence start date. ")

	@Size(min = 1, max = 35)
	public String getNumberOfPayments() {
		return numberOfPayments;
	}

	public void setNumberOfPayments(String numberOfPayments) {
		this.numberOfPayments = numberOfPayments;
	}

	public OBDomesticStandingOrder1 firstPaymentAmount(OBDomesticStandingOrder1FirstPaymentAmount firstPaymentAmount) {
		this.firstPaymentAmount = firstPaymentAmount;
		return this;
	}

	/**
	 * Get firstPaymentAmount
	 * 
	 * @return firstPaymentAmount
	 **/
	@ApiModelProperty(required = true, value = "")
	@NotNull

	@Valid

	public OBDomesticStandingOrder1FirstPaymentAmount getFirstPaymentAmount() {
		return firstPaymentAmount;
	}

	public void setFirstPaymentAmount(OBDomesticStandingOrder1FirstPaymentAmount firstPaymentAmount) {
		this.firstPaymentAmount = firstPaymentAmount;
	}

	public OBDomesticStandingOrder1 recurringPaymentAmount(
			OBDomesticStandingOrder1RecurringPaymentAmount recurringPaymentAmount) {
		this.recurringPaymentAmount = recurringPaymentAmount;
		return this;
	}

	/**
	 * Get recurringPaymentAmount
	 * 
	 * @return recurringPaymentAmount
	 **/
	@ApiModelProperty(value = "")

	@Valid

	public OBDomesticStandingOrder1RecurringPaymentAmount getRecurringPaymentAmount() {
		return recurringPaymentAmount;
	}

	public void setRecurringPaymentAmount(OBDomesticStandingOrder1RecurringPaymentAmount recurringPaymentAmount) {
		this.recurringPaymentAmount = recurringPaymentAmount;
	}

	public OBDomesticStandingOrder1 finalPaymentAmount(OBDomesticStandingOrder1FinalPaymentAmount finalPaymentAmount) {
		this.finalPaymentAmount = finalPaymentAmount;
		return this;
	}

	/**
	 * Get finalPaymentAmount
	 * 
	 * @return finalPaymentAmount
	 **/
	@ApiModelProperty(value = "")

	@Valid

	public OBDomesticStandingOrder1FinalPaymentAmount getFinalPaymentAmount() {
		return finalPaymentAmount;
	}

	public void setFinalPaymentAmount(OBDomesticStandingOrder1FinalPaymentAmount finalPaymentAmount) {
		this.finalPaymentAmount = finalPaymentAmount;
	}

	public OBDomesticStandingOrder1 debtorAccount(OBCashAccountDebtor3 debtorAccount) {
		this.debtorAccount = debtorAccount;
		return this;
	}

	/**
	 * Get debtorAccount
	 * 
	 * @return debtorAccount
	 **/
	@ApiModelProperty(value = "")

	@Valid

	public OBCashAccountDebtor3 getDebtorAccount() {
		return debtorAccount;
	}

	public void setDebtorAccount(OBCashAccountDebtor3 debtorAccount) {
		this.debtorAccount = debtorAccount;
	}

	public OBDomesticStandingOrder1 creditorAccount(OBCashAccountCreditor2 creditorAccount) {
		this.creditorAccount = creditorAccount;
		return this;
	}

	/**
	 * Get creditorAccount
	 * 
	 * @return creditorAccount
	 **/
	@ApiModelProperty(required = true, value = "")
	@NotNull

	@Valid

	public OBCashAccountCreditor2 getCreditorAccount() {
		return creditorAccount;
	}

	public void setCreditorAccount(OBCashAccountCreditor2 creditorAccount) {
		this.creditorAccount = creditorAccount;
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

	@NotNull
	public String getFirstPaymentDateTime() {
		return this.firstPaymentDateTime;
	}

	public void setFirstPaymentDateTime(String firstPaymentDateTime) {
		this.firstPaymentDateTime = firstPaymentDateTime;
	}

	public String getRecurringPaymentDateTime() {
		return recurringPaymentDateTime;
	}

	public void setRecurringPaymentDateTime(String recurringPaymentDateTime) {
		this.recurringPaymentDateTime = recurringPaymentDateTime;
	}

	public String getFinalPaymentDateTime() {
		return finalPaymentDateTime;
	}

	public void setFinalPaymentDateTime(String finalPaymentDateTime) {
		this.finalPaymentDateTime = finalPaymentDateTime;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		OBDomesticStandingOrder1 obDomesticStandingOrder1 = (OBDomesticStandingOrder1) o;
		return Objects.equals(this.frequency, obDomesticStandingOrder1.frequency)
				&& Objects.equals(this.reference, obDomesticStandingOrder1.reference)
				&& Objects.equals(this.numberOfPayments, obDomesticStandingOrder1.numberOfPayments)
				&& Objects.equals(this.firstPaymentDateTime, obDomesticStandingOrder1.firstPaymentDateTime)
				&& Objects.equals(this.recurringPaymentDateTime, obDomesticStandingOrder1.recurringPaymentDateTime)
				&& Objects.equals(this.finalPaymentDateTime, obDomesticStandingOrder1.finalPaymentDateTime)
				&& Objects.equals(this.firstPaymentAmount, obDomesticStandingOrder1.firstPaymentAmount)
				&& Objects.equals(this.recurringPaymentAmount, obDomesticStandingOrder1.recurringPaymentAmount)
				&& Objects.equals(this.finalPaymentAmount, obDomesticStandingOrder1.finalPaymentAmount)
				&& Objects.equals(this.debtorAccount, obDomesticStandingOrder1.debtorAccount)
				&& Objects.equals(this.creditorAccount, obDomesticStandingOrder1.creditorAccount);
	}

	@Override
	public int hashCode() {
		return Objects.hash(frequency, reference, numberOfPayments, firstPaymentDateTime, recurringPaymentDateTime,
				finalPaymentDateTime, firstPaymentAmount, recurringPaymentAmount, finalPaymentAmount, debtorAccount,
				creditorAccount);
	}

	@Override
	public String toString() {
		return "OBDomesticStandingOrder1 [frequency=" + frequency + ", reference=" + reference + ", numberOfPayments="
				+ numberOfPayments + ", firstPaymentDateTime=" + firstPaymentDateTime + ", recurringPaymentDateTime="
				+ recurringPaymentDateTime + ", finalPaymentDateTime=" + finalPaymentDateTime + ", firstPaymentAmount="
				+ firstPaymentAmount + ", recurringPaymentAmount=" + recurringPaymentAmount + ", finalPaymentAmount="
				+ finalPaymentAmount + ", debtorAccount=" + debtorAccount + ", creditorAccount=" + creditorAccount
				+ "]";
	}
}
