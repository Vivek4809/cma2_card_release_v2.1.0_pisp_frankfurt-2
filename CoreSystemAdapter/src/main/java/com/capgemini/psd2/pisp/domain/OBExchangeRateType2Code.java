package com.capgemini.psd2.pisp.domain;

import java.util.Objects;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Specifies the type used to complete the currency exchange.
 */
public enum OBExchangeRateType2Code {
  
  ACTUAL("Actual"),
  
  AGREED("Agreed"),
  
  INDICATIVE("Indicative");

  private String value;

  OBExchangeRateType2Code(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static OBExchangeRateType2Code fromValue(String text) {
    for (OBExchangeRateType2Code b : OBExchangeRateType2Code.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

