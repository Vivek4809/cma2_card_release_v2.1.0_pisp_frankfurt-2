package com.capgemini.psd2.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Iterator;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { LengthValidator.ListValidator.class })
@Documented
public @interface LengthValidator {
	
	 String message() default "String length should be between 1 and 2000";
	    Class<?>[] groups() default {};
	    Class<? extends Payload>[] payload() default {};
	    
	    public class ListValidator implements
	    ConstraintValidator<LengthValidator, Iterable<String>> {

			@Override
			public void initialize(LengthValidator arg0) {
				
			}

			@Override
			public boolean isValid(Iterable<String> items, ConstraintValidatorContext arg1) {
				if(items==null)
					return true;
				else {
					Iterator<String> itr=items.iterator();
					while (itr.hasNext()) {
						String str=itr.next();
						if(str==null || !(str.length()>=1 && str.length()<=2000))
							return false;
					}
				}
				return true;
			}

			
	    }

}
