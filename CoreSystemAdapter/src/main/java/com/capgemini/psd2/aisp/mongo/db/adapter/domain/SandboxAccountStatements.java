package com.capgemini.psd2.aisp.mongo.db.adapter.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="MockSandboxAccountStatements")
public class SandboxAccountStatements {

	@Id
	private String id;
	
	private String accountNumber;
	
	private String accountNSC;
	
	private String statementId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountNSC() {
		return accountNSC;
	}

	public void setAccountNSC(String accountNSC) {
		this.accountNSC = accountNSC;
	}

	public String getStatementId() {
		return statementId;
	}

	public void setStatementId(String statementId) {
		this.statementId = statementId;
	}
	
	
}