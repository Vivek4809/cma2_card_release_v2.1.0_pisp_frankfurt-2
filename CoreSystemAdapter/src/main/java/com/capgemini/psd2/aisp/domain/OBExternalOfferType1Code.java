package com.capgemini.psd2.aisp.domain;

import java.util.Objects;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Offer type, in a coded form.
 */
public enum OBExternalOfferType1Code {
  
  BALANCETRANSFER("BalanceTransfer"),
  
  LIMITINCREASE("LimitIncrease"),
  
  MONEYTRANSFER("MoneyTransfer"),
  
  OTHER("Other"),
  
  PROMOTIONALRATE("PromotionalRate");

  private String value;

  OBExternalOfferType1Code(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static OBExternalOfferType1Code fromValue(String text) {
    for (OBExternalOfferType1Code b : OBExternalOfferType1Code.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

