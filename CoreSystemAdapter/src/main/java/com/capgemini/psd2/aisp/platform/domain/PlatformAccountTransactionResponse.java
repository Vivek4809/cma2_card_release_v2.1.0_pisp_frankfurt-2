package com.capgemini.psd2.aisp.platform.domain;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadTransaction3;

public class PlatformAccountTransactionResponse {

	// CMA2 response
	private OBReadTransaction3 oBReadTransaction3;

	// Additional Information
	private Map<String, String> additionalInformation;

	public OBReadTransaction3 getoBReadTransaction3() {
		return oBReadTransaction3;
	}

	public void setoBReadTransaction3(OBReadTransaction3 oBReadTransaction3) {
		this.oBReadTransaction3 = oBReadTransaction3;
	}

	public Map<String, String> getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(Map<String, String> additionalInformation) {
		this.additionalInformation = additionalInformation;
	}
}
