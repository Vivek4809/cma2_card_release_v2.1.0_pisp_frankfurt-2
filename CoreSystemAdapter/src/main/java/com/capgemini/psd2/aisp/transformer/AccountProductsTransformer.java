package com.capgemini.psd2.aisp.transformer;

import java.util.Map;

import com.capgemini.psd2.aisp.platform.domain.PlatformAccountProductsResponse;

public interface AccountProductsTransformer {
	public <T> PlatformAccountProductsResponse transformAccountProducts(T source, Map<String, String> params);
}
