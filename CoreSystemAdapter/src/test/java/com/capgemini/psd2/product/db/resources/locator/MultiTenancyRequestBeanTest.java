package com.capgemini.psd2.product.db.resources.locator;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

public class MultiTenancyRequestBeanTest {

	@InjectMocks
	private MultiTenancyRequestBean multiTenancyRequestBean;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getTenantIdTest(){
		multiTenancyRequestBean.getTenantId();
	}

	@Test
	public void setTenantIdTest(){
		multiTenancyRequestBean.setTenantId("BOIUK");
	}

	@Test
	public void toStringTest(){
		ReflectionTestUtils.setField(multiTenancyRequestBean, "tenantId", "BOIUK");
		multiTenancyRequestBean.toString();
	}	




}
