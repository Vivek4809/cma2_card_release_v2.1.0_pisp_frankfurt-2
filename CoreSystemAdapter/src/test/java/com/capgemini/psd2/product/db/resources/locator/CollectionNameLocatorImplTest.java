package com.capgemini.psd2.product.db.resources.locator;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class CollectionNameLocatorImplTest {
	
	@InjectMocks
	private CollectionNameLocatorImpl collectionNameLocatorImpl;
	
	@Mock
	private MultiTenancyRequestBean requestBean;
	
	@Before
	public void before() {
		
		MockitoAnnotations.initMocks(this);
		
	}
	
	@Test
	public void getAccountAccessSetupCollectionNameTest(){
	Mockito.when(requestBean.getTenantId()).thenReturn("BOIUK");
		collectionNameLocatorImpl.getAccountAccessSetupCollectionName();
	}
	
	@Test
	public void getAccountAccessSetupCollectionName1Test(){
	Mockito.when(requestBean.getTenantId()).thenReturn("BOIROI");
		collectionNameLocatorImpl.getAccountAccessSetupCollectionName();
	}
	
	@Test
	public void getAispConsentCollectionNameTest(){
	Mockito.when(requestBean.getTenantId()).thenReturn("BOIUK");
		collectionNameLocatorImpl.getAispConsentCollectionName();
	}
	
	@Test
	public void getAispConsentCollectionName1Test(){
	Mockito.when(requestBean.getTenantId()).thenReturn("BOIROI");
		collectionNameLocatorImpl.getAispConsentCollectionName();
	}
	
	@Test
	public void getPaymentSetupCollectionNameTest(){
	Mockito.when(requestBean.getTenantId()).thenReturn("BOIUK");
		collectionNameLocatorImpl.getPaymentSetupCollectionName();
	}
	
	@Test
	public void getPaymentSetupCollectionName1Test(){
	Mockito.when(requestBean.getTenantId()).thenReturn("BOIROI");
		collectionNameLocatorImpl.getPaymentSetupCollectionName();
	}

	@Test
	public void getPaymentSubmissionCollectionNameTest(){
	Mockito.when(requestBean.getTenantId()).thenReturn("BOIUK");
		collectionNameLocatorImpl.getPaymentSubmissionCollectionName();
	}
	
	@Test
	public void getPaymentSubmissionCollectionName1Test(){
	Mockito.when(requestBean.getTenantId()).thenReturn("BOIROI");
		collectionNameLocatorImpl.getPaymentSubmissionCollectionName();
	}
	
	@Test
	public void getFundsSetupCollectionName(){
	Mockito.when(requestBean.getTenantId()).thenReturn("BOIUK");
		collectionNameLocatorImpl.getFundsSetupCollectionName();
	}
	
	@Test
	public void getFundsSetupCollectionName1Test(){
	Mockito.when(requestBean.getTenantId()).thenReturn("BOIROI");
		collectionNameLocatorImpl.getFundsSetupCollectionName();
	}
	
	@Test
	public void getCPNPCollectionName(){
	Mockito.when(requestBean.getTenantId()).thenReturn("BOIUK");
		collectionNameLocatorImpl.getCPNPCollectionName();
	}
	
	@Test
	public void getCPNPCollection1Name(){
	Mockito.when(requestBean.getTenantId()).thenReturn("BOIROI");
		collectionNameLocatorImpl.getCPNPCollectionName();
	}
	
	@Test
	public void getPispConsentCollectionName(){
	Mockito.when(requestBean.getTenantId()).thenReturn("BOIUK");
		collectionNameLocatorImpl.getPispConsentCollectionName();
	}
	
	@Test
	public void getPispConsentCollectionName1(){
	Mockito.when(requestBean.getTenantId()).thenReturn("BOIROI");
		collectionNameLocatorImpl.getPispConsentCollectionName();
	}
	
	@Test
	public void getCispConsentCollectionName(){
	Mockito.when(requestBean.getTenantId()).thenReturn("BOIUK");
		collectionNameLocatorImpl.getCispConsentCollectionName();
	}
	
	@Test
	public void getCispConsentCollection1Name(){
	Mockito.when(requestBean.getTenantId()).thenReturn("BOIROI");
		collectionNameLocatorImpl.getCispConsentCollectionName();
	}
}
