package com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.InternationalPaymentConsentsFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.client.InternationalPaymentConsentsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.delegate.InternationalPaymentConsentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.transformer.InternationalPaymentConsentsFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentConsentsFoundationServiceAdapterTest {
	
	@InjectMocks
	InternationalPaymentConsentsFoundationServiceAdapter adapter;
	@Mock
	InternationalPaymentConsentsFoundationServiceDelegate delegate;
	@Mock
	InternationalPaymentConsentsFoundationServiceClient client;
	@Mock
	InternationalPaymentConsentsFoundationServiceTransformer transformer;
	
	CustomIPaymentConsentsPOSTRequest paymentConsentsRequest= new CustomIPaymentConsentsPOSTRequest();
	OBExternalConsentStatus1Code successStatus=OBExternalConsentStatus1Code.AUTHORISED;
	OBExternalConsentStatus1Code failureStatus=OBExternalConsentStatus1Code.REJECTED;
	CustomPaymentStageIdentifiers customStageIdentifiers= new CustomPaymentStageIdentifiers();
	Map<String, String> params= new HashMap<>();
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	
	@Test
	public void testcreateStagingInternationalPaymentConsents(){
		adapter.createStagingInternationalPaymentConsents(paymentConsentsRequest, customStageIdentifiers, params);
	}

	@Test
	public void testretrieveStagedInternationalPaymentConsents(){
		adapter.retrieveStagedInternationalPaymentConsents(customStageIdentifiers, params);
	}
	
	@Test
	public void testprocessInternationalPaymentConsents(){
		
		adapter.processInternationalPaymentConsents(paymentConsentsRequest, customStageIdentifiers, params, successStatus, failureStatus);
	}
}
