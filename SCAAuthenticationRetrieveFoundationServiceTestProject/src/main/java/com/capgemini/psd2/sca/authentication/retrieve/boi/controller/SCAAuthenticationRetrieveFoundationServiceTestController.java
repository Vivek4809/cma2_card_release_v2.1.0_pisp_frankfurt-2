package com.capgemini.psd2.sca.authentication.retrieve.boi.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServiceGetResponse;
import com.capgemini.psd2.foundationservice.sca.authentication.retrieve.boi.adapter.SCAAuthenticationRetrieveFoundationServiceAdapter;

@RestController
public class SCAAuthenticationRetrieveFoundationServiceTestController {
	@Autowired
	SCAAuthenticationRetrieveFoundationServiceAdapter adapter;

	@RequestMapping(value = "/testSCAAuthenticationRetrieveAdapter/channels/{channel-code}/digital-users/{digital-user-id}", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public CustomAuthenticationServiceGetResponse getResponse(@PathVariable("channel-code") String channelCode,
			@PathVariable("digital-user-id") String digitalUserId) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("x-user-id", digitalUserId);
		params.put("eventType", "International Payment - Trusted Payee");
		params.put("x-channel-id", "abcd");
		params.put("channel-code", channelCode);
		params.put("X-BOI-USER", "abcd");
		params.put("x-fapi-interaction-id", "abcd");
		return adapter.getAuthenticate(params);

	}
}
