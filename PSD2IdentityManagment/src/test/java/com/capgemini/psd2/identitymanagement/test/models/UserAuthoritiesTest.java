package com.capgemini.psd2.identitymanagement.test.models;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.psd2.identitymanagement.models.UserAuthorities;

public class UserAuthoritiesTest {
	private UserAuthorities userAuthorities;
	@Before
	public void setUp() throws Exception {
		userAuthorities = new UserAuthorities("UserAuthorities");
	}
	
	@Test
	public void test(){
		assertEquals("UserAuthorities",userAuthorities.getAuthority());
	}
	
	@Test
	public void testToString(){
		assertEquals("UserAuthorities",userAuthorities.toString());
	}
}
