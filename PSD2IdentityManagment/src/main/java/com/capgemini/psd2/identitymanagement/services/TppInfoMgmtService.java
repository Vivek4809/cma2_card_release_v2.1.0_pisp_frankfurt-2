package com.capgemini.psd2.identitymanagement.services;

import com.capgemini.psd2.identitymanagement.models.CustomClientDetails;

public interface TppInfoMgmtService {
	public CustomClientDetails findTppInfo(String clientId);
	
}
