package com.capgemini.psd2.aisp.validation.test.adapter.impl;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.OBReadConsent1;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountRequestsValidatorImpl;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.aisp.validation.test.mock.data.AccountRequestMockData;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountRequestValidatorImplTest {

	@InjectMocks
	private AccountRequestsValidatorImpl accountRequestsValidatorImpl;
	
	@Mock
	private CommonAccountValidations commonAccountValidations;

	@Mock
	private PSD2Validator psd2Validator;
	
	@Mock
	private OBReadConsent1 obReadConsent1;
	
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Before
	public void setUp() {
		Map<String,String> genericErrorMessages=new HashMap<>();
		Map<String,String> specificErrorMessages=new HashMap<>();
		genericErrorMessages.put("INTERNAL", "INTERNAL Error");
		specificErrorMessages.put("Idempotency", "Idempotency duration is not configured in correct format.");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessages);
	}
	
	@Test
	public void validateRequestParamsTestTrue(){
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountRequestsValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountRequestsValidatorImpl, true);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		accountRequestsValidatorImpl.validateRequestParams(AccountRequestMockData.getAccountRequestsParamatersMockData());
	}
	
	@Test
	public void validateRequestParamsTestFalse(){
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountRequestsValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountRequestsValidatorImpl, false);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		accountRequestsValidatorImpl.validateRequestParams(AccountRequestMockData.getAccountRequestsParamatersMockData());
	}
	
	@Test
	public void validateRequestParamsTestNull(){
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountRequestsValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountRequestsValidatorImpl, true);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		accountRequestsValidatorImpl.validateRequestParams(AccountRequestMockData.getAccountRequestsParamatersMockData_NullDates());
	}
	
	@Test
	public void validateRequestParamsTestBlank(){
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountRequestsValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountRequestsValidatorImpl, true);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		accountRequestsValidatorImpl.validateRequestParams(AccountRequestMockData.getAccountRequestsParamatersMockData_BlankDates());
	}
	
	
	@Test
	public void validateResponseParamsTestTrue() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountRequestsValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountRequestsValidatorImpl, true);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		assertTrue(accountRequestsValidatorImpl.validateResponseParams(AccountRequestMockData.getAccountRequestsMockResponseData()));
	}
	
	@Test
	public void validateResponseParamsTestFalse() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountRequestsValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountRequestsValidatorImpl, false);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		assertTrue(accountRequestsValidatorImpl.validateResponseParams(AccountRequestMockData.getAccountRequestsMockResponseData()));
	}
	
	@Test
	public void validateResponseParamsTestNull() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountRequestsValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountRequestsValidatorImpl, true);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		assertTrue(accountRequestsValidatorImpl.validateResponseParams(AccountRequestMockData.getAccountRequestsMockResponseDataNullDates()));
	}
	
	@Test
	public void validateResponseParamsTestBlank() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountRequestsValidatorImpl.class.getDeclaredField("reqValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountRequestsValidatorImpl, true);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		assertTrue(accountRequestsValidatorImpl.validateResponseParams(AccountRequestMockData.getAccountRequestsMockResponseDataBlankDates()));
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateUniqueIdNull(){
		accountRequestsValidatorImpl.validateUniqueId(null);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateUniqueIdEmpty(){
		accountRequestsValidatorImpl.validateUniqueId("");
	}

	@Test
	public void testValidateUniqueIdNotNull(){
		accountRequestsValidatorImpl.validateUniqueId("c154ad51-4cd7-400f-9d84-532347f87360");
	}

	

}
