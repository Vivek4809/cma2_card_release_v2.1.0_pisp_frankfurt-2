/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.balance.mock.foundationservice.domain.Balanceresponse;
import com.capgemini.psd2.account.balance.mock.foundationservice.domain.CreditcardsBalanceresponse;
import com.capgemini.psd2.account.balance.mock.foundationservice.service.AccountBalanceService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;


/**
 * The Class AccountBalanceController.
 */
@RestController
@RequestMapping("/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.21/core-banking/p/abt")
public class AccountBalanceController {

	/** The account balance service. */
	@Autowired
	private AccountBalanceService accountBalanceService;
	
	/** The adapterUtility. */
	@Autowired
	private ValidationUtility validatorUtility;


	/**
	 * Reterive account balance.
	 *
	 * @param nsc the nsc
	 * @param accountNumber the account number
	 * @param boiUser the boi user
	 * @param boiChannel the boi channel
	 * @param boiPlatform the boi platform
	 * @param correlationID the correlation ID
	 * @return the accounts
	 * @throws Exception the exception
	 */
	@RequestMapping(value = "/{version}/accounts/{parent-national-sort-code-nsc-number}/{account-number}/balance", method = RequestMethod.GET, produces = "application/json")
	public Balanceresponse reteriveAccountsBalance(
			@PathVariable("version") String version,
			@PathVariable("parent-national-sort-code-nsc-number") String accountclassifier,
			@PathVariable("account-number") String accountId,
			@RequestHeader(required = false, value = "x-api-channel-Code") String boiChannel,
			@RequestHeader(required = false, value = "x-api-source-user") String boiUser,
			@RequestHeader(required = false, value = "x-api-transaction-id") String boiTransaction,
			@RequestHeader(required = false, value = "x-api-correlation-id") String correlationID,
			@RequestHeader(required = false, value = "x-api-source-system") String sourceSystem) throws Exception {
		
		validatorUtility.validateErrorCode(boiTransaction);
		
		if (boiUser == null || boiUser.isEmpty() || boiChannel == null || boiChannel.isEmpty() || boiTransaction == null
				|| boiTransaction.isEmpty() || sourceSystem == null || sourceSystem.isEmpty()) {
			throw MockFoundationServiceException
			.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PAC_BAL);
		}

		if (accountclassifier == null || accountclassifier.isEmpty() || accountId == null || accountId.isEmpty()) {
			throw MockFoundationServiceException
			.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PAC_BAL);
		}
		return accountBalanceService.reteriveAccountsBalance(accountId);
	}
	
	@RequestMapping(value = "/{version}/accounts/creditcards/{customer-reference}/{account-number}/balance", method = RequestMethod.GET, produces = "application/json")
	public CreditcardsBalanceresponse reteriveCreditCardBalance(
			@PathVariable("version") String version,
			@PathVariable("customer-reference") String customerReference,
			@PathVariable("account-number") String accountId,
			@RequestHeader(required = false, value = "x-masked-pan") String maskedPan,
			@RequestHeader(required = false, value = "x-api-channel-Code") String boiChannel,
			@RequestHeader(required = false, value = "x-api-source-user") String boiUser,
			@RequestHeader(required = false, value = "x-api-transaction-id") String boiTransaction,
			@RequestHeader(required = false, value = "x-api-correlation-id") String correlationID,
			@RequestHeader(required = false, value = "x-api-source-system") String sourceSystem) throws Exception {
		
		validatorUtility.validateErrorCode(boiTransaction);
		
		if (maskedPan == null || maskedPan.isEmpty() || boiUser == null || boiUser.isEmpty() || boiChannel == null || boiChannel.isEmpty() || boiTransaction == null
				|| boiTransaction.isEmpty() || sourceSystem == null || sourceSystem.isEmpty() ) {
			throw MockFoundationServiceException
			.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PAC_BAL);
		}

		if (customerReference == null || customerReference.isEmpty() || accountId == null || accountId.isEmpty()) {
			throw MockFoundationServiceException
			.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PAC_BAL);
		}

		return accountBalanceService.reteriveCreditCardBalance(accountId);	
	}
}
