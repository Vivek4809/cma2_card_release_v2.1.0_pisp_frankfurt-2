server:
  port: 8798
spring:
  application:
    name: adapter
foundationService:
  platform: PSD2API
  timeZone: Europe/Dublin
  standingOrdersBaseURL : http://localhost:9086/fs-abt-service/services/account
  foundationCustProfileUrl:
      BOL: http://localhost:9096/fs-customeraccountprofile-service/services/customerAccountProfile/channel/business/profile
      B365: http://localhost:9096/fs-customeraccountprofile-service/services/customerAccountProfile/channel/personal/profile
      PO: http://localhost:9096/fs-customeraccountprofile-service/services/customerAccountProfile/channel/personal/profile
      AA: http://localhost:9096/fs-customeraccountprofile-service/services/customerAccountProfile/channel/personal/profile
  
  standingOrdersEndURL : standingorders
  maskStandingOrdersResponse : true
  userInReqHeader: X-BOI-USER
  channelInReqHeader: X-BOI-CHANNEL
  platformInReqHeader: X-BOI-PLATFORM
  correlationReqHeader: X-CORRELATION-ID
  consentFlowType : AISP
  apiId: AccountStandingOrderFoundationServiceAdapter
  errormap:
  	 FS_ESSO_001: BAD_REQUEST
     FS_ESSO_002: AUTHENTICATION_FAILURE_ERROR
     FS_ESSO_003: NO_STANDINGORDER_FOUND
     FS_ESSO_004: TECHNICAL_ERROR
     FS_ESSO_005: TECHNICAL_ERROR
     FS_CAP_001:  BAD_REQUEST
     FS_CAP_002:  AUTHENTICATION_FAILURE_ERROR
     FS_CAP_003:  ACCOUNT_DETAILS_NOT_FOUND
     FS_CAP_004:  TECHNICAL_ERROR
     FS_CAP_005:  ACCOUNT_DETAILS_NOT_FOUND
     FS_CAP_006:  USER_IS_BLOCKED
  accountFiltering: 
    permission: 
      AISP:
       - V
       - A
      CISP: 
       - V
       - A
      PISP:
       - X
       - A
    accountType:
      AISP:
       - Current Account
      CISP: 
       - Current Account
      PISP:
       - Current Account
    jurisdiction:
      AISP:
        - NORTHERN_IRELAND.SchemeName=SORTCODEACCOUNTNUMBER
        - NORTHERN_IRELAND.Identification=SORTCODEACCOUNTNUMBER
        - NORTHERN_IRELAND.ServicerSchemeName=
        - NORTHERN_IRELAND.ServicerIdentification=
        - GREAT_BRITAIN.SchemeName=SORTCODEACCOUNTNUMBER
        - GREAT_BRITAIN.Identification=SORTCODEACCOUNTNUMBER
        - GREAT_BRITAIN.ServicerSchemeName=
        - GREAT_BRITAIN.ServicerIdentification=
      CISP: 
        - NORTHERN_IRELAND.SchemeName=SORTCODEACCOUNTNUMBER
        - NORTHERN_IRELAND.Identification=SORTCODEACCOUNTNUMBER
        - NORTHERN_IRELAND.ServicerSchemeName=
        - NORTHERN_IRELAND.ServicerIdentification=
        - GREAT_BRITAIN.SchemeName=SORTCODEACCOUNTNUMBER
        - GREAT_BRITAIN.Identification=SORTCODEACCOUNTNUMBER
        - GREAT_BRITAIN.ServicerSchemeName=
        - GREAT_BRITAIN.ServicerIdentification=
      PISP: 
        - NORTHERN_IRELAND.SchemeName=SORTCODEACCOUNTNUMBER
        - NORTHERN_IRELAND.Identification=SORTCODEACCOUNTNUMBER
        - NORTHERN_IRELAND.ServicerSchemeName=
        - NORTHERN_IRELAND.ServicerIdentification=
        - GREAT_BRITAIN.SchemeName=SORTCODEACCOUNTNUMBER
        - GREAT_BRITAIN.Identification=SORTCODEACCOUNTNUMBER
        - GREAT_BRITAIN.ServicerSchemeName=
        - GREAT_BRITAIN.ServicerIdentification=
app:
  platform: platform
  rules:
   response:
    restTransportForAccountStandingOrder: 
     $.standingOrders|*|.payeeAccountNumber: '[0-9a-zA-z](?=\w{3}),X'
     $.standingOrders|*|.payeeSortCode: '(?<=\w{3})[0-9a-zA-z],X'   