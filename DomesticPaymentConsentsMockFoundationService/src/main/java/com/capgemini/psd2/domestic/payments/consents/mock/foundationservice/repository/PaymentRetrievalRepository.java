package com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.repository;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.foundationservice.pisp1.domain.PaymentInstruction;





/**
 * The Interface PaymentRetrievalRepository.
 */
public interface PaymentRetrievalRepository extends MongoRepository<PaymentInstruction , String> {

	/**
	 * Find by paymentId.
	 *
	
	 * @param paymentId
	 * @return the payment
	 */
	public PaymentInstruction findByPaymentPaymentId(String paymentId);
	

}
