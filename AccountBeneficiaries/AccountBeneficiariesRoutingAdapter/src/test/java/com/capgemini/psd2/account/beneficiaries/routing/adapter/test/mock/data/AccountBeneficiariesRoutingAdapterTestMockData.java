package com.capgemini.psd2.account.beneficiaries.routing.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBBeneficiary2;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary2;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary2Data;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBeneficiariesResponse;

public class AccountBeneficiariesRoutingAdapterTestMockData {

	public static PlatformAccountBeneficiariesResponse getMockBeneficiariesGETResponse() {
		
		PlatformAccountBeneficiariesResponse platformAccountBeneficiariesResponse = new PlatformAccountBeneficiariesResponse();
		OBReadBeneficiary2Data obReadBeneficiary2Data = new OBReadBeneficiary2Data();
		List<OBBeneficiary2> obBeneficiaryList = new ArrayList<>();
		obReadBeneficiary2Data.setBeneficiary(obBeneficiaryList);
		
		OBReadBeneficiary2 obReadBeneficiary2 = new OBReadBeneficiary2();
		obReadBeneficiary2.setData(obReadBeneficiary2Data);
		platformAccountBeneficiariesResponse.setoBReadBeneficiary2(obReadBeneficiary2);
		return platformAccountBeneficiariesResponse;
	}
}
