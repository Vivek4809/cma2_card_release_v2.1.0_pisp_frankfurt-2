package com.capgemini.psd2.accounts.schedulepayments.mongo.db.adapter.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountSchedulePaymentsCMA2;

@Repository
public interface AccountSchedulePaymentRepository extends MongoRepository<AccountSchedulePaymentsCMA2, String> {

	/**
	 * Find by account number and NSC.
	 *
	 * @param account Number
	 * @param accountNSC 
	 * @return the schedulePayments GET response data
	 */
	public List<AccountSchedulePaymentsCMA2> findByAccountNumberAndAccountNSC(String accountNumber, String accountNSC);
}
