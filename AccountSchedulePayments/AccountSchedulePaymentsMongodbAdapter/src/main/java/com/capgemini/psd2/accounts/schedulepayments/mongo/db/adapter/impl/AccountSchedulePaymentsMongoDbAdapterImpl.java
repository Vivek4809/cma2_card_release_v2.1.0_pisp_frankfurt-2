package com.capgemini.psd2.accounts.schedulepayments.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.accounts.schedulepayments.mongo.db.adapter.repository.AccountSchedulePaymentRepository;
import com.capgemini.psd2.aisp.adapter.AccountSchedulePaymentsAdapter;
import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment1;
import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment1Data;
import com.capgemini.psd2.aisp.domain.OBScheduledPayment1;
import com.capgemini.psd2.aisp.domain.OBStandingOrder3;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountSchedulePaymentsCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSchedulePaymentsResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@Component
public class AccountSchedulePaymentsMongoDbAdapterImpl implements AccountSchedulePaymentsAdapter{

	@Autowired
	private AccountSchedulePaymentRepository repository;
	
	@Autowired
	private SandboxValidationUtility utility;

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AccountSchedulePaymentsMongoDbAdapterImpl.class);
	
	@Override
	public PlatformAccountSchedulePaymentsResponse retrieveAccountSchedulePayments(AccountMapping accountMapping,
			Map<String, String> params) {
		PlatformAccountSchedulePaymentsResponse platformAccountSchedulePaymentsResponse = new PlatformAccountSchedulePaymentsResponse();
		OBReadScheduledPayment1 oBReadScheduledPayment1 = new OBReadScheduledPayment1();
		List<AccountSchedulePaymentsCMA2> mockSchedulePayments = new ArrayList<>();
		
		try {
			if(utility.isValidPsuId(accountMapping.getPsuId())){
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
			}
			mockSchedulePayments = repository.findByAccountNumberAndAccountNSC(
					accountMapping.getAccountDetails().get(0).getAccountNumber(),
					accountMapping.getAccountDetails().get(0).getAccountNSC());
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.CONNECTION_ERROR));
		}

		List<OBScheduledPayment1> schedulePaymentList = new ArrayList<>();
		
		if (null != mockSchedulePayments && !mockSchedulePayments.isEmpty()) {
			for (AccountSchedulePaymentsCMA2 scheduledPayment : mockSchedulePayments) {
				scheduledPayment.setScheduledPaymentDateTime(utility.addDaysToCurrentDtTm(5));
				scheduledPayment.setAccountId(accountMapping.getAccountDetails().get(0).getAccountId());
				schedulePaymentList.add(scheduledPayment);
			}
		}
		
		OBReadScheduledPayment1Data oBReadScheduledPayment1Data = new OBReadScheduledPayment1Data();
		oBReadScheduledPayment1Data.setScheduledPayment(schedulePaymentList);
		oBReadScheduledPayment1.setData(oBReadScheduledPayment1Data);
		platformAccountSchedulePaymentsResponse.setObReadScheduledPayment1(oBReadScheduledPayment1);
		return platformAccountSchedulePaymentsResponse;
	}

}
