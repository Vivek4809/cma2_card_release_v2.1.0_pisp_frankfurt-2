package com.capgemini.psd2.account.schedulepayments.routing.adapter.routing;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.AccountSchedulePaymentsAdapter;

@Component
public class AccountSchedulePaymentsCoreSystemAdapterFactory implements ApplicationContextAware,AccountSchedulePaymentsAdapterFactory{

	/** The application context. */
	private ApplicationContext applicationContext;
	
	@Override
	public AccountSchedulePaymentsAdapter getAdapterInstance(String adapterName) {
		return (AccountSchedulePaymentsAdapter)applicationContext.getBean(adapterName);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	
}
