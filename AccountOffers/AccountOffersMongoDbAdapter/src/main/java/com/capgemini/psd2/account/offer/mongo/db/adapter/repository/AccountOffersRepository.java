package com.capgemini.psd2.account.offer.mongo.db.adapter.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountOffersCMA2;

public interface AccountOffersRepository extends MongoRepository<AccountOffersCMA2, String> {
	
	/**
	 * Find by account id.
	 *
	 * @param accountId the account id
	 * @return the balances GET response data
	 */
	public List<AccountOffersCMA2> findByAccountNumberAndAccountNSC(String accountNumber, String accountNSC);
}
