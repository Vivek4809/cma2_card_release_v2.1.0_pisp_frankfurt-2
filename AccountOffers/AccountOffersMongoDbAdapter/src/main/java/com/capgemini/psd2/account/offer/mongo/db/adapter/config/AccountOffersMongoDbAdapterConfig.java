package com.capgemini.psd2.account.offer.mongo.db.adapter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.account.offer.mongo.db.adapter.impl.AccountOffersMongoDbAdapterImpl;
import com.capgemini.psd2.aisp.adapter.AccountOffersAdapter;

@Configuration
public class AccountOffersMongoDbAdapterConfig {
	
	@Bean(name="accountOffersMongoDbAdapter")
	public AccountOffersAdapter accountOffersMongoDbAdapter()
	{
		return new AccountOffersMongoDbAdapterImpl();
	}

}
