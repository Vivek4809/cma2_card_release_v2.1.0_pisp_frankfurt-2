/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.statement.mock.foundationservice.service;

import java.io.IOException;

import com.capgemini.psd2.account.statement.mock.foundationservice.raml.domain.Statementsresponse;

//import com.capgemini.psd2.account.statement.mock.foundationservice.raml.domain.StatementsResponse;

/**
 * The Interface AccountStatementsService.
 */
public interface AccountStatementService {

	public Statementsresponse retrieveAccountStatements(String accountId, String FromBookingDateTime, String ToBookingDateTime) throws Exception;

	public byte[] retrivePdf(String accountId, String statementId) throws IOException;

}
