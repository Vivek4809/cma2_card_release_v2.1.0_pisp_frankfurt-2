package com.capgemini.psd2.payment.retrieval.mock.foundationservice.service;


import com.capgemini.psd2.payment.retrieval.mock.foundationservice.domain.PaymentInstruction;

public interface PaymentRetrievalServiceInterface {
	
	public PaymentInstruction retrievePayment(String paymentId) throws Exception;

}
