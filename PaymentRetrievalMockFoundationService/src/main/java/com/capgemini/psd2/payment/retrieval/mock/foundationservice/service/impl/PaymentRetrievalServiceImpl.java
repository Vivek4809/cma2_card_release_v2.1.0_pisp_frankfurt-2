package com.capgemini.psd2.payment.retrieval.mock.foundationservice.service.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.payment.retrieval.mock.foundationservice.domain.PaymentInstruction;
import com.capgemini.psd2.payment.retrieval.mock.foundationservice.repository.PaymentRetrievalRepository;
import com.capgemini.psd2.payment.retrieval.mock.foundationservice.service.PaymentRetrievalServiceInterface;


/**
 * The Class RetrivePaymenServiceImpl.
 */
@Service
public class PaymentRetrievalServiceImpl implements PaymentRetrievalServiceInterface{

	/** The repository. */
	@Autowired
	private PaymentRetrievalRepository repository;

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.mock.foundationservice.service.RetrivePayment(java.lang.String, java.lang.String)
	 */
	
	@Override
	public PaymentInstruction retrievePayment(String paymentId) throws Exception {
		PaymentInstruction paymentInstruction = repository.findByPaymentPaymentId(paymentId);

		if (paymentInstruction == null) {
			throw MockFoundationServiceException
			.populateMockFoundationServiceException(ErrorCodeEnum.NO_PAYMENT_DETAILS_FOUND_PMR);
		}
		/*PaymentInstruction paymentinstruction = new PaymentInstruction();
		paymentinstruction.getPayment().add(payment);*/

		return paymentInstruction;

	}
}




	


