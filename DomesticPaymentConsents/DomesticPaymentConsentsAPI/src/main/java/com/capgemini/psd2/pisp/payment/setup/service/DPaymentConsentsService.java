package com.capgemini.psd2.pisp.payment.setup.service;

import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;

public interface DPaymentConsentsService {

	public CustomDPaymentConsentsPOSTResponse createDomesticPaymentConsentsResource(
			CustomDPaymentConsentsPOSTRequest paymentRequest);

	public CustomDPaymentConsentsPOSTResponse retrieveDomesticPaymentConsentsResource(
			PaymentRetrieveGetRequest paymentRetrieveRequest);

}
