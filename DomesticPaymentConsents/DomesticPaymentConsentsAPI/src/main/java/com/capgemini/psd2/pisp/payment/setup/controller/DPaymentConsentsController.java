package com.capgemini.psd2.pisp.payment.setup.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticConsent1;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.payment.setup.service.DPaymentConsentsService;

@RestController
public class DPaymentConsentsController {

	@Autowired
	private DPaymentConsentsService dPaymentConsentsService;

	@Value("${app.responseErrorMessageEnabled}")
	private String responseErrorMessageEnabled;

	@RequestMapping(value = "/domestic-payment-consents", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<CustomDPaymentConsentsPOSTResponse> createDomesticPaymentConsentsResource(
			@RequestBody OBWriteDomesticConsent1 paymentConsentsRequest) {

		CustomDPaymentConsentsPOSTRequest customPaymentConsentsRequest = new CustomDPaymentConsentsPOSTRequest();
		customPaymentConsentsRequest.setData(paymentConsentsRequest.getData());
		customPaymentConsentsRequest.setRisk(paymentConsentsRequest.getRisk());

		CustomDPaymentConsentsPOSTResponse paymentConsentsResponse = dPaymentConsentsService
				.createDomesticPaymentConsentsResource(customPaymentConsentsRequest);
		return new ResponseEntity<>(paymentConsentsResponse, HttpStatus.CREATED);

	}

	@RequestMapping(value = "/domestic-payment-consents/{ConsentId}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	public ResponseEntity<CustomDPaymentConsentsPOSTResponse> retrieveDomesticPaymentConsentsResource(
			PaymentRetrieveGetRequest paymentRetrieveRequest) {

		CustomDPaymentConsentsPOSTResponse paymentConsentsResponse = dPaymentConsentsService
				.retrieveDomesticPaymentConsentsResource(paymentRetrieveRequest);
		return new ResponseEntity<>(paymentConsentsResponse, HttpStatus.OK);

	}
}
