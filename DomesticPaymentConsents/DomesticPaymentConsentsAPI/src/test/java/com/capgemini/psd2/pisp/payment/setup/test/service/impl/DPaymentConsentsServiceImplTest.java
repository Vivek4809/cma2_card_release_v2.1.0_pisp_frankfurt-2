package com.capgemini.psd2.pisp.payment.setup.test.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.payment.setup.comparator.DPaymentConsentsPayloadComparator;
import com.capgemini.psd2.pisp.payment.setup.service.impl.DPaymentConsentsServiceImpl;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentConsentProcessingAdapter;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticPaymentStagingAdapter;

public class DPaymentConsentsServiceImplTest {

	@InjectMocks
	DPaymentConsentsServiceImpl dpaymentconsentsserviceimpl;

	@Mock
	private PaymentConsentProcessingAdapter paymemtRequestAdapter;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private DPaymentConsentsPayloadComparator dPaymentConsentsComparator;

	@Mock
	private DomesticPaymentStagingAdapter dPaymentStagingAdapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testCreateDomesticPaymentConsentResouceWithNullId() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setPaymentConsentId(null);
		resource.setCreatedAt("30/11/18");

		Mockito.when(paymemtRequestAdapter.preConsentProcessFlows(anyObject(), anyObject())).thenReturn(resource);

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		data.setStatus(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION);
		data.setConsentId("45612");
		response.setData(data);
		OBDomestic1 initiation = new OBDomestic1();
		initiation.setLocalInstrument("test");
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setName("debtorAccount");
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);

		Mockito.when(dPaymentStagingAdapter.processDomesticPaymentConsents(anyObject(), anyObject(), anyMap(),
				Matchers.any(), Matchers.any())).thenReturn(response);

		CustomDPaymentConsentsPOSTRequest request = new CustomDPaymentConsentsPOSTRequest();
		request.setData(new OBWriteDataDomesticConsent1());
		request.getData().setInitiation(new OBDomestic1());
		OBRemittanceInformation1 remittanceInfo = new OBRemittanceInformation1();
		request.getData().getInitiation().setRemittanceInformation(remittanceInfo);
		request.getData().getInitiation().getRemittanceInformation().setReference("1");
		response.getData().getInitiation().setRemittanceInformation(remittanceInfo);
		OBPostalAddress6 postalAddress = new OBPostalAddress6();
		request.getData().getInitiation().setCreditorPostalAddress(postalAddress);
		request.getData().getInitiation().getCreditorPostalAddress().setCountry("Ireland");
		response.getData().getInitiation().setCreditorPostalAddress(postalAddress);
		request.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		request.getData().getInitiation().getDebtorAccount().setName("DebtorName");;
		OBRisk1 requestRisk = new OBRisk1();
		requestRisk.setDeliveryAddress(new OBRisk1DeliveryAddress());
		requestRisk.getDeliveryAddress().buildingNumber("1");
		request.setRisk(requestRisk);
		response.setRisk(requestRisk);

		dpaymentconsentsserviceimpl.createDomesticPaymentConsentsResource(request);
		assertEquals("45612", data.getConsentId());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testCreateandRetrieveDomesticPaymentConsentResouceWithPaymentId() {

		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setPaymentConsentId("123456");
		resource.setCreatedAt("01/11/18");

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		data.setStatus(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION);
		data.setConsentId("45612");
		response.setData(data);
		OBDomestic1 initiation = new OBDomestic1();
		initiation.setLocalInstrument("test");
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setName("debtorAccount");
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);

		Mockito.when(dPaymentStagingAdapter.processDomesticPaymentConsents(anyObject(), anyObject(), anyMap(),
				Matchers.any(), Matchers.any())).thenReturn(response);

		CustomDPaymentConsentsPOSTRequest request = new CustomDPaymentConsentsPOSTRequest();
		request.setData(new OBWriteDataDomesticConsent1());
		request.getData().setInitiation(new OBDomestic1());
		OBRemittanceInformation1 remittanceInfo = new OBRemittanceInformation1();
		request.getData().getInitiation().setRemittanceInformation(remittanceInfo);
		request.getData().getInitiation().getRemittanceInformation().setReference("1");
		response.getData().getInitiation().setRemittanceInformation(remittanceInfo);
		OBPostalAddress6 postalAddress = new OBPostalAddress6();
		request.getData().getInitiation().setCreditorPostalAddress(postalAddress);
		request.getData().getInitiation().getCreditorPostalAddress().setCountry("Ireland");
		response.getData().getInitiation().setCreditorPostalAddress(postalAddress);
		OBRisk1 requestRisk = new OBRisk1();
		requestRisk.setDeliveryAddress(new OBRisk1DeliveryAddress());
		requestRisk.getDeliveryAddress().buildingNumber("1");
		request.setRisk(requestRisk);
		response.setRisk(requestRisk);

		Mockito.when(paymemtRequestAdapter.preConsentProcessFlows(anyObject(), anyObject())).thenReturn(resource);

		Mockito.when(dPaymentStagingAdapter.retrieveStagedDomesticPaymentConsents(anyObject(), anyMap()))
				.thenReturn(response);

		Mockito.when(dPaymentConsentsComparator.comparePaymentDetails(anyObject(), anyObject(), anyObject()))
				.thenReturn(0);

		dpaymentconsentsserviceimpl.createDomesticPaymentConsentsResource(request);
		assertEquals("45612", data.getConsentId());
	}

	@Test
	public void testRetrieveDomesticPaymentConsentsResource() {

		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setSetupCmaVersion(PSD2Constants.CMAVERSION);

		Mockito.when(paymemtRequestAdapter.preConsentProcessGETFlow(anyObject())).thenReturn(resource);

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		data.setConsentId("45612");
		response.setData(data);
		OBDomestic1 initiation = new OBDomestic1();
		initiation.setLocalInstrument("test");
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setName("debtorAccount");
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);

		Mockito.when(dPaymentStagingAdapter.retrieveStagedDomesticPaymentConsents(anyObject(), anyMap()))
				.thenReturn(response);

		CustomDPaymentConsentsPOSTResponse response1 = new CustomDPaymentConsentsPOSTResponse();
		Mockito.when(
				paymemtRequestAdapter.postConsentProcessFlows(anyObject(), anyObject(), anyObject(), Matchers.any()))
				.thenReturn((CustomDPaymentConsentsPOSTResponse) response1);

		PaymentRetrieveGetRequest retrieveRequest = new PaymentRetrieveGetRequest();
		retrieveRequest.setConsentId("45612");
		dpaymentconsentsserviceimpl.retrieveDomesticPaymentConsentsResource(retrieveRequest);
	}
	
	@Test
	public void testRetrieveDomesticPaymentConsentsResource_cmaVersionNull() {

		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setSetupCmaVersion(null);

		Mockito.when(paymemtRequestAdapter.preConsentProcessGETFlow(anyObject())).thenReturn(resource);

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		data.setConsentId("45612");
		response.setData(data);
		OBDomestic1 initiation = new OBDomestic1();
		initiation.setLocalInstrument("test");
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setName("debtorAccount");
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);

		Mockito.when(dPaymentStagingAdapter.retrieveStagedDomesticPaymentConsents(anyObject(), anyMap()))
				.thenReturn(response);

		CustomDPaymentConsentsPOSTResponse response1 = new CustomDPaymentConsentsPOSTResponse();
		Mockito.when(
				paymemtRequestAdapter.postConsentProcessFlows(anyObject(), anyObject(), anyObject(), Matchers.any()))
				.thenReturn((CustomDPaymentConsentsPOSTResponse) response1);

		PaymentRetrieveGetRequest retrieveRequest = new PaymentRetrieveGetRequest();
		retrieveRequest.setConsentId("45612");
		dpaymentconsentsserviceimpl.retrieveDomesticPaymentConsentsResource(retrieveRequest);
	}


}