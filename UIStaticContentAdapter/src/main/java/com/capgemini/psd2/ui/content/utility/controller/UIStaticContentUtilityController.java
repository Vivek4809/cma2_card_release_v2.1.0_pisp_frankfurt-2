package com.capgemini.psd2.ui.content.utility.controller;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.yaml.snakeyaml.Yaml;

import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.jayway.jsonpath.JsonPath;

@RestController
public class UIStaticContentUtilityController {

	@Value("${ui.config}")
	private String uiAppName;
	
	@Value("${spring.profiles}")
	private String profile;
	
	@Value("${spring.cloud.config.uri}")
	private String configServerUrl;

	@Autowired
	private RestClientSync restClientSync;

	private String uiContentCache;

	public String getConfigVariable() {
		return uiContentCache;
	}

	public void setConfigVariable(String configCache) {
		this.uiContentCache = configCache;
	}

	@PostConstruct
	public void init() {
		updateStaticContentoForUI();
	}

	@RequestMapping(value = "/ui/staticContentUpdate", method = RequestMethod.GET)
	public void updateStaticContentoForUI() {
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(configServerUrl + "/" + uiAppName + "-" + profile + ".yml");
		uiContentCache = convertToJson(restClientSync.callForGet(requestInfo, String.class, null));
	}

	@RequestMapping(value = "/ui/staticContentGet", method = RequestMethod.GET)
	public String getStaticContentForUI() {
		return uiContentCache;
	}

	private String convertToJson(String yamlString) {
		Yaml yaml = new Yaml();
		Map<String, Object> map = (Map<String, Object>) yaml.load(yamlString);
		return JSONUtilities.getJSONOutPutFromObject(map);
	}
	
	public String retrieveUiParamValue(String  param,String parameter){
		String uiDataString = getStaticContentForUI();
		Map<String,Object> map = JSONUtilities.getObjectFromJSONString(uiDataString, Map.class);
		Object paramValue = map.get(param);
	    if(paramValue instanceof Map){
	    	paramValue =((Map) paramValue).get(parameter);
	    }
		return paramValue.toString();
	}

	public String retrieveUiParamValue(String parameter){
		String uiDataString = getStaticContentForUI();
		return JsonPath.read(uiDataString, parameter);
	}

}