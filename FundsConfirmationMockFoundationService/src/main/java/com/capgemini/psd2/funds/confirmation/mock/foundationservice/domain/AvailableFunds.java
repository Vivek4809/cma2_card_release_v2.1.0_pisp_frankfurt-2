package com.capgemini.psd2.funds.confirmation.mock.foundationservice.domain;

import java.util.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Available Funds for Withdrawal Amount
 */
@ApiModel(description = "Available Funds for Withdrawal Amount")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-02-07T14:38:09.445+05:30")

@JsonInclude(Include.NON_NULL)
public class AvailableFunds {
	@JsonProperty("parentNationalSortCodeNSCNumber")
	private String parentNationalSortCodeNSCNumber = null;

	@JsonProperty("accountCurrency")
	private Currency accountCurrency = null;

	@JsonProperty("accountNumber")
	private String accountNumber = null;

	@JsonProperty("availableFundsForWithdrawalAmount")
	private AvailableFundsAvailableFundsForWithdrawalAmount availableFundsForWithdrawalAmount = null;

	@JsonProperty("availableFundsCheckDatetime")
	private Date availableFundsCheckDatetime = null;

	public AvailableFunds parentNationalSortCodeNSCNumber(String parentNationalSortCodeNSCNumber) {
		this.parentNationalSortCodeNSCNumber = parentNationalSortCodeNSCNumber;
		return this;
	}

	/**
	 * Parent NSC of the branch where account is held
	 * 
	 * @return parentNationalSortCodeNSCNumber
	 **/
	@ApiModelProperty(example = "987654", required = true, value = "Parent NSC of the branch where account is held")
	@NotNull

	@Pattern(regexp = "^[0-9]{6}$")
	public String getParentNationalSortCodeNSCNumber() {
		return parentNationalSortCodeNSCNumber;
	}

	public void setParentNationalSortCodeNSCNumber(String parentNationalSortCodeNSCNumber) {
		this.parentNationalSortCodeNSCNumber = parentNationalSortCodeNSCNumber;
	}

	public AvailableFunds accountCurrency(Currency accountCurrency) {
		this.accountCurrency = accountCurrency;
		return this;
	}

	/**
	 * Get accountCurrency
	 * 
	 * @return accountCurrency
	 **/
	@ApiModelProperty(required = true, value = "")
	@NotNull

	@Valid

	public Currency getAccountCurrency() {
		return accountCurrency;
	}

	public void setAccountCurrency(Currency accountCurrency) {
		this.accountCurrency = accountCurrency;
	}

	public AvailableFunds accountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
		return this;
	}

	/**
	 * The identifier of the Account. This will be unique within its Parent NSC,
	 * but will not be globally unique.
	 * 
	 * @return accountNumber
	 **/
	@ApiModelProperty(required = true, value = "The identifier of the Account. This will be unique within its Parent NSC, but will not be globally unique.")
	@NotNull

	@Pattern(regexp = "^[A-Za-z0-9/?:().,'+ -]{0,34}$")
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public AvailableFunds availableFundsForWithdrawalAmount(
			AvailableFundsAvailableFundsForWithdrawalAmount availableFundsForWithdrawalAmount) {
		this.availableFundsForWithdrawalAmount = availableFundsForWithdrawalAmount;
		return this;
	}

	/**
	 * Get availableFundsForWithdrawalAmount
	 * 
	 * @return availableFundsForWithdrawalAmount
	 **/
	@ApiModelProperty(required = true, value = "")
	@NotNull

	@Valid

	public AvailableFundsAvailableFundsForWithdrawalAmount getAvailableFundsForWithdrawalAmount() {
		return availableFundsForWithdrawalAmount;
	}

	public void setAvailableFundsForWithdrawalAmount(
			AvailableFundsAvailableFundsForWithdrawalAmount availableFundsForWithdrawalAmount) {
		this.availableFundsForWithdrawalAmount = availableFundsForWithdrawalAmount;
	}

	public AvailableFunds availableFundsCheckDatetime(Date availableFundsCheckDatetime) {
		this.availableFundsCheckDatetime = availableFundsCheckDatetime;
		return this;
	}

	/**
	 * Reflects the time fund check were made.
	 * 
	 * @return availableFundsCheckDatetime
	 **/
	@ApiModelProperty(value = "Reflects the time fund check were made.")

	@Valid

	public Date getAvailableFundsCheckDatetime() {
		return availableFundsCheckDatetime;
	}

	public void setAvailableFundsCheckDatetime(Date availableFundsCheckDatetime) {
		this.availableFundsCheckDatetime = availableFundsCheckDatetime;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		AvailableFunds availableFunds = (AvailableFunds) o;
		return Objects.equals(this.parentNationalSortCodeNSCNumber, availableFunds.parentNationalSortCodeNSCNumber)
				&& Objects.equals(this.accountCurrency, availableFunds.accountCurrency)
				&& Objects.equals(this.accountNumber, availableFunds.accountNumber)
				&& Objects.equals(this.availableFundsForWithdrawalAmount,
						availableFunds.availableFundsForWithdrawalAmount)
				&& Objects.equals(this.availableFundsCheckDatetime, availableFunds.availableFundsCheckDatetime);
	}

	@Override
	public int hashCode() {
		return Objects.hash(parentNationalSortCodeNSCNumber, accountCurrency, accountNumber,
				availableFundsForWithdrawalAmount, availableFundsCheckDatetime);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class AvailableFunds {\n");

		sb.append("    parentNationalSortCodeNSCNumber: ").append(toIndentedString(parentNationalSortCodeNSCNumber))
				.append("\n");
		sb.append("    accountCurrency: ").append(toIndentedString(accountCurrency)).append("\n");
		sb.append("    accountNumber: ").append(toIndentedString(accountNumber)).append("\n");
		sb.append("    availableFundsForWithdrawalAmount: ").append(toIndentedString(availableFundsForWithdrawalAmount))
				.append("\n");
		sb.append("    availableFundsCheckDatetime: ").append(toIndentedString(availableFundsCheckDatetime))
				.append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
