package com.capgemini.psd2.pisp.file.payment.setup.mongo.db.adapter.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.BasicAWSCredentials;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;

@Configuration
public class FPaymentConsentsConfiguration {
	
	
	@Value("${aws.proxyHost:null}")
	private String proxyHost;

	@Value("${aws.proxyPort:null}")
	private String proxyPort;

	@Value("#{T(com.amazonaws.Protocol).valueOf('${aws.protocol:HTTPS}')}")
	private Protocol protocol;

	@Value("${aws.proxy:#{false}}")
	private boolean proxy;

	@Bean
	public BasicAWSCredentials getAWSCredentials(
			@Value("${app.awsAccesskey:null}") String accessKey, @Value("${app.awsSecretkey:null}")String secretKey) throws Exception {
		
		BasicAWSCredentials awsCredentials = null;
		try {
			awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
			return awsCredentials;

		}catch(Exception exception) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
		
	}
	
	@Bean
	public ClientConfiguration getClientConfig() {
		ClientConfiguration clientConfiguration = null;
		if(proxy){
			clientConfiguration = new ClientConfiguration();
			clientConfiguration.setProxyHost(proxyHost);
			clientConfiguration.setProxyPort(Integer.valueOf(proxyPort));
			clientConfiguration.setProtocol(protocol);
		}
		return clientConfiguration;
	}

}
