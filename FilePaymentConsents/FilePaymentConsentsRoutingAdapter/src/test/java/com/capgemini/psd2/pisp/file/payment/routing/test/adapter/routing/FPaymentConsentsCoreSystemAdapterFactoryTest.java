package com.capgemini.psd2.pisp.file.payment.routing.test.adapter.routing;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.adapter.FilePaymentConsentsAdapter;
import com.capgemini.psd2.pisp.file.payment.routing.adapter.impl.FPaymentConsentsRoutingAdapterImpl;
import com.capgemini.psd2.pisp.file.payment.routing.adapter.routing.FPaymentConsentsCoreSystemAdapterFactory;

@RunWith(SpringJUnit4ClassRunner.class)
public class FPaymentConsentsCoreSystemAdapterFactoryTest {

	@InjectMocks
	FPaymentConsentsCoreSystemAdapterFactory factory;

	@Mock
	ApplicationContext applicationContext;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	@Test
	public void testGetFilePaymentSetupStagingInstance() {
		FPaymentConsentsRoutingAdapterImpl adapter = new FPaymentConsentsRoutingAdapterImpl();
		when(applicationContext.getBean(anyString())).thenReturn(adapter);
		FilePaymentConsentsAdapter fAdapter = factory.getFilePaymentSetupStagingInstance("test");
		assertNotNull(fAdapter);
	}

	@Test
	public void testSetApplicationContext() {
		factory.setApplicationContext(new ApplicationContextMock());
	}

}
