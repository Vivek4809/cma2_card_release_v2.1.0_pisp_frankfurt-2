package com.capgemini.psd2.pisp.file.payment.routing.test.adapter.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.multipart.MultipartFile;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.FilePaymentConsentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.file.payment.routing.adapter.impl.FPaymentConsentsRoutingAdapterImpl;
import com.capgemini.psd2.pisp.file.payment.routing.adapter.routing.FPaymentConsentsAdapterFactory;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.token.Token;

@RunWith(SpringJUnit4ClassRunner.class)
public class FPaymentConsentsRoutingAdapterImplTest {

	@InjectMocks
	FPaymentConsentsRoutingAdapterImpl adapter;

	@Mock
	FPaymentConsentsAdapterFactory factory;

	@Mock
	RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private FilePaymentConsentsAdapter paymentConsentsAdapter;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	@Test
	public void testCreateStagingFilePaymentConsents() {
		CustomFilePaymentSetupPOSTRequest request = new CustomFilePaymentSetupPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Mockito.when(factory.getFilePaymentSetupStagingInstance(anyString())).thenReturn(paymentConsentsAdapter);
		adapter.createStagingFilePaymentConsents(request, stageIdentifiers, new HashMap<>());
	}

	@Test
	public void testCreateStagingFilePaymentConsentsNull() {
		CustomFilePaymentSetupPOSTRequest request = new CustomFilePaymentSetupPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Mockito.when(factory.getFilePaymentSetupStagingInstance(anyString())).thenReturn(paymentConsentsAdapter);
		adapter.createStagingFilePaymentConsents(request, stageIdentifiers, null);
	}

	@Test
	public void testCreateStagingFilePaymentConsentsNotNull() {
		CustomFilePaymentSetupPOSTRequest request = new CustomFilePaymentSetupPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Token token = new Token();
		Map<String, String> seviceParams = new HashMap<>();
		token.setSeviceParams(seviceParams);
		reqHeaderAtrributes.setToken(token);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		Mockito.when(factory.getFilePaymentSetupStagingInstance(anyString())).thenReturn(paymentConsentsAdapter);
		adapter.createStagingFilePaymentConsents(request, stageIdentifiers, new HashMap<>());
		assertNotNull(adapter);
	}
	
	@Test
	public void testCreateStagingFilePaymentConsentsNotNullPartially() {
		CustomFilePaymentSetupPOSTRequest request = new CustomFilePaymentSetupPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Token token = new Token();
		reqHeaderAtrributes.setToken(token);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		Mockito.when(factory.getFilePaymentSetupStagingInstance(anyString())).thenReturn(paymentConsentsAdapter);
		adapter.createStagingFilePaymentConsents(request, stageIdentifiers, new HashMap<>());
		assertNotNull(adapter);
	}

	@Test
	public void testUploadFilePaymentConsents() {
		Mockito.when(factory.getFilePaymentSetupStagingInstance(anyString())).thenReturn(paymentConsentsAdapter);
		MultipartFile paymentFile = null;
		String consentId = "123";
		adapter.uploadFilePaymentConsents(paymentFile, consentId);
	}

	@Test
	public void testUpdateStagingFilePaymentConsents() {
		CustomFilePaymentConsentsPOSTResponse response = new CustomFilePaymentConsentsPOSTResponse();
		Mockito.when(factory.getFilePaymentSetupStagingInstance(anyString())).thenReturn(paymentConsentsAdapter);
		adapter.updateStagingFilePaymentConsents(response);
	}

	@After
	public void tearDown() throws Exception {
		factory = null;
		adapter = null;
		reqHeaderAtrributes = null;
	}

}
