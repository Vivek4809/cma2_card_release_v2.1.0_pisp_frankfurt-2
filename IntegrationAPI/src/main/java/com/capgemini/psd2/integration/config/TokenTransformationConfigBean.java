package com.capgemini.psd2.integration.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "app")
public class TokenTransformationConfigBean {

	private List<String> serviceparams = new ArrayList<>();

	public List<String> getServiceparams() {
		return serviceparams;
	}

	public void setServiceparams(List<String> serviceparams) {
		this.serviceparams = serviceparams;
	}

}
