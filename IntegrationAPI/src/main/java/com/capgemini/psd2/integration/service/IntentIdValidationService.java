package com.capgemini.psd2.integration.service;

public interface IntentIdValidationService {

	public void validateIntentId(String intentId, String clientId, String scope);
	
}
