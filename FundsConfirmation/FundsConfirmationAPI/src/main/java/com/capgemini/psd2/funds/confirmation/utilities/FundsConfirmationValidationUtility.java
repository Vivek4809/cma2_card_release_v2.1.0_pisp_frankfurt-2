package com.capgemini.psd2.funds.confirmation.utilities;

import java.util.Currency;

import org.springframework.stereotype.Component;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;

@Component
public class FundsConfirmationValidationUtility {
	
	public static boolean isEmpty(String data){
		boolean status = false;
		if(data == null || data.trim().length() == 0)
			status = true;
		
		return status;
	}
	
	public boolean isValidCurrency(String currencyCode){
		
		if(isEmpty(currencyCode))
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO (OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_CURRENCY, ErrorMapKeys.CURRENCY_NOT_SUPPORTED));
		
		try{
			Currency.getInstance(currencyCode.trim());				
			return true;
		}catch(Exception exception){
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO (OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_CURRENCY, exception.getMessage()));
			
		}
}
		
}
