package com.capgemini.psd2.foundationservice.domestic.payment.submission.boi.adapter.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.DomesticScheduledPaymentsFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.client.DomesticScheduledPaymentSubmissionFoundationServiceClient;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.delegate.DomesticScheduledPaymentSubmissionFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.transformer.DomesticScheduledPaymentSubmissionFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticScheduledPaymentsFoundationServiceAdapterTest {
	
	@InjectMocks
	DomesticScheduledPaymentsFoundationServiceAdapter adapter;
	@Mock
	DomesticScheduledPaymentSubmissionFoundationServiceDelegate delegate;
	@Mock
	DomesticScheduledPaymentSubmissionFoundationServiceClient client;
	@Mock
	DomesticScheduledPaymentSubmissionFoundationServiceTransformer transformer;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testretrieveStagedDomesticPaymentConsents(){
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers= new CustomPaymentStageIdentifiers();
		Map<String, String> params= new HashMap<>();
		customPaymentStageIdentifiers.setPaymentSetupVersion("2.0");
		customPaymentStageIdentifiers.setPaymentSubmissionId("121222");
		adapter.retrieveStagedDSPaymentsResponse(customPaymentStageIdentifiers, params);
	}
	
	@Test
	public void testProcessDomesticPaymentConsents(){
		CustomDSPaymentsPOSTRequest paymentConsentsRequest= new CustomDSPaymentsPOSTRequest();
		CustomPaymentStageIdentifiers customStageIdentifiers= new CustomPaymentStageIdentifiers();
		Map<String, String> params= new HashMap<>();	
		CustomDSPConsentsPOSTRequest domesticScheduledPaymentRequest=new CustomDSPConsentsPOSTRequest();
		OBExternalConsentStatus1Code successStatus = null;
		OBExternalConsentStatus1Code failureStatus = null;
		Map<String, OBExternalStatus1Code> paymentStatusMap = null;
		adapter.processDSPayments(paymentConsentsRequest, paymentStatusMap, params);
	}
}
