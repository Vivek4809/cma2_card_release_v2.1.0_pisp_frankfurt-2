//package com.capgemini.psd2.pisp.domestic.payments.routing.test.adapter.helper;
//
//import static org.mockito.Matchers.anyObject;
//import static org.mockito.Mockito.when;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import com.capgemini.psd2.logger.RequestHeaderAttributes;
//import com.capgemini.psd2.pisp.adapter.DomesticPaymentsAdapter;
//import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
//import com.capgemini.psd2.pisp.domain.DomesticPaymentExecutionResource;
//import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
//import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticResponse1;
//import com.capgemini.psd2.pisp.domain.PaymentConsentsValidationResponse;
//import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
//import com.capgemini.psd2.pisp.domestic.payments.routing.adapter.helper.impl.DomesticPaymentsAdapterHelperImpl;
//import com.capgemini.psd2.token.Token;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//public class DomesticPaymentsAdapterHelperImplTest {
//
//	@InjectMocks
//	DomesticPaymentsAdapterHelperImpl adapter;
//
//	@Mock
//	@Qualifier("domesticPaymentsRoutingAdapterImpl")
//	private DomesticPaymentsAdapter domesticPaymentsRoutingAdapter;
//
//	@Mock
//	private RequestHeaderAttributes reqHeaderAtrributes;
//
//	@Before
//	public void setUp() {
//		MockitoAnnotations.initMocks(this);
//	}
//
//	@Test
//	public void contextLoads() {
//	}
//
//	@Test
//	public void preDomesticPaymentsValidationTest() {
//
//		Token token = new Token();
//		Map<String, String> map = new HashMap<>();
//		map.put("channelId", "test");
//		token.setSeviceParams(map);
//		reqHeaderAtrributes.setToken(token);
//		when(reqHeaderAtrributes.getToken()).thenReturn(token);
//
//		OBTransactionIndividualStatus1Code paymentSetupValidationStatus = null;
//		PaymentConsentsValidationResponse value = new PaymentConsentsValidationResponse();
//		value.setPaymentSetupValidationStatus(paymentSetupValidationStatus.PENDING);
//
//		Mockito.when(domesticPaymentsRoutingAdapter.preAuthorizeDomesticPayments(anyObject(), anyObject()))
//				.thenReturn(value);
//		CustomDPaymentConsentsPOSTResponse paymentSetupResponse = new CustomDPaymentConsentsPOSTResponse();
//		adapter.preDomesticPaymentsValidation(paymentSetupResponse);
//
//	}
//
//	@Test
//	public void preDomesticPaymentsValidationTest_rejectedTest() {
//
//		Token token = new Token();
//		Map<String, String> map = new HashMap<>();
//		map.put("channelId", "test");
//		token.setSeviceParams(map);
//		reqHeaderAtrributes.setToken(token);
//		when(reqHeaderAtrributes.getToken()).thenReturn(token);
//
//		OBTransactionIndividualStatus1Code paymentSetupValidationStatus = null;
//		PaymentConsentsValidationResponse value = new PaymentConsentsValidationResponse();
//		value.setPaymentSetupValidationStatus(paymentSetupValidationStatus.REJECTED);
//
//		Mockito.when(domesticPaymentsRoutingAdapter.preAuthorizeDomesticPayments(anyObject(), anyObject()))
//				.thenReturn(value);
//		CustomDPaymentConsentsPOSTResponse paymentSetupResponse = new CustomDPaymentConsentsPOSTResponse();
//		adapter.preDomesticPaymentsValidation(paymentSetupResponse);
//
//	}
//	
//	
//	@Test
//	public void executeDomesticPaymentsTest()
//	{
//	
//		Token token = new Token();
//		Map<String, String> map = new HashMap<>();
//		map.put("channelId", "test");
//		token.setSeviceParams(map);
//		reqHeaderAtrributes.setToken(token);
//		when(reqHeaderAtrributes.getToken()).thenReturn(token);
//		
//		
//		DomesticPaymentExecutionResource executionResource=new DomesticPaymentExecutionResource();
//		PaymentDomesticSubmitPOST201Response value=new PaymentDomesticSubmitPOST201Response();
//		OBWriteDataDomesticResponse1 data=new OBWriteDataDomesticResponse1();
//		value.setData(data);
//		data.setConsentId("123456");
//		
//		
//		
//		
//		Mockito.when(domesticPaymentsRoutingAdapter.executeDomesticPayments(anyObject(), anyObject())).thenReturn(value);
//		adapter.executeDomesticPayments(executionResource);
//	}
//}
