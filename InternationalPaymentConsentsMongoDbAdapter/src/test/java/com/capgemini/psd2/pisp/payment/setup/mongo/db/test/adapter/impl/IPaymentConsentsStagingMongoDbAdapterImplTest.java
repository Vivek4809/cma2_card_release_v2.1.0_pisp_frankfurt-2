package com.capgemini.psd2.pisp.payment.setup.mongo.db.test.adapter.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExchangeRate1;
import com.capgemini.psd2.pisp.domain.OBExchangeRate2;
import com.capgemini.psd2.pisp.domain.OBExchangeRateType2Code;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsentResponse1;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.domain.ExchangeRateDetails;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl.IPaymentConsentsStagingMongoDbAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.IPaymentConsentsChargesDataRepository;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.IPaymentConsentsExchangeRateRepository;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.IPaymentConsentsFoundationRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.SandboxValidationUtility;
import com.capgemini.psd2.validator.impl.PSD2ValidatorImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class IPaymentConsentsStagingMongoDbAdapterImplTest {

	@Mock
	private IPaymentConsentsFoundationRepository iPaymentConsentsBankRepository;

	@Mock
	private IPaymentConsentsChargesDataRepository iPaymentConsentsChargesRepository;

	@Mock
	private IPaymentConsentsExchangeRateRepository iPaymentConsentsExchangeRateRepository;

	@Mock
	private PSD2ValidatorImpl psd2Validator;

	@Mock
	private SandboxValidationUtility utility;
	
	@Mock
	private RequestHeaderAttributes reqAttributes;

	@InjectMocks
	private IPaymentConsentsStagingMongoDbAdapterImpl mongoAdapter;

	@InjectMocks
	private List<OBCharge1> chargesList = new ArrayList<OBCharge1>();;

	@Mock
	private OBPSD2ExceptionUtility util;

	@Before
	public void setUp() {

		MockitoAnnotations.initMocks(this);

		Map<String, String> map = new HashMap<>();
		map.put("HEADER", "header error message");
		map.put("FREQUENCY", "freq ma");
		map.put("AFTER_CUTOFF_DATE", "freq ma");
		map.put("FIELD", "freq ma");
		map.put("SIGNATURE", "freq ma");
		map.put("INCOMPATIBLE", "freq ma");
		map.put("RES_NOTFOUND", "freq ma");
		map.put("INTERNAL", "freq ma");

		Map<String, String> map1 = new HashMap<>();
		map1.put("signature_missing", "drtyrty");

		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(map1);

	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testcreateStagingInternationalPaymentConsents() {

		CustomIPaymentConsentsPOSTRequest Request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBExternalConsentStatus1Code paymentConsentStatus = null;
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.ACTUAL);
		initiation.setExchangeRateInformation(exchangeRateInformation);
		initiation.setCurrencyOfTransfer("USD");
		OBChargeBearerType1Code chargeBearer = null;
		initiation.setChargeBearer(chargeBearer.SHARED);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("GBP");
		initiation.setInstructedAmount(instructedAmount);
		List<ExchangeRateDetails> exchangelist = new ArrayList<ExchangeRateDetails>();
		ExchangeRateDetails e = new ExchangeRateDetails();
		// e.setCurrencyOfTransfer("GBP");
		e.setCurrencyOfTransfer("USD");
		e.setUnitCurrency("GBP");
		BigDecimal value3 = new BigDecimal(9.99);
		e.setExchangeRate(value3);
		exchangelist.add(e);
		// iPaymentConsentsExchangeRateRepository.findAll();
		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);

		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		Map<String, String> params = new HashMap<String, String>();
		params.put("1", "etc");
		ReflectionTestUtils.setField(mongoAdapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		OBCharge1 charge = new OBCharge1();
		OBCharge1Amount x = new OBCharge1Amount();
		x.setAmount("11.11");
		x.setCurrency("GBP");
		charge.setAmount(x);
		charge.setChargeBearer(OBChargeBearerType1Code.SHARED);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		
		mongoAdapter.processInternationalPaymentConsents(Request, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);

	}

	@Test
	public void testcreateStagewithNULLExchangeInformation() {

		CustomIPaymentConsentsPOSTRequest Request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBChargeBearerType1Code chargeBearer = null;
		initiation.setChargeBearer(chargeBearer.SHARED);
		initiation.setCurrencyOfTransfer("USD");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("GBP");
		initiation.setInstructedAmount(instructedAmount);
		List<ExchangeRateDetails> exchangelist = new ArrayList<ExchangeRateDetails>();
		ExchangeRateDetails e = new ExchangeRateDetails();
		e.setCurrencyOfTransfer("USD");
		e.setUnitCurrency("GBP");
		BigDecimal value3 = new BigDecimal(9.99);
		e.setExchangeRate(value3);
		exchangelist.add(e);
		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);

		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		Map<String, String> params = new HashMap<String, String>();
		params.put("1", "etc");
		

		OBCharge1 charge = new OBCharge1();
		OBCharge1Amount x = new OBCharge1Amount();
		x.setAmount("11.11");
		x.setCurrency("GBP");
		charge.setAmount(x);
		charge.setChargeBearer(OBChargeBearerType1Code.SHARED);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		
		//Mockito.when(utility.getOBCharge(anyString(), anyString())).thenReturn(charge);
		mongoAdapter.processInternationalPaymentConsents(Request, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);
	}

	@Test
	public void testCreateStageWithChargeBearer() {

		CustomIPaymentConsentsPOSTRequest Request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBExternalConsentStatus1Code paymentConsentStatus = null;
		// OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		// exchangeRateInformation.setUnitCurrency("GBP");
		// exchangeRateInformation.setRateType(OBExchangeRateType2Code.ACTUAL);
		// initiation.setExchangeRateInformation(exchangeRateInformation);
		initiation.setCurrencyOfTransfer("USD");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("GBP");
		initiation.setInstructedAmount(instructedAmount);
		OBChargeBearerType1Code chargeBearer = null;
		initiation.setChargeBearer(chargeBearer.SHARED);
		List<ExchangeRateDetails> exchangelist = new ArrayList<ExchangeRateDetails>();
		ExchangeRateDetails e = new ExchangeRateDetails();
		// e.setCurrencyOfTransfer("GBP");
		e.setCurrencyOfTransfer("USD");
		e.setUnitCurrency("GBP");
		BigDecimal value3 = new BigDecimal(9.99);
		e.setExchangeRate(value3);
		exchangelist.add(e);
		// iPaymentConsentsExchangeRateRepository.findAll();
		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);
		List<OBCharge1> chargelist = new ArrayList<>();
		OBCharge1 charge = new OBCharge1();
		charge.setAmount(new OBCharge1Amount());
		charge.getAmount().setCurrency("GBP");
		chargelist.add(charge);
		Mockito.when(iPaymentConsentsChargesRepository.findAll()).thenReturn(chargelist);
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		Map<String, String> params = new HashMap<String, String>();
		params.put("1", "etc");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		
		mongoAdapter.processInternationalPaymentConsents(Request, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);
	}

	@Test
	public void createstage_Test_Agreedtype_RateNotFound() {

		CustomIPaymentConsentsPOSTRequest Request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBChargeBearerType1Code chargeBearer = null;
		initiation.setChargeBearer(chargeBearer.SHARED);

		OBExternalConsentStatus1Code paymentConsentStatus = null;
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		initiation.setExchangeRateInformation(exchangeRateInformation);
		initiation.setCurrencyOfTransfer("USD");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("GBP");
		initiation.setInstructedAmount(instructedAmount);
		List<ExchangeRateDetails> exchangelist = new ArrayList<ExchangeRateDetails>();
		ExchangeRateDetails e = new ExchangeRateDetails();
		e.setCurrencyOfTransfer("GBP");
		e.setUnitCurrency("USD");
		BigDecimal value3 = new BigDecimal(9.99);
		e.setExchangeRate(value3);
		exchangelist.add(e);
		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);

		ReflectionTestUtils.setField(mongoAdapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<String, String>();
		mongoAdapter.processInternationalPaymentConsents(Request, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);

	}

	@Test
	public void createstage_Test_CurencyMismatch() {

		CustomIPaymentConsentsPOSTRequest Request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBExternalConsentStatus1Code paymentConsentStatus = null;
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		ReflectionTestUtils.setField(mongoAdapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		exchangeRateInformation.setUnitCurrency("GBPP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		initiation.setExchangeRateInformation(exchangeRateInformation);
		initiation.setCurrencyOfTransfer("GBP");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("GBbP");
		initiation.setInstructedAmount(instructedAmount);
		
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		
		List<ExchangeRateDetails> exchangelist = new ArrayList<ExchangeRateDetails>();
		ExchangeRateDetails e = new ExchangeRateDetails();
		e.setCurrencyOfTransfer("GBP");

		exchangelist.add(e);
		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<String, String>();
		mongoAdapter.processInternationalPaymentConsents(Request, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);
	}

	@Test
	public void createstage_Test_notIpayment() {

		CustomIPaymentConsentsPOSTRequest Request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBExternalConsentStatus1Code paymentConsentStatus = null;
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		ReflectionTestUtils.setField(mongoAdapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		initiation.setExchangeRateInformation(exchangeRateInformation);
		initiation.setCurrencyOfTransfer("GBP");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("GBP");
		initiation.setInstructedAmount(instructedAmount);
		List<ExchangeRateDetails> exchangelist = new ArrayList<ExchangeRateDetails>();
		ExchangeRateDetails e = new ExchangeRateDetails();
		e.setCurrencyOfTransfer("GBP");

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		
		exchangelist.add(e);
		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);

		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<String, String>();
		mongoAdapter.processInternationalPaymentConsents(Request, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);

	}

	@Test
	public void testRetrieve() {
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		customStageIdentifiers.setPaymentSetupVersion("3.0");
		customStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		Map<String, String> params = new HashMap<>();
		params.put("1", "PSD");

		CustomIPaymentConsentsPOSTResponse value = new CustomIPaymentConsentsPOSTResponse();
		value.setData(new OBWriteDataInternationalConsentResponse1());
		value.getData().setInitiation(new OBInternational1());
		value.getData().getInitiation().setCurrencyOfTransfer("EUR");
		value.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		value.getData().getInitiation().getInstructedAmount().setAmount("1721");
		value.getData().getInitiation().getInstructedAmount().setCurrency("EUR");
		Mockito.when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		Mockito.when(iPaymentConsentsBankRepository.findOneByDataConsentId(anyString())).thenReturn(value);

		mongoAdapter.retrieveStagedInternationalPaymentConsents(customStageIdentifiers, params);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testRetrieveException() {
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		customStageIdentifiers.setPaymentSetupVersion("3.0");
		customStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		Map<String, String> params = new HashMap<>();
		params.put("1", "PSD");

		CustomIPaymentConsentsPOSTResponse value = new CustomIPaymentConsentsPOSTResponse();
		value.setData(new OBWriteDataInternationalConsentResponse1());
		value.getData().setInitiation(new OBInternational1());
		value.getData().getInitiation().setCurrencyOfTransfer("EUR");
		value.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		value.getData().getInitiation().getInstructedAmount().setAmount("1721");
		value.getData().getInitiation().getInstructedAmount().setCurrency("EUR");
		Mockito.when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);
		Mockito.when(iPaymentConsentsBankRepository.findOneByDataConsentId(anyString())).thenReturn(value);

		mongoAdapter.retrieveStagedInternationalPaymentConsents(customStageIdentifiers, params);
		assertEquals("INTERNAL_SERVER_ERROR", "700",value.getData());
	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieve_NUllResource() {
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		customStageIdentifiers.setPaymentSetupVersion("3.0");
		customStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		Map<String, String> params = new HashMap<>();
		params.put("1", "PSD");

		CustomIPaymentConsentsPOSTResponse value = new CustomIPaymentConsentsPOSTResponse();

		Mockito.when(iPaymentConsentsBankRepository.findOneByDataConsentId(anyString())).thenReturn(value);
		mongoAdapter.retrieveStagedInternationalPaymentConsents(customStageIdentifiers, params);
	}

	/*@Test
	public void testUpdateStagingNULLResource() {
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		customStageIdentifiers.setPaymentSetupVersion("3.0");
		customStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		Map<String, String> params = new HashMap<>();
		params.put("1", "PSD");

		CustomIPaymentConsentsPOSTResponse updatedStagedResource = new CustomIPaymentConsentsPOSTResponse();
		updatedStagedResource.setData(new OBWriteDataInternationalConsentResponse1());
		updatedStagedResource.getData().setInitiation(new OBInternational1());
		CustomPaymentStageUpdateData stageUpdateData = new CustomPaymentStageUpdateData();
		OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
		debtorDetails.setIdentification("Identificaion");
		debtorDetails.setName("Debitor ");
		debtorDetails.setSchemeName("sn");
		debtorDetails.setSecondaryIdentification("secondaryIdentification");
		stageUpdateData.setDebtorDetails(debtorDetails);
		stageUpdateData.setDebtorDetailsUpdated(true);
		stageUpdateData.setFraudScoreUpdated(true);
		stageUpdateData.setSetupStatusUpdated(true);
		Mockito.when(iPaymentConsentsBankRepository.findOneByDataConsentId(anyString()))
				.thenReturn(updatedStagedResource);
		mongoAdapter.updateStagedIPaymentConsents(customStageIdentifiers, stageUpdateData, params);
	}*/

	@Test(expected = NullPointerException.class)
	public void createStagingInternationalPaymentConsentsCOTNullTest() {

		CustomIPaymentConsentsPOSTRequest Request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBExternalConsentStatus1Code paymentConsentStatus = null;
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		initiation.setExchangeRateInformation(exchangeRateInformation);
		// initiation.setCurrencyOfTransfer("USD");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("GBP");
		initiation.setInstructedAmount(instructedAmount);
		List<ExchangeRateDetails> exchangelist = new ArrayList<ExchangeRateDetails>();
		ExchangeRateDetails e = new ExchangeRateDetails();
		e.setCurrencyOfTransfer("USD");
		e.setUnitCurrency("GBP");
		BigDecimal value3 = new BigDecimal(9.99);
		e.setExchangeRate(value3);
		exchangelist.add(e);
		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);
		ReflectionTestUtils.setField(mongoAdapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");

		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		Map<String, String> params = new HashMap<String, String>();
		params.put("1", "etc");
		mongoAdapter.processInternationalPaymentConsents(Request, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);

	}

	@Test(expected = NullPointerException.class)
	public void createStagingInternationalPaymentConsentsTest() {

		CustomIPaymentConsentsPOSTRequest Request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.ACTUAL);
		initiation.setExchangeRateInformation(exchangeRateInformation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("GBP");
		initiation.setInstructedAmount(instructedAmount);
		List<ExchangeRateDetails> exchangelist = new ArrayList<ExchangeRateDetails>();
		ReflectionTestUtils.setField(mongoAdapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		ExchangeRateDetails e = new ExchangeRateDetails();
		e.setCurrencyOfTransfer("GBP");
		e.setCurrencyOfTransfer("USD");
		e.setUnitCurrency("GBP");
		BigDecimal value3 = new BigDecimal(9.99);
		e.setExchangeRate(value3);
		exchangelist.add(e);
		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);

		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		Map<String, String> params = new HashMap<String, String>();
		params.put("1", "etc");
		mongoAdapter.processInternationalPaymentConsents(Request, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);

	}

	@Test(expected = NullPointerException.class)
	public void createStagingInternationalPaymentConsentsUnitCurrencyNullTest() {

		CustomIPaymentConsentsPOSTRequest Request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		initiation.setCurrencyOfTransfer("GBP");
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.ACTUAL);
		initiation.setExchangeRateInformation(exchangeRateInformation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("GBP");
		initiation.setInstructedAmount(instructedAmount);
		data.setInitiation(initiation);
		List<ExchangeRateDetails> exchangelist = new ArrayList<ExchangeRateDetails>();
		ReflectionTestUtils.setField(mongoAdapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		ExchangeRateDetails e = new ExchangeRateDetails();
		e.setCurrencyOfTransfer("GBP");
		e.setCurrencyOfTransfer("USD");
		e.setUnitCurrency("GBP");
		BigDecimal value3 = new BigDecimal(9.99);
		e.setExchangeRate(value3);
		exchangelist.add(e);
		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);

		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		Map<String, String> params = new HashMap<String, String>();
		params.put("1", "etc");
		mongoAdapter.processInternationalPaymentConsents(Request, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);

	}

	@Test(expected = NullPointerException.class)
	public void createStagingInternationalPaymentConsentsExchangeNotFoundTest() {

		CustomIPaymentConsentsPOSTRequest Request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		initiation.setCurrencyOfTransfer("GBP");
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.ACTUAL);
		initiation.setExchangeRateInformation(exchangeRateInformation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("GBP");
		initiation.setInstructedAmount(instructedAmount);
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		ReflectionTestUtils.setField(mongoAdapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("1", "etc");
		mongoAdapter.processInternationalPaymentConsents(Request, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);
	}

	@Test(expected = NullPointerException.class)
	public void createStagingInternationalPaymentConsentsExchangeRateEmptyTest() {

		CustomIPaymentConsentsPOSTRequest Request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		initiation.setCurrencyOfTransfer("GBP");
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.ACTUAL);
		initiation.setExchangeRateInformation(exchangeRateInformation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("GBP");
		initiation.setInstructedAmount(instructedAmount);
		data.setInitiation(initiation);
		List<ExchangeRateDetails> exchangelist = new ArrayList<ExchangeRateDetails>();
		ExchangeRateDetails e = new ExchangeRateDetails();
		exchangelist.add(e);
		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);
		ReflectionTestUtils.setField(mongoAdapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");

		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		Map<String, String> params = new HashMap<String, String>();
		params.put("1", "etc");
		mongoAdapter.processInternationalPaymentConsents(Request, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);

	}

	@Test
	public void createStageExchangeRateNotFoundInformation() {

		CustomIPaymentConsentsPOSTRequest Request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("INR");
		ReflectionTestUtils.setField(mongoAdapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		initiation.setExchangeRateInformation(exchangeRateInformation);
		initiation.setCurrencyOfTransfer("GBP");
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("GBP");
		initiation.setInstructedAmount(instructedAmount);
		List<ExchangeRateDetails> exchangelist = new ArrayList<ExchangeRateDetails>();
		ExchangeRateDetails e = new ExchangeRateDetails();
		e.setCurrencyOfTransfer("USD");
		e.setUnitCurrency("GBP");
		BigDecimal value3 = new BigDecimal(9.99);
		e.setExchangeRate(value3);
		exchangelist.add(e);
		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);
		
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		

		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		Map<String, String> params = new HashMap<String, String>();
		params.put("1", "etc");
		mongoAdapter.processInternationalPaymentConsents(Request, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);

	}

	@Test
	public void createStageExchangeRateNotFoundExchangeRateInReqNullTest() {

		CustomIPaymentConsentsPOSTRequest Request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		OBExternalConsentStatus1Code paymentConsentStatus = null;
		initiation.setCurrencyOfTransfer("GBP");
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("GBP");
		initiation.setInstructedAmount(instructedAmount);
		List<ExchangeRateDetails> exchangelist = new ArrayList<ExchangeRateDetails>();
		ExchangeRateDetails e = new ExchangeRateDetails();
		e.setCurrencyOfTransfer("USD");
		e.setUnitCurrency("GBP");
		BigDecimal value3 = new BigDecimal(9.99);
		e.setExchangeRate(value3);
		exchangelist.add(e);
		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		

		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		Map<String, String> params = new HashMap<String, String>();
		params.put("1", "etc");
		mongoAdapter.processInternationalPaymentConsents(Request, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);

	}

	@Test(expected = PSD2Exception.class)
	public void testNullResponseRetrieve() {
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		customStageIdentifiers.setPaymentSetupVersion("3.0");
		customStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		Map<String, String> params = new HashMap<>();
		params.put("1", "PSD");

		mongoAdapter.retrieveStagedInternationalPaymentConsents(customStageIdentifiers, params);
	}

	@Test(expected = PSD2Exception.class)
	public void retrieveDataNullTest() {
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		customStageIdentifiers.setPaymentSetupVersion("3.0");
		customStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		Map<String, String> params = new HashMap<>();
		params.put("1", "PSD");

		CustomIPaymentConsentsPOSTResponse value = new CustomIPaymentConsentsPOSTResponse();
		Mockito.when(iPaymentConsentsBankRepository.findOneByDataConsentId(anyString())).thenReturn(value);

		mongoAdapter.retrieveStagedInternationalPaymentConsents(customStageIdentifiers, params);
	}

	@Test(expected = PSD2Exception.class)
	public void retrieveInitiationNullTest() {
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		customStageIdentifiers.setPaymentSetupVersion("3.0");
		customStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		Map<String, String> params = new HashMap<>();
		params.put("1", "PSD");

		CustomIPaymentConsentsPOSTResponse value = new CustomIPaymentConsentsPOSTResponse();
		// OBInternational1 initiation=new OBInternational1();
		// OBWriteDataInternationalConsentResponse1 new
		// OBWriteDataInternationalConsentResponse1();
		value.setData(new OBWriteDataInternationalConsentResponse1());
		Mockito.when(iPaymentConsentsBankRepository.findOneByDataConsentId(anyString())).thenReturn(value);

		mongoAdapter.retrieveStagedInternationalPaymentConsents(customStageIdentifiers, params);
	}

	@Test
	public void chargesListNullTest() {

		CustomIPaymentConsentsPOSTRequest Request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.ACTUAL);
		initiation.setExchangeRateInformation(exchangeRateInformation);
		initiation.setCurrencyOfTransfer("USD");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("GBP");
		initiation.setInstructedAmount(instructedAmount);
		List<ExchangeRateDetails> exchangelist = new ArrayList<ExchangeRateDetails>();
		ExchangeRateDetails e = new ExchangeRateDetails();
		e.setCurrencyOfTransfer("USD");
		e.setUnitCurrency("GBP");
		BigDecimal value3 = new BigDecimal(9.99);
		e.setExchangeRate(value3);
		exchangelist.add(e);
		ReflectionTestUtils.setField(mongoAdapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		
		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);
		Mockito.when(iPaymentConsentsChargesRepository.findAll()).thenReturn(chargesList);
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		Map<String, String> params = new HashMap<String, String>();
		params.put("1", "etc");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		
		mongoAdapter.processInternationalPaymentConsents(Request, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);
	}

	@Test
	public void chargeBearerNullTest() {

		CustomIPaymentConsentsPOSTRequest Request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		ReflectionTestUtils.setField(mongoAdapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		initiation.setExchangeRateInformation(exchangeRateInformation);
		initiation.setCurrencyOfTransfer("USD");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("GBP");
		initiation.setInstructedAmount(instructedAmount);
		List<ExchangeRateDetails> exchangelist = new ArrayList<ExchangeRateDetails>();
		ExchangeRateDetails e = new ExchangeRateDetails();
		e.setCurrencyOfTransfer("USD");
		e.setUnitCurrency("GBP");
		BigDecimal value3 = new BigDecimal(9.99);
		e.setExchangeRate(value3);
		exchangelist.add(e);
		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);
		OBCharge1 charge = new OBCharge1();
		charge.setAmount(new OBCharge1Amount());
		charge.getAmount().setCurrency("GBP");
		chargesList.add(charge);
		Mockito.when(iPaymentConsentsChargesRepository.findAll()).thenReturn(chargesList);
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		Map<String, String> params = new HashMap<String, String>();
		params.put("1", "etc");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		
		mongoAdapter.processInternationalPaymentConsents(Request, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);
	}

	@Test 
	public void chargesDoNotMatchTest() {

		CustomIPaymentConsentsPOSTRequest Request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		initiation.setExchangeRateInformation(exchangeRateInformation);
		initiation.setCurrencyOfTransfer("USD");
		ReflectionTestUtils.setField(mongoAdapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("GBP");
		initiation.setInstructedAmount(instructedAmount);
		List<ExchangeRateDetails> exchangelist = new ArrayList<ExchangeRateDetails>();
		ExchangeRateDetails e = new ExchangeRateDetails();
		e.setCurrencyOfTransfer("USD");
		e.setUnitCurrency("GBP");
		BigDecimal value3 = new BigDecimal(9.99);
		e.setExchangeRate(value3);
		exchangelist.add(e);
		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);
		OBCharge1 charge = new OBCharge1();
		charge.setAmount(new OBCharge1Amount());
		charge.getAmount().setCurrency("ABC");
		chargesList.add(charge);
		Mockito.when(iPaymentConsentsChargesRepository.findAll()).thenReturn(chargesList);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		Map<String, String> params = new HashMap<String, String>();
		params.put("1", "etc");
		//ReflectionTestUtils.setField(mongoAdapter, "chargesList", chargesList);
		mongoAdapter.processInternationalPaymentConsents(Request, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);
	}

	@Test
	public void createstage_Test_Agreedtype() {

		CustomIPaymentConsentsPOSTRequest Request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBChargeBearerType1Code chargeBearer = null;
		initiation.setChargeBearer(chargeBearer.SHARED);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		OBExternalConsentStatus1Code paymentConsentStatus = null;
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		ReflectionTestUtils.setField(mongoAdapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		initiation.setExchangeRateInformation(exchangeRateInformation);
		initiation.setCurrencyOfTransfer("USD");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("USD");
		initiation.setInstructedAmount(instructedAmount);
		List<ExchangeRateDetails> exchangelist = new ArrayList<ExchangeRateDetails>();
		ExchangeRateDetails e = new ExchangeRateDetails();
		e.setCurrencyOfTransfer("USD");
		e.setUnitCurrency("GBP");
		BigDecimal value3 = new BigDecimal(9.99);
		e.setExchangeRate(value3);
		exchangelist.add(e);
		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);

		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);

		ReflectionTestUtils.setField(mongoAdapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<String, String>();
		OBCharge1 charge = new OBCharge1();
		OBCharge1Amount x = new OBCharge1Amount();
		x.setAmount("11.11");
		x.setCurrency("USD");
		charge.setAmount(x);
		charge.setChargeBearer(OBChargeBearerType1Code.SHARED);
		mongoAdapter.processInternationalPaymentConsents(Request, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);

	}

	@Test
	public void createstage_Test_CIExRate() {

		CustomIPaymentConsentsPOSTRequest Request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBChargeBearerType1Code chargeBearer = null;
		initiation.setChargeBearer(chargeBearer.SHARED);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		
		ReflectionTestUtils.setField(mongoAdapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		OBExternalConsentStatus1Code paymentConsentStatus = null;
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("Test");
		initiation.setExchangeRateInformation(exchangeRateInformation);
		initiation.setCurrencyOfTransfer("USD");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("USD");
		initiation.setInstructedAmount(instructedAmount);
		List<ExchangeRateDetails> exchangelist = new ArrayList<ExchangeRateDetails>();
		ExchangeRateDetails e = new ExchangeRateDetails();
		e.setCurrencyOfTransfer("USD");
		e.setUnitCurrency("GBP");
		e.setContractIdentification("Test");
		BigDecimal value3 = new BigDecimal(9.99);
		e.setExchangeRate(value3);
		exchangelist.add(e);
		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);

		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);

		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<String, String>();
		OBCharge1 charge = new OBCharge1();
		OBCharge1Amount x = new OBCharge1Amount();
		x.setAmount("11.11");
		x.setCurrency("USD");
		charge.setAmount(x);
		charge.setChargeBearer(OBChargeBearerType1Code.SHARED);
		//Mockito.when(utility.getOBCharge(anyString(), anyString())).thenReturn(charge);
		mongoAdapter.processInternationalPaymentConsents(Request, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);

	}
	
	@Test
	public void createstage_Test_CIExRate_NonMatching() {

		CustomIPaymentConsentsPOSTRequest Request = new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		Request.setData(data);
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBChargeBearerType1Code chargeBearer = null;
		initiation.setChargeBearer(chargeBearer.SHARED);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSecondaryIdentification("Test");
		initiation.setCreditorAccount(creditorAccount);
		
		ReflectionTestUtils.setField(mongoAdapter, "contractIdentificationPattern", "^Test[a-zA-Z0-9]*");
		OBExternalConsentStatus1Code paymentConsentStatus = null;
		OBExchangeRate1 exchangeRateInformation = new OBExchangeRate1();
		exchangeRateInformation.setUnitCurrency("GBP");
		exchangeRateInformation.setRateType(OBExchangeRateType2Code.AGREED);
		exchangeRateInformation.setContractIdentification("NonMatchingTest");
		initiation.setExchangeRateInformation(exchangeRateInformation);
		initiation.setCurrencyOfTransfer("USD");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("165.00");
		instructedAmount.setCurrency("USD");
		initiation.setInstructedAmount(instructedAmount);
		List<ExchangeRateDetails> exchangelist = new ArrayList<ExchangeRateDetails>();
		ExchangeRateDetails e = new ExchangeRateDetails();
		e.setCurrencyOfTransfer("USD");
		e.setUnitCurrency("GBP");
		e.setContractIdentification("Test");
		BigDecimal value3 = new BigDecimal(9.99);
		e.setExchangeRate(value3);
		exchangelist.add(e);
		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);

		Mockito.when(iPaymentConsentsExchangeRateRepository.findAll()).thenReturn(exchangelist);

		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<String, String>();
		OBCharge1 charge = new OBCharge1();
		OBCharge1Amount x = new OBCharge1Amount();
		x.setAmount("11.11");
		x.setCurrency("USD");
		charge.setAmount(x);
		charge.setChargeBearer(OBChargeBearerType1Code.SHARED);
		//Mockito.when(utility.getOBCharge(anyString(), anyString())).thenReturn(charge);
		mongoAdapter.processInternationalPaymentConsents(Request, customStageIdentifiers, params,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);

	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieve_invalidAmount() {
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");

		CustomIPaymentConsentsPOSTResponse value = (CustomIPaymentConsentsPOSTResponse) new CustomIPaymentConsentsPOSTResponse()
				.data(new OBWriteDataInternationalConsentResponse1()
						.initiation(new OBInternational1()
								.instructedAmount(new OBDomestic1InstructedAmount()
										.amount("100.00"))));

		Mockito.when(iPaymentConsentsBankRepository.findOneByDataConsentId(anyString())).thenReturn(value);
		Mockito.when(reqAttributes.getMethodType()).thenReturn("GET");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(Boolean.TRUE);
		mongoAdapter.retrieveStagedInternationalPaymentConsents(customStageIdentifiers, null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testRetrieve_DataAccessResourceFailureException () {
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentConsentId("1234");
		
		Mockito.when(iPaymentConsentsBankRepository.findOneByDataConsentId(anyString())).thenThrow(DataAccessResourceFailureException.class);
		mongoAdapter.retrieveStagedInternationalPaymentConsents(customStageIdentifiers, null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testcreateStagingInternationalPaymentConsents_invalidAmount() {
		CustomIPaymentConsentsPOSTResponse paymentConsentsBankResource = new CustomIPaymentConsentsPOSTResponse();
		CustomIPaymentConsentsPOSTRequest request = (CustomIPaymentConsentsPOSTRequest) new CustomIPaymentConsentsPOSTRequest()
				.data(new OBWriteDataInternationalConsent1()
						.initiation(new OBInternational1()
								.instructedAmount(new OBDomestic1InstructedAmount()
										.currency("EUR")
										.amount("100.00"))));
		
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(Boolean.TRUE);
		
		mongoAdapter.processInternationalPaymentConsents(request, null, null,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testcreateStagingInternationalPaymentConsents_inValidSecIdentification() {
		CustomIPaymentConsentsPOSTResponse paymentConsentsBankResource = new CustomIPaymentConsentsPOSTResponse();
		CustomIPaymentConsentsPOSTRequest request = (CustomIPaymentConsentsPOSTRequest) new CustomIPaymentConsentsPOSTRequest()
				.data(new OBWriteDataInternationalConsent1()
						.initiation(new OBInternational1()
								.instructedAmount(new OBDomestic1InstructedAmount()
										.currency("EUR")
										.amount("100.00"))
								.creditorAccount(new OBCashAccountCreditor2()
										.secondaryIdentification("identification"))));
		
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(Boolean.FALSE);
		when(utility.isValidSecIdentification(anyString())).thenReturn(Boolean.TRUE);
		
		mongoAdapter.processInternationalPaymentConsents(request, null, null,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testcreateStagingInternationalPaymentConsents_DataAccessResourceFailureException() {
		CustomIPaymentConsentsPOSTResponse paymentConsentsBankResource = new CustomIPaymentConsentsPOSTResponse();
		CustomIPaymentConsentsPOSTRequest request = (CustomIPaymentConsentsPOSTRequest) new CustomIPaymentConsentsPOSTRequest()
				.data(new OBWriteDataInternationalConsent1()
						.initiation(new OBInternational1()
								.instructedAmount(new OBDomestic1InstructedAmount()
										.currency("EUR")
										.amount("100.00"))
								.creditorAccount(new OBCashAccountCreditor2()
										.secondaryIdentification("identification"))
								.chargeBearer(OBChargeBearerType1Code.BORNEBYCREDITOR)));
		
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(Boolean.FALSE);
		when(utility.isValidSecIdentification(anyString())).thenReturn(Boolean.FALSE);
		when(utility.getMockedOBChargeList(anyString(), anyString(), anyObject())).thenReturn(new ArrayList<OBCharge1>());
		when(utility.getMockedExchangeRateInformation(anyObject(), anyString())).thenReturn(new OBExchangeRate2());
		when(iPaymentConsentsBankRepository.save(any(CustomIPaymentConsentsPOSTResponse.class))).thenThrow(DataAccessResourceFailureException.class);
		
		mongoAdapter.processInternationalPaymentConsents(request, null, null,
				OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);
	}
	
}
