package com.capgemini.psd2.constants;

public class LdapConstants {

	public static final String CN = "cn";
	public static final String DN = "DN";
	public static final String OU = "ou";
	public static final String UID = "uid";
	public static final String DC = "dc";
	public static final String IS_MEMBER_OF = "isMemberOf";
	public static final String UNIQUE_MEMBER = "uniqueMember";
	public static final String OBJECT_CLASS = "objectClass";

	public static final String LEGAL_ENTITY_NAME = "o";
	public static final String CORRELATION_URL_PARAM = "correlationId";

	public static final String GIVEN_NAME = "givename";
	public static final String SN = "sn";

	public static final String X_BLACKLIST = "x-blacklist";
	public static final String X_CERT = "x-cert";
	public static final String X_BLOCK = "x-block";

	public static final String X_CERT_SSID = "x-cert-ssid";
	public static final String X_CREATEDDATE = "x-createddate";
	public static final String X_GRANTS = "x-grants";
	public static final String X_SSID = "x-ssid";
	public static final String X_STATUS = "x-status";

	public static final String X_COMPETENT_AUTH = "x-competentAuth";
	public static final String X_DOMAIN_NAMES = "x-domainNames";
	public static final String X_SOURCE = "x-source";
	public static final String X_REDIRECT_URL = "x-redirecturl";

	public static final String X_ROLES = "x-roles";
	public static final String X_SECRET = "x-secret";
	public static final String X_SCOPES = "x-scopes";

	public static final String X_CLIENT_DECSRIPTION = "x-client-description";

	public static final String X_CLIENT_NAME = "x-client-name";
	public static final String X_CLIENT_URI = "x-client-uri";

	public static final String X_CLIENT_ID = "x-clientid";

	public static final String X_ENVIRONMENT = "x-environment";
	public static final String X_MODE = "x-mode";

	public static final String X_ORG_COMPETENT_AUTH_ID = "x-org-competent-auth-id";
	public static final String X_ORG_ID = "x-org-id";
	public static final String X_ORG_JWKS_ENDPOINT = "x-org-jwks-endpoint";
	public static final String X_ORG_JWKS_REVOKED_ENDPOINT = "x-org-jwks-revoked-endpoint";
	public static final String X_ORG_NAME = "x-org-name";
	public static final String X_ORG_STATUS = "x-org-status";

	public static final String CLIENT_ATTRIBUTES = "clientAttributes";

	public static final String SOFTWARE_VERSION = "softwareVersion";

	public static final String CLIENT_ID = "software_client_id";
	public static final String POLICY_URI = "software_policy_uri";
	public static final String REDIRECT_URIS = "software_redirect_uris";
	public static final String JWKS_ENDPOINT = "software_jwks_endpoint";
	public static final String CLIENT_DECSRIPTION = "software_client_description";
	public static final String CLIENT_NAME = "software_client_name";
	public static final String STATUS = "status";
	public static final String ROLES = "software_roles";
	public static final String ORG_NAME = "org_name";
	public static final String CLAIMS = "organisation_competent_authority_claims";
	public static final String XBLOCK = "x-block";
	public static final String SSA_TOKEN = "ssaToken";
	public static final String APP_ID = "appID";
	public static final String CREATE_TIMESTAMP = "createTimestamp";

	private LdapConstants() {
	}

}
