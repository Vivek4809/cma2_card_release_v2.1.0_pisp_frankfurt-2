package com.capgemini.psd2.exception;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorInfo;
@RunWith(SpringJUnit4ClassRunner.class)
public class ClientRegistrationExceptionTest {

	@InjectMocks
	ClientRegistrationException clientRegistrationException;
	@Before
	public void setUp() throws Exception {
		
		ErrorInfo errorInfo = new ErrorInfo("500");
		errorInfo.setErrorCode("110");
		errorInfo.setErrorMessage("Consent exists");
		clientRegistrationException =  new ClientRegistrationException("Consent already exists", errorInfo );
	}
	@Test
	public void testPopulatePortalException() {
		assertNotNull(ClientRegistrationException.populatePortalException(ErrorCodeEnum.CONSENT_EXISTS));
		assertNotNull(ClientRegistrationException.populatePortalException("Consent already exists",
				ErrorCodeEnum.CONSENT_EXISTS));
		assertNotNull(ClientRegistrationException.populatePortalException("Technical Error",
				ClientRegistrationErrorCodeEnum.TECHNICAL_ERROR));
		assertNotNull(ClientRegistrationException
				.populatePortalException(ClientRegistrationErrorCodeEnum.TECHNICAL_ERROR, "Technical Error"));
		assertNotNull(
				ClientRegistrationException.populatePortalException(ClientRegistrationErrorCodeEnum.TECHNICAL_ERROR));
	}
	
}
