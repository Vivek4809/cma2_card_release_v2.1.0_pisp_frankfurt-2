package com.capgemini.psd2.revoke.consent.service.test.impl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.product.db.resources.locator.MultiTenancyRequestBean;
import com.capgemini.psd2.revoke.consent.routing.impl.RevokeConsentRoutingAdapter;
import com.capgemini.psd2.revoke.consent.service.impl.RevokeConsentServiceImpl;
import com.capgemini.psd2.revoke.consent.validator.RevokeConsentValidatorImpl;

public class RevokeConsentServiceImplTest {

	@InjectMocks
	RevokeConsentServiceImpl impl;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Mock
	private RevokeConsentValidatorImpl revokeConsentValidator;

	@Mock
	private MultiTenancyRequestBean multiTenantReqBean;

	@Mock
	private RevokeConsentRoutingAdapter revokeConsentAdapter;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Test
	public void removeConsentRequestTest(){
		impl.removeConsentRequest("123445", "BOIUK");
	}
}
