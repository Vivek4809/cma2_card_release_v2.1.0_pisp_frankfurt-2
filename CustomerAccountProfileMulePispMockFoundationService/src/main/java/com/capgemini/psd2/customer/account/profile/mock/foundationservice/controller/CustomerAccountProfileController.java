package com.capgemini.psd2.customer.account.profile.mock.foundationservice.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.customer.account.profile.mock.foundationservice.domain.PISP.DigitalUserProfile;
import com.capgemini.psd2.customer.account.profile.mock.foundationservice.service.CustomerAccountInformationService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;

@RestController
@RequestMapping("core-banking/p/party")
@ConfigurationProperties(prefix = "mockFoundationService")
@EnableAutoConfiguration
public class CustomerAccountProfileController {
	@Autowired
	CustomerAccountInformationService customerAccountInformationService;
	/** The adapterUtility. */
	@Autowired
	private ValidationUtility validatorUtility;

	private List<String> blockedUserList=new ArrayList<String>();
	
	@RequestMapping(value = "/{version}/party-positions/profile-accounts-balances/channels/{channel-Code}", method = RequestMethod.GET, produces = "application/json")
	public  DigitalUserProfile channelAReteriveCustomerAccountInformation(
			@PathVariable("channel-Code") String channelid,
			@RequestParam(value = "digitalUserId", required = false) String digitalUserId,
			@RequestHeader(required = false,value="X-API-SOURCE-USER") String boiUser,
			@RequestHeader(required = false,value="X-API-TRANSACTION-ID") String transactionID,
			@RequestHeader(required = false,value="X-API-SOURCE-SYSTEM") String boiPlatform,
			@RequestHeader(required = false,value="X-API-CORRELATION-ID") String correlationID) throws Exception
	{
		
		validatorUtility.validateErrorCodeForConsent(digitalUserId);
		validatorUtility.validateErrorCode(correlationID);
		
		if(boiUser==null || boiPlatform==null || correlationID==null ){
			throw MockFoundationServiceException
			.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPR_DUP);
		}			
		if(digitalUserId==null)
		{
			throw MockFoundationServiceException
			.populateMockFoundationServiceException(ErrorCodeEnum.NO_USER_FOUND_PPR_DUP);
		}
		if(blockedUserList.contains(digitalUserId))
		{
			throw MockFoundationServiceException
			.populateMockFoundationServiceException(ErrorCodeEnum.NO_USER_FOUND_PPR_DUP);
		}
		DigitalUserProfile digiUserProfile = customerAccountInformationService.retrieveCustomerAccountInformation(digitalUserId);
		return digiUserProfile;
		
	}
}
