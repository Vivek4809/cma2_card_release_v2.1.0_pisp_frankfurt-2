package com.capgemini.psd2.account.statements.service;

import com.capgemini.psd2.aisp.domain.OBReadStatement1;
import com.capgemini.psd2.aisp.domain.OBReadTransaction3;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSatementsFileResponse;
public interface AccountStatementsService 
{
	/**
	 * Retrieve account standing order.
	 *
	 * @param accountId the account id
	 * @return the StandingOrdersGETResponse
	 */
	public OBReadStatement1 retrieveAccountStatements(String accountId, String paramFromDate, String paramToDate, 
			Integer paramPageNumber, Integer paramPageSize, String txnKey);

	public OBReadStatement1 retrieveAccountStatementsByStatementsId(String accountId, String statementId, 
			Integer paramPageNumber, Integer paramPageSize, String txnKey);
	
	public OBReadTransaction3 retrieveAccountTransactionsByStatementsId(String accountId, String statementId, Integer paramPageNumber, Integer paramPageSize, String txnKey);
	
	public PlatformAccountSatementsFileResponse downloadStatementFileByStatementsId(String accountId, String statementId);

}
