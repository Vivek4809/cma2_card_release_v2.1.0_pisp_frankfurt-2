package com.capgemini.psd2.account.statements.mongo.db.adapter.test.impl;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.capgemini.psd2.account.statements.mongo.db.adapter.constants.AccountStatementMongoDbAdapterConstants;
import com.capgemini.psd2.account.statements.mongo.db.adapter.domain.DateRange;
import com.capgemini.psd2.account.statements.mongo.db.adapter.impl.AccountStatementsMongoDbAdapterImpl;
import com.capgemini.psd2.account.statements.mongo.db.adapter.repository.AccountStatementFileRepository;
import com.capgemini.psd2.account.statements.mongo.db.adapter.repository.AccountStatementTransactionRepository;
import com.capgemini.psd2.account.statements.mongo.db.adapter.repository.AccountStatementsRepository;
import com.capgemini.psd2.account.statements.mongo.db.adapter.test.mock.data.AccountStatementTestData;
import com.capgemini.psd2.account.statements.mongo.db.adapter.test.mock.data.AccountStatementTransactionTestData;
import com.capgemini.psd2.account.statements.mongo.db.adapter.utility.AccountStatementsMongoDbAdapterUtility;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSatementsFileResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.mongo.db.config.AccountAwsObject;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.SandboxValidationConfig;
import com.capgemini.psd2.utilities.SandboxValidationUtility;
import com.capgemini.psd2.validator.PSD2Validator;
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStatementMongoDBAdapterImplTest {

	@Mock
	private AccountStatementsRepository accountStatementsRepository;

	@Mock
	private AccountStatementTransactionRepository transactionRepository;

	@Mock
	private AccountStatementFileRepository fileRepository;

	@Mock
	private AccountStatementsMongoDbAdapterUtility utility;

	@Mock
	PSD2Validator psd2Validator;
	
	@Mock
	public AccountAwsObject s3objectmock;


	@Mock
	private BasicAWSCredentials awsCredentials;
	
	@Mock
	public AmazonS3 s3cleint;
	
	@InjectMocks
	private AccountStatementsMongoDbAdapterImpl dbAdapterImpl;
	

	@Mock
	private SandboxValidationConfig sandboxValidationConfig;
	
	@Mock
	private SandboxValidationUtility sandutility;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	
	
	@Test
	public void testDownloadStatementFileByStatementsIdSuccessFlow() {
	
		when(accountStatementsRepository.findByAccountNumberAndAccountNSCAndStatementId(anyString(), anyString(),
				anyString(), any(Pageable.class))).thenReturn(AccountStatementTestData.getAccountStatementsDataPage());

		Map<String, String> params = new HashMap<>();
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_STATEMENT, "333333");
		ReflectionTestUtils.setField(dbAdapterImpl, "awsRegion", "eu-west-1");
		ReflectionTestUtils.setField(dbAdapterImpl, "awsS3BucketName", "file-payment-consents-bucket");
		ReflectionTestUtils.setField(dbAdapterImpl, "awsReportfileName", "local.pdf");	
	try {
		Resource resource = new ClassPathResource(AccountStatementMongoDbAdapterConstants.LOCAL_FILE);
		File file = resource.getFile();
		InputStream is = new FileInputStream(file);
		ObjectMetadata objMeta = mock(ObjectMetadata.class);		
		
		S3Object fullObject = new S3Object();
		fullObject.setKey("local.pdf");
		fullObject.setBucketName("file-payment-consents-bucket");
		fullObject.setObjectContent(is);
		fullObject.setObjectMetadata(objMeta);
		
		when(s3objectmock.getAccountAwsClient(any(BasicAWSCredentials.class), anyString()))
		.thenReturn(s3cleint);

		when(s3objectmock.getAccountAwsObjectS3Object(any(AmazonS3Client.class), anyString(), anyString())).thenReturn(fullObject);
	
		PlatformAccountSatementsFileResponse accountSatementsFileResponse = dbAdapterImpl
				.downloadStatementFileByStatementsId(AccountStatementTestData.getMockAccountMapping(), params);
		assertEquals("local.pdf", accountSatementsFileResponse.getFileName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		
	}

	
	@Test(expected = PSD2Exception.class)
	public void testDownloadStatementFileByStatementsIdExceptionFlow() {
		
		Mockito.when(accountStatementsRepository.findByAccountNumberAndAccountNSCAndStatementId(
				anyString(), anyString(), anyString(), any(Pageable.class)))
		.thenThrow(new DataAccessResourceFailureException("Test"));

		Map<String, String> params = new HashMap<>();
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_STATEMENT, "333333");

		try {
			dbAdapterImpl.downloadStatementFileByStatementsId(AccountStatementTestData.getMockAccountMapping(), params);
		/*}catch (PSD2Exception e) {
			assertEquals("Error while establishing connection,please try again..", e.getErrorInfo().getErrorMessage());
			throw e;
		}*/
		} catch (PSD2Exception e) {
			assertEquals("Technical error while establishing data base connection", e.getOBErrorResponse1().getErrors().get(0).getMessage());		
			throw e;
		}
				
	}
	
	
	@Test(expected = PSD2Exception.class)
	public void testDownloadStatementFileByStatementsIdExceptionFlow1() {
		
		Mockito.when(sandutility.isValidPsuIdForDownload(anyObject())).thenReturn(true);
		Map<String, String> params = new HashMap<>();
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_STATEMENT, "333333");

		
			dbAdapterImpl.downloadStatementFileByStatementsId(AccountStatementTestData.getMockAccountMapping(), params);
	
				
	}
	
	@Test(expected = PSD2Exception.class)
	public void testDownloadStatementFileByStatementsIdExceptionFlow2() {
		
		Mockito.when(sandutility.isValidPsuId(anyObject())).thenReturn(true);
		Map<String, String> params = new HashMap<>();
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_STATEMENT, "333333");

		
			dbAdapterImpl.downloadStatementFileByStatementsId(AccountStatementTestData.getMockAccountMapping(), params);
	
				
	}

	
	
	@Test
	public void testRetrieveAccountStatementsSuccessFlow() {
		when(accountStatementsRepository.findByAccountNumberAndAccountNSCAndStartDateTimeCopyBetween(
				anyString(), anyString(), any(Instant.class), any(Instant.class), any(Pageable.class)))
		.thenReturn(AccountStatementTestData.getAccountStatementsDataPage());

		Map<String, String> params = new HashMap<>();
		params.put("RequestedPageNumber", "1");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2015-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2017-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2015-01-01T00:00:00" );
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2018-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2020-01-01T00:00:00");
		params.put("RequestedPageSize", "10");
		LocalDateTime localDate = LocalDateTime.now();
		LocalDateTime localDate2 = LocalDateTime.of(2015, Month.MAY, 15, 10, 0);
		DateRange transactionDateRange = new DateRange();
		transactionDateRange.setNewFilterFromDate(localDate2);
		transactionDateRange.setNewFilterToDate(localDate);
		
		when(sandutility.isValidPsuId("32118465")).thenReturn(Boolean.FALSE);

		when(utility.fsCallFilter(null)).thenReturn(transactionDateRange);
		PlatformAccountStatementsResponse response = dbAdapterImpl
				.retrieveAccountStatements(AccountStatementTestData.getMockAccountMapping(), params);

		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(response);
	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountStatementsDataAccessResourceFailureException() {
		
		when(sandutility.isValidPsuId("32118465")).thenReturn(Boolean.FALSE);
		
		Mockito.when(accountStatementsRepository.findByAccountNumberAndAccountNSCAndStartDateTimeCopyBetween(
				anyString(), anyString(), any(Instant.class), any(Instant.class), any(Pageable.class)))
		.thenThrow(new DataAccessResourceFailureException("Test"));

		Map<String, String> params = new HashMap<>();
		params.put("RequestedPageNumber", "2");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2015-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2017-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2015-01-01T00:00:00" );
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2018-01-01T00:00:00");
		params.put(AccountStatementMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2020-01-01T00:00:00");
		params.put("RequestedPageSize", "10");
		LocalDateTime localDate = LocalDateTime.now();
		LocalDateTime localDate2 = LocalDateTime.of(2015, Month.MAY, 15, 10, 0);

		DateRange transactionDateRange = new DateRange();
		transactionDateRange.setNewFilterFromDate(localDate2);
		transactionDateRange.setNewFilterToDate(localDate);
		when(utility.fsCallFilter(null)).thenReturn(transactionDateRange);
		try {
			dbAdapterImpl
			.retrieveAccountStatements(AccountStatementTestData.getMockAccountMapping(), params);
		/*} catch (PSD2Exception e) {
			assertEquals("Error while establishing connection,please try again..", e.getErrorInfo().getErrorMessage());
			throw e;
		}*/
		} catch (PSD2Exception e) {
			assertEquals("Technical error while establishing data base connection", e.getOBErrorResponse1().getErrors().get(0).getMessage());		
			throw e;
		}
	}

	@Test
	public void testRetrieveAccountStatementByIdSuccessFlow() {
		when(accountStatementsRepository.findByAccountNumberAndAccountNSCAndStatementId(
				anyString(), anyString(), anyString(), any(Pageable.class)))
		.thenReturn(AccountStatementTestData.getAccountStatementsDataPage());

		Map<String, String> params = new HashMap<>();
		params.put("RequestedPageNumber", "1");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_STATEMENT, "333333");
		params.put("RequestedPageSize", "10");

		PlatformAccountStatementsResponse response = dbAdapterImpl
				.retrieveAccountStatementsByStatementId(AccountStatementTestData.getMockAccountMapping(), params);

		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(response);
	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountStatementByIdDataAccessResourceFailureException() {
		Mockito.when(accountStatementsRepository.findByAccountNumberAndAccountNSCAndStatementId(
				anyString(), anyString(), anyString(), any(Pageable.class)))
		.thenThrow(new DataAccessResourceFailureException("Test"));

		Map<String, String> params = new HashMap<>();
		params.put("RequestedPageNumber", "2");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_STATEMENT, "333333");
		params.put("RequestedPageSize", "10");

		try {
			dbAdapterImpl
			.retrieveAccountStatementsByStatementId(AccountStatementTestData.getMockAccountMapping(), params);
		/*} catch (PSD2Exception e) {
			assertEquals("Error while establishing connection,please try again..", e.getErrorInfo().getErrorMessage());
			throw e;
		}*/
		} catch (PSD2Exception e) {
			assertEquals("Technical error while establishing data base connection", e.getOBErrorResponse1().getErrors().get(0).getMessage());		
			throw e;
		}
	}

	@Test
	public void testRetrieveAccountStatementTransactionSuccessFlowNoFilter() {
		when(transactionRepository.findByAccountNumberAndAccountNSCAndStatementId(
				anyString(), anyString(), anyString(), any(Pageable.class)))
		.thenReturn(AccountStatementTransactionTestData.getAccountTransactionsDataPage());

		Map<String, String> params = new HashMap<>();
		params.put("RequestedPageNumber", "1");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_STATEMENT, "333333");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TXN_FILTER, "ALL");
		params.put("RequestedPageSize", "10");

		PlatformAccountTransactionResponse accountTransactionsGETResponse = dbAdapterImpl
				.retrieveAccountTransactionsByStatementsId(AccountStatementTransactionTestData.getMockAccountMapping(), params);

		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(accountTransactionsGETResponse);
	}

	@Test
	public void testRetrieveAccountStatementTransactionSuccessFlowWithFilter() {
		Mockito.when(transactionRepository
				.findByAccountNumberAndAccountNSCAndStatementIdAndCreditDebitIndicator(anyString(),
						anyString(), anyString(), anyString(),
						any(Pageable.class)))
		.thenReturn(AccountStatementTransactionTestData.getAccountTransactionsDataPage());

		Map<String, String> params = new HashMap<>();
		params.put("RequestedPageNumber", "2");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_STATEMENT, "333333");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TXN_FILTER, "DEBIT");
		params.put("RequestedPageSize", "10");

		PlatformAccountTransactionResponse accountTransactionsGETResponse = dbAdapterImpl
				.retrieveAccountTransactionsByStatementsId(AccountStatementTransactionTestData.getMockAccountMapping(), params);


		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(accountTransactionsGETResponse);
	}



	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountStatementTransactionDataAccessResourceFailureExceptionWithNoFilter() {
		Mockito.when(transactionRepository.findByAccountNumberAndAccountNSCAndStatementId(
				anyString(), anyString(),anyString(), any(Pageable.class)))
		.thenThrow(new DataAccessResourceFailureException("Test"));

		Map<String, String> params = new HashMap<>();
		params.put("RequestedPageNumber", "2");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TXN_FILTER, "ALL");
		params.put("RequestedPageSize", "10");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_STATEMENT, "333333");
		try {
			dbAdapterImpl
			.retrieveAccountTransactionsByStatementsId(AccountStatementTransactionTestData.getMockAccountMapping(), params);
		/*} catch (PSD2Exception e) {
			assertEquals("Error while establishing connection,please try again..", e.getErrorInfo().getErrorMessage());
			throw e;
		}*/
		} catch (PSD2Exception e) {
			assertEquals("Technical error while establishing data base connection", e.getOBErrorResponse1().getErrors().get(0).getMessage());		
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountStatementTransactionDataAccessResourceFailureExceptionWithFilter() {
		Mockito.when(transactionRepository
				.findByAccountNumberAndAccountNSCAndStatementIdAndCreditDebitIndicator(anyString(),
						anyString(), anyString(), anyString(),
						any(Pageable.class)))
		.thenThrow(new DataAccessResourceFailureException("Test"));

		Map<String, String> params = new HashMap<>();
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_PAGE_NUMBER, "2");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_STATEMENT, "333333");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_TXN_FILTER, "CREDIT");
		params.put(AccountStatementMongoDbAdapterConstants.REQUESTED_PAGE_SIZE, "10");

		try {
			dbAdapterImpl
			.retrieveAccountTransactionsByStatementsId(AccountStatementTransactionTestData.getMockAccountMapping(), params);
		/*} catch (PSD2Exception e) {
			assertEquals("Error while establishing connection,please try again..", e.getErrorInfo().getErrorMessage());
			throw e;
		}*/
		} catch (PSD2Exception e) {
			assertEquals("Technical error while establishing data base connection", e.getOBErrorResponse1().getErrors().get(0).getMessage());		
			throw e;
		}
	}

}
