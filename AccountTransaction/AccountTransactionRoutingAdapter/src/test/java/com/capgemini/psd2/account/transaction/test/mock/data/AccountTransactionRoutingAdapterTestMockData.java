package com.capgemini.psd2.account.transaction.test.mock.data;

import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;

public class AccountTransactionRoutingAdapterTestMockData {

	public static PlatformAccountTransactionResponse getMockTransactionGETResponse() {
		return new PlatformAccountTransactionResponse();
	}
}
