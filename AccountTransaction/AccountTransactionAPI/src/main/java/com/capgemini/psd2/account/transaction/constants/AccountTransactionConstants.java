/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.transaction.constants;

/**
 */
public class AccountTransactionConstants {
	
	public static final String REQUESTED_FROM_DATETIME = "RequestedFromDateTime";
	
	public static final String REQUESTED_TO_DATETIME = "RequestedToDateTime";
	
	public static final String REQUESTED_TO_CONSENT_DATETIME = "RequestedToConsentDateTime";
	
	public static final String REQUESTED_FROM_CONSENT_DATETIME = "RequestedFromConsentDateTime";
	
	public static final String CONSENT_EXPIRATION_DATETIME = "ConsentExpirationDateTime";
	
	public static final String ACCOUNT_SCOPE_NAME = "accounts";
	
	public static final String REQUESTED_PAGE_NUMBER = "RequestedPageNumber";
	
	public static final String REQUESTED_PAGE_SIZE = "RequestedPageSize";
	
	public static final String REQUESTED_MIN_PAGE_SIZE = "RequestedMinPageSize";
	
	public static final String REQUESTED_MAX_PAGE_SIZE = "RequestedMaxPageSize";
	
	public static final String REQUESTED_TXN_FILTER = "RequestedTxnFilter";
	
	public static final String REQUESTED_TXN_KEY = "RequestedTxnKey";
	
	public static final String DEFAULT_PAGE = String.valueOf(1);
	
	public static final String FROM_DATE_PARAM_NAME = "fromBookingDateTime";
	
	public static final String TO_DATE_PARAM_NAME = "toBookingDateTime";
	
	public static final String PAGE_SIZE_PARAM_NAME = "pageSize";
	
	public static final String PAGE_NUMBER_PARAM_NAME = "page";

	public static final String TXN_KEY_PARAM_NAME = "transactionRetrievalKey";
	
	public enum FilterEnum {
		CREDIT, DEBIT, ALL
	}
}
