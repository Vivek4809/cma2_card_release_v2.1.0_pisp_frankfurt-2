/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.information.mock.foundationservice.raml.domain.Account;
import com.capgemini.psd2.account.information.mock.foundationservice.service.AccountInformationService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;

/**
 * The Class AccountInformationController.
 */
@RestController
@RequestMapping("/fs-customeraccountprofile-service/services/customerAccountProfilev2")

public class AccountInformationController {

	/** The adapterUtility. */
	@Autowired
	private ValidationUtility validatorUtility;

	/** The account information service. */
	@Autowired
	private AccountInformationService accountInformationService;

	/**
	 * Channel A reterive account information.
	 *
	 * @param accountNsc
	 *            the account nsc
	 * @param accountNumber
	 *            the account number
	 * @param boiUser
	 *            the boi user
	 * @param boiChannel
	 *            the boi channel
	 * @param boiPlatform
	 *            the boi platform
	 * @param correlationID
	 *            the correlation ID
	 * @return the accounts
	 * @throws Exception
	 *             the exception
	 */
	@RequestMapping(value = "/{version}/accounts/{nsc}/{accountNumber}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Account channelAReteriveAccountInformation(@PathVariable("nsc") String accountNSC,
			@PathVariable("accountNumber") String accountNumber,
			@RequestHeader(required = false, value = "x-api-source-user") String boiUser,
			@RequestHeader(required = false, value = "x-api-channel-Code") String boiChannel,
			@RequestHeader(required = false, value = "x-api-source-system") String boiPlatform,
			@RequestHeader(required = false, value = "x-api-transaction-id") String correlationID) throws Exception {

		validatorUtility.validateErrorCode(correlationID);

		if (boiUser == null || boiChannel == null || boiPlatform == null || correlationID == null) {

			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_ACC_PAC);
		}

		if (accountNumber == null) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_ACC_PAC);
		}

		return accountInformationService.retrieveAccountInformation(accountNumber, accountNSC);

	}

	/**
	 * Channel B reterive account information.
	 *
	 * @param accountNsc
	 *            the account nsc
	 * @param accountNumber
	 *            the account number
	 * @param boiUser
	 *            the boi user
	 * @param boiChannel
	 *            the boi channel
	 * @param boiPlatform
	 *            the boi platform
	 * @param correlationID
	 *            the correlation ID
	 * @return the accounts
	 * @throws Exception
	 *             the exception
	 */
	@RequestMapping(value = "/personal/account/{version}/{nsc}/{accountNumber}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Account channelBReteriveAccountInformation(@PathVariable("nsc") String accountNSC,
			@PathVariable("accountNumber") String accountNumber,
			@RequestHeader(required = false, value = "X-BOI-USER") String boiUser,
			@RequestHeader(required = false, value = "X-BOI-CHANNEL") String boiChannel,
			@RequestHeader(required = false, value = "X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false, value = "X-CORRELATION-ID") String correlationID) throws Exception {

		validatorUtility.validateErrorCode(correlationID);

		if (boiUser == null || boiChannel == null || boiPlatform == null || correlationID == null) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_ACC_PAC);
		}

		if (accountNSC == null || accountNumber == null) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_ACC_PAC);
		}
		return accountInformationService.retrieveAccountInformation(accountNumber, accountNSC);

	}

	@RequestMapping(value = "/{version}/accounts/creditcards/{maskedCarddPANNumber}/{accountNumber}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Account channelAReteriveAccountInformationRaml(
			@PathVariable("maskedCarddPANNumber") String maskedCarddPANNumber,
			@PathVariable("accountNumber") String accountNumber,
			@RequestHeader(required = false, value = "x-api-source-user") String boiUser,
			@RequestHeader(required = false, value = "x-api-channel-Code") String boiChannel,
			@RequestHeader(required = false, value = "x-api-source-system") String boiPlatform,
			@RequestHeader(required = false, value = "x-api-transaction-id") String correlationID) throws Exception {

		validatorUtility.validateErrorCode(correlationID);

		if (boiUser == null || boiChannel == null || boiPlatform == null) {
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_ACC_PAC);
		}

		if (maskedCarddPANNumber == null || accountNumber == null) {
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_ACC_PAC);
		}

		return accountInformationService.retrieveAccountInformationRaml(maskedCarddPANNumber, accountNumber);
	}
}