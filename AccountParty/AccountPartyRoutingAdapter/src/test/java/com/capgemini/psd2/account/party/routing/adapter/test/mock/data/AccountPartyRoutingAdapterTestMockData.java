/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.party.routing.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBParty1;
import com.capgemini.psd2.aisp.domain.OBPostalAddress8;
import com.capgemini.psd2.aisp.domain.OBReadParty1;
import com.capgemini.psd2.aisp.domain.OBReadParty1Data;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountPartyResponse;


/**
 * The Class AccountBalanceRoutingAdapterTestMockData.
 */
public class AccountPartyRoutingAdapterTestMockData {

	public static PlatformAccountPartyResponse getMockPartyGETResponse() {
		PlatformAccountPartyResponse platformAccountPartyResponse = new PlatformAccountPartyResponse();
		OBReadParty1Data data = new OBReadParty1Data();
		OBParty1 oBParty1 = new OBParty1();
		data.setParty(oBParty1);
		List<OBPostalAddress8> partyList = new ArrayList<>();
		OBPostalAddress8 oBPostalAddress8 = new OBPostalAddress8();
		partyList.add(oBPostalAddress8);
		oBParty1.addAddressItem(oBPostalAddress8);
		oBParty1.setAddress(partyList);
		
		OBReadParty1 oBReadParty1 = new OBReadParty1();
		oBReadParty1.setData(data);
		platformAccountPartyResponse.setobReadParty1(oBReadParty1);
		return platformAccountPartyResponse;
		}
	}
		
	
