/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.party.routing.adapter.test.routing;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;

import com.capgemini.psd2.account.party.routing.adapter.impl.AccountPartyRoutingAdapter;
import com.capgemini.psd2.account.party.routing.adapter.routing.AccountPartyCoreSystemAdapterFactory;
import com.capgemini.psd2.aisp.adapter.AccountPartyAdapter;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;

/**
 * The Class AccountBalanceAdapterFactoryTest.
 */
public class AccountPartyAdapterFactoryTest {
	
	/** The application context. */
	@Mock
	private ApplicationContext applicationContext;
	
	/** The account balance core system adapter factory. */
	@InjectMocks
	private AccountPartyCoreSystemAdapterFactory accountPartyCoreSystemAdapterFactory;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}
	
	/**
	 * Test account balance adapter.
	 */
	@Test
	public void testAccountPartyAdapter() {
		AccountPartyAdapter accountPartyAdapter = new AccountPartyRoutingAdapter();
		when(applicationContext.getBean(anyString())).thenReturn(accountPartyAdapter);
		AccountPartyAdapter accountPartyAdapterResult = (AccountPartyRoutingAdapter) accountPartyCoreSystemAdapterFactory.getAdapterInstance("accountPartyAdapter");
		assertEquals(accountPartyAdapter, accountPartyAdapterResult);
	}
	
	

}
