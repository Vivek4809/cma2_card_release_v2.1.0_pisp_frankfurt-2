package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.client.FraudnetViaMulesoftProxyClientImpl;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.FraudServiceRequest;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@RunWith(SpringJUnit4ClassRunner.class)
public class FraudnetViaMulesoftProxyClientImplTest {

	@InjectMocks
	private FraudnetViaMulesoftProxyClientImpl fraudnetViaMulesoftProxyClientImpl;

	@Mock
	private RestClientSync restClient;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	@Test
	public void fraudnetViaMulesoftCallTest() {
		RequestInfo requestInfo = new RequestInfo();
		FraudServiceRequest fraudServiceRequest = new FraudServiceRequest();
		FraudServiceResponse fraudServiceResponse = new FraudServiceResponse();
		HttpHeaders httpHeaders = new HttpHeaders();

		Mockito.when(restClient.callForPost(anyObject(), anyObject(), anyObject(), anyObject())).thenReturn(fraudServiceResponse);

		FraudServiceResponse response = fraudnetViaMulesoftProxyClientImpl.fraudnetViaMulesoftCall(requestInfo,fraudServiceRequest, FraudServiceResponse.class, httpHeaders);
		
		assertNotNull(response);
	}

}
