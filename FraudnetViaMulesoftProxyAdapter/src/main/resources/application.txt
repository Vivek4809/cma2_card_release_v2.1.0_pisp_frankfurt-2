server:
  port: 9099
spring:
  application:
    name: adapter       
foundationService:
  apiId: fs_payment
  fraudnetMulesoftEndpointURL: https://mocksvc.mulesoft.com/mocks/2c385785-b388-4c68-8456-fb6ca4803d3f/it-boi/fraud-service/event/fraud-case
  interactionIdReqHeader: x-api-transaction-id
  clientIdReqHeader: client_id
  clientSecretReqHeader: client_secret 
  clientId: 6795ee9ca8e3407694f866725303db37
  clientSecret: bb169ffbe5c44a40B9AC1CA5A2720FA8  
  xApiTransactionId: abcdefghijklmnop
  timeZone: Europe/Dublin