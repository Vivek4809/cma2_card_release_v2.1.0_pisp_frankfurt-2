package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * This is structure of the event that is being submitted for fraud risk
 * assessment
 */
@ApiModel(description = "This is structure of the event that is being submitted for fraud risk assessment")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FraudServiceRequest {
	@SerializedName("userEvent")
	private UserEvent userEvent;

	public FraudServiceRequest userEvent(UserEvent userEvent) {
		this.userEvent = userEvent;
		return this;
	}

	/**
	 * Get userEvent
	 * 
	 * @return userEvent
	 **/
	@ApiModelProperty(required = true, value = "")
	public UserEvent getUserEvent() {
		return userEvent;
	}

	public void setUserEvent(UserEvent userEvent) {
		this.userEvent = userEvent;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		FraudServiceRequest fraudServiceType = (FraudServiceRequest) o;
		return Objects.equals(this.userEvent, fraudServiceType.userEvent);
	}

	@Override
	public int hashCode() {
		return Objects.hash(userEvent);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class FraudServiceType {\n");

		sb.append("    userEvent: ").append(toIndentedString(userEvent)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
