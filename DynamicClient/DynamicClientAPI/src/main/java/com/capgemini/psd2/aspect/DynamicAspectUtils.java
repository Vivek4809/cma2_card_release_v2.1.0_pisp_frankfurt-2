package com.capgemini.psd2.aspect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.utilities.JSONUtilities;

@Component
public class DynamicAspectUtils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DynamicAspectUtils.class);

	public void exceptionPayload(LoggerAttribute loggerAttribute, Exception e, String error) {
		LOGGER.error("{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":{}}",loggerAttribute.getOperationName(), loggerAttribute,
				JSONUtilities.getJSONOutPutFromObject(error));
		if (LOGGER.isDebugEnabled()) {
			LOGGER.error("{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":\"{}\"}",loggerAttribute.getOperationName(), loggerAttribute, e.getStackTrace());
		}
	}
	
}
