package com.capgemini.psd2.validator.enums;

public enum CertificateTypes {

	OB_EXISTING_CERT("obold"),
	OB_EIDAS_CERT("obeidas"),
	NON_OB_EIDAS_CERT("eidas");
	
	private String type;

	CertificateTypes(String s) {
		this.type = s;
	}

	public String getType() {
		return type;
	}
}
