package com.capgemini.psd2.pisp.payments.standing.order.test.comparator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FinalPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FirstPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1RecurringPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.payments.standing.order.comparator.DomesticStandingOrderPayloadComparator;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticStandingOrderPayloadComparatorTest {

	@InjectMocks
	private DomesticStandingOrderPayloadComparator comparator;

	@Test
	public void testCompareValue0() {

		CustomDStandingOrderConsentsPOSTResponse paymentResponse = new CustomDStandingOrderConsentsPOSTResponse();
		CustomDStandingOrderConsentsPOSTResponse adaptedPaymentResponse = new CustomDStandingOrderConsentsPOSTResponse();

		paymentResponse.setData(new OBWriteDataDomesticStandingOrderConsentResponse1());
		OBDomesticStandingOrder1 initiation = new OBDomesticStandingOrder1();
		paymentResponse.getData().setInitiation(initiation);
		OBDomesticStandingOrder1FirstPaymentAmount firstPaymentAmount = new OBDomesticStandingOrder1FirstPaymentAmount();
		initiation.setFirstPaymentAmount(firstPaymentAmount );
		firstPaymentAmount.setAmount("20.0");
		paymentResponse.setRisk(new OBRisk1());

		adaptedPaymentResponse.setData(new OBWriteDataDomesticStandingOrderConsentResponse1());
		adaptedPaymentResponse.getData().setInitiation(initiation);
		adaptedPaymentResponse.setRisk(new OBRisk1());

		int i = comparator.compare(paymentResponse, adaptedPaymentResponse);
		assertEquals(0, i);
	}

	@Test
	public void testCompareValue1() {

		CustomDStandingOrderConsentsPOSTResponse paymentResponse = new CustomDStandingOrderConsentsPOSTResponse();
		CustomDStandingOrderConsentsPOSTResponse adaptedPaymentResponse = new CustomDStandingOrderConsentsPOSTResponse();

		paymentResponse.setData(new OBWriteDataDomesticStandingOrderConsentResponse1());
		OBDomesticStandingOrder1 initiation = new OBDomesticStandingOrder1();
		paymentResponse.getData().setInitiation(initiation);
		OBDomesticStandingOrder1FirstPaymentAmount firstPaymentAmount = new OBDomesticStandingOrder1FirstPaymentAmount();
		
		initiation.setFirstPaymentAmount(firstPaymentAmount );
		firstPaymentAmount.setAmount("20.0");
		paymentResponse.getData().getInitiation().setNumberOfPayments("15");
		paymentResponse.setRisk(new OBRisk1());

		adaptedPaymentResponse.setData(new OBWriteDataDomesticStandingOrderConsentResponse1());
		adaptedPaymentResponse.getData().setInitiation(initiation);
		adaptedPaymentResponse.getData().getInitiation().setNumberOfPayments("10");
		adaptedPaymentResponse.setRisk(new OBRisk1());

		int i = comparator.compare(paymentResponse, adaptedPaymentResponse);
		assertEquals(0, i);
	}

	@Test
	public void comparePaymentDetails_TrueDebtorDetails() {
		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		CustomDStandingOrderPOSTRequest request = new CustomDStandingOrderPOSTRequest();

		response.setData(new OBWriteDataDomesticStandingOrderConsentResponse1());
		response.getData().setInitiation(new OBDomesticStandingOrder1());
		response.getData().getInitiation().setFirstPaymentAmount(new OBDomesticStandingOrder1FirstPaymentAmount());
		response.getData().getInitiation().getFirstPaymentAmount().setAmount("153");
		response.getData().getInitiation()
				.setRecurringPaymentAmount(new OBDomesticStandingOrder1RecurringPaymentAmount());
		response.getData().getInitiation().getRecurringPaymentAmount().setAmount("154");
		response.getData().getInitiation().setFinalPaymentAmount(new OBDomesticStandingOrder1FinalPaymentAmount());
		response.getData().getInitiation().getFinalPaymentAmount().setAmount("160");
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		response.getData().getInitiation().getDebtorAccount().setName("test");

		request.setData(new OBWriteDataDomesticStandingOrder1());
		request.getData().setInitiation(new OBDomesticStandingOrder1());
		request.getData().getInitiation().setFirstPaymentAmount(new OBDomesticStandingOrder1FirstPaymentAmount());
		request.getData().getInitiation().getFirstPaymentAmount().setAmount("153");
		request.getData().getInitiation()
				.setRecurringPaymentAmount(new OBDomesticStandingOrder1RecurringPaymentAmount());
		request.getData().getInitiation().getRecurringPaymentAmount().setAmount("154");
		request.getData().getInitiation().setFinalPaymentAmount(new OBDomesticStandingOrder1FinalPaymentAmount());
		request.getData().getInitiation().getFinalPaymentAmount().setAmount("160");
		request.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		request.getData().getInitiation().getDebtorAccount().setName("test1");

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setTppDebtorDetails("true");
		paymentSetupPlatformResource.setTppDebtorNameDetails("testdebtor");

		int i = comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
		assertEquals(1, i);
	}
	
	@Test
	public void comparePaymentDetails_FalseDebtorDetails() {
		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		CustomDStandingOrderPOSTRequest request = new CustomDStandingOrderPOSTRequest();

		response.setData(new OBWriteDataDomesticStandingOrderConsentResponse1());
		response.getData().setInitiation(new OBDomesticStandingOrder1());
		response.getData().getInitiation().setFirstPaymentAmount(new OBDomesticStandingOrder1FirstPaymentAmount());
		response.getData().getInitiation().getFirstPaymentAmount().setAmount("153");
		response.getData().getInitiation()
				.setRecurringPaymentAmount(new OBDomesticStandingOrder1RecurringPaymentAmount());
		response.getData().getInitiation().getRecurringPaymentAmount().setAmount("154");
		response.getData().getInitiation().setFinalPaymentAmount(new OBDomesticStandingOrder1FinalPaymentAmount());
		response.getData().getInitiation().getFinalPaymentAmount().setAmount("160");
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		response.getData().getInitiation().getDebtorAccount().setName("test");

		request.setData(new OBWriteDataDomesticStandingOrder1());
		request.getData().setInitiation(new OBDomesticStandingOrder1());
		request.getData().getInitiation().setFirstPaymentAmount(new OBDomesticStandingOrder1FirstPaymentAmount());
		request.getData().getInitiation().getFirstPaymentAmount().setAmount("153");
		request.getData().getInitiation()
				.setRecurringPaymentAmount(new OBDomesticStandingOrder1RecurringPaymentAmount());
		request.getData().getInitiation().getRecurringPaymentAmount().setAmount("154");
		request.getData().getInitiation().setFinalPaymentAmount(new OBDomesticStandingOrder1FinalPaymentAmount());
		request.getData().getInitiation().getFinalPaymentAmount().setAmount("160");

		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine);
		addressLine.add("warner bros");
		addressLine.add("warner bros2");
		List<String> countrySubDivision = new ArrayList<>();
		deliveryAddress.setCountrySubDivision("warner bros2");
		countrySubDivision.add("Dublin west");
		request.setRisk(risk);
		response.setRisk(risk);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setTppDebtorDetails("false");

		int i = comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
		assertEquals(0, i);
	}
	
	@Test
	public void comparePaymentDetails_FalseDebtorDetails_NullPayments() {
		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		CustomDStandingOrderPOSTRequest request = new CustomDStandingOrderPOSTRequest();

		response.setData(new OBWriteDataDomesticStandingOrderConsentResponse1());
		response.getData().setInitiation(new OBDomesticStandingOrder1());
		response.getData().getInitiation().setFirstPaymentAmount(new OBDomesticStandingOrder1FirstPaymentAmount());
		response.getData().getInitiation().getFirstPaymentAmount().setAmount("153");
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		response.getData().getInitiation().getDebtorAccount().setName("test");

		request.setData(new OBWriteDataDomesticStandingOrder1());
		request.getData().setInitiation(new OBDomesticStandingOrder1());
		request.getData().getInitiation().setFirstPaymentAmount(new OBDomesticStandingOrder1FirstPaymentAmount());
		request.getData().getInitiation().getFirstPaymentAmount().setAmount("153");

		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine);
		addressLine.add("warner bros");
		addressLine.add("warner bros2");
		List<String> countrySubDivision = new ArrayList<>();
		deliveryAddress.setCountrySubDivision("warner bros2");
		countrySubDivision.add("Dublin west");
		request.setRisk(risk);
		response.setRisk(risk);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setTppDebtorDetails("false");

		int i = comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
		assertEquals(0, i);
	}
	
	@Test
	public void validateDebtorDetailsTest() {
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		OBDomesticStandingOrder1 responseInitiation = new OBDomesticStandingOrder1();
		OBDomesticStandingOrder1 requestInitiation = new OBDomesticStandingOrder1();
		PaymentConsentsPlatformResource PaymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		PaymentConsentsPlatformResource.setTppDebtorDetails("false");
		requestInitiation.setDebtorAccount(debtorAccount);
		boolean result = comparator.validateDebtorDetails(responseInitiation, requestInitiation,
				PaymentConsentsPlatformResource);
		assertThat(result).isEqualTo(false);
		requestInitiation.setDebtorAccount(null);
		responseInitiation.setDebtorAccount(null);
		result = comparator.validateDebtorDetails(responseInitiation, requestInitiation,
				PaymentConsentsPlatformResource);
		assertThat(result).isEqualTo(true);
	}
	
	@Test
	public void comparetest_nullAddressLine1() {

		CustomDStandingOrderPOSTRequest request = new CustomDStandingOrderPOSTRequest();
		OBRisk1 risk = new OBRisk1();
		OBWriteDataDomesticStandingOrder1 data = new OBWriteDataDomesticStandingOrder1();

		OBDomesticStandingOrder1 initiation = new OBDomesticStandingOrder1();
		data.setInitiation(initiation);
		OBDomesticStandingOrder1FirstPaymentAmount firstPaymentAmount = new OBDomesticStandingOrder1FirstPaymentAmount();		
		OBDomesticStandingOrder1FinalPaymentAmount finalPaymentAmount = new OBDomesticStandingOrder1FinalPaymentAmount();
		firstPaymentAmount.setAmount("23.32");
		finalPaymentAmount.setAmount("23.32");
		initiation.setFirstPaymentAmount(firstPaymentAmount);
		initiation.setFinalPaymentAmount(finalPaymentAmount);

		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount);
		creditorAccount.setIdentification("08080021325698");
		creditorAccount.setName("ACME Inc");
		creditorAccount.setSecondaryIdentification("0002");

		request.setData(data);
		request.setRisk(risk);

		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<>();
		addressLine.add("");
		deliveryAddress.setAddressLine(addressLine);
		String countrySubDivision = "Dublin west";
		deliveryAddress.setCountrySubDivision(countrySubDivision);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setId("144");
		paymentSetupPlatformResource.setPaymentId("58923-001");
		paymentSetupPlatformResource.setPaymentConsentId("58923");
		paymentSetupPlatformResource.setSetupCmaVersion("3");

		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		OBWriteDataDomesticStandingOrderConsentResponse1 data1 = new OBWriteDataDomesticStandingOrderConsentResponse1();
		OBRisk1 risk1 = new OBRisk1();
		
		OBRisk1DeliveryAddress deliveryAddress1 = new OBRisk1DeliveryAddress();
		List<String> addressLine1 = null;
		deliveryAddress1.setAddressLine(addressLine1);
		risk1.setDeliveryAddress(deliveryAddress1);

		OBDomesticStandingOrder1 initiation1 = new OBDomesticStandingOrder1();
		data1.setInitiation(initiation1);
		initiation1.setFirstPaymentAmount(firstPaymentAmount);
		initiation1.setFinalPaymentAmount(finalPaymentAmount);
		OBCashAccountDebtor3 debtorAccount1 = new OBCashAccountDebtor3();
		initiation1.setDebtorAccount(debtorAccount1);
		debtorAccount1.setName("PSD2");
		response.setData(data1);
		response.setRisk(risk1);
		comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
	}
	
}
