package com.capgemini.psd2.account.request.routing.adapter.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.capgemini.psd2.account.request.routing.adapter.routing.SaaSAdapterFactory;
import com.capgemini.psd2.authentication.adapter.AuthenticationAdapter;

public class SaaSRoutingAdapter implements AuthenticationAdapter {
	
	/** The default adapter. */
	@Value("${app.defaultRoutingAdapterSaaS}")
	private String defaultAdapter;
	
	@Autowired
	private SaaSAdapterFactory saaSAdapterFactory;

	@Override
	public <T> T authenticate(T authenticaiton, Map<String, String> headers) {
		AuthenticationAdapter authenticationAdapter = saaSAdapterFactory.getAdapterInstance(defaultAdapter);
		return authenticationAdapter.authenticate(authenticaiton, headers);
	}

}
