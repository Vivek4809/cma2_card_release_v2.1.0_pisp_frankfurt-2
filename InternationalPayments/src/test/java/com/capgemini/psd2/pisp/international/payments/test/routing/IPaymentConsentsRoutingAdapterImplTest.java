package com.capgemini.psd2.pisp.international.payments.test.routing;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.international.payments.routing.IPaymentConsentsRoutingAdapterImpl;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalPaymentStagingAdapter;
import com.capgemini.psd2.token.Token;

@RunWith(SpringJUnit4ClassRunner.class)
public class IPaymentConsentsRoutingAdapterImplTest {

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@InjectMocks
	private IPaymentConsentsRoutingAdapterImpl adapter;

	@Mock
	private ApplicationContext applicationContext;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testprocess() {
		CustomIPaymentConsentsPOSTRequest internationalPaymentConsentsRequest = new CustomIPaymentConsentsPOSTRequest();
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		OBExternalConsentStatus1Code successStatus = null;
		OBExternalConsentStatus1Code failureStatus = null;

		Map<String, String> seviceParams = new HashMap<>();
		seviceParams.put("channelId", "wedec");

		Token token = new Token();
		token.setSeviceParams(seviceParams);

		reqHeaderAtrributes.setToken(token);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);

		ReflectionTestUtils.setField(adapter, "beanName", "iPaymentConsentsStagingMongoDbAdapter");

		InternationalPaymentStagingAdapter value = new InternationalPaymentStagingAdapter() {

			@Override
			public CustomIPaymentConsentsPOSTResponse retrieveStagedInternationalPaymentConsents(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public CustomIPaymentConsentsPOSTResponse processInternationalPaymentConsents(
					CustomIPaymentConsentsPOSTRequest internationalPaymentConsentsRequest,
					CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
					OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
				return null;
			}
		};

		when(applicationContext.getBean(anyString())).thenReturn(value);

		adapter.processInternationalPaymentConsents(internationalPaymentConsentsRequest, customStageIdentifiers, params,
				successStatus, failureStatus);
	}

	@Test
	public void retrieve() {
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = null;

		/*Map<String, String> seviceParams = new HashMap<>();
		seviceParams.put("channelId", "wedec");

		Token token = new Token();
		token.setSeviceParams(seviceParams);

		reqHeaderAtrributes.setToken(token);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);*/

		ReflectionTestUtils.setField(adapter, "beanName", "iPaymentConsentsStagingMongoDbAdapter");

		InternationalPaymentStagingAdapter value = new InternationalPaymentStagingAdapter() {

			@Override
			public CustomIPaymentConsentsPOSTResponse retrieveStagedInternationalPaymentConsents(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public CustomIPaymentConsentsPOSTResponse processInternationalPaymentConsents(
					CustomIPaymentConsentsPOSTRequest internationalPaymentConsentsRequest,
					CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
					OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
				return null;
			}
		};

		when(applicationContext.getBean(anyString())).thenReturn(value);

		adapter.retrieveStagedInternationalPaymentConsents(customPaymentStageIdentifiers, params);
	}

	@After
	public void tearDown() throws Exception {
		reqHeaderAtrributes = null;
		adapter = null;
	}

}
