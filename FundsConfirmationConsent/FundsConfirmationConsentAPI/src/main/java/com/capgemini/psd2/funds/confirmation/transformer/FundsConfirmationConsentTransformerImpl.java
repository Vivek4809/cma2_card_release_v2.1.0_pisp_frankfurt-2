package com.capgemini.psd2.funds.confirmation.transformer;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.utilities.CardSetupConstants;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("fundsConfirmationConsentTransformer")
public class FundsConfirmationConsentTransformerImpl
		implements FundsConfirmationConsentTransformer<OBFundsConfirmationConsentResponse1> {

	@Override
	public OBFundsConfirmationConsentResponse1 fundsConfirmationConsentResponseTransformer(
			OBFundsConfirmationConsentResponse1 response) {

		if (!NullCheckUtils.isNullOrEmpty(response.getData().getDebtorAccount())) {
			String debtorAccountScheme = response.getData().getDebtorAccount().getSchemeName();
			if (CardSetupConstants.prefixToAddSchemeNameList().contains(debtorAccountScheme)) {
				response.getData().getDebtorAccount()
						.setSchemeName(PSD2Constants.OB_SCHEME_PREFIX.concat(debtorAccountScheme));
			}
		}
		return response;

	}

}
