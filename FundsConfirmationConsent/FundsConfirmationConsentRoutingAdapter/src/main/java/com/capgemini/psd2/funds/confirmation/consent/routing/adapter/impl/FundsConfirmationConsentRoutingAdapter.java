package com.capgemini.psd2.funds.confirmation.consent.routing.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.cisp.domain.OBExternalRequestStatus1Code;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentDataResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.funds.confirmation.consent.routing.adapter.routing.FundsConfirmationConsentAdapterFactory;

@Component
public class FundsConfirmationConsentRoutingAdapter implements FundsConfirmationConsentAdapter {

	@Value("${app.defaultAdapterFundsConfirmationConsent}")
	private String defaultAdapter;

	@Autowired
	private FundsConfirmationConsentAdapterFactory fundsConfirmationConsentAdapterFactory;

	@Override
	public OBFundsConfirmationConsentResponse1 createFundsConfirmationConsentPOSTResponse(
			OBFundsConfirmationConsentDataResponse1 fundsConfirmationConsentPOSTResponseData1) {
		FundsConfirmationConsentAdapter fundsConfirmationConsentAdapter = fundsConfirmationConsentAdapterFactory
				.getAdapterInstance(defaultAdapter);
		return fundsConfirmationConsentAdapter
				.createFundsConfirmationConsentPOSTResponse(fundsConfirmationConsentPOSTResponseData1);
	}

	@Override
	public OBFundsConfirmationConsentResponse1 getFundsConfirmationConsentPOSTResponse(String consentId) {
		FundsConfirmationConsentAdapter fundsConfirmationConsentAdapter = fundsConfirmationConsentAdapterFactory
				.getAdapterInstance(defaultAdapter);
		return fundsConfirmationConsentAdapter.getFundsConfirmationConsentPOSTResponse(consentId);
	}

	@Override
	public void removeFundsConfirmationConsent(String consentId, String cId) {
		FundsConfirmationConsentAdapter fundsConfirmationConsentAdapter = fundsConfirmationConsentAdapterFactory
				.getAdapterInstance(defaultAdapter);
		fundsConfirmationConsentAdapter.removeFundsConfirmationConsent(consentId, cId);
	}

	@Override
	public OBFundsConfirmationConsentResponse1 updateFundsConfirmationConsentResponse(String consentId,
			OBExternalRequestStatus1Code statusEnum) {
		FundsConfirmationConsentAdapter fundsConfirmationConsentAdapter = fundsConfirmationConsentAdapterFactory
		.getAdapterInstance(defaultAdapter);
		return fundsConfirmationConsentAdapter.updateFundsConfirmationConsentResponse(consentId, statusEnum);
	}

}
