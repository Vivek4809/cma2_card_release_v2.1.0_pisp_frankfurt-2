package com.capgemini.psd2.funds.confirmation.consent.mongo.db.adapter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.funds.confirmation.consent.mongo.db.adapter.impl.FundsConfirmationConsentMongoDbAdapterImpl;

@Component
public class FundsConfirmationConsentMongoDbAdapterConfig {

	@Bean(name = "fundsConfirmationConsentMongoDbAdapter")
	public FundsConfirmationConsentAdapter fundsConfirmationConsentMongoDBAdapter() {
		return new FundsConfirmationConsentMongoDbAdapterImpl();
	}

}
