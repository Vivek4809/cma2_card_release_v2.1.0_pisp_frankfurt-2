package com.capgemini.psd2.validate.prestage.payment.boi.fs;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.ValidatePaymentFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.constants.ValidatePaymentFoundationServiceConstants;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class ValidatePaymentFoundationServiceTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(ValidatePaymentFoundationServiceTestProjectApplication.class, args);
	}
}

@RestController
@RequestMapping("/testValidatePayment")
class ValidatePaymentController {
	
	@Autowired
	private ValidatePaymentFoundationServiceAdapter adapter;

	@RequestMapping(method = RequestMethod.POST, consumes= {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public PaymentSetupValidationResponse getResponse(
			@RequestBody CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse,
			@RequestHeader(required = false, value = "X-BOI-USER") String boiUser,
			@RequestHeader(required = false, value = "X-BOI-CHANNEL") String boiChannel,
			@RequestHeader(required = false, value = "X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false, value = "X-CORRELATION-ID") String correlationID) {
		
		 Map<String, String> params = new HashMap<String, String>();

		    params.put(ValidatePaymentFoundationServiceConstants.USER_ID, boiUser);
		    params.put(ValidatePaymentFoundationServiceConstants.CHANNEL_ID, boiChannel);
		    params.put(ValidatePaymentFoundationServiceConstants.CORRELATION_ID, correlationID);
		    params.put(ValidatePaymentFoundationServiceConstants.PLATFORM_ID, boiPlatform);
		    params.put(PSD2Constants.CONSENT_FLOW_TYPE, "PISP");
		    paymentSetupPOSTResponse.setAccountNSC("903779");
		    paymentSetupPOSTResponse.setAccountNumber("76528776");
		   return adapter.preAuthorizationPaymentValidation(paymentSetupPOSTResponse, params);

	}
	
}

