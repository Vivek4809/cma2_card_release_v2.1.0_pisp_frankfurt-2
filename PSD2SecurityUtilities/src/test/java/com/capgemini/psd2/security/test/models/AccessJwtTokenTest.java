/*package com.capgemini.psd2.security.test.models;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.security.models.AccessJwtToken;

import io.jsonwebtoken.Claims;

public class AccessJwtTokenTest {

	@InjectMocks
	private AccessJwtToken jwtToken;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testConstructorWithTokenArg (){
		jwtToken = new AccessJwtToken("token");
		assertEquals("token",jwtToken.getToken());
	}
	
	@Test
	public void testConstructorWithArg(){
		Claims claims = mock(Claims.class) ;
		jwtToken = new AccessJwtToken("token", claims);
		assertNotNull(jwtToken.getClaims());
	}
}
*/