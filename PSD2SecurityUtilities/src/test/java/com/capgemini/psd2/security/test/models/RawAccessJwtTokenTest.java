/*package com.capgemini.psd2.security.test.models;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;
import com.capgemini.psd2.security.models.RawAccessJwtToken;

public class RawAccessJwtTokenTest {

	RequestHeaderAttributes requestHeaderAttributes = null;
	String token = null;
	RawAccessJwtToken rawAccessJwtToken = null;

	@Before
	public void setUp() {
		requestHeaderAttributes = new RequestHeaderAttributes();
		requestHeaderAttributes.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
	}

	@Test
	public void getTokentest() {
		token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJib2kxMjMiLCJzY29wZXMiOlsiQ1VTVE9NRVJfUk9MRSJdLCJpc3MiOiJodHRwOi8vU2FhUy5jb20iLCJqdGkiOiI3NmRiN2JjZS05Y2RhLTQ5NDMtOGM2ZS01NTFmNGUzNWMxYzciLCJpYXQiOjE1MDAyOTcyODMsImV4cCI6MTUxNTg0OTI4M30.eEpXpsLaoUH70-a19xqCcCtvDUsZ3b8sQ3mH6ovmgXDNiBvGFIts819nU9dnLJt3IMbdsscL5a9IaRGHNu980Q";
		rawAccessJwtToken = new RawAccessJwtToken(token, requestHeaderAttributes);
		assertEquals(token, rawAccessJwtToken.getToken());
	}
	
	@Test
	public void parseClaimsSuccessFlow(){
		token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJib2kxMjMiLCJzY29wZXMiOlsiQ1VTVE9NRVJfUk9MRSJdLCJpc3MiOiJodHRwOi8vU2FhUy5jb20iLCJqdGkiOiI3NmRiN2JjZS05Y2RhLTQ5NDMtOGM2ZS01NTFmNGUzNWMxYzciLCJpYXQiOjE1MDAyOTcyODMsImV4cCI6MTUxNTg0OTI4M30.eEpXpsLaoUH70-a19xqCcCtvDUsZ3b8sQ3mH6ovmgXDNiBvGFIts819nU9dnLJt3IMbdsscL5a9IaRGHNu980Q";
		rawAccessJwtToken = new RawAccessJwtToken(token, requestHeaderAttributes);
		rawAccessJwtToken.parseClaims("0123456789abcdef");
	}
	
	@Test(expected=PSD2AuthenticationException.class)
	public void parseClaimsExpiredJwtException(){
		token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIzNjM3NjYwMCxkZW1vdXNlciIsInNjb3BlcyI6WyJST0xFX01PTkVZV0lTRSIsIlJPTEVfQ1VTVE9NRVJTIl0sImlzcyI6Imh0dHA6Ly9BbHBoYVNhYVMuY29tIiwianRpIjoiYmQxZjFhZTUtMTAzOC00ZDI2LWExNDEtMzg3MTc4NzhlOWY0IiwiaWF0IjoxNDkwMDg0Nzg0LCJleHAiOjE0OTI2NzY3ODR9.MEeIkwx2uYPjXmoP36jMuPqixrkrkOduIGSxW8sN6zlBxTANXZKthUEI_FtrOz7xEbUUGuT5a52PIHiXWrkPqw";
		rawAccessJwtToken = new RawAccessJwtToken(token, requestHeaderAttributes);
		rawAccessJwtToken.parseClaims("0123456789abcdef");
	}

	@Test(expected=PSD2AuthenticationException.class)
	public void parseClaimsPSD2AuthenticationException(){
		token = "vntwKKvsq6nBd1YkYTVVqVOMEZW5xtZ4MF0vnyS2ocnbckNRi5gK/yBXtpA914/x905zxV86dZkCtnU69DWKgwBcyTlruh0hPe8rlGMdeKdJW+lx2h8WUHDI1g54xcI+OfokOqk59KzOcS+ESIxfzXtTz4peB8naywgSxmHxDrlec5MQihO9zpzsygJcYoVqd4p/SXsb/oHp4c3meK6tgyIF+v5WOeg6zCQWfXj52TJAcJocaFlo8VrgK+2nCrDjRMwX8r0qEo3bNLvdzh4Zu/Dcphk9P9nDO5i9UYKI3OQXBphFEeMGhsZ79fqhrcp7lx2tBfoebuxOpqY5i687ABOIASSlXMxOYXrlNLq685LvzIUwbgY58Kp1MSlPR6WfqXF2mn784GfOKr5ZQmss+w==";
		rawAccessJwtToken = new RawAccessJwtToken(token, requestHeaderAttributes);
		rawAccessJwtToken.parseClaims("0123456789");
	}
	
}*/