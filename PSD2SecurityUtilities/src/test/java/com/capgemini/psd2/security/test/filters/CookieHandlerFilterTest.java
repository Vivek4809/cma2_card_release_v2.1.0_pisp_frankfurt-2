/*package com.capgemini.psd2.security.test.filters;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.filters.CookieHandlerFilter;

public class CookieHandlerFilterTest {

	@InjectMocks
	private CookieHandlerFilter cookieHandlerFilter;
	 
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private FilterChain filterChain;
	@Mock
	private LoggerUtils loggerUtils;

    
	
	 @Before
		public void setUp() throws Exception {
			MockitoAnnotations.initMocks(this);
			cookieHandlerFilter = new CookieHandlerFilter();
		}
	
	 	@SuppressWarnings("unchecked")
		@Test(expected = PSD2SecurityException.class)
		public void test() throws ServletException, IOException{
			ReflectionTestUtils.setField(cookieHandlerFilter,"tokenSigningKey", "0123456789abcdef");
			ReflectionTestUtils.setField(cookieHandlerFilter, "protectedEndPoints", "/aisp/customerconsentview");
			ReflectionTestUtils.setField(cookieHandlerFilter, "loggerUtils", loggerUtils);
			when(request.getServletPath()).thenReturn("/customerconsentview");
			when(request.getRequestDispatcher(anyString())).thenThrow(PSD2SecurityException.class);
			cookieHandlerFilter.doFilter(request, response, filterChain);
		}
	 	
}
*/