/*package com.capgemini.psd2.utilities;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.utilities.URLUtilities;


@RunWith(SpringJUnit4ClassRunner.class)
public class URLUtilitiesTest {

	@InjectMocks
	private URLUtilities urlUtilities;
	@Mock
	private HttpServletRequest request;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		urlUtilities = new URLUtilities();
	}

	@SuppressWarnings("static-access")
	@Test
	public void testRetriveParams(){
		String url = "https://localhost:8091/openbanking-nOAuth/oauth/authorize?grant_type=authorization_code&client_id=Moneywise-Wealth&scope=accounts&brandId=ROI&AccountRequestId=1234&redirect_uri=https://www.getpostman.com/oauth2/callback&response_type=code";
		assertNotNull(urlUtilities.retriveParams(url));
	}

	@SuppressWarnings("static-access")
	@Test
	public void testCreateURLEncString(){
		String url ="https://localhost:8091/openbanking-nOAuth/oauth/authorize?grant_type=authorization_code&client_id=Moneywise-Wealth&scope=accounts&brandId=ROI&AccountRequestId=1234&redirect_uri=https%3A%2F%2Fwww.getpostman.com%2Foauth2%2Fcallback&response_type=code&channelId=BOL";
		assertNotNull(urlUtilities.createURLEncString(url, PSD2SecurityConstants.OAUTH_ENC_URL,"0123456789abcdef"));
	}

	@SuppressWarnings("static-access")
	@Test
	public void testRetriveParam(){
		when(request.getScheme()).thenReturn("schemeName");
		when(request.getServerName()).thenReturn("https://localhost");
		when(request.getServerPort()).thenReturn(8091);
		when(request.getContextPath()).thenReturn("/openbanking-consent");
		when(request.getServletPath()).thenReturn("/customerconsentview");
		when(request.getPathInfo()).thenReturn("null");
		when(request.getQueryString()).thenReturn("?grant_type=authorization_code&client_id=Moneywise-Wealth&scope=accounts&brandId=ROI&AccountRequestId=1234&redirect_uri=https%3A%2F%2Fwww.getpostman.com%2Foauth2%2Fcallback&response_type=code&channelId=BOL");
		urlUtilities.retriveParams(request);
	}

	@SuppressWarnings("static-access")
	@Test
	public void testRemoveQueryParameter(){
		String url ="https://localhost:8091/openbanking-nOAuth/oauth/authorize?grant_type=authorization_code&client_id=Moneywise-Wealth&scope=accounts&brandId=ROI&AccountRequestId=1234&redirect_uri=https%3A%2F%2Fwww.getpostman.com%2Foauth2%2Fcallback&response_type=code&channelId=BOL";
		assertNotNull(urlUtilities.removeQueryParameter(url, PSD2SecurityConstants.CHANNEL_ID));
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void testFindParam(){
		String callBackUrl = "https://psd2.apiboidev.com:443/openbanking-nOAuth/oauth/authorize?grant_type=authorization_code&client_id=6443e15975554bce8099e35b88b40465&scope=accounts&brandId=ROI&AccountRequestId=7b42b335-404e-4bac-a23a-55d3ec71e77a&state=2234567&redirect_uri=https://www.getpostman.com/oauth2/callback&response_type=code&x-fapi-interaction-id=85623918-14e9-4e69-b93f-abbda3eebdbc&channelId=BOL&idToken=vntwKKvsq6nBd1YkYTVVqVOMEZW5xtZ4MF0vnyS2ocmVoPTRj4gk6SRWYcHvPBBmwl1CaqrQr2Ykmjujc3T/NkyNO6xOZ3GccBTv2JO11RwujGnbnPvilCfWhI9awl5FDwMEjesn9+Uqr/j40YIT89M39ehoca0E9qEJMvQi8mXjd6X3B2Ka1nwEjEpjJzTtjkSl8/mEE5R8aUtHqB3nYdMwYSOzDaviS5qBsbFoe+C2zL55CpkwIRg1k2YyXyZwFuawtt49Z3Xy6ohyFtOfF8XsAXuB/9Hm4m2BQz2AF36mZ6o6ALgKFxJBq38XpLO+jl64Gp5/HZR3c1RLTMn0J+YZh2aI5bxCiroXshZ5UMkdeNVbp4VEibgIp8VOiPn3LPuES2vci3QE/Z+ps3F8WL+suUjVyYnfY5RxhNW7YeE=";
		assertNotNull(urlUtilities.findParam(callBackUrl, PSD2SecurityConstants.ID_TOKEN_PARAM));
	}
	
	@SuppressWarnings("static-access")
	@Test
	public void testDecodeUrl(){
		String callBackUrl = "https://psd2.apiboidev.com:443/openbanking-nOAuth/oauth/authorize?grant_type=authorization_code&client_id=6443e15975554bce8099e35b88b40465&scope=accounts&brandId=ROI&AccountRequestId=7b42b335-404e-4bac-a23a-55d3ec71e77a&state=2234567&redirect_uri=https://www.getpostman.com/oauth2/callback&response_type=code&x-fapi-interaction-id=85623918-14e9-4e69-b93f-abbda3eebdbc&channelId=BOL&idToken=vntwKKvsq6nBd1YkYTVVqVOMEZW5xtZ4MF0vnyS2ocmVoPTRj4gk6SRWYcHvPBBmwl1CaqrQr2Ykmjujc3T/NkyNO6xOZ3GccBTv2JO11RwujGnbnPvilCfWhI9awl5FDwMEjesn9+Uqr/j40YIT89M39ehoca0E9qEJMvQi8mXjd6X3B2Ka1nwEjEpjJzTtjkSl8/mEE5R8aUtHqB3nYdMwYSOzDaviS5qBsbFoe+C2zL55CpkwIRg1k2YyXyZwFuawtt49Z3Xy6ohyFtOfF8XsAXuB/9Hm4m2BQz2AF36mZ6o6ALgKFxJBq38XpLO+jl64Gp5/HZR3c1RLTMn0J+YZh2aI5bxCiroXshZ5UMkdeNVbp4VEibgIp8VOiPn3LPuES2vci3QE/Z+ps3F8WL+suUjVyYnfY5RxhNW7YeE=";
		assertNotNull(urlUtilities.decodeUrl(callBackUrl));
	}

}
*/