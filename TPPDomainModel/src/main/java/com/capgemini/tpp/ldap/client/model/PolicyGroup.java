package com.capgemini.tpp.ldap.client.model;

public class PolicyGroup {
	private String id;
	private String location;

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
}