package com.capgemini.tpp.registration.model;
/**
 * Ping federate Validation Error Object
 * @author nagoswam
 *
 */
public class PfValidationError {

	private String message;
	
	private String fieldPath;
	
	private String errorId;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getFieldPath() {
		return fieldPath;
	}

	public void setFieldPath(String fieldPath) {
		this.fieldPath = fieldPath;
	}

	public String getErrorId() {
		return errorId;
	}

	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}
}
