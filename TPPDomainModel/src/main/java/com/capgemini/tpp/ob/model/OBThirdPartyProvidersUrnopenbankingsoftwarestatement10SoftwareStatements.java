package com.capgemini.tpp.ob.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements   {
  @JsonProperty("Active")
  private Boolean active = null;

  @JsonProperty("ClientId")
  private String clientId = null;

  @JsonProperty("ClientName")
  private String clientName = null;

  @JsonProperty("Description")
  private String description = null;

  @JsonProperty("Id")
  private String id = null;

  @JsonProperty("Mode")
  private String mode = null;

  @JsonProperty("ObClientCreated")
  private Boolean obClientCreated = null;

  @JsonProperty("OnBehalfOfObOrganisation")
  private String onBehalfOfObOrganisation = null;

  @JsonProperty("PolicyUri")
  private String policyUri = null;

  @JsonProperty("RedirectUri")
  private List<String> redirectUri = null;

  @JsonProperty("Roles")
  private List<String> roles = null;

  @JsonProperty("SigningKeyIds")
  private List<String> signingKeyIds = null;

  @JsonProperty("TermsOfServiceUri")
  private String termsOfServiceUri = null;

  @JsonProperty("TransportKeyIds")
  private List<String> transportKeyIds = null;

  @JsonProperty("Version")
  private BigDecimal version = null;

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements active(Boolean active) {
    this.active = active;
    return this;
  }

   /**
   * Status indicating if a software record is active or not.
   * @return active
  **/
  @ApiModelProperty(value = "Status indicating if a software record is active or not.")


  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements clientId(String clientId) {
    this.clientId = clientId;
    return this;
  }

   /**
   * Requested Client Id - note that OB will issue a set of credentials with this clientid for this given piece of software. ASPSPs are not obliged to honour this requested clientid
   * @return clientId
  **/
  @ApiModelProperty(value = "Requested Client Id - note that OB will issue a set of credentials with this clientid for this given piece of software. ASPSPs are not obliged to honour this requested clientid")


  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements clientName(String clientName) {
    this.clientName = clientName;
    return this;
  }

   /**
   * Human readable client name. May be localized.
   * @return clientName
  **/
  @ApiModelProperty(value = "Human readable client name. May be localized.")


  public String getClientName() {
    return clientName;
  }

  public void setClientName(String clientName) {
    this.clientName = clientName;
  }

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements description(String description) {
    this.description = description;
    return this;
  }

   /**
   * Description of the unique instance of this piece of software. If only one instance of a piece of software is to be registered then this should be the same as the SoftwareDescription
   * @return description
  **/
  @ApiModelProperty(value = "Description of the unique instance of this piece of software. If only one instance of a piece of software is to be registered then this should be the same as the SoftwareDescription")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements id(String id) {
    this.id = id;
    return this;
  }

   /**
   * Unique Scheme Wide Software id
   * @return id
  **/
  @ApiModelProperty(value = "Unique Scheme Wide Software id")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements mode(String mode) {
    this.mode = mode;
    return this;
  }

   /**
   * A flag to identify if a piece of software should have access to production PSU accounts. This field has been added at the request of an ASPSP to allow BETA or Non Production testing against production platforms. The default for this system should be \"Live\"
   * @return mode
  **/
  @ApiModelProperty(value = "A flag to identify if a piece of software should have access to production PSU accounts. This field has been added at the request of an ASPSP to allow BETA or Non Production testing against production platforms. The default for this system should be \"Live\"")


  public String getMode() {
    return mode;
  }

  public void setMode(String mode) {
    this.mode = mode;
  }

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements obClientCreated(Boolean obClientCreated) {
    this.obClientCreated = obClientCreated;
    return this;
  }

   /**
   * An indicator to show if the client has been created in Open Banking
   * @return obClientCreated
  **/
  @ApiModelProperty(value = "An indicator to show if the client has been created in Open Banking")


  public Boolean getObClientCreated() {
    return obClientCreated;
  }

  public void setObClientCreated(Boolean obClientCreated) {
    this.obClientCreated = obClientCreated;
  }

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements onBehalfOfObOrganisation(String onBehalfOfObOrganisation) {
    this.onBehalfOfObOrganisation = onBehalfOfObOrganisation;
    return this;
  }

   /**
   * The organisation on whom this software statement is behalf of
   * @return onBehalfOfObOrganisation
  **/
  @ApiModelProperty(value = "The organisation on whom this software statement is behalf of")


  public String getOnBehalfOfObOrganisation() {
    return onBehalfOfObOrganisation;
  }

  public void setOnBehalfOfObOrganisation(String onBehalfOfObOrganisation) {
    this.onBehalfOfObOrganisation = onBehalfOfObOrganisation;
  }

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements policyUri(String policyUri) {
    this.policyUri = policyUri;
    return this;
  }

   /**
   * An optional document containing a link to a Policy document governing the privacy information policy of for the application. Purely to be displayed a PSU at a ASPSP if the ASPSP supports it. Optional for the TPP to provide.
   * @return policyUri
  **/
  @ApiModelProperty(value = "An optional document containing a link to a Policy document governing the privacy information policy of for the application. Purely to be displayed a PSU at a ASPSP if the ASPSP supports it. Optional for the TPP to provide.")


  public String getPolicyUri() {
    return policyUri;
  }

  public void setPolicyUri(String policyUri) {
    this.policyUri = policyUri;
  }

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements redirectUri(List<String> redirectUri) {
    this.redirectUri = redirectUri;
    return this;
  }

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements addRedirectUriItem(String redirectUriItem) {
    if (this.redirectUri == null) {
      this.redirectUri = new ArrayList<String>();
    }
    this.redirectUri.add(redirectUriItem);
    return this;
  }

   /**
   * Redirect Uri's for the registered piece of software. May be overridden by the RFC7591 payload.
   * @return redirectUri
  **/
  @ApiModelProperty(value = "Redirect Uri's for the registered piece of software. May be overridden by the RFC7591 payload.")


  public List<String> getRedirectUri() {
    return redirectUri;
  }

  public void setRedirectUri(List<String> redirectUri) {
    this.redirectUri = redirectUri;
  }

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements roles(List<String> roles) {
    this.roles = roles;
    return this;
  }

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements addRolesItem(String rolesItem) {
    if (this.roles == null) {
      this.roles = new ArrayList<String>();
    }
    this.roles.add(rolesItem);
    return this;
  }

   /**
   * Get roles
   * @return roles
  **/
  @ApiModelProperty(value = "")


  public List<String> getRoles() {
    return roles;
  }

  public void setRoles(List<String> roles) {
    this.roles = roles;
  }

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements signingKeyIds(List<String> signingKeyIds) {
    this.signingKeyIds = signingKeyIds;
    return this;
  }

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements addSigningKeyIdsItem(String signingKeyIdsItem) {
    if (this.signingKeyIds == null) {
      this.signingKeyIds = new ArrayList<String>();
    }
    this.signingKeyIds.add(signingKeyIdsItem);
    return this;
  }

   /**
   * KeyIds of Keys  used for signing messages
   * @return signingKeyIds
  **/
  @ApiModelProperty(value = "KeyIds of Keys  used for signing messages")


  public List<String> getSigningKeyIds() {
    return signingKeyIds;
  }

  public void setSigningKeyIds(List<String> signingKeyIds) {
    this.signingKeyIds = signingKeyIds;
  }

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements termsOfServiceUri(String termsOfServiceUri) {
    this.termsOfServiceUri = termsOfServiceUri;
    return this;
  }

   /**
   * An optional document containing a link to a Terms of Service document governing the terms of service for the application. Purely to be displayed a PSU at a ASPSP if the ASPSP supports it. Optional for the TPP to provide.
   * @return termsOfServiceUri
  **/
  @ApiModelProperty(value = "An optional document containing a link to a Terms of Service document governing the terms of service for the application. Purely to be displayed a PSU at a ASPSP if the ASPSP supports it. Optional for the TPP to provide.")


  public String getTermsOfServiceUri() {
    return termsOfServiceUri;
  }

  public void setTermsOfServiceUri(String termsOfServiceUri) {
    this.termsOfServiceUri = termsOfServiceUri;
  }

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements transportKeyIds(List<String> transportKeyIds) {
    this.transportKeyIds = transportKeyIds;
    return this;
  }

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements addTransportKeyIdsItem(String transportKeyIdsItem) {
    if (this.transportKeyIds == null) {
      this.transportKeyIds = new ArrayList<String>();
    }
    this.transportKeyIds.add(transportKeyIdsItem);
    return this;
  }

   /**
   * KeyIds of Keys used for securing message transport (TLS)
   * @return transportKeyIds
  **/
  @ApiModelProperty(value = "KeyIds of Keys used for securing message transport (TLS)")


  public List<String> getTransportKeyIds() {
    return transportKeyIds;
  }

  public void setTransportKeyIds(List<String> transportKeyIds) {
    this.transportKeyIds = transportKeyIds;
  }

  public OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements version(BigDecimal version) {
    this.version = version;
    return this;
  }

   /**
   * Version of the sofware
   * @return version
  **/
  @ApiModelProperty(value = "Version of the sofware")

  @Valid

  public BigDecimal getVersion() {
    return version;
  }

  public void setVersion(BigDecimal version) {
    this.version = version;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements obThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements = (OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements) o;
    return Objects.equals(this.active, obThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements.active) &&
        Objects.equals(this.clientId, obThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements.clientId) &&
        Objects.equals(this.clientName, obThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements.clientName) &&
        Objects.equals(this.description, obThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements.description) &&
        Objects.equals(this.id, obThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements.id) &&
        Objects.equals(this.mode, obThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements.mode) &&
        Objects.equals(this.obClientCreated, obThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements.obClientCreated) &&
        Objects.equals(this.onBehalfOfObOrganisation, obThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements.onBehalfOfObOrganisation) &&
        Objects.equals(this.policyUri, obThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements.policyUri) &&
        Objects.equals(this.redirectUri, obThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements.redirectUri) &&
        Objects.equals(this.roles, obThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements.roles) &&
        Objects.equals(this.signingKeyIds, obThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements.signingKeyIds) &&
        Objects.equals(this.termsOfServiceUri, obThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements.termsOfServiceUri) &&
        Objects.equals(this.transportKeyIds, obThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements.transportKeyIds) &&
        Objects.equals(this.version, obThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements.version);
  }

  @Override
  public int hashCode() {
    return Objects.hash(active, clientId, clientName, description, id, mode, obClientCreated, onBehalfOfObOrganisation, policyUri, redirectUri, roles, signingKeyIds, termsOfServiceUri, transportKeyIds, version);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements {\n");
    
    sb.append("    active: ").append(toIndentedString(active)).append("\n");
    sb.append("    clientId: ").append(toIndentedString(clientId)).append("\n");
    sb.append("    clientName: ").append(toIndentedString(clientName)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    mode: ").append(toIndentedString(mode)).append("\n");
    sb.append("    obClientCreated: ").append(toIndentedString(obClientCreated)).append("\n");
    sb.append("    onBehalfOfObOrganisation: ").append(toIndentedString(onBehalfOfObOrganisation)).append("\n");
    sb.append("    policyUri: ").append(toIndentedString(policyUri)).append("\n");
    sb.append("    redirectUri: ").append(toIndentedString(redirectUri)).append("\n");
    sb.append("    roles: ").append(toIndentedString(roles)).append("\n");
    sb.append("    signingKeyIds: ").append(toIndentedString(signingKeyIds)).append("\n");
    sb.append("    termsOfServiceUri: ").append(toIndentedString(termsOfServiceUri)).append("\n");
    sb.append("    transportKeyIds: ").append(toIndentedString(transportKeyIds)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

