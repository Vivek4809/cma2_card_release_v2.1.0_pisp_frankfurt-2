package com.capgemini.tpp.ob.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * AccountStatePasswordHistory
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class AccountStatePasswordHistory   {
  @JsonProperty("passwordExpirationTime")
  private OffsetDateTime passwordExpirationTime = null;

  @JsonProperty("passwordRetiredTime")
  private OffsetDateTime passwordRetiredTime = null;

  public AccountStatePasswordHistory passwordExpirationTime(OffsetDateTime passwordExpirationTime) {
    this.passwordExpirationTime = passwordExpirationTime;
    return this;
  }

   /**
   * The expiration time of the password.
   * @return passwordExpirationTime
  **/
  @ApiModelProperty(value = "The expiration time of the password.")

  @Valid

  public OffsetDateTime getPasswordExpirationTime() {
    return passwordExpirationTime;
  }

  public void setPasswordExpirationTime(OffsetDateTime passwordExpirationTime) {
    this.passwordExpirationTime = passwordExpirationTime;
  }

  public AccountStatePasswordHistory passwordRetiredTime(OffsetDateTime passwordRetiredTime) {
    this.passwordRetiredTime = passwordRetiredTime;
    return this;
  }

   /**
   * The time that the password was retired.
   * @return passwordRetiredTime
  **/
  @ApiModelProperty(value = "The time that the password was retired.")

  @Valid

  public OffsetDateTime getPasswordRetiredTime() {
    return passwordRetiredTime;
  }

  public void setPasswordRetiredTime(OffsetDateTime passwordRetiredTime) {
    this.passwordRetiredTime = passwordRetiredTime;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AccountStatePasswordHistory accountStatePasswordHistory = (AccountStatePasswordHistory) o;
    return Objects.equals(this.passwordExpirationTime, accountStatePasswordHistory.passwordExpirationTime) &&
        Objects.equals(this.passwordRetiredTime, accountStatePasswordHistory.passwordRetiredTime);
  }

  @Override
  public int hashCode() {
    return Objects.hash(passwordExpirationTime, passwordRetiredTime);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AccountStatePasswordHistory {\n");
    
    sb.append("    passwordExpirationTime: ").append(toIndentedString(passwordExpirationTime)).append("\n");
    sb.append("    passwordRetiredTime: ").append(toIndentedString(passwordRetiredTime)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

