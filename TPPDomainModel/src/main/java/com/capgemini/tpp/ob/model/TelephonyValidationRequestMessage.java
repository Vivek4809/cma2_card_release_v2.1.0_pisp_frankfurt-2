package com.capgemini.tpp.ob.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * The message used to send the verification code
 */
@ApiModel(description = "The message used to send the verification code")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class TelephonyValidationRequestMessage   {
  @JsonProperty("language")
  private String language = null;

  @JsonProperty("message")
  private String message = null;

  public TelephonyValidationRequestMessage language(String language) {
    this.language = language;
    return this;
  }

   /**
   * The language and locale of the message
   * @return language
  **/
  @ApiModelProperty(value = "The language and locale of the message")


  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public TelephonyValidationRequestMessage message(String message) {
    this.message = message;
    return this;
  }

   /**
   * The message to be delivered
   * @return message
  **/
  @ApiModelProperty(value = "The message to be delivered")


  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TelephonyValidationRequestMessage telephonyValidationRequestMessage = (TelephonyValidationRequestMessage) o;
    return Objects.equals(this.language, telephonyValidationRequestMessage.language) &&
        Objects.equals(this.message, telephonyValidationRequestMessage.message);
  }

  @Override
  public int hashCode() {
    return Objects.hash(language, message);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TelephonyValidationRequestMessage {\n");
    
    sb.append("    language: ").append(toIndentedString(language)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

