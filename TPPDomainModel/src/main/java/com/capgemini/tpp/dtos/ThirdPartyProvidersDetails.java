package com.capgemini.tpp.dtos;

import java.util.List;

public class ThirdPartyProvidersDetails {
 
	private String organizationName;
	private String tppOrganizationId;
	private List<String> tppRoles;
	private List<String> tppPhoneNumber;
	private String tppMemberStateCompetentAuthority;
	private List<String> tppEmailID;
	
	public String getOrganizationName() {
		return organizationName;
	}
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	public String getTppOrganizationId() {
		return tppOrganizationId;
	}
	public void setTppOrganizationId(String tppOrganizationId) {
		this.tppOrganizationId = tppOrganizationId;
	}
	public List<String> getTppRoles() {
		return tppRoles;
	}
	public void setTppRoles(List<String> tppRoles) {
		this.tppRoles = tppRoles;
	}

	public List<String> getTppPhoneNumber() {
		return tppPhoneNumber;
	}

	public void setTppPhoneNumber(List<String> tppPhoneNumber) {
		this.tppPhoneNumber = tppPhoneNumber;
	}
	public String getTppMemberStateCompetentAuthority() {
		return tppMemberStateCompetentAuthority;
	}
	public void setTppMemberStateCompetentAuthority(String tppMemberStateCompetentAuthority) {
		this.tppMemberStateCompetentAuthority = tppMemberStateCompetentAuthority;
	}

	public List<String> getTppEmailID() {
		return tppEmailID;
	}

	public void setTppEmailID(List<String> tppEmailID) {
		this.tppEmailID = tppEmailID;
	}
	public String getTppRegistrationID() {
		return tppRegistrationID;
	}
	public void setTppRegistrationID(String tppRegistrationID) {
		this.tppRegistrationID = tppRegistrationID;
	}
	private String tppRegistrationID;

}
